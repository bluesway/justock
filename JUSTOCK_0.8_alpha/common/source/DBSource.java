package common.source;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author   Administrator
 */
public interface DBSource {
	public Connection getConnection() throws SQLException;
	public void closeConnection(Connection conn) throws SQLException;
}