<%@ page contentType="text/html; charset=big5" language="java" import="java.sql.*" errorPage="" %>
<script language="javascript">
function sendMsg(){
ans=window.confirm("確認送出下單資料?")
if(ans)
document.forms[0].submit()
}
</script>
<%
// *** Restrict Access To Page: Grant or deny access to this page
String MM_authorizedUsers="";
String MM_authFailedURL="/relogin.jsp";
boolean MM_grantAccess=false;
if (session.getValue("MM_Username") != null && !session.getValue("MM_Username").equals("")) {
  if (true || (session.getValue("MM_UserAuthorization")=="") || 
          (MM_authorizedUsers.indexOf((String)session.getValue("MM_UserAuthorization")) >=0)) {
    MM_grantAccess = true;
  }
}
if (!MM_grantAccess) {
  String MM_qsChar = "?";
  if (MM_authFailedURL.indexOf("?") >= 0) MM_qsChar = "&";
  String MM_referrer = request.getRequestURI();
  if (request.getQueryString() != null) MM_referrer = MM_referrer + "?" + request.getQueryString();
  MM_authFailedURL = MM_authFailedURL + MM_qsChar + "accessdenied=" + java.net.URLEncoder.encode(MM_referrer);
  response.sendRedirect(response.encodeRedirectURL(MM_authFailedURL));
  return;
}
%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5" />
<title>下單</title>
<script type="text/JavaScript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } if (errors) alert('The following error(s) occurred:\n'+errors);
  document.MM_returnValue = (errors == '');
}
//-->
</script>
<style type="text/css">
<!--
.style2 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 16px;
}
body {
	background-image: url();
	background-repeat: repeat;
	background-color: #e7f3ff;
}
.style6 {font-size: 15px; color: #000000; }
.style8 {color: #666666; font-size: 14px; }
.style9 {font-size: 15px}
-->
</style>
</head><fmt:requestEncoding value="Big5"/>

<body onLoad="MM_validateForm('tSid','','NinRange');return document.MM_returnValue">
<form name="ttt" action="purchaseorder_2.jsp">
  <table width="70%" height="0%" border="0" align="center" cellpadding="4" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td colspan="2" align="center" bgcolor="#CCCCCC"><span class="style2">開 始 下 單</span></td>
    </tr>
  <tr>
    <td width="25%" bgcolor="#FFFFCC"><div align="center" class="style6">股票代號</div></td>
    <td width="75%" nowrap="nowrap" bgcolor="#FFFFFF"><p align="left">
      <input name="tSid" type="text" id="tSid" onBlur="MM_validateForm('tSid','','NisNum');return document.MM_returnValue" size="15" maxlength="4" /><input name="tUid" type="hidden" id="tUid" value="<%= ((session.getValue("Uid")!=null)?session.getValue("Uid"):"") %>" /> <span class="style8">(上方連結可查詢股票代碼)      </span></p></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFCC"><div align="center" class="style6">下單價位</div></td>
    <td bgcolor="#FFFFFF">
      <div align="left">
<input name="tPrice" type="text" id="tPrice" onBlur="MM_validateForm('tSid','','NisNum','tPrice','','NisNum');return document.MM_returnValue" size="15" />        
<span class="style9">元</span>      <span class="style8">(限制在開盤價的7%之間)</span></div></td></tr>
  <tr>
    <td bgcolor="#FFFFCC"><div align="center" class="style6">下單股數</div></td>
    <td bgcolor="#FFFFFF">
      <div align="left">
        <input name="tQuan" type="text" id="tQuan" onBlur="MM_validateForm('tQuan','','NisNum');return document.MM_returnValue" size="15" />
        <span class="style9">張</span></div></td></tr>
  <tr>
    <td bgcolor="#FFFFCC"><div align="center" class="style6">交割期限 </div></td>
    <td bgcolor="#FFFFFF"><label>
      
        <div align="left">
          <label>
          <input name="tDue" type="text" onBlur="MM_validateForm('tDue','','NinRange0:270');return document.MM_returnValue" size="15" maxlength="3" />
          </label>
        <span class="style9">分鐘</span>        <span class="style8">(限制範圍1~270分 0為不限制)</span></div>
        </label></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFCC"><div align="center" class="style6">買賣方式</div></td>
    <td bgcolor="#FFFFFF"><label>
    
      <div align="left">
        <select name="select">
          <option value="81" selected="selected">買</option>
          <option value="50">賣</option>
        </select>
        </div>
    </label>
    <label></label></td>
  </tr>
  <tr>
    <td colspan="2" align="center" bgcolor="#FFFFFF"><input name="submit" type="submit" value=" 確 定  " onClick="sendMsg()" />
    <input name="reset" type="reset" value=" 取 消 " /></td>
    </tr>
</table>
</form>
<p>&nbsp;	</p>
</body>
</html>