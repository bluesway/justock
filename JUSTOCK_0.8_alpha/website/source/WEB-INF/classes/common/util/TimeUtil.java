/**
 * @author bluesway
 * TARGET 分析 info_tse 資料表取得的時間資料
 */

package common.util;

import java.util.Date;
import java.util.Locale;
import java.text.DateFormat;

public class TimeUtil {
	static String h;	    // 時
	static String m;	// 分
	static String s; 	// 秒
	static String nowDate = "";
	static Date d;
	
	// null Constructor
	public TimeUtil() {
	
	}
	
	// 取得目前時間
	public static String getCurrentTime() {
		d = new Date();
		DateFormat df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.TAIWAN);
		nowDate = df.format(d);
		
		return nowDate.substring(12, nowDate.length()).trim();
	}
	
	// 設定顯示的格式 12/24
	public static String getCurrentTime(int timeFormat) {
		if(timeFormat == 12 && Integer.valueOf(getCurrentHour()) > 12)
			return "P.M.  " + String.valueOf(Integer.valueOf(getCurrentHour())-12) + "/" + getCurrentMinute() + "/" + getCurrentSecond();
		else if(timeFormat == 12 && Integer.valueOf(getCurrentHour()) <= 12)
			return "A.M.  " + getCurrentTime();
		else if(timeFormat == 24)
			return getCurrentTime();
		else
			return "請輸入正確格式！";
	}

	// 轉換 info_tse 取得的時間數字資料
	public void transTime(int time) {
    	String time_mark = String.valueOf(time);
    	if (time_mark.length() == 5) {
    		h = time_mark.substring(0, 1);
    		m = time_mark.substring(1, 3);
    		s = time_mark.substring(3, 5);
    	} else {
    		h = time_mark.substring(0, 2);
    		s = time_mark.substring(2, 4);
    		s = time_mark.substring(4, 6);
    	}
	}
	
	// 取得目前小時
	public static String getCurrentHour() {
		return getCurrentTime().substring(0, 2).trim();
	}
	
	// 取得目前分鐘
	public static String getCurrentMinute() {
		return getCurrentTime().substring(3, 5).trim();
	}
	
	// 取得目前秒數
	public static String getCurrentSecond() {
		return getCurrentTime().substring(6, 8).trim();
	}
	
	// 取得小時
	public static String getHour() {
		return h;
	}
	
	// 取得分鐘
	public static String getMinute() {
		return m;
	}
	
	// 取得秒
	public static String getSecond() {
		return s;
	}
	
	// 設定12/24時制
	public String getHour(int timeFormat) {
		if(timeFormat == 12 && Integer.valueOf(getHour()) > 12)
			return "P.M. " + String.valueOf(Integer.valueOf(getHour())-12);
		else if(timeFormat == 12 && Integer.valueOf(getHour()) <= 12)
			return "A.M. " + String.valueOf(Integer.valueOf(getHour()));
		else if(timeFormat == 24)
			return String.valueOf(Integer.valueOf(getHour()));
		else
			return "請輸入正確的格式！";
	}
	
	// 取得小時，若小時數字只有一位，則在最前面附加 prefix 字串，ex: getHour("0");
	public String getHour(String prefix) {
		if (h.length() == 1)
			return prefix + h;
		else 
			return h;
	}
	
	// 設定前置詞和12/24時制
	public String getHour(String prefix, int timeFormat) {
		if(timeFormat == 12 && h.length() == 1 && Integer.valueOf(getHour()) > 12)
			return "P.M. " + prefix + h;
		else if(timeFormat == 12 && h.length() == 1 && Integer.valueOf(getHour()) <= 12)
			return "A.M. " + prefix + h;
		else if(timeFormat == 12 && Integer.valueOf(getHour()) <= 12)
			return "A.M. " + prefix + h;
		else if(timeFormat == 12 && Integer.valueOf(getHour()) > 12)
			return "P.M. " + h;
		else if(timeFormat == 24 && h.length() == 1)
			return prefix + h;
		else if(timeFormat == 24)
			return h;
		else 
			return "請輸入正確的格式！";
	}
	

	// 取得時間，使用sep字串當作分隔符號，ex: getTime(":");
	public String getTime(String sep) {
		return h + sep + m + sep + s;
	}
	
	// 取得時間，使用prefix作為小時位數為1時的前綴，並使用 sep 字串當作分隔符號，ex: getTime(" ", "-");
	public String getTime(String prefix, String sep) {
		return getHour(prefix) + sep + m + sep + s;
	}
	
	// 取得時間，使用 sep分隔 prefix 前置詞 和12/24時制 	
	public String getTime(String prefix, String sep, String timeFormat) {
		if(timeFormat.equals(12))
			return getHour(prefix) + sep + m + sep + s;
		else if(timeFormat.equals(24))
			return getHour(prefix) + sep + m + sep + s;
		else 
			return "請輸入正確的格式！";
	}
	
	public static void main(String[] args) {
		TimeUtil ttf = new TimeUtil();
		ttf.transTime(192445);
//		System.out.println(ttf.getTime("", ":"));
//		System.out.println(ttf.getHour(24));
//		System.out.println(ttf.getTime());
//		System.out.println(getCurrentDate());
//		System.out.println(getcurrentSecond());
		System.out.println(getCurrentTime(12));
	}
}
