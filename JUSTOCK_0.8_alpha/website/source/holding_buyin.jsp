<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.text.DecimalFormat" errorPage="" %>
<%@ include file="/Connections/justockdb.jsp" %>
<jsp:useBean id="dateBean" class="common.util.DateUtil"/>
<jsp:useBean id="timeBean" class="common.util.TimeUtil"/>
<%
// *** Restrict Access To Page: Grant or deny access to this page
String MM_authorizedUsers="";
String MM_authFailedURL="/relogin.jsp";
boolean MM_grantAccess=false;
if (session.getValue("MM_Username") != null && !session.getValue("MM_Username").equals("")) {
  if (true || (session.getValue("MM_UserAuthorization")=="") || 
          (MM_authorizedUsers.indexOf((String)session.getValue("MM_UserAuthorization")) >=0)) {
    MM_grantAccess = true;
  }
}
if (!MM_grantAccess) {
  String MM_qsChar = "?";
  if (MM_authFailedURL.indexOf("?") >= 0) MM_qsChar = "&";
  String MM_referrer = request.getRequestURI();
  if (request.getQueryString() != null) MM_referrer = MM_referrer + "?" + request.getQueryString();
  MM_authFailedURL = MM_authFailedURL + MM_qsChar + "accessdenied=" + java.net.URLEncoder.encode(MM_referrer);
  response.sendRedirect(response.encodeRedirectURL(MM_authFailedURL));
  return;
}
%>
<%
String holding_jsp__MMColParam = "1";
if (session.getValue("Uid") !=null) {holding_jsp__MMColParam = (String)session.getValue("Uid");}
%>
<%
Driver Driverholding_jsp = (Driver)Class.forName(MM_justockdb_DRIVER).newInstance();
Connection Connholding_jsp = DriverManager.getConnection(MM_justockdb_STRING,MM_justockdb_USERNAME,MM_justockdb_PASSWORD);
PreparedStatement Statementholding_jsp = Connholding_jsp.prepareStatement("SELECT * FROM justock.holding WHERE hUid = '" + holding_jsp__MMColParam + "' ORDER BY hBuyin DESC " );
ResultSet holding_jsp = Statementholding_jsp.executeQuery();
boolean holding_jsp_isEmpty = !holding_jsp.next();
boolean holding_jsp_hasData = !holding_jsp_isEmpty;
Object holding_jsp_data;
int holding_jsp_numRows = 0;
DecimalFormat df = new DecimalFormat("0.000");
%>
<%
int Repeat1__numRows = 10;
int Repeat1__index = 0;
holding_jsp_numRows += Repeat1__numRows;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<title>投資組合</title>
<style type="text/css">
<!--
a:link {
	color: 000066;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: 000066;
}
a:hover {
	text-decoration: none;
	color: 000066;
}
a:active {
	text-decoration: none;
	color: 000066;
}
.style4 {color: #FF3300}
.style5 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 16px;
}
body {
	background-color: #e7f3ff;
}
.style8 {font-family: "新細明體"}
.style10 {font-size: 15px; color: #000066; }
.style11 {
	font-size: 14px;
	color: #FF0000;
}
-->
</style>
</head>

<body>
<div>
<table width="70%" border="0" align="center" cellpadding="4" cellspacing="1" bgcolor="#CCCCCC">
                <tbody>
	 <tr bgcolor="#CCCCCC">
      <td colspan="8" align="center"><span class="style5">投 資 組 合</span></td>
    </tr>
  <tr>
    <td bgcolor="#FFFFCC"><a href="/holding_hSid.jsp"><p align="center" class="style10">股票代碼</p></a></td>
    <td bgcolor="#FFFFCC"><a href="/holding_hPrice.jsp">
    <p align="center" class="style10">持有成本 </p>
    </a></td>
    <td bgcolor="#FFFFCC"><a href="/holding_hQuan.jsp">
    <p align="center" class="style10">持有股數</p>
    </a></td>
    <td bgcolor="#FFFFCC"><a href="/holding_buyin2.jsp">
      <p align="center" class="style10">買進日期</p></a></td>
    <td colspan="4" bgcolor="#FFFFCC"><a href="/holding_sellout.jsp"><div align="center" class="style10">賣出日期</div></a></td>
    </tr>
  <tr bgcolor="#FFFFFF">
    <% while ((holding_jsp_hasData)&&(Repeat1__numRows-- != 0)) { %>
      <td align="center" bgcolor="#FFFFFF"><span class="style8"><%=(((holding_jsp_data = holding_jsp.getObject("hSid"))==null || holding_jsp.wasNull())?"":holding_jsp_data)%></span></td>
      <td align="center" bgcolor="#FFFFFF"><span class="style8"><%=(((holding_jsp_data = df.format(holding_jsp.getDouble("hPrice")))==null || holding_jsp.wasNull())?"":holding_jsp_data)%></span></td>
      <td align="center" bgcolor="#FFFFFF"><span class="style8"><%=(((holding_jsp_data = holding_jsp.getObject("hQuan"))==null || holding_jsp.wasNull())?"":holding_jsp_data)%></span></td>
  <td align="center" bgcolor="#FFFFFF"><span class="style8"><%
	  String kk=String.valueOf(((((holding_jsp_data = holding_jsp.getObject("hBuyin"))==null || holding_jsp.wasNull())?"":holding_jsp_data)));
	  int in_kk=Integer.parseInt(kk);
	  out.println(dateBean.transDate(in_kk));
	  
	  %>
</span></td>
      <td align="center" bgcolor="#FFFFFF"><span class="style8"><%=(((holding_jsp_data = holding_jsp.getObject("hSellout"))==null || holding_jsp.wasNull())?"":holding_jsp_data)%></span></td> </tr>
               </tbody> <%
  Repeat1__index++;
  holding_jsp_hasData = holding_jsp.next();
}
%>   
</table><% if (holding_jsp_isEmpty ) { %>
  <p align="center" class="style4 style11">您沒有持有股票</p>
  <% } /* end holding_jsp_isEmpty */ %>
</div>

</body>
</html>
<%
holding_jsp.close();
Statementholding_jsp.close();
Connholding_jsp.close();
%>
