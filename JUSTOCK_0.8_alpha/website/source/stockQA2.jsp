<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>股市問答 - 進階篇</title>
<style type="text/css">
<!--
.style1 {
	color: #9c9a9c;
	font-size: 15px;
}
a:link {
	color: #9C9A9C;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: #9C9A9C;
}
a:hover {
	text-decoration: underline;
	color: #9C9A9C;
}
a:active {
	text-decoration: none;
	color: #9C9A9C;
}

.style4 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 16px;
}
.style5 {color: #999999}
body {
	background-color: #e7f3ff;
}
.style7 {font-size: 15px; }
-->
</style>
<script language="javascript">
<!--
function showlayer(id){
	for(var i=1;i<=20;i++){
		if(document.all("q"+i).style.display=='inline'){
		    document.all("q"+i).style.display='none';
			document.all("q"+i+"_title").style.fontWeight ='normal';
			break;
		}
	}
		document.all("q"+id).style.display='inline';
		document.all("q"+id+"_title").style.fontWeight ='bold';
		document.all("q"+id+"_title").style.fontColor ='#CC3300';
	}

function MM_openBrWindow(theURL,winName,features) { //v2.0
	window.open(theURL,winName,features);
}
//-->
</script>
</head>

<body>
<form id="form1" name="form1" method="post" action="">
  <div align="center" class="style1"><span class="style5"><a href="/noun.jsp" target="_blank">證券辭典</a> ｜ </span><a href="/stockQA1.jsp" target="_self">入門股市問答</a> │進階股市問答</div>
</form>

<table width="80%" border="0" align="center" cellpadding="4" cellspacing="1" bgcolor="#CCCCCC">
<tr class="content_title">
	<td colspan="2" align="center" valign="top" border="0"><span class="style4">【進階股市問答】</span></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td border="0" valign="top"><div align="right" class="style7">01：</div></td>
	<td width="95%" valign="top"><div align="left" class="style7" id="q1_title"><a href="javascript:showlayer(1)" class="mt "> 除了發行量加權股價指數外，還有什麼能反應市場股價之指標？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q1" style="display:inline" class="content_answer">
	<td valign="top"><div align="right" class="style7">Ans：</div></td>
	<td valign="top"><div align="left" class="style7"> 台灣證券交易所另編製有第一、二類發行量加權股價指數，不含金融股發行量加權股價指數和產業分類發行量加權股價指數、以及綜合股價平均數、工業股價平均數供投資人參考，另外經濟日報及工商時報亦分別編製有股價指數。</div></td>
</tr>

<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style7">02：</div></td>
	<td valign="top"><div align="left" class="style7" id="q2_title"><a href="javascript:showlayer(2)" class="mt "> 為什麼有時候大多數股票之股價均上漲，發行量加權股價指數反而下跌？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q2" style="display:none" class="content_answer">
  	<td valign="top"><div align="right" class="style7">Ans：</div></td>
  	<td valign="top"><div align="left" class="style7"> 因為發行量加權股價指數是以各上市公司股票發行量為權數，因此權數大的股票，其股價之漲跌影響指數之昇降幅度亦較大。因此當大多數小型股股價上漲，少數大型股股價下跌時，可能會導致發行量加權股價指數之下降。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style7">03：</div></td>
	<td valign="top"><div align="left" class="style7" id="q3_title"><a href="javascript:showlayer(3)" class="mt "> 何謂產業分類股價指數？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q3" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style7">Ans：</div></td>
  	<td valign="top"><div align="left" class="style7"> 分類股價指數係採樣樣本僅限於某一產業的一種股價指數。證交所目前所編算之產業分類股價指數是以各股之發行量當作權數來計算，產業分類股價指數計有水泥窯製、食品、塑膠化工、紡織纖維、機電、造紙、營造建材、金融保險等八種行業分類股價指數。其中水泥窯製類之採樣樣本涵蓋水泥業和玻璃陶瓷業；塑膠化工類則包括塑膠工業、化學工業和橡膠工業；機電類則包括電機機械業、電器電纜業和電子工業。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style7">04：</div></td>
	<td valign="top"><div align="left" class="style7" id="q4_title"><a href="javascript:showlayer(4)" class="mt "> 發行量加權股價指數之意義為何？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q4" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style7">Ans：</div></td>
  	<td valign="top"><div align="left" class="style7"> 發行量加權股價指數是反應整體市場股票價值變動的指標。其係以上市股票之發行量當作權數來計算股價指數，目前為台灣證券市場中最為人熟悉的指數。發行量加權股價指數係以民國五十五年為基期，基期指數設為100，其採樣樣本除特別股、全額交割股外，其餘上市股票均包括在內。發行量加權股價指數的特色是股本較大的股票對指數的影響會大於股本小的股票，如您對股價指數之編算方法欲進一步地了解，可至台灣證券交易所公關室取閱股價指數簡介作為參考。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style7">05：</div></td>
	<td valign="top"><div align="left" class="style7" id="q5_title"><a href="javascript:showlayer(5)" class="mt style3"> 股票上市對股東有那些利益？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q5" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style7">Ans：</div></td>
  	<td valign="top"><div align="left" class="style7"> 1.易於流通及變現<br />
2.得為融資融券或金融機構貸款之擔保。<br />
3.透過各方資料提供，廣泛瞭解市場動態。<br />
4.享受股利所得在二十七萬元以內免納所得稅之獎勵優惠。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style7">06：</div></td>
	<td valign="top"><div align="left" class="style7" id="q6_title"><a href="javascript:showlayer(6)" class="mt style3"> 股票上市對公司有那些利益？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q6" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style7">Ans：</div></td>
  	<td valign="top"><div align="left" class="style7"> 1.便利籌措長期資金，加速企業成長。<br />
2.提高公司聲譽，帶動業績成長。<br />
3.促進內部管理健全，增加經營績效。<br />
4.易獲國內外各界之支持。<br />
5.透過市場分散股份，以達利潤分享之均富目的。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style7">07：</div></td>
	<td valign="top"><div align="left" class="style7" id="q7_title"><a href="javascript:showlayer(7)" class="mt style3">
投資人買股票或受益憑證後，與證券商交割時應以何種方式繳付價款？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q7" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style7">Ans：</div></td>
  	<td valign="top"><div align="left" class="style7">
應於證券商指定之金融機構開立款項劃撥帳戶，辦理收付價款。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style7">08：</div></td>
	<td valign="top"><div align="left" class="style7" id="q8_title"><a href="javascript:showlayer(8)" class="mt style3"> 投資人買賣股票或受益憑證，可否委託他人代辦交割？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q8" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style7">Ans：</div></td>
  	<td valign="top"><div align="left" class="style7">
買投資人得以書面委任書委託他人代辦交割。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style7">09：</div></td>
	<td valign="top"><div align="left" class="style7" id="q9_title"><a href="javascript:showlayer(9)" class="mt style3"> 投資人買賣全額交割的股票，應於何時辦理交割？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q9" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style7">Ans：</div></td>
  	<td valign="top"><div align="left" class="style7"> 投資人買賣全額交割之股票，應於委託買賣前，預先繳交股票或價款。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style7">10：</div></td>
	<td valign="top"><div align="left" class="style7" id="q10_title"><a href="javascript:showlayer(10)" class="mt style3"> 投資人買賣普通股票或受益憑證，應於何時辦理交割？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q10" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style7">Ans：</div></td>
  	<td valign="top"><div align="left" class="style7"> 投資人買賣股票或受益憑證應於委託買進賣出成交日後的第一營業日上午十二時前（星期六為上午十一時前），與受託證券商辦理交割。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style7">11：</div></td>
	<td valign="top"><div align="left" class="style7" id="q11_title"><a href="javascript:showlayer(11)" class="mt style3"> 投資人所持之股票如缺過戶申請書，應如何辦理過戶？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q11" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style7">Ans：</div></td>
  	<td valign="top"><div align="left" class="style7"> 缺過戶申請書的股票，即無法申請過戶，但從市場購入之股票持有人，可向上市公司查詢，找出過戶書所載最後出讓人的賣出證券商，請該證券商洽原出讓人在新過戶申請書上蓋原留印鑑後，即可辦理過戶。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style7">12：</div></td>
	<td valign="top"><div align="left" class="style7" id="q12_title"><a href="javascript:showlayer(12)" class="mt style3"> 投資人遺失股票除向上市公司辦理掛失股票手續後，還須辦理甚麼手續？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q12" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style7">Ans：</div></td>
  	<td valign="top"><div align="left" class="style7"> 投資人應在五日內向法院聲請公示催告，並以聲請狀副本及法院收文收據影本送上市公司。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style7">13：</div></td>
	<td valign="top"><div align="left" class="style7" id="q13_title"><a href="javascript:showlayer(13)" class="mt style3"> 投資人遺失股票經向上市公司辦理掛失及公示催告手續後又找到該股票，應如何處理？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q13" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style7">Ans：</div></td>
  	<td valign="top"><div align="left" class="style7"> 投資人應先向法院聲請撤銷公示催告，再向上市公司申請撤銷股票掛失。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style7">14：</div></td>
	<td valign="top"><div align="left" class="style7" id="q14_title"><a href="javascript:showlayer(14)" class="mt style3"> 投資人領回股票或受益憑證後，因不慎遺失或被盜時，應如何處理？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q14" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style7">Ans：</div></td>
  	<td valign="top"><div align="left" class="style7"> 1.向警察機關報案。<br />
2.向上市公司或股務代理機構辦理該股票之掛失手續。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style7">15：</div></td>
	<td valign="top"><div align="left" class="style7" id="q15_title"><a href="javascript:showlayer(15)" class="mt style3"> 投資人如於當月份無成交紀錄，是否仍可請求證券經紀商寄發對帳單？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q15" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style7">Ans：</div></td>
  	<td valign="top"><div align="left" class="style7"> 投資人當月雖無成交紀錄，仍得以書面請求證券經紀商寄發對帳單。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style7">16：</div></td>
	<td valign="top"><div align="left" class="style7" id="q16_title"><a href="javascript:showlayer(16)" class="mt style3"> 私人間可否不透過證券經紀商直接讓受上市公司股票？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q16" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style7">Ans：</div></td>
  	<td valign="top"><div align="left" class="style7"> 不可以。除非私人間讓受之上市股票不超過一個成交單位，且與前次讓受行為相隔至少三個月，否則要受處罰。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style7">17：</div></td>
	<td valign="top"><div align="left" class="style7" id="q17_title"><a href="javascript:showlayer(17)" class="mt style3"> 投資人委託買賣股票及受益憑證後可否撤銷或變更委託事項？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q17" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style7">Ans：</div></td>
  	<td valign="top"><div align="left" class="style7"> 普通交易<br />
委託買賣股票後，若其委託事項尚未成交者，得通知受託證券經紀商辦理撤銷或變更委託。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style7">18：</div></td>
	<td valign="top"><div align="left" class="style7" id="q18_title"><a href="javascript:showlayer(18)" class="mt style3"> 投資人如何確定自己未被他人冒名開戶？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q18" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style7">Ans：</div></td>
  	<td valign="top"><div align="left" class="style7"> 可由本人親持身分證明文件正本向台灣證券交易所稽核室查詢。</div></td>
</tr>
</table>
</body>

</html>
