<%@ page contentType="text/html; charset=big5" language="java" import="java.sql.*,java.text.DecimalFormat" errorPage="" %>
<%@ include file="/Connections/test.jsp" %>
<%
// *** Restrict Access To Page: Grant or deny access to this page
String MM_authorizedUsers="";
String MM_authFailedURL="/relogin.jsp";
boolean MM_grantAccess=false;
if (session.getValue("MM_Username") != null && !session.getValue("MM_Username").equals("")) {
  if (true || (session.getValue("MM_UserAuthorization")=="") || 
          (MM_authorizedUsers.indexOf((String)session.getValue("MM_UserAuthorization")) >=0)) {
    MM_grantAccess = true;
  }
}
if (!MM_grantAccess) {
  String MM_qsChar = "?";
  if (MM_authFailedURL.indexOf("?") >= 0) MM_qsChar = "&";
  String MM_referrer = request.getRequestURI();
  if (request.getQueryString() != null) MM_referrer = MM_referrer + "?" + request.getQueryString();
  MM_authFailedURL = MM_authFailedURL + MM_qsChar + "accessdenied=" + java.net.URLEncoder.encode(MM_referrer);
  response.sendRedirect(response.encodeRedirectURL(MM_authFailedURL));
  return;
}
%>
<%
String Recordset1__MMColParam = "1";
if (session.getValue("Uid") !=null) {Recordset1__MMColParam = (String)session.getValue("Uid");}
%>
<%
Driver DriverRecordset1 = (Driver)Class.forName(MM_test_DRIVER).newInstance();
Connection ConnRecordset1 = DriverManager.getConnection(MM_test_STRING,MM_test_USERNAME,MM_test_PASSWORD);
PreparedStatement StatementRecordset1 = ConnRecordset1.prepareStatement("SELECT * FROM justock.bank WHERE bUid = '" + Recordset1__MMColParam + "'");
ResultSet Recordset1 = StatementRecordset1.executeQuery();
boolean Recordset1_isEmpty = !Recordset1.next();
boolean Recordset1_hasData = !Recordset1_isEmpty;
Object Recordset1_data;
int Recordset1_numRows = 0;
DecimalFormat df = new DecimalFormat("0,000");

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5" />


<title>我的帳戶</title>
<style type="text/css">
<!--
a:link {
	color: #aba000;
	text-decoration: none;
}
a:visited {
	color: #ABA000;
	text-decoration: none;
}
a:hover {
	color: #ABA000;
	text-decoration: underline;
}
a:active {
	color: #ABA000;
	text-decoration: none;
}
.style3 {
	color: #FFFFFF;
	font-weight: bold;
}
body {
	background-color: #e7f3ff;
}
.style4 {font-size: 16px}
.style6 {color: #000000; font-size: 15px; }
-->
</style>
</head>
 
<div>
<body>
 <table width="50%" border="0" align="center" cellpadding="4" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td colspan="2" nowrap="nowrap" bgcolor="#CCCCCC"><div align="center" class="style3 style4">我 的 帳 戶 </div></td>
  </tr>
  <tr>
    <td width="100" nowrap="nowrap" bgcolor="#FFFFCC"><div align="center" class="style6">會員帳號</div></td>
    <td width="269" bgcolor="#FFFFFF">　<%=(((Recordset1_data = Recordset1.getObject("bUid"))==null || Recordset1.wasNull())?"":Recordset1_data)%></td>
  </tr>
  <tr>
    <td nowrap="nowrap" bgcolor="#FFFFCC"><div align="center" class="style6">銀行代碼</div></td>
    <td bgcolor="#FFFFFF">　<%=(((Recordset1_data = Recordset1.getObject("Bid"))==null || Recordset1.wasNull())?"":Recordset1_data)%></td>
  </tr>
  <tr>
    <td nowrap="nowrap" bgcolor="#FFFFCC"><div align="center" class="style6">銀行帳號</div></td>
    <td bgcolor="#FFFFFF">　<%=(((Recordset1_data = Recordset1.getObject("bAcctnum"))==null || Recordset1.wasNull())?"":Recordset1_data)%></td>
  </tr>
  <tr>
    <td nowrap="nowrap" bgcolor="#FFFFCC"><div align="center" class="style6">帳戶餘額</div></td>
    <td bgcolor="#FFFFFF">　<%=(((Recordset1_data = df.format(Recordset1.getDouble("bBalance")))==null || Recordset1.wasNull())?"":Recordset1_data)%></td>
  </tr>
</table>

</div>
</body>
</html>
<%
Recordset1.close();
StatementRecordset1.close();
ConnRecordset1.close();
%>
