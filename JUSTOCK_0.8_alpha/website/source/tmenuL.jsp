<%@ page contentType="text/html; charset=big5" language="java" import="java.sql.*,java.text.*" errorPage="" %>
<jsp:useBean id="dateBean" class="common.util.DateUtil"/>
<jsp:useBean id="timeBean" class="common.util.TimeUtil"/>
<%
// *** Logout the current user.
String MM_Logout = request.getRequestURI() + "?MM_Logoutnow=1";
if (request.getParameter("MM_Logoutnow") != null && request.getParameter("MM_Logoutnow").equals("1")) {
  session.putValue("MM_Username", "");
  session.putValue("MM_UserAuthorization", "");
  String MM_logoutRedirectPage = "/index.jsp";
  // redirect with URL parameters (remove the "MM_Logoutnow" query param).
  if (MM_logoutRedirectPage.equals("")) MM_logoutRedirectPage = request.getRequestURI();
  if (MM_logoutRedirectPage.indexOf("?") == -1 && request.getQueryString() != null) {
    String MM_newQS = request.getQueryString();
    String URsearch = "MM_Logoutnow=1";
    int URStart = MM_newQS.indexOf(URsearch);
    if (URStart >= 0) {
      MM_newQS = MM_newQS.substring(0,URStart) + MM_newQS.substring(URStart + URsearch.length());
    }
    if (MM_newQS.length() > 0) MM_logoutRedirectPage += "?" + MM_newQS;
  }
  response.sendRedirect(response.encodeRedirectURL(MM_logoutRedirectPage));
  return;
}
%>
<%
// *** Restrict Access To Page: Grant or deny access to this page
String MM_authorizedUsers="";
String MM_authFailedURL="/index.jsp";
boolean MM_grantAccess=false;
if (session.getValue("MM_Username") != null && !session.getValue("MM_Username").equals("")) {
  if (true || (session.getValue("MM_UserAuthorization")=="") || 
          (MM_authorizedUsers.indexOf((String)session.getValue("MM_UserAuthorization")) >=0)) {
    MM_grantAccess = true;
  }
}
if (!MM_grantAccess) {
  String MM_qsChar = "?";
  if (MM_authFailedURL.indexOf("?") >= 0) MM_qsChar = "&";
  String MM_referrer = request.getRequestURI();
  if (request.getQueryString() != null) MM_referrer = MM_referrer + "?" + request.getQueryString();
  MM_authFailedURL = MM_authFailedURL + MM_qsChar + "accessdenied=" + java.net.URLEncoder.encode(MM_referrer);
  response.sendRedirect(response.encodeRedirectURL(MM_authFailedURL));
  return;
}
%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5" />
<title>JUSTOCK</title>
<style type="text/css">
<!--
#Layer1 {	position:absolute;
	left:4px;
	top:19px;
	width:102px;
	height:66px;
	z-index:1;
}
a:link {
	color: #999999;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: #999999;
}
a:hover {
	text-decoration: none;
	color: #999999;
}
a:active {
	text-decoration: none;
	color: #999999;
}
#Layer2 {
	position:absolute;
	left:47px;
	top:132px;
	width:143px;
	height:334px;
	z-index:2;
}
#Layer3 {
	position:absolute;
	left:18px;
	top:8px;
	width:202px;
	height:19px;
	z-index:3;
}
.style2 {
	font-size: 18px;
	color: #000066;
	line-height:1.3;
}
.style6 {
	font-size: 14px; 
	color: #CCCCCC; 
	line-height:1.3;}
#Layer4 {
	position:absolute;
	left:17px;
	top:104px;
	width:170px;
	height:23px;
	z-index:4;
}
body {
	background-image: url(/images/bg/menu.gif);
	background-repeat: repeat;
}
.style9 {font-size: 15px; color: #CCCCCC; line-height: 1.3; }
.style10 {font-size: 15px}
.style11 {
	font-size: 16px;
	color: #000066;
	line-height: 1.3;
	font-weight: bold;
}
.style15 {color: #000063}

-->
</style>
<script language="javascript">
<!--
function showlayer(id){
	for(var i=1;i<=14;i++){
		if(document.all("q"+i).style.display=='inline'){
			document.all("q"+i).style.display='none';
			document.all("q"+i+"_title").style.fontWeight ='normal';
			break;
		}
	}
	
	document.all("q"+id).style.display='inline';
	document.all("q"+id+"_title").style.fontWeight ='bold';
	document.all("q"+id+"_title").style.fontColor ='#CC3300';
	}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</head>

<body>

<a href="/main.jsp" target="mainFrame"><img src="images/1105 l.gif" width="222" height="79" hspace="0" vspace="0" border="0" /></a><br />
<br />


  <table width="80%" height="85%" border="0" align="center" cellpadding="2" cellspacing="1" bgcolor="bddbef" >

    <tr>
      <td background="/images/bg/link.gif" bgcolor="#FFFFFF" class="style11"><div align="center">+ 大盤行情 +</div></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="right" class="style9">
        <div align="center"><a href="/TSE.jsp" target="mainFrame"> 集中市場大盤走勢</a></div>
      </div></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF"><div align="right" class="style9">
        <div align="center"><a href="/OTC.jsp" target="mainFrame">上櫃股票大盤走勢</a></div>
      </div></td>
    </tr> 
    <tr>
      <td background="/images/bg/link.gif" bgcolor="#FFFFFF" class="style11"><div align="center">+ 交易專區 +</div></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF" class="style6"><div align="center" class="style10"><a href="/purchaseorder.jsp" target="mainFrame">下單</a></div></td>
    </tr>
	    <tr>
      <td bgcolor="#FFFFFF" class="style6"><div align="center" class="style10"><a href="/orderlist.jsp" target="mainFrame">下單一覽</a></div></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF" class="style6"><div align="center" class="style10"><a href="/holding.jsp" target="mainFrame">投資組合</a></div></td></tr>
    <tr>
      <td bgcolor="#FFFFFF" class="style6"><div align="center" class="style10"><a href="/traderecord.jsp" target="mainFrame">交易記錄一覽</a></div></td>
    </tr>  
    <tr>
      <td background="/images/bg/link.gif" bgcolor="#FFFFFF" class="style11"><div align="center">+ 會員專區 +</div></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF" class="style6"><div align="center" class="style10"><a href="/myaccount.jsp" target="mainFrame">我的帳戶</a></div></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF" class="style6"><div align="center" class="style10"><a href="/memberdata.jsp" target="mainFrame">個人資料修改</a></div></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF" class="style6"><div align="center" class="style10"><a href="/userecord.jsp" target="mainFrame">使用者記錄列表</a></div></td>
    </tr> 
    <tr>
      <td background="/images/bg/link.gif" bgcolor="#FFFFFF" class="style2"><div align="left" class="style11">
        <div align="center">+ 使用說明 +</div>
      </div></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF" class="style6"><div align="center" class="style10"><a href="/newuser.jsp" target="mainFrame">新手上路</a></div></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF" class="style6"><div align="center" class="style10"><a href="/siteDoctemp.jsp" target="mainFrame">系統說明</a></div></td>
    </tr>
	<tr>
      <td bgcolor="#FFFFFF" class="style6"><div align="center" class="style10"><a href="/goodsite.jsp" target="mainFrame">好站連結</a></div></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF" class="style6"><div align="center" class="style10"><a href="mailto:BDKY.justock@gmail.com">聯絡系統管理員</a></div></td>
    </tr>

</table>

</body>
</html>