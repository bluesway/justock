<%@ page contentType="text/html; charset=utf-8" language="java" import="java.util.*,java.sql.*,java.text.DecimalFormat" errorPage="" %>
<%@ include file="Connections/justockdb.jsp" %>
<jsp:useBean id="dateBean" class="common.util.DateUtil"/>
<jsp:useBean id="timeBean" class="common.util.TimeUtil"/>
<%
Driver DriverTSE = (Driver)Class.forName(MM_justockdb_DRIVER).newInstance();
Connection ConnTSE = DriverManager.getConnection(MM_justockdb_STRING,MM_justockdb_USERNAME,MM_justockdb_PASSWORD);
PreparedStatement StatementTSE = ConnTSE.prepareStatement("SELECT * FROM justock.info_tse ORDER BY oTime DESC");
ResultSet TSE = StatementTSE.executeQuery();
boolean TSE_isEmpty = !TSE.next();
boolean TSE_hasData = !TSE_isEmpty;
Object TSE_data;
int TSE_numRows = 0;
int teddy = Integer.parseInt(String.valueOf((((TSE_data = TSE.getObject("oTime"))==null || TSE.wasNull())?"":TSE_data)));
%>
<%
Driver DriverTSE1 = (Driver)Class.forName(MM_justockdb_DRIVER).newInstance();
Connection ConnTSE1 = DriverManager.getConnection(MM_justockdb_STRING,MM_justockdb_USERNAME,MM_justockdb_PASSWORD);
PreparedStatement StatementTSE1 = ConnTSE1.prepareStatement("SELECT oIndex FROM justock.info_tse ORDER BY oIndex ASC");
ResultSet TSE1 = StatementTSE1.executeQuery();
boolean TSE1_isEmpty = !TSE1.next();
boolean TSE1_hasData = !TSE1_isEmpty;
Object TSE1_data;
int TSE1_numRows = 0;
%>
<%
Driver DriverTSE2 = (Driver)Class.forName(MM_justockdb_DRIVER).newInstance();
Connection ConnTSE2 = DriverManager.getConnection(MM_justockdb_STRING,MM_justockdb_USERNAME,MM_justockdb_PASSWORD);
PreparedStatement StatementTSE2 = ConnTSE2.prepareStatement("SELECT oIndex FROM justock.info_tse ORDER BY oIndex DESC");
ResultSet TSE2 = StatementTSE2.executeQuery();
boolean TSE2_isEmpty = !TSE2.next();
boolean TSE2_hasData = !TSE2_isEmpty;
Object TSE2_data;
int TSE2_numRows = 0;
%>
<% request.setCharacterEncoding("utf-8"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<title>集中市場大盤走勢</title>
<style type="text/css">
<!--
.style3 {
	color: #666666;
	font-weight: bold;
	font-family: "新細明體";
	font-size: 20px;
}
.style6 {font-family: "新細明體";font-size: 15px; }
body {
	background-color: #e7f3ff;
}
.style26 {font-family: "新細明體"; font-size: 15px; }
.style28 {font-family: "新細明體"; font-size: 15px; color: #8b0041; }
.style30 {font-family: "新細明體"; font-size: 15px; color: #006583; }
.style31 {
	font-size: 12px;
	color: #666666;
}
a:link {
	color: #0000CC;
}
a:visited {
	color: #0000CC;
}
a:hover {
	color: #0000CC;
}
a:active {
	color: #0000CC;
}
.style32 {font-size: 15px}

--> 
</style>
</head>

<body>
<p align="center"><font class="style3">
<%
  String today = "";
  Calendar cal = Calendar.getInstance();
  cal.setTime(new java.util.Date(System.currentTimeMillis()));

  if (cal.get(Calendar.DAY_OF_WEEK) == 7) {
  	cal.roll(Calendar.DAY_OF_YEAR, -1);
	today = new java.text.SimpleDateFormat("yyyy/MM/dd").format(cal.getTime());
  } else if (cal.get(Calendar.DAY_OF_WEEK) == 1) {
  	cal.roll(Calendar.DAY_OF_YEAR, -2);
	today = new java.text.SimpleDateFormat("yyyy/MM/dd").format(cal.getTime());
  } else if (cal.get(Calendar.HOUR_OF_DAY) < 9) {
  	cal.roll(Calendar.DAY_OF_YEAR, -1);
	today = new java.text.SimpleDateFormat("yyyy/MM/dd").format(cal.getTime());
  } else  
  	today = dateBean.show(1).substring(0,10);;

  out.print(today);
%>
集中市場大盤走勢圖</font><span class="style31">　<a href="/OTC.jsp" target="_self">上櫃股票大盤走勢圖</a></span><br />
<img src="overallGraph.jsp?stockId=0&amp;otc=false&amp;screen_x=600&amp;screen_y=300" align="middle" /><br />
<table width="50%" border="0" align="center" cellpadding="1" cellspacing="1">

  <tr>
    <td width="82" align="right" nowrap="nowrap"><span class="style26"><font color="#0000FF">加權指數：</font></span></td>
    <td width="106" align="left"><span class="style6 "><%=(((TSE_data = TSE.getObject("oIndex"))==null || TSE.wasNull())?"":TSE_data)%></span></td>
    <td align="right" nowrap="nowrap"><span class="style32">時間：</span></td>
    <td width="98" align="left"><span class="style26"><%=timeBean.transTime(teddy)%></span></td>
  </tr>
  <tr>
    <td align="right" nowrap="nowrap"><span class="style26"><font color="#0000FF">最高：</font></span></td>
    <td align="left"><span class="style6 "><%=(((TSE2_data = TSE2.getObject("oIndex"))==null || TSE2.wasNull())?"":TSE2_data)%></span></td>
    <td align="right" nowrap="nowrap"><span class="style26"><font color="#0000FF">　　最低：</font></span></td>
    <td align="left"><span class="style6 "><%=(((TSE1_data = TSE1.getObject("oIndex"))==null || TSE1.wasNull())?"":TSE1_data)%></span></td>
  </tr>
  <tr>
    <td align="right" nowrap="nowrap"><span class="style26"><font color="#FF0000">成交金額：</font></span></td>
    <td align="left"><span class="style6 ">
	<%
	String num = String.valueOf(((TSE_data = TSE.getObject("oAmount"))==null || TSE.wasNull())?"":TSE_data);
	double quan = Long.parseLong(num) / 100000000.0;
	DecimalFormat df = new DecimalFormat("000.00");
	out.print(df.format(quan));
	%>
	億</span></td>
    <td align="right"></td>
    <td align="left"></td>
  </tr>
  <tr>
    <td align="right" nowrap="nowrap"><span class="style26"><font color="#FF0000">成交張數：</font></span></td>
    <td align="left"><span class="style6"><%=(((TSE_data = TSE.getObject("oShare"))==null || TSE.wasNull())?"":TSE_data)%></span></td>
    <td align="right" nowrap="nowrap"><span class="style26"><font color="#FF0000">成交筆數：</font></span></td>
    <td align="left"><span class="style6 "><%=(((TSE_data = TSE.getObject("oCnt"))==null || TSE.wasNull())?"":TSE_data)%></span></td>
  </tr>
  <tr>
    <td align="right" nowrap="nowrap"><span class="style28">委買張數：</span></td>
    <td align="left"><span class="style6 "><%=(((TSE_data = TSE.getObject("oBQuan"))==null || TSE.wasNull())?"":TSE_data)%></span></td>
    <td align="right" nowrap="nowrap"><span class="style28">委買筆數：</span></td>
    <td align="left"><span class="style6 "><%=(((TSE_data = TSE.getObject("oBCnt"))==null || TSE.wasNull())?"":TSE_data)%></span></td>
  </tr>
  <tr>
    <td align="right" nowrap="nowrap"><span class="style30">委賣張數：</span></td>
    <td align="left"><span class="style6 "><%=(((TSE_data = TSE.getObject("oSQuan"))==null || TSE.wasNull())?"":TSE_data)%></span></td>
    <td align="right" nowrap="nowrap"><span class="style30">委賣筆數：</span></td>
    <td align="left"><span class="style6 "><%=(((TSE_data = TSE.getObject("oSCnt"))==null || TSE.wasNull())?"":TSE_data)%></span></td>
  </tr>
</table>
</body>
</html>
<%
TSE.close();
StatementTSE.close();
ConnTSE.close();
%>
<%
TSE1.close();
StatementTSE1.close();
ConnTSE1.close();
%>
<%
TSE2.close();
StatementTSE2.close();
ConnTSE2.close();
%>
