<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Pragma" content="no-cache">
<title>股票代碼查詢 - 上市</title>
<style type="text/css">
<!--
body {
	background-color: #E7F3FF;
}
a:hover {
	color: #0000CC;
	text-transform: none;
	font-size: 24px;
	text-decoration: none;
}
a:link {
	color: #0000cc;
	text-decoration: none;
}
a:visited {
	color: #0000CC;
	text-decoration: none;
}
a:active {
	color: #FF0000;
	text-decoration: none;
}
.style1 {
	color: #0000CC;
	font-weight: bold;
}
-->
</style>
</head>

<body>
<form id="form1" name="form1" method="post" action="">

<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' height='28' >
<tr align='center' bgcolor='#CCCCCC'>
<td width='100%' colspan='2'><table width='100%' border='0' cellspacing='1' cellpadding='4'>
<tr align="center" bgcolor="#FFFFFF">
  <td height="25" rowspan="3" align="center" nowrap="nowrap" bgcolor="#FFFFCC" class="c3 style1">上市</td>
<td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=0" target="bottomFrame">水泥</a></td>
<td  height="25" align="center" nowrap="nowrap" bgcolor="#FFFFFF" class="c3"><a href="/code1.jsp?class=1" target="bottomFrame">食品</a></td>
<td  height="25" align="center" nowrap="nowrap" bgcolor="#FFFFFF" class="c3"><a href="/code1.jsp?class=2" target="bottomFrame">塑膠</a></td>
<td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=3" target="bottomFrame">紡織</a></td>

<td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=4" target="bottomFrame">電機</a></td>
<td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=5" target="bottomFrame">電器電纜</a></td>
<td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=6" target="bottomFrame">化學</a></td>
<td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=7" target="bottomFrame">生技醫療</a></td>
<td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=8" target="bottomFrame">玻璃</a></td>
<td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=9" target="bottomFrame">造紙</a></td>
<td height="25" rowspan="3" align="center" nowrap="nowrap" bgcolor="#FFFFCC" class="c3"><a href="/code_OTC.jsp" target="_self">上櫃</a></td>
</tr>
<tr align="center" bgcolor="#FFFFFF">
  <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=10" target="bottomFrame">鋼鐵</a></td>
  <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=11" target="bottomFrame">橡膠</a></td>
  <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=12" target="bottomFrame">汽車</a></td>
<td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=13" target="bottomFrame">半導體</a></td>
<td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=14" target="bottomFrame">電腦週邊</a></td>
<td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=15" target="bottomFrame">光電</a></td>
<td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=16" target="bottomFrame">通信網路</a></td>
<td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=17" target="bottomFrame">電子零組件</a></td>
<td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=18" target="bottomFrame">電子通路</a></td>
<td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=19" target="bottomFrame">資訊服務</a></td>
</tr>
<tr align="center" bgcolor="#FFFFFF">
  <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=20" target="bottomFrame">其它電子</a></td>
<td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=21" target="bottomFrame">營建</a></td>
<td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=22" target="bottomFrame">航運</a></td>
<td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=23" target="bottomFrame">觀光</a></td>
<td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=24" target="bottomFrame">金融</a></td>
<td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=25" target="bottomFrame">貿易百貨</a></td>
<td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=26" target="bottomFrame">油電燃氣</a></td>
<td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=27" target="bottomFrame">綜合</a></td>
<td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=28" target="bottomFrame">其他</a></td>
<td  height="25" align="center" nowrap="nowrap" class="c3">&nbsp;</td>
</tr>
</table>
</td>
</tr>
</table>
</form>
</body>
</html>
