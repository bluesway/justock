/**
 * @author bluesway
 * TARGET 顯示個股及大盤的當日資訊
 */

package website.graph;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.LinkedList;

import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class OverallGraphJSP extends HttpServlet {
	// 視窗元件大小變數
    private static final Dimension user_screen_size = Toolkit.getDefaultToolkit().getScreenSize();
    private static int[] overall_size;
    
    // 資料庫連線變數
    private static Connection conn;
    private static Statement stmt;
	private static final int TYPE_LIMITED = 0;
	private static final int TYPE_UNLIMITED = 1;
	
    // 繪圖用變數
	private BufferedImage image;
    private int screen_x;
    private int screen_y;
    private Rectangle graphD;	// 線圖邊框範圍
    private int line_cap;				// 框線寬度
    private int start_main;			// 主線圖上緣
    private int height_main;		// 主線圖高度
    private int start_quan;			// 成交量上緣
    private int height_quan;		// 成交量高度
    private int left_space;			// 顯示指數、成交量資訊的寬度
    private int right_space;		// 顯示指標資訊的寬度
    private int char_height;		// 預設字高
    private int limiteType;			// 限制類別 0:有7%限制 1:無7%限制 
    private double scale;			// 線圖縮放級距
	private double ratio;			// 數值在圖表中的相對比例
	private NumberFormat nf;
	
	// 資料處理用變數
	private int stockId;					// 繪圖資料來源 - 0:大盤 id:個股
	private LinkedList<double[]> sourceData;		// 原始資料
    private String[] time_mark = new String[2];	// 時間字串
    private int priceNum;				// 指數參考資料顯示筆數 (須為單數筆且大於3)
    private int quanNum;				// 成交量參考資料顯示筆數 (須為單數筆)
    private double[] data;				// 圖表繪製range資料
	private double[] priceInitial;		// 指數參考資料值
	private double[] quanInitial;		// 成交量參考資料值
	private double priceSep;			// 指數參考資料區間
	private double quanSep;				// 成交量參考資料區間
	private double[] prev;				// 前項指標
	private double[] now;				// 當項指標
	private int time;					// 當前處理時間
	private int range;					// 資料讀取區間
	
	// 資料庫用變數
    private ResultSet result;
//	private double yesterday;		// 昨日收盤價
	private double[] tmp;
	private boolean otc;

	public void init (ServletConfig config) throws ServletException {
		super.init(config);
	}
	
	private void connectDB() {
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://JUSTOCK.twbbs.org:3306/justock";
		String user = "XDDDDD";
		String pwd = "specification";
		
		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url + "?user=" + user + "&password=" + pwd);
			stmt = conn.createStatement();
		} catch (InstantiationException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		screen_x = Integer.parseInt(request.getParameter("screen_x"));
		screen_y = Integer.parseInt(request.getParameter("screen_y"));
		stockId = Integer.parseInt(request.getParameter("stockId"));
		otc = Boolean.parseBoolean(request.getParameter("otc"));
		// 顯示型態 0:有7%限制 1:無7%限制
		limiteType = 0;
		
    	overall_size = new int[400];
    	for (int i = 0; i < 400; i++)
    		overall_size[i] = (int) Math.round(user_screen_size.getWidth() * 0.0025 * i);
		// 數字格式
    	nf = NumberFormat.getInstance();
    	nf.setMaximumFractionDigits(2);
    	
    	// 變數處理
    	graphD = new Rectangle();
    	
    	// 圖形繪製
    	line_cap = 2;
    	char_height = overall_size[5];
    	left_space = overall_size[22];
    	right_space = 10;
    	start_main = 15;

    	// 資料處理
    	sourceData = new LinkedList<double[]>();
    	priceNum = 9;
    	quanNum = 5;
    	
    	response.setContentType("image/png"); 
    	
		connectDB();
		readData();
		paint();
		
		ServletOutputStream out = response.getOutputStream();   
        try {
            ImageIO.write(image, "PNG", out);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
		out.flush();  
        out.close();
	}
	
    private void readData() {
		double[] result_info;
		
		sourceData.clear();
		try {
			if (stockId == 0) {
				result_info = new double[10];
				
				if (otc)
					result = stmt.executeQuery("SELECT * FROM info_otc ORDER BY oTime ASC");
				else
					result = stmt.executeQuery("SELECT * FROM info_tse ORDER BY oTime ASC");
				
				if (result.next())
//					System.out.println(result.getDouble(5));
					;
//					yesterday = result.getDouble(2);
				
				if (result.next())
					for (int i = 0; i < 10; i++){
						result_info[i] = result.getDouble(i + 1);
					}
				tmp = result_info.clone();
				sourceData.add(result_info.clone());
				
				numToTime(result_info[0]);

				while (result.next()) {
					for (int i = 0; i < 3; i++) {
						result_info[i] = result.getDouble(i + 1);
						tmp[i] = result_info[i];
					}
					for (int i = 3; i < 10; i++) {
						result_info[i] = result.getDouble(i + 1) - tmp[i];
						tmp[i] = result.getDouble(i + 1);
					}
					
					sourceData.add(result_info.clone());
					numToTime(result_info[0]);
				}
			} else {
				result_info = new double[8];
				
				// 取當盤資料
				result = stmt.executeQuery("SELECT * FROM info_today WHERE tSid='" + 
            		String.valueOf(stockId) + "' ORDER BY tTime ASC");

				if(result.next());
				while (result.next()) {
					// 為了和大盤資料相容
					result_info[0] = result.getDouble(2);
					result_info[1] = result.getDouble(4);
					result_info[2] = result.getDouble(3);
					for (int i = 3; i < 8; i++)
						result_info[i] = result.getDouble(i + 2);

					sourceData.add(result_info.clone());
					numToTime(result_info[0]);
				}

				// 取當日資料
				result = stmt.executeQuery("SELECT * FROM info_current WHERE cSid='" + 
	            		String.valueOf(stockId) + "'");

				if (result.next()) {
					for (int i = 0; i <5; i++)
						result_info[i] = result.getDouble(i + 2);
					
					tmp = result_info.clone();
				}
				
				// 取得公司名稱
	            result = stmt.executeQuery("SELECT * FROM stock WHERE Sid='" + 
	            		String.valueOf(stockId) + "'");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
    
    private void setData() {
    	int tmp = sourceData.size();
    	
    	time = 0;
    	range = tmp;
    	data = new double[tmp];
    	
    	prev = sourceData.get(time++);
    	
    	// 處理指數部分
    	priceInitial = new double[priceNum + 1];
    	
		for (int i = 0; i < range; i++)
			data[i] = sourceData.get(i)[1];
		
		if (limiteType == TYPE_LIMITED) {
			priceInitial[0] = data[0];
			Arrays.sort(data);
			if (priceInitial[0] - data[0] > data[data.length - 1] - priceInitial[0]) {
				priceInitial[1] = priceInitial[0] * 2 - data[0];
				priceInitial[priceNum] = data[0];
			} else {
				priceInitial[1] = data[data.length - 1];
				priceInitial[priceNum] = priceInitial[0] * 2 - data[data.length - 1];
			}
			priceSep = (priceInitial[1] - priceInitial[priceNum]) / (priceNum - 1) * 1.0;
			
			for (int i = priceNum - 1; i > 1; i--)
				priceInitial[i] = priceInitial[1] - priceSep * (i-1);
		} else {
			Arrays.sort(data);
    	
			priceInitial[1] = data[data.length - 1];
			priceInitial[priceNum] = data[0];
			priceSep = (priceInitial[1] - priceInitial[priceNum]) / (priceNum - 1) * 1.0;
			
			for (int i = priceNum - 1; i > 1; i--)
				priceInitial[i] = priceInitial[1] - priceSep * (i-1);
		}
    	
    	// 處理成交量部分
    	quanInitial = new double[quanNum + 1];
    	
		for (int i = 0; i < range; i++)
			if (stockId == 0)
				data[i] = sourceData.get(i)[3] / 100000000.0;
			else
				data[i] = sourceData.get(i)[3];
		
    	Arrays.sort(data);
    	
    	quanInitial[1] = data[data.length - 1];
    	quanInitial[quanNum] = 0;
    	
    	quanSep = quanInitial[1] / (quanNum - 1) * 1.0;
    	
    	for (int i = quanNum - 1; i > 1; i--) {
    		quanInitial[i] = quanInitial[1] - quanSep * (i-1);
    	}
    }
    
	private BufferedImage paint() {
		image = new BufferedImage(screen_x, screen_y, BufferedImage.TYPE_INT_RGB);
    	Graphics g = image.createGraphics();
    	Graphics2D page = (Graphics2D) g;
    	
    	// 重新讀取資料
     	graphD.setLocation(left_space, 10);
    	graphD.setSize(screen_x - left_space - right_space, screen_y - 25);
    	scale = graphD.width / 275.0;
    	height_main = (int) Math.round(graphD.height * 0.55);
    	start_quan = (int) Math.round(graphD.height * 0.7);
    	height_quan = (int) Math.round(graphD.height * 0.3);
    	
    	if (sourceData.size() <= 0)
    		this.log("error!");
    	else
    		setData();

    	// 清除背景
		page.setColor(Color.BLACK);
		page.fillRect(0, 0, screen_x, screen_y);
    	
		int x1, x2;
		
		// 顯示指數資訊
		drawChartLabel(page, 0);

        // 顯示成交量資訊
    	drawChartLabel(page, 1);

        // 畫線圖、柱圖
        while (time < sourceData.size()) {
        	now = sourceData.get(time);

        	drawLineChart(page, prev[1], now[1], x1 = numToAxis(prev[0]), x2 = numToAxis(now[0]) - x1);
        	if (stockId == 0)
        		drawQuanChart(page, now[3] / 100000000.0, x1, x2);
        	else
        		drawQuanChart(page, now, x1);

            prev = now;
            time++;
        }
        
        // 顯示時間資訊
        drawChartLabel(page, numToAxis(100000), 100000);
        drawChartLabel(page, numToAxis(110000), 110000);
        drawChartLabel(page, numToAxis(120000), 120000);
        drawChartLabel(page, numToAxis(130000), 130000);
        
        // 畫邊框
		drawCanvas(page);
		
		return image;
	}
    
    // 畫個股當日成交量圖
    private void drawQuanChart (Graphics page, double[] data, int x1) {
    	int y = calculateAxis(data[3], 1);
    	if (data[1] == data[4])
    		page.setColor(Color.RED);
    	else
    		page.setColor(Color.GREEN);
    	page.drawLine(x1, y, x1, graphD.y + start_quan + height_quan - line_cap);	// 畫柱圖分隔線
	}
    
    // 標示線圖資訊 (時間)
    private void drawChartLabel (Graphics page, int x, int time) {
    	page.setColor(Color.LIGHT_GRAY);
    	page.drawString(numToTime(time), x - overall_size[6], screen_y - 1);	// 秀出時間資訊
    	page.setColor(Color.DARK_GRAY);
    	drawBrokenLine(page, x, graphD.y, x, graphD.y + graphD.height);	// 畫縱虛線
    }
    
    // 畫虛線
    private void drawBrokenLine (Graphics page, int x0, int y0, int x1, int y1) {
        int b_width;				// 虛線寬
    	int b_height;				// 虛線高
    	int tmpX;					// 曲線圖 x 軸分段繪製坐標
    	int tmpY;					// 曲線圖 y 軸分段繪製坐標
    	
    	b_width = (x1 - x0) / 5;
    	b_height = (y1 - y0) / 5;
    	tmpX = x0;
    	tmpY = y0;
    	
    	while (true) {
    		if (tmpX + (b_width == 0 ? 0 : 5) > x1 || tmpY + (b_height == 0 ? 0 : 5) > y1 || (b_width == 0 && b_height == 0))
    			break;
    		page.drawLine(tmpX, tmpY, tmpX + (b_width == 0 ? 0 : 5), tmpY + (b_height == 0 ? 0 : 5));
    		tmpX += (b_width == 0 ? 0 : 5) * 2;
    		tmpY += (b_height == 0 ? 0 : 5) * 2;
    	}
    }
    
    // 畫折線圖
    private void drawLineChart (Graphics page, double dataPrev, double dataCurr, int x, int scale) {
    	if (dataCurr < dataPrev)
    		page.setColor(Color.CYAN);
//    	else if (dataCurr == dataPrev)
//    		if (dataCurr == priceInitial[1])
//    			page.setColor(Color.RED);
//    		else if (dataCurr == priceInitial[priceNum])
//    			page.setColor(Color.GREEN);
//    		else;
    	else
    		page.setColor(Color.WHITE);
    	
        page.drawLine(x, calculateAxis(dataPrev, 0), x + scale, calculateAxis(dataCurr, 0));
    }
    
    // 畫成交量圖
    private void drawQuanChart (Graphics page, double data, int x, int scale) {
    	int y = calculateAxis(data, 1);
        page.setColor(new Color(255, 156, 0));
        page.fillRect(x, y, scale, graphD.y + start_quan + height_quan - y);	// 畫柱圖
        page.setColor(Color.WHITE);
        page.drawLine(x += scale > 1 ? 0 : scale, y, x, graphD.y + start_quan + height_quan - line_cap);	// 畫柱圖分隔線
	}
    
    // 標示線圖資訊 (指數及成交量)
    private void drawChartLabel (Graphics page, int drawType) {	// 0:指數 1:成交量
    	int y;
    	int showNum = drawType == 0 ? priceNum : quanNum;
    	double[] showString = drawType == 0 ? priceInitial : quanInitial;
    	
		for (int i = 0; i < showNum; i++) {
			if (limiteType == TYPE_UNLIMITED)
				if (drawType == 0)
					page.setColor(Color.WHITE);
				else
					page.setColor(new Color(255, 156, 0));
			else if (drawType == 0)
	    		if (i == 0 && showString[1] > showString[0] * 1.07 - calculateUnit(showString[0]))
	                page.setColor(Color.RED);
	    		else if (i == priceNum - 1 && showString[priceNum] < showString[0] * 0.93 + calculateUnit(showString[0]))
	                page.setColor(new Color(0, 128, 0));
	    		else
	                page.setColor(Color.WHITE);
			else
				page.setColor(new Color(255, 156, 0));
			
			if (i < showNum - 1 || drawType != 1)
				page.drawString(nf.format(showString[i + 1]), 0, calculateAxis(-1.0 * i - 1, drawType));
			
			if (limiteType == TYPE_UNLIMITED)
	        	if (drawType == 0 && i == (showNum + 1) / 2)
	        		page.setColor(Color.LIGHT_GRAY);
	        	else
	        		page.setColor(Color.DARK_GRAY);
			else if (drawType == 1)
				page.setColor(Color.DARK_GRAY);
			else if (i == 0)
				page.setColor(new Color(128, 0, 0));
			else if (i == (showNum - 1) / 2)
				page.setColor(Color.LIGHT_GRAY);
			else if (i == showNum - 1)
				page.setColor(new Color(0, 128, 0));
			else
				page.setColor(Color.DARK_GRAY);
			
			y = calculateAxis(showString[i + 1], drawType);
			drawBrokenLine(page, graphD.x, y, graphD.x + graphD.width, y); // 畫橫虛線
		}
		
		if (drawType == 1) {
			page.setColor(new Color(255, 156, 0));
			page.drawString(stockId == 1000 || stockId == 0 || stockId == 4000 ? "億(新台幣)" : "張", 
					0, graphD.y + start_quan + height_quan + char_height / 2);
		}
    }
    
    private int numToAxis (double time) {
    	numToTime(time);
    	return graphD.x + line_cap + (int) Math.round(((
    			Integer.parseInt(time_mark[0]) - 9) * 60 + Integer.parseInt(time_mark[1])) * scale);
    }
    
    private void drawCanvas (Graphics2D page) {
        page.setPaint(Color.BLUE);
        page.setStroke(new BasicStroke(line_cap, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND));
        page.draw(new Line2D.Double(graphD.x, graphD.y, graphD.x, graphD.y + graphD.height));	// 畫左框
        page.draw(new Line2D.Double(graphD.x + graphD.width, graphD.y, graphD.x + graphD.width, graphD.y + graphD.height));	// 畫右框
        page.draw(new Line2D.Double(graphD.x, graphD.y, graphD.x + graphD.width, graphD.y)); 	// 畫上框
        page.draw(new Line2D.Double(graphD.x, graphD.y + graphD.height, graphD.x + graphD.width, graphD.y + graphD.height)); 	// 畫下框
        page.setStroke(new BasicStroke(1, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND));
    }
    
    // 由數值計算座標
    private int calculateAxis (double data, int calType) {	// 0:指數 1:成交量
    	if (data < 0)
    		if (calType == 0)
    			return (int) Math.round((height_main) / (priceNum - 1) * -1.0 * (data + 1) + 
    					graphD.y + start_main + char_height / 2);
    		else
        		return (int) Math.round(height_quan / (quanNum - 1) * -1.0 * (data + 1) + 
        				graphD.y + start_quan + char_height / 2);
    	if (calType == 0) {
   			ratio = (priceInitial[1] - data) / priceSep;
        	return (int) Math.round((height_main) / (priceNum - 1) * 1.0 * ratio + graphD.y + start_main);
    	}
    	else {
       		ratio = (quanInitial[1] - data) / quanSep;
        	return (int) Math.round(height_quan / (quanNum - 1) * ratio + graphD.y + start_quan);
    	}
    }
    
    // 計算升降單位
    private static double calculateUnit (double data) {
    	double unit;
    	
    	if (data < 10.0)
    		unit = 0.01;
    	else if (data < 50.0)
    		unit = 0.05;
    	else if (data < 100.0)
    		unit = 0.1;
    	else if (data < 500.0)
    		unit = 0.5;
    	else if (data < 1000.0)
    		unit = 1.0;
    	else
    		unit = 5.0;
    	
    	return unit;
    }
    
    // 產生時間格式
    private String numToTime (double time) {
    	time_mark[0] = String.valueOf((int) time);
    	if (time_mark[0].length() == 5) {
    		time_mark[1] = time_mark[0].substring(1, 3);
    		time_mark[0] = time_mark[0].substring(0, 1);
    	} else {
    		time_mark[1] = time_mark[0].substring(2, 4);
    		time_mark[0] = time_mark[0].substring(0, 2);
    	}
    	
    	return time_mark[0].concat(":").concat(time_mark[1]);
    }
    
    public void destroy() {
    	;
    }
}
