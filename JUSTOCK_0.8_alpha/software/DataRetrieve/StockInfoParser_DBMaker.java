package software.DataRetrieve;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import software.DB;
import software.Routine;

public class StockInfoParser_DBMaker extends HTMLEditorKit.ParserCallback implements Job
{
    private boolean inHeader;
    private HTML.Tag tag;
    private boolean start;
    private byte count; // 檔案行數指標
    private String stockId = "";
    private String [] info;
    private Scanner scan;
    private ArrayList<String[]> history;
    private int s_date;
    private int e_date;
    private int count_sql;
    private int skip_line;
    
    // 建構式
    public StockInfoParser_DBMaker(HTML.Tag tag) 
    {
        this.tag = tag;
        
        inHeader = start = false;
        history = new ArrayList<String[]>();
        info = new String[6];	// 0.時間、1.開盤、2.最高、3.最低、4.收盤、5.成交量
        count = 0;
        s_date = 2006;
        e_date = 2100;
        count_sql = 0;
        skip_line = 0;
    }
    
    public void handleText(char[] text, int position) 
    {
        if(inHeader) 
        {
            String content = String.valueOf(text);
            
            if (!start && content.matches("[0-9][0-9][0-9][0-9]/[0-9][0-9]/[0-9][0-9]")) {
            	content = content.replace("/", "");
                start = true;
            }
            
            if (start) {
            	info[count] = content;
            	count++;
            	
            	if (count >= 6) {
            		history.add(info.clone());
            		start = false;
            		count = 0;
            	}
            }
        }        
    }
    
    public void handleStartTag(HTML.Tag tag, MutableAttributeSet attributes, int position) 
    {
        if(this.tag == tag) 
	{
            this.inHeader = true;
        }
    }
 
    public void handleEndTag(HTML.Tag tag, int position) 
    {
        if(this.tag == tag) 
        {
            inHeader = false;
        }
    }
    
    private void writeToDB(Object data)
    {
    	Connection conn = null;
    	Statement stmt = null;
    	ResultSet result = null;
        info = (String[]) data;
        
        try {
        	conn = DB.db.getConnection();
        	stmt = conn.createStatement();
            result = stmt.executeQuery("SELECT hSid, hDate FROM info_history WHERE hSid='" + stockId + 
            		"' AND hDate=" + Integer.parseInt(info[0]));
            if (!result.next()) {
	            stmt.addBatch("INSERT INTO info_history VALUES('" + stockId + 
	                    "', " + Integer.parseInt(info[0]) + ", " + 
	                    Double.parseDouble(info[1]) + ", " + Double.parseDouble(info[2]) + ", " + 
	                    Double.parseDouble(info[3]) + ", " + Double.parseDouble(info[4]) + ", " + 
	                    Integer.parseInt(info[5]) + ")");
	            
	            count_sql++;
            }
            
            if (history.size() <= 0 && count_sql > 0) {
            	stmt.executeBatch();
            	count_sql = 0;
            }
        } catch (SQLException e) {
            System.err.println(e);
		} finally {
			try {
				if (result != null)
					result.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					DB.db.closeConnection(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
    }
    
    private void readData(int skip) {
        File tmpfile;
        
        tmpfile = new File("common/source/stockId.all");
        try {
            scan = new Scanner(tmpfile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        
        for (int i = 0; i < skip; i++)
        	scan.nextLine();
    }
    
    public void execute(JobExecutionContext context) throws JobExecutionException {
    	String jobName = context.getJobDetail().getFullName();
    	
    	Date date;
    	String thread_time[] = new String[2];
    	
    	date = new Date();
    	thread_time[0] = date.toString();
    	
    	ParserGetter kit = new ParserGetter();
    	StockInfoParser_DBMaker sip = new StockInfoParser_DBMaker(HTML.Tag.TD);
    	HTMLEditorKit.Parser parser = kit.getParser();   
    	HTMLEditorKit.ParserCallback callback = sip;
    	
    	Routine.log.info("StockInfoParser_DBMaker says: " + jobName + " executing at " + new Date());
    	
        sip.readData(sip.skip_line);
        
        while (sip.scan.hasNext()) {
            sip.stockId = sip.scan.nextLine().split(" ")[0];
            if (sip.stockId.startsWith("#"))
                continue;
            System.out.print(sip.stockId + ": ");
            
            try {	
                URL u = new URL("http://www.dbmaker.com.tw/stock/cgihistory.cgi?id=" + sip.stockId + 
                		"&begin=" + sip.s_date + "&end=" + sip.e_date);
                InputStream in = u.openStream();
                InputStreamReader reader = new InputStreamReader(in);
                parser.parse(reader, callback, true);
            } catch (IOException e) {
                System.err.println(e);
            }
            
            System.out.print(sip.history.size());
            if (sip.history.size() > 0) {
            	while (sip.history.size() > 0) {
            		sip.writeToDB(sip.history.remove(0));
//            		System.out.println(((String[])history.remove(0))[0]);
            	}
                System.out.println("已完成");
            }
            else
                System.out.println("略過");
        }

        date = new Date();
        thread_time[1] = date.toString();
        
    	System.out.println("開始時間：" + thread_time[0] + "\n結束時間：" + thread_time[1]);
    }
    
    public static void main(String[] args) 
    {
    	Date date;
    	String thread_time[] = new String[2];
    	
    	date = new Date();
    	thread_time[0] = date.toString();
    	
    	ParserGetter kit = new ParserGetter();
    	StockInfoParser_DBMaker sip = new StockInfoParser_DBMaker(HTML.Tag.TD);
    	HTMLEditorKit.Parser parser = kit.getParser();   
    	HTMLEditorKit.ParserCallback callback = sip;
    	
        sip.readData(sip.skip_line);
        
        while (sip.scan.hasNext()) {
            sip.stockId = sip.scan.nextLine().split(" ")[0];
            if (sip.stockId.startsWith("#"))
                continue;
            System.out.print(sip.stockId + ": ");
            
            try {	
                URL u = new URL("http://www.dbmaker.com.tw/stock/cgihistory.cgi?id=" + sip.stockId + 
                		"&begin=" + sip.s_date + "&end=" + sip.e_date);
                InputStream in = u.openStream();
                InputStreamReader reader = new InputStreamReader(in);
                parser.parse(reader, callback, true);
            } catch (IOException e) {
                System.err.println(e);
            }
            
            System.out.print(sip.history.size());
            if (sip.history.size() > 0) {
            	while (sip.history.size() > 0) {
            		sip.writeToDB(sip.history.remove(0));
//            		System.out.println(((String[])history.remove(0))[0]);
            	}
                System.out.println("已完成");
            }
            else
                System.out.println("略過");

            try {
                Thread.sleep(0);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }

        date = new Date();
        thread_time[1] = date.toString();
        
    	System.out.println("開始時間：" + thread_time[0] + "\n結束時間：" + thread_time[1]);
    }
}
