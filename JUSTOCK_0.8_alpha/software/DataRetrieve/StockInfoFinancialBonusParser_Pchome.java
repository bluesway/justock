/*
*      author by Kevingo
*      Data @ 2007/09/10
*      抓取股利狀況分析資料  
*      
*/

package software.DataRetrieve;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.swing.JOptionPane;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;

public class StockInfoFinancialBonusParser_Pchome extends HTMLEditorKit.ParserCallback
{
    private boolean inHeader = false;
    private HTML.Tag tag;
    private int count,i,j, endCount = 0;
    private String stockId;
    public static int ROW = 20;
    public static int COLUMN = 9;
    private Object [][] data = new Object[ROW][COLUMN];
    
    // 建構式
    public StockInfoFinancialBonusParser_Pchome() {      
    	setTag(HTML.Tag.TD);
    }
    
    public void retrieveData (StockInfoFinancialBonusParser_Pchome sfy, String stockId, boolean READY) {
    	try{	
	    		this.stockId = stockId;
    			if(READY)
	    		{
	    			ParserGetter kit = new ParserGetter();
	    			HTMLEditorKit.Parser parser = kit.getParser();
	    			HTMLEditorKit.ParserCallback callback = sfy;
	    			URL u = new URL("http://pchome.syspower.com.tw/stock/?stock_no=" + stockId + "&item=12");
	    			InputStream in = u.openStream();
	    			InputStreamReader reader = new InputStreamReader(in);
	    			parser.parse(reader, callback, true);  
	    		}    		
    	} catch (IOException e) {
    			JOptionPane.showMessageDialog(null, "查無資料！請重新選擇");
    	}
    }
    
    public void setTag(HTML.Tag tag) {
    	this.tag = tag;
    }
    
    public void handleText(char[] text, int position) {
        if(inHeader) {
            String tmp = String.valueOf(text);  
            count++;
            if(count>72) {
    				i++;
    				setData(tmp, i);
    			}
        }
        endCount = count;
    }
    
    public void handleStartTag(HTML.Tag tag, MutableAttributeSet attributes, int position) {
        if(this.tag == tag) {
            this.inHeader = true;
        }
    }

    public void handleEndTag(HTML.Tag tag, int position) {
        if(this.tag == tag) {
            inHeader = false;
        }
    }
    
    public void setData(String tmp,int count) {
    	if(count%9 == 1 && count!=1)
    		j++;
    	if(count > 180)
    		;
    	else
    		data[j][(count-(9*j))-1] = tmp;
    }
    
    public Object[][] getData() {
    	return data;
    }
    
    public int getEndCount() {
    	return endCount;
    }
    
    public String getStockID() {
    	return stockId;
    }
}