/*
*      author by Kevingo
*      Data @ 2007/11/20
*      抓取當日上市漲幅排行
*      
*/

package software.DataRetrieve;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.swing.JOptionPane;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;

public class StockInfoPriceUp extends HTMLEditorKit.ParserCallback
{
    private boolean inHeader = false;
    private HTML.Tag tag;
    private int count,i,j, endCount = 0;
    private String stockId;
    public static int ROW = 30;
    public static int COLUMN = 10;
    private Object [][] data = new Object[ROW][COLUMN];
    
    // 建構式
    public StockInfoPriceUp() {      
    	setTag(HTML.Tag.TD);
    }
    
    public void retrieveData (StockInfoPriceUp sfy,boolean READY, int page) {
    	try{	
    			if(READY) {
	    			ParserGetter kit = new ParserGetter();
	    			HTMLEditorKit.Parser parser = kit.getParser();
	    			HTMLEditorKit.ParserCallback callback = sfy;
	        		if(READY && page == 1) {
		    			URL u = new URL("http://pchome.syspower.com.tw/top/?tops=0-4-0");
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 2) {
		    			URL u = new URL("http://pchome.syspower.com.tw/top/?tops=0-4-1");
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 3) {
		    			URL u = new URL("http://pchome.syspower.com.tw/top/?tops=0-4-2");
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		}
	    		}    		
    	} catch (IOException e) {
    			JOptionPane.showMessageDialog(null, "查無資料！請重新選擇");
    	}
    }
    
    public void setTag(HTML.Tag tag) {
    	this.tag = tag;
    }
    
    public void handleText(char[] text, int position) {
        if(inHeader) {
            String tmp = String.valueOf(text);       
            count++;
			if(count > 24 && count < 325) {
				i++;
				setData(tmp, i);
			}
        }
        endCount = count;
    }
    
    public void handleStartTag(HTML.Tag tag, MutableAttributeSet attributes, int position) {
        if(this.tag == tag) {
            this.inHeader = true;
        }
    }

    public void handleEndTag(HTML.Tag tag, int position) {
        if(this.tag == tag) {
            inHeader = false;
        }
    }
    
    public void setData(String tmp,int count) {
    	if(count%10 == 1 && count!=1)
    		j++;
    	data[j][(count-(10*j))-1] = tmp;
    }
    
    public Object[][] getData() {
    	return data;
    }
    
    public int getEndCount() {
    	return endCount;
    }
    
    public String getStockID() {
    	return stockId;
    }
}