/*
*      author by Kevingo
*      Data @ 1/24
*      modify by bluesway
*      Update @ 7/6
*      For testing HTML Retrieve
*/
package software.DataRetrieve;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;

import software.DB;
    
public class StockInfoParser_Yahoo extends HTMLEditorKit.ParserCallback 
{
    private boolean inHeader = false;
    private HTML.Tag tag;
    private boolean start = false;
    private boolean stockName = false;
    private static boolean write = false;
    private byte count = 0;
    private byte count_info = 0;
    private static String stockId = "";
    private static String [] info = new String[8]; // 0.時間、1.成交、2.漲跌、3.張數、4.開盤、5.最高、6.最低、7.個股名稱
    private static Scanner scan;
    
    // 建構式
    public StockInfoParser_Yahoo(HTML.Tag tag) 
    {
        this.tag = tag;
    }
    
    public void handleText(char[] text, int position) 
    {
        if(inHeader) 
	{
            String content = String.valueOf(text);
            
            if (!stockName && content.startsWith("個股資料")) {
                stockName = true;
                return;
            }
            
            if (!start && content.startsWith("加到投資組合")) {
                start = true;
                write = true;
                return;
            }
            else if (start && content.startsWith("成交明細")) {
                start = false;
                return;
            }
            
            if (start) {
                count ++;
                switch(count) {
                    case 3:
                    case 4:
                    case 7:
                        break;
                    default:
                        if (count_info == 2) {
                            content = content.replaceAll("[△▲]", "");
                            content = content.replaceAll("[▽▼]", "-");
                        }
                        if (count_info == 3)
                            content = content.replace(",", "");
                        info[count_info] = content;
                        
                        if (count_info < 6)
                        count_info++;
                        break;
                }
            }
            
            if (stockName) {
                content = content.replace(String.valueOf(stockId), "");
                info[7] = content;
                System.out.print(info[7]);
                stockName = false;
            }
        }        
    }
    
    public void handleStartTag(HTML.Tag tag, MutableAttributeSet attributes, int position) 
    {
        if(this.tag == tag) 
	{
            this.inHeader = true;
        }
    }
 
    public void handleEndTag(HTML.Tag tag, int position) 
    {
        if(this.tag == tag) 
        {
            inHeader = false;
        }
    }
    
    private void writeToDB()
    {
    	Connection conn = null;
    	Statement stmt = null;
    	ResultSet result = null;
        try {
        	conn = DB.db.getConnection();
        	stmt = conn.createStatement();
        	
            result = stmt.executeQuery("SELECT * FROM stock WHERE Sid='" + stockId + "'");
            if (!result.next())
                stmt.addBatch("INSERT INTO stock VALUES('" + stockId + "', '" + info[7] + "')");
            
            result = stmt.executeQuery("SELECT * FROM info_current WHERE cSid='" + stockId + "'");
            if (result.next())
                stmt.addBatch("UPDATE info_current SET cHigh=" + Double.parseDouble(info[5]) + ", cLow=" + 
                        Double.parseDouble(info[6]) + ", cChg=" + Double.parseDouble(info[2]) + " WHERE cSid='" + stockId + "'");
            else
                stmt.addBatch("INSERT INTO info_current VALUES('" + stockId + "', " + 
                        Double.parseDouble(info[5]) + ", " + Double.parseDouble(info[6]) + ", " + Double.parseDouble(info[2]) + 
                        ", " + Double.parseDouble(info[4]) + ")");
            
            result = stmt.executeQuery("SELECT * FROM info_today WHERE tSid='" + stockId + "' AND tTime='" + info[0] + "'");
            if (result.next())
                stmt.addBatch("UPDATE info_today SET tPrice=" + Double.parseDouble(info[1]) + ", tQuan=" + 
                        Integer.parseInt(info[3]) + " WHERE tSid='" + stockId + "' AND tTime='" + info[0] + "'");
            else
                stmt.addBatch("INSERT INTO info_today VALUES('" + stockId + "', '" + info[0] + "', " + 
                        Double.parseDouble(info[1]) + ", " + Integer.parseInt(info[3]) + ")");
        } catch (SQLException e) {
            System.err.println(e);
		} finally {
			try {
				if (result != null)
					result.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					DB.db.closeConnection(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
    }
    
    private void readData() {
        File tmpfile;
        
        tmpfile = new File("common/source/stockId.list");
        try {
            scan = new Scanner(tmpfile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) 
    {
    	StockInfoParser_Yahoo yahoo = new StockInfoParser_Yahoo(HTML.Tag.TD);

    	yahoo.readData();
        
        while (scan.hasNext()) {
            stockId = scan.nextLine().split(" ")[0];
            if (stockId.startsWith("#"))
                continue;
            System.out.print(stockId + ": ");
            
            try {	
                ParserGetter kit = new ParserGetter();
                HTMLEditorKit.Parser parser = kit.getParser();   
                HTMLEditorKit.ParserCallback callback = yahoo;	

                URL u = new URL("http://tw.stock.yahoo.com/q/q?s=" + stockId);                
                InputStream in = u.openStream();
                InputStreamReader reader = new InputStreamReader(in);
                parser.parse(reader, callback, true);
            } catch (IOException e) {
                System.err.println(e);
            }
            
            if (write) {
                yahoo.writeToDB();
                write = false;
                System.out.println("已完成");
            }
            else
                System.out.println("略過");

            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
