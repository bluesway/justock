/*
 *      author by Kevingo
 *      Data @ 2007/09/03
 *      抓取當日成交金額排行資料  
 *      
 */

package software.DataRetrieve;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.text.html.HTMLEditorKit;
import software.DB;

public class StockInfoSeperateStock extends HTMLEditorKit.ParserCallback {
	private int j = 0, endCount = 0;

	private String stockId;

	public int row;

	public int column;

	public int index;

	String id;

	String[][] queryData;

	// 建構式
	public StockInfoSeperateStock() {
		index = 1;
	}

	public void retrieveData(StockInfoSeperateStock sfy, boolean READY, int page) throws SQLException {
		Connection conn = null;
		ResultSet result = null;
		
		if (READY) {
			try {
				conn = DB.db.getConnection();
				result = conn.createStatement().executeQuery(
						"SELECT * from stock s, info_current i  where s.class ="
								+ page + " AND " + "s.sid=i.cSid ORDER BY s.sid ASC");
				if (result.last())
					row = result.getRow();
				//        	        	System.out.println("Row:" + row);
				result.beforeFirst();
				column = result.getMetaData().getColumnCount();

				queryData = new String[row][column - 2];

				while (result.next()) {
					j++;
					for (int i = 1; i <= column; i++) {
						if (i == column) {
							queryData[j - 1][0] = result.getString("Sid");
							queryData[j - 1][1] = result.getString("Sname");
							queryData[j - 1][2] = result.getString("cSid");
							queryData[j - 1][3] = result.getString("cHigh");
							queryData[j - 1][4] = result.getString("cLow");
							queryData[j - 1][5] = result.getString("cChg");
							queryData[j - 1][6] = result.getString("cOpen");
							//                					queryData[j-1][7] = result.getString("cQuan");
						}
					}
				}
			} catch (SQLException e) {
				System.err.println(e);
			} finally {
				try {
					if (result != null)
						result.close();
					if (conn != null)
						DB.db.closeConnection(conn);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			result.close();
		}
	}

	public Object[][] getData() {
		return queryData;
	}

	public int getEndCount() {
		return endCount;
	}

	public String getStockID() {
		return stockId;
	}

	public int getRow() {
		return row;
	}

	public int getColumn() {
		return column;
	}
}
