package software.Subitem.Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import javax.swing.JComboBox;

import software.DB;
import software.GUI;
import software.Subitem.GeneralGraph.stockAction;
import common.util.StockBox;

public class StockBoxListener implements ActionListener, ItemListener, MouseListener, KeyListener {
	int sid;
	
    public void itemStateChanged(ItemEvent ie) {
    	int type = -1;
    	String cname;
    	Connection conn = null;
    	Statement stmt = null;
    	ResultSet result = null;
    	
    	try {
    		conn = DB.db.getConnection();
			stmt = conn.createStatement();
			
	    	if (ie.getStateChange() == ItemEvent.SELECTED) {
	    		if (String.valueOf(ie.getItem()).equals("上市")) {
	    			GUI.gui.classBox.removeAllItems();
	    			result = stmt.executeQuery("SELECT cName FROM stock_class WHERE cId < 100 ORDER BY cId ASC");
	    			while (result.next()) {
	    				GUI.gui.classBox.addItem(result.getString("cName"));
	    			}
	    		} else if (String.valueOf(ie.getItem()).equals("上櫃")) {
	    			GUI.gui.classBox.removeAllItems();
	    			result = stmt.executeQuery("SELECT cName FROM stock_class WHERE cId >= 100 ORDER BY cId ASC");
	    			while (result.next()) {
	    				GUI.gui.classBox.addItem(result.getString("cName"));
	    			}
	    		} else {
	    			if (String.valueOf(GUI.gui.typeBox.getSelectedItem()).equals("上市"))
	    				type = 0;
	    			else
	    				type = 100;
	    			
	    			cname = String.valueOf(ie.getItem());
	    			
	    			GUI.gui.stockBox.removeAllItems();
	    			result = stmt.executeQuery("SELECT s.Sid, s.Sname FROM stock s, stock_class c WHERE c.cId >= " + type + " AND c.cName LIKE '" + cname + "' AND c.cId = s.class");
	    			while (result.next()) {
	    				GUI.gui.stockBox.addItem(result.getString("s.Sid").concat(" ").concat(result.getString("s.Sname")));
	    			}
	    		}
	    	}
		} catch (SQLException e1) {
			e1.printStackTrace();
		} finally {
			try {
				if (result != null)
					result.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					DB.db.closeConnection(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
    }
    
    public void actionPerformed(ActionEvent ae) {
    	sid = StockBox.getStockId(GUI.gui.stockBox);
    	if (sid != -1) {
    		GUI.gui.setStockId(sid);
    		GUI.gui.repaint();
			GUI.gui.createFrame(GUI.gui.stockBox.getActionCommand());
    	}
    }

	public void mouseClicked(MouseEvent me) {
    	System.out.println("滑鼠");
		sid = StockBox.getStockId(GUI.gui.stockBox);
		if (sid != -1) {
			GUI.gui.stockId = sid;
			GUI.gui.repaint();
			GUI.gui.createFrame(GUI.gui.stockBox.getActionCommand());
		}
	}
	
	public void mouseEntered(MouseEvent me) {}
	public void mouseExited(MouseEvent me) {}
	public void mousePressed(MouseEvent me) {}
	public void mouseReleased(MouseEvent me) {}

	public void keyPressed(KeyEvent ke) {
		sid = StockBox.getStockId(GUI.gui.stockBox);
		if (sid != -1) {
			GUI.gui.stockId = sid;
			GUI.gui.repaint();
			GUI.gui.createFrame(GUI.gui.stockBox.getActionCommand());
		}
		
//		GUI.gui.stockBox.requestFocusInWindow();
	}
	
	public void keyReleased(KeyEvent ke) {}
	public void keyTyped(KeyEvent ke) {}
}