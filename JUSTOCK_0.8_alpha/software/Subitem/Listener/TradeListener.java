/**
 * @author bluesway
 * TARGET 交易類別動作監聽程式
 */

package software.Subitem.Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import software.GUI;

public class TradeListener implements ActionListener {
	public void actionPerformed(ActionEvent e) {
		GUI.trade.readData(Integer.parseInt(e.getActionCommand()));
		GUI.trade.passField.requestFocusInWindow();
	}
}