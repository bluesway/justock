package software.Subitem;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import software.DB;

public class UserDataInJSP
{
    String id, pwd = "";
	
	public void setUserInfo(String id, String pwd) {
    	this.id = id;
    	this.pwd = pwd;
    	getUserData(id, pwd);
    }
	
	public static String[] getUserData(String userId, String passwd) {
		Connection conn = null;
		Statement stmt = null;
    	ResultSet result = null;
    	String[] userData = new String[9];
    	
        try {
        	conn = DB.db.getConnection();
        	stmt = conn.createStatement();
        	result = stmt.executeQuery("SELECT * from account where Uid='" + userId + "'");
        	String encyptPasswd;
        	encyptPasswd = common.util.PwdMD5.encrypt(passwd);
        	
        	if(result.next() && result.getString("uPwd").equals(encyptPasswd)) // �b���B�K�X���T
        		for (int i = 0; i < 9; i++)
        			userData[i] = result.getString(i + 1);
        } catch (SQLException e) {
            System.err.println(e);
		} finally {
			try {
				if (result != null)
					result.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					DB.db.closeConnection(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
		
        return userData;
    }
//    public static void main(String [] args) {
//    	String [] data = new String [9];
//    	data = new UserDataInJSP().getUserData("XDDDDD", "specification");
//    	for(int i=0 ; i<data.length ; i++)
//    		System.out.println(data[i]);
//    }
}