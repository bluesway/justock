/*
*      author by Kevingo
*      Data @ 2007/09/03
*      上市成交價排行資料
*      pchome version
*      
*      TODO 增加所有的排行榜  http://pchome.syspower.com.tw/top/
*/

package software.Subitem.table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import software.DB;
import software.DataRetrieve.StockInfoSeperateStock;

import common.util.MyTableModel_17;

public class SeperateStockInfo implements ActionListener {
	private static final long serialVersionUID = 1L;
	public boolean DONE = false;
	private int TABLE_HEIGHT, TABLE_WIDTH, DATA_ROW, DATA_COLUMN, vColIndex, width, currentPage;
	private Object [][] data;
	private Container container;
	private JPanel panel_Table; // 放置表格的panel_Table
    private JTable table; // 表格
    private JMenuItem [] menuItem;
    private JPopupMenu popupMenu;
    private MyTableModel_17 model; // tableModel
	private StockInfoSeperateStock sss;
	private String [] item = new String [] {
			"水泥",
			"食品",
			"塑膠",
			"紡織",
			"電機",
			"電器電纜",
			"化學",
			"生技醫療",
			"玻璃",
			"造紙",
			"鋼鐵",
			"橡膠",
			"汽車",
			"半導體",
			"電腦週邊",
			"光電",
			"通信網路",
			"電子零件組",
			"電子通路",
			"資訊服務業",
			"其他電子",
			"營建",
			"航運",
			"觀光",
			"金融",
			"貿易百貨",
			"油氣燃料",
			"綜合",
			"其他"
	};
	private String [] columnNames = new String[] {
			"股票代碼",
			"股票名稱",
			"現價",
			"最高點",
			"最低點",
			"漲跌",
			"開盤"
	};
	private String tmpProcess;

    public SeperateStockInfo() {
    	container = new Container();
    	container.setLayout(new BorderLayout());
		
    	try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) {
			e.printStackTrace();
		}
		

    	TABLE_HEIGHT = 550;
    	TABLE_WIDTH = 300;

		DONE = true;
		currentPage = 14;
		sss = new StockInfoSeperateStock();
		try {
			sss.retrieveData(sss, DONE, currentPage);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		data = sss.getData();

		DATA_ROW = sss.getRow();
		DATA_COLUMN = 7;
		
		vColIndex = 0; // 決定column 的 index
    	width = 70; // column index 的寬度
		
    	panel_Table = new JPanel();
    	
    	// 表格
		model = new MyTableModel_17(100, DATA_COLUMN);
		model.setColumnNames(columnNames);
		
    	table = new JTable(model);
		TableCellRenderer renderer = new CustomSeperateCellRender();
		table.setDefaultRenderer( Object.class, renderer );
        table.setColumnSelectionAllowed(true);
        table.clearSelection();
        table.revalidate();
        table.repaint();
    	table.setPreferredScrollableViewportSize(new Dimension(TABLE_HEIGHT, TABLE_WIDTH));
    	table.setFillsViewportHeight(true);   
    	table.setRowSelectionAllowed(true);
    	table.setColumnSelectionAllowed(true);
        model.addTableModelListener(new TableModelListener(){
            public void tableChanged(TableModelEvent e) {
                table.updateUI();
            }});
        
        // 設定column的寬度
        TableColumn col = table.getColumnModel().getColumn(vColIndex);
        col.setPreferredWidth(width);
        
    	for(int k=0 ; k<DATA_ROW ; k++) {
    		for(int j=0 ; j<DATA_COLUMN ; j++) {
    			tmpProcess = String.valueOf(data[k][j]);
    			if(!tmpProcess.startsWith("-") &&  j==5) {
    				tmpProcess = "▲ " + tmpProcess.substring(0, tmpProcess.length());
    				table.setSelectionForeground(Color.YELLOW);
    			}
    			else if(tmpProcess.startsWith("-") && tmpProcess.length() != 1)
    				tmpProcess = "▼ " + tmpProcess.substring(0, tmpProcess.length());
    			else if(tmpProcess == "null")
    				tmpProcess = "";

    			model.setValueAt(tmpProcess, k, j);
    		}		
    	}
        model.fireTableDataChanged(); 

        // 產生彈出式選單
        createPopupMenu();
        
        // 拉動式捲軸 和 主要Frame
    	JScrollPane scrollPane = new JScrollPane(table);
        
        RowHeaderer rowHeader= new RowHeaderer(table);
        scrollPane.setRowHeaderView(rowHeader);
        scrollPane.getRowHeader().setPreferredSize(new Dimension(35,100));
        
        table.setBackground(Color.BLACK);
        table.setForeground(Color.WHITE);
        table.setFont(new Font("標楷體", Font.PLAIN, 14));
        container.add(panel_Table, BorderLayout.NORTH);
        container.add(scrollPane, BorderLayout.WEST);
        container.setVisible(true);
    }
    
    // 處理快速鍵的問題   enter 確定 esc 取消
    public void enterPressesWhenFocused(JComponent object) {
    		object.registerKeyboardAction(
    				object.getActionForKeyStroke(
    						KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)), 
    						KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false), 
    						JComponent.WHEN_IN_FOCUSED_WINDOW);
    		
    		object.registerKeyboardAction(
    				object.getActionForKeyStroke(
    						KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)), 
    						KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), 
    						JComponent.WHEN_IN_FOCUSED_WINDOW);
    }
    
    public Container getFrame() {
    	return container;
    }
    
    // popupMenu 呈現資料
    public void createPopupMenu() {
        popupMenu = new JPopupMenu();
    	menuItem = new JMenuItem[29];
    	for(int i=0 ; i<item.length ; i++) {
    		menuItem[i] = new JMenuItem(item[i]);
    		menuItem[i].addActionListener(this);
    		popupMenu.add(menuItem[i]);
    	}
    	MouseListener popupListener = new PopupListener(popupMenu);
    	table.addMouseListener(popupListener);
    }

	public void actionPerformed(ActionEvent ae) {
		sss = new StockInfoSeperateStock();

		for (int i = 0; i < item.length; i++) {
			if (ae.getActionCommand() == item[i]) {
				try {
					sss.retrieveData(sss, DONE, i);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				currentPage = i;
			}
		}

		
		DATA_ROW = sss.getRow();
		DATA_COLUMN = 7;
    	data = sss.getData();
    	
    	// 表格
		model = new MyTableModel_17(100, DATA_COLUMN);
		model.setColumnNames(columnNames);
        
		for (int i = 0; i < DATA_ROW; i++)
			for (int j = 0; j < DATA_COLUMN; j++) {
				tmpProcess = String.valueOf(data[i][j]);
    			if(!tmpProcess.startsWith("-") &&  j==5) {
    				tmpProcess = "▲ " + tmpProcess.substring(0, tmpProcess.length());
    				table.setSelectionForeground(Color.YELLOW);
    			}
    			else if(tmpProcess.startsWith("-") && tmpProcess.length() != 1)
    				tmpProcess = "▼ " + tmpProcess.substring(0, tmpProcess.length());
    			else if(tmpProcess == "null")
    				tmpProcess = "";

    			model.setValueAt(tmpProcess, i, j);
			}

		table.repaint();
		model.fireTableDataChanged();
	} 
	
	public String [] returnTypeOfStock() throws SQLException {
		Connection conn = null;
		ResultSet result = null;
		String [] tmp = new String[54];
		int row=0,column=0;
		
		try{
			conn = DB.db.getConnection();
			result = conn.createStatement().executeQuery("SELECT * from stock_class");
			if(result.last())
        		row = result.getRow();
        	
        	result.beforeFirst();
        	column = result.getMetaData().getColumnCount();
			while(result.next()) {
				int j=0;
				for(int k=1 ;k<=column ;k++) {
	    			if(k == column) {
	    				tmp[k-1] = result.getString("cName");
	    			}
	    		}
			}
		}catch(SQLException e) {
			System.err.println(e);
		} finally {
			try {
				if (result != null)
					result.close();
				if (conn != null)
					DB.db.closeConnection(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		result.close();
		return tmp;
	}
}