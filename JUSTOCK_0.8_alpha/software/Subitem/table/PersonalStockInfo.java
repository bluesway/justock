package software.Subitem.table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import software.GUI;
import software.DataRetrieve.StockInfoAddPersonalInfo;

import common.util.MyTableModel_16;

public class PersonalStockInfo extends JFrame {
	private static final long serialVersionUID = 1L;

	// 表格變數
	public boolean DONE = false;
	private int TABLE_HEIGHT, TABLE_WIDTH, DATA_ROW, DATA_COLUMN, vColIndex, width;
	private Vector<String[]> data;
	public Container container;
	private JPanel paint1_Panel,btn_Panel; // 放置表格的panel_Table
    private JButton delBtn, clearAllBtn; // 確定 取消 清除全部按鈕
    private JTable table; // 表格
    private MyTableModel_16 model; // tableModel
    
	// 圖形變數
//	private MainGraph mg;
	
	public PersonalStockInfo() {

		setTitle( "自選股看盤" );
		setResizable(false);
		GUI.gui.siap = new StockInfoAddPersonalInfo();
		createPage1();
	}	

	// 顯示自選股 報價
	public void createPage1() {		
		container = new Container();
		container.setLayout(new BorderLayout());
		
		DATA_ROW = GUI.gui.siap.maxData;
		DATA_COLUMN = 8;
		data = new Vector<String[]>();
		String [] columnNames = new String[] {
					"股票代碼",
					"股票名稱",
					"現價",
					"最高點",
					"最低點",
					"漲跌幅",
					"開盤價",
					"總成交量"
			};
		
			try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch(Exception e) {
				e.printStackTrace();
			}	
	
			vColIndex = 0; // 決定column 的 index
	    	width = 80; // column index 的寬度
			
	    	TABLE_HEIGHT = 500;
	    	TABLE_WIDTH = 500;
	    	
	    	paint1_Panel = new JPanel(new GridLayout(1,3));
			btn_Panel = new JPanel();
	    	
	    	// 表格
			model = new MyTableModel_16(DATA_ROW, DATA_COLUMN);
			model.setColumnNames(columnNames);
			
	    	table = new JTable(model);
			TableCellRenderer renderer = new CustomTableCellOfWisdom();
			table.setDefaultRenderer(Object.class, renderer );
	        table.setColumnSelectionAllowed(true);
	        table.clearSelection();
	        table.revalidate();
	        table.repaint();
	    	table.setBackground(Color.BLACK);
	    	table.setForeground(Color.white);
	    	table.setFont(new Font("微軟正黑體",
	    	        		Font.BOLD,
	    	        		12));
	    	table.setPreferredScrollableViewportSize(new Dimension(TABLE_HEIGHT, TABLE_WIDTH));
	    	table.setFillsViewportHeight(true);   
	    	table.setRowSelectionAllowed(true);
	    	table.setColumnSelectionAllowed(true);
	        model.addTableModelListener(new TableModelListener(){
	            public void tableChanged(TableModelEvent e) {
	                table.updateUI();
	            }});
	        
	        // 設定column的寬度
	        TableColumn col = table.getColumnModel().getColumn(vColIndex);
	        col.setPreferredWidth(width);

	        updateData();
	        
			delBtn = new JButton("刪除資料");
			delBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if(table.getSelectedRow() == -1)
						;
					else {
						GUI.gui.siap.delData(table.getSelectedRow());
						updateData();
					}
				}
			});
			btn_Panel.add(delBtn);
			
			clearAllBtn = new JButton("清除全部自選股");
			clearAllBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					GUI.gui.siap.delFile();

				}
			});
			btn_Panel.add(clearAllBtn);
	        
	        // 拉動式捲軸 和 主要Frame
	    	JScrollPane scrollPane = new JScrollPane(table);
	        RowHeaderer rowHeader= new RowHeaderer(table);
	        scrollPane.setRowHeaderView(rowHeader);
	        scrollPane.getRowHeader().setPreferredSize(new Dimension(35,100));
	        
	        table.setBackground(Color.BLACK);
	        table.setForeground(Color.WHITE);
	        
	        container.add(btn_Panel, BorderLayout.SOUTH);
	        container.add(paint1_Panel, BorderLayout.EAST);
	        container.add(scrollPane, BorderLayout.WEST);
	        container.setVisible(true);
	}	
	
	public void updateData() {
        data = GUI.gui.siap.getData();
    	for(int k = 0; k < DATA_ROW; k++)
    		if (k < data.size())
    			for(int j = 0; j < DATA_COLUMN; j++)
        			model.setValueAt(data.get(k)[j], k, j);
    		else
    			for(int j = 0; j < DATA_COLUMN; j++)
        			model.setValueAt("", k, j);
    	
    	 model.fireTableDataChanged();
	}
	
    public Container getFrame() {
		return container;
    }
}