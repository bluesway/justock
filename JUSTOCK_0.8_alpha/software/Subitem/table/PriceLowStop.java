/*
*      author by Kevingo
*      Data @ 2007/09/10
*      顯示上市跌幅排行資料
*      
*/

package software.Subitem.table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseListener;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import software.DataRetrieve.StockInfoPriceLowParser_Pchome;
import software.DataRetrieve.StockInfoPriceLowStop;

import common.util.MyTableModel_22;

public class PriceLowStop {
	private static final long serialVersionUID = 1L;
	public boolean DONE = false;
	private int TABLE_HEIGHT, TABLE_WIDTH, DATA_ROW, DATA_COLUMN, vColIndex, width, currentPage;
	private Object [][] data;
	private Container container;
	private JPanel panel_Table,hint_Panel; // 放置表格的panel_Table
    private JTable table; // 表格
    private JMenuItem menuItem_1, menuItem_2;
    private JPopupMenu popupMenu;
    private MyTableModel_22 model; // tableModel
	private StockInfoPriceLowStop sips;
	private String [] columnNames = new String[] {
			"排行",
			"股票",
			"成交",
			"漲跌",
			"漲跌幅度",
			"最高價",
			"最低價",
			"成交量(張)",
			"成交金額(百萬)"
	};
	private String tmpProcess;
	private JLabel hintLabel;

    public PriceLowStop() {
    	container = new Container();
    	container.setLayout(new BorderLayout());
    	
    	try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) {
			e.printStackTrace();
		}
		
    	hint_Panel = new JPanel();
    	hintLabel = new JLabel("* 使用滑鼠右鍵會有更多功能喔！");
    	hint_Panel.add(hintLabel);
		
		DATA_ROW = StockInfoPriceLowParser_Pchome.ROW;
		DATA_COLUMN = StockInfoPriceLowParser_Pchome.COLUMN;
		
		vColIndex = 0; // 決定column 的 index
    	width = 30; // column index 的寬度
		
    	panel_Table = new JPanel();
    	data = new Object[DATA_ROW][DATA_COLUMN];
    	
    	TABLE_HEIGHT = 800;
    	TABLE_WIDTH = 350;
    	
		DONE = true;
		sips = new StockInfoPriceLowStop();
		sips.retrieveData(sips, DONE, 1);
		data = sips.getData();
		currentPage = 1;
    	
    	// 表格
		model = new MyTableModel_22(DATA_ROW, DATA_COLUMN);
		model.setColumnNames(columnNames);
		
    	table = new JTable(model);
		TableCellRenderer renderer = new CustomTableInitialStatus();
		table.setDefaultRenderer( Object.class, renderer );
        table.setColumnSelectionAllowed(true);
        table.clearSelection();
        table.revalidate();
        table.repaint();
    	table.setPreferredScrollableViewportSize(new Dimension(TABLE_HEIGHT, TABLE_WIDTH));
    	table.setFillsViewportHeight(true);   
    	table.setRowSelectionAllowed(true);
    	table.setColumnSelectionAllowed(true);
        model.addTableModelListener(new TableModelListener(){
            public void tableChanged(TableModelEvent e) {
                table.updateUI();
            }});
        
        // 設定column的寬度
        TableColumn col = table.getColumnModel().getColumn(vColIndex);
        col.setPreferredWidth(width);
        
    	for(int k=0 ; k<DATA_ROW ; k++) {
    		for(int j=0 ; j<DATA_COLUMN ; j++) {
    			tmpProcess = String.valueOf(data[k][j]);
    			if(tmpProcess.startsWith("+")) {
    				tmpProcess = "▲ " + tmpProcess.substring(1, tmpProcess.length());
    				table.setSelectionForeground(Color.YELLOW);
    			}
    			else if(tmpProcess.startsWith("-") && tmpProcess.length() != 1)
    				tmpProcess = "▼ " + tmpProcess.substring(1, tmpProcess.length());
    			else if(tmpProcess == "null")
    				tmpProcess = "";

    			model.setValueAt(tmpProcess, k, j);
    		}		
    	}
        model.fireTableDataChanged(); 
        
        // 產生彈出式選單
        createPopupMenu();
        
        // 拉動式捲軸 和 主要Frame
    	JScrollPane scrollPane = new JScrollPane(table);
        
        RowHeaderer rowHeader= new RowHeaderer(table);
        scrollPane.setRowHeaderView(rowHeader);
        scrollPane.getRowHeader().setPreferredSize(new Dimension(35,100));
        
        table.setBackground(Color.BLACK);
        table.setForeground(Color.WHITE);
        table.setFont(new Font("標楷體", Font.PLAIN, 14));
        container.add(panel_Table, BorderLayout.NORTH);
        container.add(hint_Panel, BorderLayout.SOUTH);
        container.add(scrollPane, BorderLayout.WEST);
        container.setVisible(true);
    }

    // 處理快速鍵的問題   enter 確定 esc 取消
    public void enterPressesWhenFocused(JComponent object) {
    		object.registerKeyboardAction(
    				object.getActionForKeyStroke(
    						KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)), 
    						KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false), 
    						JComponent.WHEN_IN_FOCUSED_WINDOW);
    		
    		object.registerKeyboardAction(
    				object.getActionForKeyStroke(
    						KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)), 
    						KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), 
    						JComponent.WHEN_IN_FOCUSED_WINDOW);
    }
    
    public Container getFrame() {
    	return container;
    }
    
    // popupMenu 呈現下30筆和上30筆資料
    public void createPopupMenu() {
    	popupMenu = new JPopupMenu();
    	menuItem_1 = new JMenuItem("下30筆資料");
    	popupMenu.add(menuItem_1);
    	menuItem_1.addActionListener(new ActionListener() {    		 
    		public void actionPerformed(ActionEvent ae) {
    			DONE = true;
				sips = new StockInfoPriceLowStop();
				
				if(currentPage == 1) {
					sips.retrieveData(sips, DONE, 2);
					currentPage = 2;
				}
				else if(currentPage == 2) {
					sips.retrieveData(sips, DONE, 3);
					currentPage = 3;
				} else {
					sips.retrieveData(sips, DONE, currentPage);
				}
				
				data = sips.getData();
		    	for(int i=0 ; i<DATA_ROW ; i++) 
		    		for(int j=0 ; j<DATA_COLUMN ; j++) 
		        			model.setValueAt(data[i][j], i, j);
		        model.fireTableDataChanged(); 
    		}
    	});
    	
    	menuItem_2 = new JMenuItem("上30筆資料");
    	popupMenu.add(menuItem_2);
    	menuItem_2.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent ae) {
    			DONE = true;
				sips = new StockInfoPriceLowStop();
				
				if(currentPage == 2) {
					sips.retrieveData(sips, DONE, 1);
					currentPage = 1;
				} else if(currentPage == 3) {
					sips.retrieveData(sips, DONE, 2);
					currentPage = 2;
				} else {
					sips.retrieveData(sips, DONE, currentPage);
				}
				
				data = sips.getData();
		    	for(int i=0 ; i<DATA_ROW ; i++) 
		    		for(int j=0 ; j<DATA_COLUMN ; j++) 
		        			model.setValueAt(data[i][j], i, j);
		        model.fireTableDataChanged(); 
    		}
    	});
    	MouseListener popupListener = new PopupListener(popupMenu);
    	table.addMouseListener(popupListener);
    }
}