/*
*      author by Kevingo
*      Data @ 2007/11/19
*      抓取投資組合資料

*/

package software.Subitem.table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.sql.SQLException;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import software.DataRetrieve.StockInfoInvestCombination;
import common.util.MyTableModel_19;

public class InvestCombinationList {
	private static final long serialVersionUID = 1L;
	public boolean DONE = false;
	private int TABLE_HEIGHT, TABLE_WIDTH, vColIndex, width, DATA_ROW, DATA_COLUMN ;
	public Container container;
	private String [][] data;
	private JPanel panel_Table; // 放置表格的panel_Table
    private JTable table; // 表格
    private MyTableModel_19 model; // tableModel
	private StockInfoInvestCombination siiv;
	private String tmpProcess; // 處理表格資料轉換過程中需要暫存的字串
	private String [] columnNames = new String[] {
			"會員帳號",
			"股票代號",
			"買進日期",
			"賣出日期",
			"持有成本",
			"持有量",
			"獲利",
			"投資報酬率"
	};

    public InvestCombinationList() {
    	siiv = new StockInfoInvestCombination();
		container = new Container();
		container.setLayout(new BorderLayout());
		DATA_ROW = siiv.getRow(); 
		DATA_COLUMN = siiv.getColumn();
		TABLE_HEIGHT = 700 ; // 表格長度
		TABLE_WIDTH = 350; // 表格寬度
		
    	panel_Table = new JPanel();
    	data = new String[DATA_ROW][DATA_COLUMN];
    	
    	// 表格
		model = new MyTableModel_19(DATA_ROW, DATA_COLUMN);
		model.setColumnNames(columnNames);
    	
    	vColIndex = 0; // 決定column 的 index
    	width = 80; // column index 的寬度
    	table = new JTable(model);
		TableCellRenderer renderer = new CustomTableCellRenderer();
		table.setDefaultRenderer( Object.class, renderer );
        table.setColumnSelectionAllowed(true);
        table.clearSelection();
        table.revalidate();
        table.repaint();
    	table.setPreferredScrollableViewportSize(new Dimension(TABLE_HEIGHT, TABLE_WIDTH));
    	table.setFillsViewportHeight(true); // 確保這個表格會填滿整個元件
    	table.setRowSelectionAllowed(true);
    	table.setColumnSelectionAllowed(true); 
        model.addTableModelListener(new TableModelListener(){
            public void tableChanged(TableModelEvent e) {
                table.updateUI();
            }});
        
        // 設定column的寬度
        TableColumn col = table.getColumnModel().getColumn(vColIndex);
        col.setPreferredWidth(width);
        
        // 拉動式捲軸 和 主要Frame
    	JScrollPane scrollPane = new JScrollPane(table);
        RowHeaderer rowHeader= new RowHeaderer(table);
        scrollPane.setRowHeaderView(rowHeader);
        scrollPane.getRowHeader().setPreferredSize(new Dimension(35,100));	

        table.setBackground(Color.BLACK);
        table.setForeground(Color.WHITE);
        table.setFont(new Font("標楷體", Font.PLAIN, 14));
        container.add(panel_Table, BorderLayout.NORTH);
        container.add(scrollPane, BorderLayout.WEST);
        container.setVisible(true);

        try {
        	updataData();
        } catch (SQLException e1) {
        	e1.printStackTrace();
        }
    }
    
    public Container getFrame() {
		return container;
    }
    
    public void updataData() throws SQLException {
			DONE = true;
			data = siiv.getData();
	    	for(int i=0 ; i<DATA_ROW ; i++) {
	    		for(int j=0 ; j<DATA_COLUMN ; j++) {
	    			tmpProcess = String.valueOf(data[i][j]);
	    			if((j == 6 || j == 7) && !tmpProcess.startsWith("-") && tmpProcess != "null") {
	    				tmpProcess = "▲ " + tmpProcess.substring(1, tmpProcess.length());
	    				table.setSelectionForeground(Color.YELLOW);
	    			} else if(tmpProcess.startsWith("-") &&(j ==6 || j==7) && tmpProcess != "null")
	    				tmpProcess = "▼ " + tmpProcess.substring(1, tmpProcess.length());
	    			
	    			if(tmpProcess == "null" && j == 3)
	    				tmpProcess = "尚未賣出";
	    			else if (tmpProcess == "null" && (j == 6 || j == 7)) {
	    				tmpProcess = "尚未結算";
	    			}
	    			model.setValueAt(tmpProcess, i, j);
	    		}	
	    	}
	    	model.fireTableDataChanged();
    }
    
    // 處理快速鍵的問題   enter 確定 esc 取消
    public void enterPressesWhenFocused(JComponent object) {
    		object.registerKeyboardAction(
    				object.getActionForKeyStroke(
    						KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)), 
    						KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false), 
    						JComponent.WHEN_IN_FOCUSED_WINDOW);
    		
    		object.registerKeyboardAction(
    				object.getActionForKeyStroke(
    						KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)), 
    						KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), 
    						JComponent.WHEN_IN_FOCUSED_WINDOW);
    }
}