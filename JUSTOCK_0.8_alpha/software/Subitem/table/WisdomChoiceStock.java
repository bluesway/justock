package software.Subitem.table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import software.DataRetrieve.StockInfoWisdomChoice;

import common.util.MyTableModel_21;
import common.util.MyTableModel_21_2;

public class WisdomChoiceStock extends JFrame {
	private static final long serialVersionUID = 1L;

	// 表格變數
	public boolean DONE = false;
	private int TABLE_HEIGHT, TABLE_WIDTH, DATA_ROW, DATA_COLUMN, vColIndex, width;
	public Object [][] data;
	public Container container;
	private JPanel paint1_Panel,btn_Panel; // 放置表格的panel_Table
    private JTable table; // 表格
    private MyTableModel_21 model1; // tableModel
    private MyTableModel_21_2 model2; // tableModel
    private StockInfoWisdomChoice siwc;
    private int type;	// 20日內最高或最低
	private String tmpProcess;
    
	// 圖形變數
//	private MainGraph mg;
	
	public WisdomChoiceStock(int type) {
		this.type = type;
		setTitle( "孔明選股" );
		setResizable(false);
		createPage1();
	}	

	// 顯示自選股 報價
	public void createPage1() {		
		siwc = new StockInfoWisdomChoice(type);
		container = new Container();
		container.setLayout(new BorderLayout());
		
		DATA_ROW = siwc.getRow(); 
		DATA_COLUMN = siwc.getColumn();
		
		data = new Object[DATA_ROW][DATA_COLUMN];
		String [] columnNames = new String[] {
					"股票代碼",
					"漲跌",
					"價格",
					"成交量",
					"委買價",
					"委買量",
					"委賣價",
					"委賣量"
			};
		
			try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch(Exception e) {
				e.printStackTrace();
			}	
	
			vColIndex = 0; // 決定column 的 index
	    	width = 70; // column index 的寬度
			
	    	TABLE_HEIGHT = 500;
	    	TABLE_WIDTH = 400;
	    	
	    	paint1_Panel = new JPanel(new GridLayout(1,3));
			btn_Panel = new JPanel();

	    	// 表格
			if (this.type == 0) {
				model1 = new MyTableModel_21(DATA_ROW, DATA_COLUMN);
				model1.setColumnNames(columnNames);
				table = new JTable(model1);
			} else {
				model2 = new MyTableModel_21_2(DATA_ROW, DATA_COLUMN);
				model2.setColumnNames(columnNames);
				table = new JTable(model2);
			}
			TableCellRenderer renderer = new CustomTableCellOfWisdom();
			table.setDefaultRenderer( Object.class, renderer );
	        table.setColumnSelectionAllowed(true);
	        table.clearSelection();
	        table.revalidate();
	        table.repaint();
	    	table.setBackground(Color.BLACK);
	    	table.setForeground(Color.white);
	    	table.setFont(new Font("微軟正黑體",
	    	        		Font.BOLD,
	    	        		12));
	    	table.setPreferredScrollableViewportSize(new Dimension(TABLE_HEIGHT, TABLE_WIDTH));
	    	table.setFillsViewportHeight(true);   
	    	table.setRowSelectionAllowed(true);
	    	table.setColumnSelectionAllowed(true);
	    	if (this.type == 0) {
	    		model1.addTableModelListener(new TableModelListener(){
	    			public void tableChanged(TableModelEvent e) {
	    				table.updateUI();
	    			}});
	    	} else {
		        model2.addTableModelListener(new TableModelListener(){
		            public void tableChanged(TableModelEvent e) {
		                table.updateUI();
		            }});
	    	}
	        // 設定column的寬度
	        TableColumn col = table.getColumnModel().getColumn(vColIndex);
	        col.setPreferredWidth(width);
	        
	        // 拉動式捲軸 和 主要Frame
	    	JScrollPane scrollPane = new JScrollPane(table);
	        RowHeaderer rowHeader= new RowHeaderer(table);
	        scrollPane.setRowHeaderView(rowHeader);
	        scrollPane.getRowHeader().setPreferredSize(new Dimension(35,100));
	        
	        table.setBackground(Color.BLACK);
	        table.setForeground(Color.WHITE);
	        table.setFont(new Font("標楷體", Font.PLAIN, 14));
	        container.add(btn_Panel, BorderLayout.SOUTH);
	        container.add(paint1_Panel, BorderLayout.EAST);
	        container.add(scrollPane, BorderLayout.WEST);
	        container.setVisible(true);
	        
	        try {
				updataData();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
	    }
	
    public void updataData() throws SQLException {
		data = siwc.getData();
    	for(int i=0 ; i<DATA_ROW ; i++) {
    		for(int j=0 ; j<DATA_COLUMN ; j++) {
    			tmpProcess = String.valueOf(data[i][j]);
    			if (type == 0)
    				model1.setValueAt(tmpProcess, i, j);
    			else
    				model2.setValueAt(tmpProcess, i, j);
    		}	
    	}
    	if (type == 0)
    		model1.fireTableDataChanged();
    	else
    		model2.fireTableDataChanged();
}
	
    public Container getFrame() {
		return container;
    }
}