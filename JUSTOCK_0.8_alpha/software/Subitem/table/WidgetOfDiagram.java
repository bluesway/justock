/*
 * 	產生集中市場即時行情圖
 * */
package software.Subitem.table;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import software.Subitem.Graph.OverallGraph;

public class WidgetOfDiagram extends JFrame{
	private static final long serialVersionUID = 1L;
	private JPanel panel;
	private OverallGraph og;
	private Container container;
	
	public WidgetOfDiagram() {
		createPage();
	}
	
	public void createPage() {
		og = new OverallGraph();
		container = new Container();
		container.setLayout(new BorderLayout());
//		container.setSize(Double.valueOf(GUI.screen_x*0.1).intValue(), Double.valueOf(GUI.screen_y*0.1).intValue());
    	panel= new JPanel(new GridLayout(1,1));
    	
		try {
			og = new OverallGraph();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}		
		panel.add(og);
		
        container.add(panel, BorderLayout.CENTER);
        container.setVisible(true);
	}
	
    public Container getFrame() {
		return container;
    }
}