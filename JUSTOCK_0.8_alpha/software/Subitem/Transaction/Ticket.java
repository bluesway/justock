/**
 * @author bluesway
 * TARGET 執行交割作業 (撮合後立即執行)
 */

package software.Subitem.Transaction;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import common.util.DateUtil;

import software.DB;
import software.Routine;

public class Ticket implements Job {
	private String sql;			// 查詢語句
	private String userId;		// 使用者代號
	private String stockId;		// 股票代號
	private int status;			// 交易代號
//	private String condition;	// 條件式
	
	public Ticket() {
		this (".", ".", 0);
	}

	public Ticket(String userId) {
		this (userId, ".", 0);
	}
	
	public Ticket(String userId, String stockId, int status) {
		this.userId = userId;
		this.stockId = stockId;
		this.status = status;
		sql = "SELECT * FROM transaction WHERE tUid REGEXP \"" + this.userId + "\" && " +
				"tSid REGEXP \"" + this.stockId + "\" && tStatus=" + status + " ";
	}

	public void setUserId (String userId) {
		sql.replace("\"" + this.userId + "\"", "\"" + userId + "\"");
		this.userId = userId;
	}
	
	public void setStockId (String stockId) {
		sql.replace("\"" + this.stockId + "\"", "\"" + stockId + "\"");
		this.stockId = stockId;
	}
	
	public void setStatus (int status) {
		sql.replace("tStatus=" + this.status, "tStatus=" + status);
		this.status = status;
	}
	
	private void execute () {
		double cost;
		int id;
		String userId;
		String stockId;
		double price, price_hold;
		int quan, quan_hold;
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet update = null;
		ResultSet result = null;
		
		
		try {
			conn = DB.db.getConnection();
			stmt = conn.createStatement();
			update = stmt.executeQuery(sql);
			
			while (update.next()) {
				id = update.getInt("tId");
				userId = update.getString("tUid");
				stockId = update.getString("tSid");
				price = update.getDouble("tPrice");
				quan = update.getInt("tQuan");
				
				result = stmt.executeQuery("SELECT bBalance FROM bank WHERE bUid=\"" + userId + "\"");
				if (result.next())
					if (price > 0)
						cost = result.getDouble("bBalance") - Math.round(price * quan);
					else	// 賣出股票要扣稅
						cost = result.getDouble("bBalance") - Math.round(price * quan * 1.003);
				else
					cost = 0;
				
				// 更新 holding 資料
				result = stmt.executeQuery("SELECT * FROM holding WHERE hUid=\"" + userId + "\" AND hSid=\"" + stockId + "\"");
				if (result.next())
					if (price > 0) {
						price_hold = result.getDouble("hPrice");
						quan_hold = result.getInt("hQuan");
						price = (price * quan + price_hold * quan_hold) / (quan + quan_hold);
						quan += quan_hold;

						stmt.addBatch("UPDATE holding SET hBuyin=" + DateUtil.getDBDate(System.currentTimeMillis()) + ", hPrice=" + price + ", hQuan=" + quan + " WHERE hUid=\"" + userId + "\" AND hSid=\"" + stockId + "\"");
					} else {
						price_hold = result.getDouble("hPrice");
						quan_hold = result.getInt("hQuan");
						
						if (quan_hold > quan) {
							price = (price_hold * quan_hold - price * quan * -1) / (quan_hold - quan);
							quan = quan_hold - quan;
							
							stmt.addBatch("UPDATE holding SET hSellout=" + DateUtil.getDBDate(System.currentTimeMillis()) + ", hPrice=" + price + ", hQuan=" + quan + " WHERE hUid=\"" + userId + "\" AND hSid=\"" + stockId + "\"");
						} else
							stmt.addBatch("UPDATE holding SET hSellout=" + DateUtil.getDBDate(System.currentTimeMillis()) + ", hPrice=0, hQuan=0 WHERE hUid=\"" + userId + "\" AND hSid=\"" + stockId + "\"");
					}
				else	// holding 查無資料，一定是新買進
					stmt.addBatch("INSERT INTO holding VALUES(\"" + userId + "\", \"" + stockId + "\", " + DateUtil.getDBDate(System.currentTimeMillis()) + ", NULL, " + price + ", " + quan + ")");

				// 更新 transaction 和 bank 資料
				stmt.addBatch("UPDATE transaction SET tStatus=127 WHERE tId=" + id);
				stmt.addBatch("UPDATE bank SET bBalance=" + cost + " WHERE bUid=\"" + userId + "\"");
				System.out.println(userId+": " + String.valueOf(cost));
				stmt.executeBatch();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (result != null)
					result.close();
				if (update != null)
					update.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					DB.db.closeConnection(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
    public void execute(JobExecutionContext context) throws JobExecutionException {
    	String jobName = context.getJobDetail().getFullName();
    	
		Ticket t = new Ticket();
		
		Routine.log.info("Ticket: " + jobName + " executing at " + new Date());
		
		t.execute();
    }
    
	public static void main(String[] args) {
		Ticket t = new Ticket();
		t.execute();
		return;
	}
}
