package common.util;

import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {
	static String year;	    // 年
	static String month;	// 月
	static String day; 		// 日
	static String date_mark;	// 資料庫日期
	static Date d;
	static String nowDate = "";
	
	public DateUtil() {
		this(19000000);
	}
	
	public DateUtil(int date) {
		transDate(date);
	}

	// 轉換資料庫取得的日期數字資料
	public static String transDate(int date) {
		date_mark = String.valueOf(date);
		year = date_mark.substring(0, 4);
		month = date_mark.substring(4, 6);
		day = date_mark.substring(6, 8);
		
		return getDate("/");
	}

	// 取得西元年
	public static  String getYear() {
		return year;
	}
	
	// 取得月份
	public static String getMonth() {
		return month;
	}
	
	//取得日期
	public static String getDay() {
		return day;
	}
	
	// 取得民國年
	public String getChineseYear() {
		return String.valueOf(Integer.valueOf(getYear())-1911);
	}
	
	// 取得民國日期
	public String getChineseDate() {
		return "民國" + getChineseYear() + "年" + getMonth() + "月" + getDay() + "日";
	}
	
	// 取得民國日期 (自行定義sep)
	public String getChineseDate(String sep) {
		return getChineseYear() + sep + getMonth() + sep + getDay();
	}
	
	// 取得西元日期 (自行設定sep)
	public static String getDate(String sep) {
		return getYear() + sep + getMonth() + sep +getDay();
	}

	public static boolean isSameFivePeriod(String time1, String time2) {
		int min1, min2;
		
		if (time1.equals("") || time2.equals(""))
			return true;
		
		SimpleDateFormat format = new SimpleDateFormat("HH:mm");
		Date d1 = null;
		Date d2 = null;
		try {
			d1 = format.parse(time1);
			d2 = format.parse(time2);
		} catch (Exception e) {
			e.printStackTrace();
//			return true;
		}
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(d1);
		cal2.setTime(d2);

		min1 = cal1.get(Calendar.MINUTE) / 10;
		min2 = cal2.get(Calendar.MINUTE) / 10;
		
		if (min1 != min2)
			return false;

		min1 = cal1.get(Calendar.MINUTE) % 10;
		min2 = cal2.get(Calendar.MINUTE) % 10;

		if ((min1 < 5 && min2 < 5) || (min1 >= 5 && min2 >= 5))
			return true;

		return false;
	}

	public static boolean isSameMonth(String date1, String date2) {
		if (date1.equals("") || date2.equals(""))
			return true;
		if (date1.substring(5, 7).equals(date2.substring(5, 7)))
			return true;
		else
			return false;
	}
	
	public static boolean isSameWeek(String date1, String date2) {
		if (date1.equals("") || date2.equals(""))
			return true;
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		Date d1 = null;
		Date d2 = null;
		try {
			d1 = format.parse(date1);
			d2 = format.parse(date2);
		} catch (Exception e) {
			e.printStackTrace();
//			return true;
		}
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(d1);
		cal2.setTime(d2);
		int subYear = cal1.get(Calendar.YEAR) - cal2.get(Calendar.YEAR);
		// subYear==0,說明是同一年
		if (subYear == 0) {
			if (cal1.get(Calendar.WEEK_OF_YEAR) == cal2
					.get(Calendar.WEEK_OF_YEAR))
				return true;
		}
		// 例子:cal1是"2005-1-1"，cal2是"2004-12-25"
		// java對"2004-12-25"處理成第52周
		// "2004-12-26"它處理成了第1周，和"2005-1-1"相同了
		// 大家可以查一下自己的日曆
		// 處理的比較好
		// 說明:java的一月用"0"標識，那麼12月用"11"
		else if (subYear == 1 && cal2.get(Calendar.MONTH) == 11) {
			if (cal1.get(Calendar.WEEK_OF_YEAR) == cal2
					.get(Calendar.WEEK_OF_YEAR))
				return true;
		}
		// 例子:cal1是"2004-12-31"，cal2是"2005-1-1"
		else if (subYear == -1 && cal1.get(Calendar.MONTH) == 11) {
			if (cal1.get(Calendar.WEEK_OF_YEAR) == cal2
					.get(Calendar.WEEK_OF_YEAR))
				return true;

		}
		return false;
	}
	
	// 使用時 呼叫此方法 傳入 1,2,3,4 其中一個數值選擇顯示格式
	public static String show(int SELECTION) {
			if(SELECTION == 1)
				return getCurrentDate(DateFormat.SHORT, DateFormat.SHORT, Locale.TAIWAN);
			else if(SELECTION == 2)
				return getCurrentDate(DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.TAIWAN);
			else if(SELECTION == 3)
				return getCurrentDate(DateFormat.LONG, DateFormat.LONG, Locale.TAIWAN);
			else if(SELECTION == 4)
				return getCurrentDate(DateFormat.FULL, DateFormat.FULL, Locale.TAIWAN);
			else
				return getCurrentDate(100, 100, Locale.TAIWAN);
				
	}
	
	// 取得現在的日期 標準格式四種
	public static String getCurrentDate(int dateFormat, int timeFormat, Locale locale) {
		d = new Date();
		DateFormat df = DateFormat.getDateTimeInstance(dateFormat, timeFormat, locale);
		
		switch(dateFormat) {
			case DateFormat.SHORT :
				nowDate = df.format(d);
//				System.out.println(nowDate);
				break;
			case DateFormat.MEDIUM : 
				nowDate = df.format(d);
//				System.out.println(nowDate);
				break;
			case DateFormat.LONG :
				nowDate = df.format(d);
//				System.out.println(nowDate);
				break;
			case DateFormat.FULL :
				nowDate = df.format(d);
//				System.out.println(nowDate);
				break;
			default:
				nowDate = null;
		}
		
		return nowDate.toString();
	}
	
	// 將目前日期轉換為資料庫格式
	public static int getDBDate(long milli) {
		return Integer.parseInt(new SimpleDateFormat("yyyyMMdd").format(new Date(milli)));
	}
	
	public static void main(String [] args) {
		new DateUtil();
		show(1);
		show(2);
//		show(3);
//		show(4);
//		System.out.println(getYear());
//		System.out.println(getMonth());
//		System.out.println(getDay());
//		System.out.println(getChineseDate());
//		System.out.println(getDate("a"));
	}
}
