package common.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import software.DB;

public class UpDateStockClass {
	Scanner scan;

	int i = 0;

	String[] str = new String[1128];

	public UpDateStockClass() {
		readData();
	}

	private void readData() {
		File tmpfile;
		Connection conn = null;
		Statement stmt = null;

		try {
			tmpfile = new File("common/source/stock id list2");
			scan = new Scanner(tmpfile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		try {
			conn = DB.db.getConnection();
			stmt = conn.createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		
		for (int i = 0; i < 1128; i++) {
			str[i] = scan.next();
		}
		
		int k = 100;
		try {
			for (int j = 0; j < 564; j++) {
				if (!str[i].equals("b") && !str[i + 2].equals("b")) {
					System.out.println(str[i] + ",  " + str[i + 1] + ",  " + k);
					stmt.addBatch("INSERT INTO stock VALUES('" + str[i] + "', '" + str[i + 1] + "', '" + k + "')");
				}
				i += 2;
				if (str[i].length() == 1 && str[i + 2].length() == 4)
					k++;
			}
			
			stmt.executeBatch();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null)
					DB.db.closeConnection(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} 
	}

	public static void main(String[] args) {
		new UpDateStockClass();
	}
}
