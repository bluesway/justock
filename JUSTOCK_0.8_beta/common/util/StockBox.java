package common.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import software.DB;
import software.Subitem.Listener.StockBoxListener;

public class StockBox {
	
	public StockBox() {}
	
	public static JComboBox create() {
		JComboBox stockbox;
		Vector<String> source = new Vector<String>();
		Scanner scan;
		String tmp = "";

		try {
			scan = new Scanner(new FileInputStream("common/source/stockId.list"));

			while (scan.hasNext()) {
				tmp = scan.nextLine();
				if (tmp.startsWith("#"))
					continue;

				source.add(tmp);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		stockbox = new JComboBox(source);
		stockbox.setEditable(true);
		
		return stockbox;
	}
	
	public static JComboBox createTypeBox() {
		JComboBox typebox = new JComboBox(new Object[]{"カ", "耫"});
		typebox.addItemListener(new StockBoxListener());

		return typebox; 
	}
	
	public static JComboBox createClassBox() {
		JComboBox stockbox;
		Connection conn = null;
		ResultSet result = null;
		Vector<String> source = new Vector<String>();
		
		try {
			conn = DB.db.getConnection();
			result = conn.createStatement().executeQuery("SELECT cName FROM stock_class");
			
			while (result.next())
				source.add(result.getString("cName"));
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (result != null)
					result.close();
				if (conn != null)
					DB.db.closeConnection(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		stockbox = new JComboBox(source);
		stockbox.addItemListener(new StockBoxListener());
		
		return stockbox;
	}
	
	// 布 id 穓碝布肚ま
	public static int searchIndexOfStockId (int stockId) {
		int index = 0;
		Scanner scan;
		String tmp = "";

		try {
			scan = new Scanner(new FileInputStream("common/source/stockId.list"));

			while (scan.hasNext()) {
				tmp = scan.nextLine();
				if (tmp.startsWith("#"))
					continue;
				
				if (tmp.split(" ")[0].equals(String.valueOf(stockId)))
					return index;
				else
					index++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		// ⊿т
		return -1;
	}
	
	// 布嘿穓碝布肚布id
	public static String searchStockName(String name) {
		Scanner scan;
		String list = "";
//		System.out.println("嘿" + name);
		try {
			scan = new Scanner(new FileInputStream("common/source/stockId.list"));

			while (scan.hasNext()) {
				list = scan.nextLine();
				if (list.startsWith("#"))
					continue;
				
				if (list.split(" ")[1].equals(name))
					return list.split(" ")[0];
				else
					list = "琩礚布";
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			list = "-1";
		}
		
		return list;
	}
	
	//  id 穓碝布肚Ч俱布嘿
	public static String searchStockId(String id) {
		Scanner scan;
		String stock = "";
		try {
			scan = new Scanner(new FileInputStream("common/source/stockId.list"));
			while (scan.hasNext()) {
				stock = scan.nextLine();
				if (stock.startsWith("#"))
					continue;
				
				if (stock.split(" ")[0].equals(id)) 
					return stock;
				else
					stock = "琩礚布";
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return stock;
	}
	
	//  stockBox ず甧穓碝布肚布絏
	public static int getStockId(JComboBox stockBox) {
		int stockId;
    	String stockItem, key;
    	
    	if (stockBox == null)
    		return -1;
    	
		stockItem = String.valueOf(stockBox.getEditor().getItem());
		if (!stockItem.matches("[1-9][0-9]\\{3\\}")) {
			key = searchStockName(stockItem);
//			System.out.println("search name: " + key);
			if (key.equals("琩礚布")) {
				try {
					key = searchStockId(stockItem.split(" ")[0]);
//					System.out.println("search id: " + key);
					if (key.equals("琩礚布")) {
						JOptionPane.showMessageDialog(null, "琩礚布", "琩高", JOptionPane.WARNING_MESSAGE);
//						System.out.println(stockItem);
						stockId = -1;
					}
					else
					{
						stockId = Integer.parseInt(key.split(" ")[0]);
//						System.out.println("parse id 0 - 4: " + stockId);
					}
				} catch (StringIndexOutOfBoundsException e) {
					JOptionPane.showMessageDialog(null, "琩礚布", "琩高", JOptionPane.WARNING_MESSAGE);
					return -1;
				}
			}
			else
			{
				stockId = Integer.parseInt(key);
//				System.out.println("parse id: " + stockId);
			}
		} else if (stockItem.length() == 0){
			stockId = -1;
		} else {
			JOptionPane.showMessageDialog(null, "琩礚布", "琩高", JOptionPane.WARNING_MESSAGE);
			stockId = -1;
		}
		
		return stockId;
	}
	
	public static void main(String[] args) {
//		System.out.println(create().getItemAt(100));
	}
}
