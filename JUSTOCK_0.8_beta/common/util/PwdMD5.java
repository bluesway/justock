package common.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class PwdMD5 {
    private static String algo;
    private static MessageDigest md;

    static {
    	algo = "MD5"; // "MD5" or "SHA-1"
    	try {
			md = MessageDigest.getInstance(algo);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    public PwdMD5 () throws NoSuchAlgorithmException {
    }
    
    public static String encrypt(String text) {
        byte[] digest;
        
        md.update(text.getBytes());
        digest = md.digest();
        
        return Byte2Hex(digest);
    }
    
    private static String Byte2Hex(byte[] src) {
        byte b;
        int value;
        StringBuffer buffer = new StringBuffer();
        
        for (int i = 0; i < src.length; i++)	{
            b = src[i];
            value = (b & 0x7F) + (b < 0 ? 128 : 0);
            buffer.append(value < 16 ? "0" : "");
            buffer.append(Integer.toHexString(value));
        }

        return buffer.toString();
    }
    public static void main(String[] args) throws NoSuchAlgorithmException {
        String result;
        String verify = "";
        boolean ok = false;
        
        while (!ok) {
            try {
                verify = String.valueOf(PasswordField.getPassword(System.in, "Enter the clear password: "));
                if (verify.equals(String.valueOf(PasswordField.getPassword(System.in, "Verify the password: "))))
                    ok = true;
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
        result = PwdMD5.encrypt(verify);

        System.out.println("Result:"+ result);
    }
}


class MaskingThread extends Thread {
    private volatile boolean stop;
    private char echochar = '*';
    
    public MaskingThread(String prompt) {
        System.out.print(prompt);
    }
    
    @SuppressWarnings("static-access")
	public void run() {
        
        int priority = Thread.currentThread().getPriority();
        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
        
        try {
            stop = true;
            while(stop) {
                System.out.print("\010" + echochar);
                try {
                    Thread.currentThread().sleep(1);
                }catch (InterruptedException iex) {
                    Thread.currentThread().interrupt();
                    return;
                }
            }
        } finally {
            Thread.currentThread().setPriority(priority);
        }
    }
    
    public void stopMasking() {
        this.stop = false;
    }
}

class PasswordField {
    public static final char[] getPassword(InputStream in, String prompt) throws IOException {
        MaskingThread maskingthread = new MaskingThread(prompt);
        Thread thread = new Thread(maskingthread);
        boolean start = false;
        
        if (!start) {
            thread.start();
            start = true;
        }
        
        char[] lineBuffer;
        char[] buf;
        
        buf = lineBuffer = new char[128];
        
        int room = buf.length;
        int offset = 0;
        int c;
        
        loop:
        while (true) {
            switch (c = in.read()) {
                case -1:
                case '\n':
                    break loop;
                case '\r':
                    int c2 = in.read();
                    if ((c2 != '\n') && (c2 != -1)) {
                        if (!(in instanceof PushbackInputStream)) {
                            in = new PushbackInputStream(in);
                        }
                        ((PushbackInputStream)in).unread(c2);
                    } else {
                        break loop;
                    }
                    
                default:
                    if (--room < 0) {
                        buf = new char[offset + 128];
                        room = buf.length - offset - 1;
                        System.arraycopy(lineBuffer, 0, buf, 0, offset);
                        Arrays.fill(lineBuffer, ' ');
                        lineBuffer = buf;
                    }
                    buf[offset++] = (char) c;
                    
                    break;
            }
        }
 
        maskingthread.stopMasking();
        if (offset == 0) {
            return null;
        }
        
        char[] ret = new char[offset];
        System.arraycopy(buf, 0, ret, 0, offset);
        Arrays.fill(buf, ' ');
        return ret;
    }
}