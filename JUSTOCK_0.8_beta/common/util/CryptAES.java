package common.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.security.SecureRandom;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class CryptAES {
	public static boolean ENCRYPT = true;
	public static boolean DECRYPT = false;
	public static int FILE_OUTPUT = 0;
	public static int TEXT_OUTPUT = 1;
	public static String AES_ALGO = "AES";
	public static String DES_ALGO = "DES";
	private static String passwd = "XDDDDD_BluEswAydanIElYounGKEviNGoYaOYi";
	
	// 加密文件
	public static Object encryptfile(String pwd, String fileName, boolean cryptType, String algo, int outputType) throws Exception {
		File fileIn = new File(fileName);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		FileOutputStream outputFile = null;

		if (pwd == null)
			pwd = passwd;
		try {
			//讀取文件
			FileInputStream fis = new FileInputStream(fileIn);
		    byte[] bytIn = new byte[(int) fileIn.length()];
		      
		    for (int i = 0; i < fileIn.length(); i++)
		    	bytIn[i] = (byte) fis.read();
		      
		    fis.close();
		      
		    //AES加解密
		    KeyGenerator kgen = KeyGenerator.getInstance(algo);
		    kgen.init(128, new SecureRandom(pwd.getBytes()));
		    SecretKey skey = kgen.generateKey();
		    byte[] raw = skey.getEncoded();
		    SecretKeySpec skeySpec = new SecretKeySpec(raw, algo);
		    Cipher cipher = Cipher.getInstance(algo);
		     
		    if (cryptType)	// true:加密 false:解密
		    	cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
		    else
		    	cipher.init(Cipher.DECRYPT_MODE, skeySpec);
		    
		    //寫文件
		    byte[] bytOut = cipher.doFinal(bytIn);
		      
		    if (outputType == 0) {// 0:檔案 其它:文字
		    	outputFile = new FileOutputStream(fileIn.getPath() + (cryptType ? ".encrypted" : ".decrypted"));
		    	for (int i = 0; i < bytOut.length; i++)
		    		outputFile.write((int) bytOut[i]);
		    }
		    else
		    	for (int i = 0; i < bytOut.length; i++)
		    		outputStream.write((int) bytOut[i]);
		    if (outputType == 0 && outputFile != null)
		    	outputFile.close();
		    
		} catch (BadPaddingException e) {
			System.out.println("檔案格式錯誤！");
		} catch (FileNotFoundException e) {
			System.out.println("靠杯");
			System.out.println(Thread.currentThread().getClass().getResource(""));
		}
		
		if (outputType == 0) {
			System.out.println("檔案輸出完成！");
			return outputFile;
		} else
			return outputStream.toString();
	  }

	  public static void main(String[] args) throws Exception {
		  String filename = "common/source/JUSTOCKDB.properties";
		  System.out.println(CryptAES.encryptfile(null, filename, CryptAES.DECRYPT, CryptAES.AES_ALGO, CryptAES.TEXT_OUTPUT));
	  }
} 