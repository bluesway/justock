/**
 * @author bluesway
 * TARGET 顯示個股及大盤的當日資訊
 */

package website.graph;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.util.checkDay;

@SuppressWarnings("serial")
public class TechGraphJSP extends HttpServlet {
	// 視窗元件大小變數
    public static final Dimension user_screen_size = Toolkit.getDefaultToolkit().getScreenSize();
    public static int[] overall_size;
    
    // 資料庫連線變數
    private static Connection conn;
    private static Statement stmt;
	public static final int TYPE_LINE_CHART = 0;
	public static final int TYPE_KBAR_CHART = 1;
	public static final int TYPE_BIN_CHART = 2;
	
	// KD指標
	private int kd_len;
	private double rsv;
	private double k_pvalue;
	private double k_cvalue;
	private double d_pvalue;
	private double d_cvalue;
	private double[] kd_data;
	
	// Bias乖離率
	private double bavgPrev;
	private double bavgCurr;
	private double biasPrev;
	private double biasCurr;
	
    // 繪圖用變數
	private BufferedImage image;
    private int screen_x;
    private int screen_y;
    private Rectangle graphD;	// 線圖邊框範圍
    private int line_cap;				// 框線寬度
    private int start_main;			// 主線圖上緣
    private int height_main;		// 主線圖高度
    private int start_quan;			// 成交量上緣
    private int height_quan;		// 成交量高度
    private int start_tech1;			// 技術分析1上緣
    private int height_tech1;		// 技術分析1高度
    private int start_tech2;			// 技術分析2上緣
    private int height_tech2;		// 技術分析2高度
    private int left_space;			// 顯示指數、成交量資訊的寬度
    private int right_space;		// 顯示指標資訊的寬度
    private int char_height;		// 預設字高
	private int displayType;		// 顯示型態 0:折線圖 1:K線圖 2:兩者同時顯示
    private int scale;						// 線圖縮放級距
    private int dateType;			// 線圖日期型式 0:日線 1:週線 2:週線 3:分線
	private double ratio;				// 數值在圖表中的相對比例
	private NumberFormat nf;

	// 資料處理用變數
	private int stockId;					// 繪圖資料來源 - 0:大盤 id:個股
	private LinkedList<double[]> sourceData;		// 原始資料
    private String[] date_mark = new String[3];	// 日期字串
    private int priceNum;				// 指數參考資料顯示筆數 (須為單數筆且大於3)
    private int quanNum;				// 成交量參考資料顯示筆數 (須為單數筆)
    private double[] data;				// 圖表繪製range資料
	private double[] priceInitial;	// 指數參考資料值
	private double[] quanInitial;	// 成交量參考資料值
	private double priceSep;			// 指數參考資料區間
	private double quanSep;			// 成交量參考資料區間
	private double[] prev;				// 前項指標
	private double[] now;				// 當項指標
	private int avgNum;					// 均線數量 (包含指數及成交量，須為雙數)
	private double[] avgPrev;		// 前項均值
	private double[] avgCurr;		// 當項均值
	private int time;							// 當前處理時間
	private int range_start;				// 資料讀取起點
	private int range;						// 資料讀取區間
	private int shift;							// 資料移動區間
	
	// 資料庫用變數
    public ResultSet result;
//	private double yesterday;		// 昨日收盤價
	HashMap<Integer, double[]> sourceMap;

	public void init (ServletConfig config) throws ServletException {
		super.init(config);
	}
	
	private void connectDB() {
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://JUSTOCK.twbbs.org:3306/justock";
		String user = "XDDDDD";
		String pwd = "specification";
		
		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url + "?user=" + user + "&password=" + pwd);
			stmt = conn.createStatement();
		} catch (InstantiationException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		screen_x = Integer.parseInt(request.getParameter("screen_x"));
		screen_y = Integer.parseInt(request.getParameter("screen_y"));
		stockId = Integer.parseInt(request.getParameter("stockId"));
		// 線圖形式 0:折線圖 1:K線圖 2:兩者同時顯示
		displayType = Integer.parseInt(request.getParameter("display"));
		// 時間週期 0:日線 1:週線 2:月線 3:分線
		dateType = Integer.parseInt(request.getParameter("date"));
		
		if (stockId == 0)
			stockId = 1000;
		
    	overall_size = new int[400];
    	for (int i = 0; i < 400; i++)
    		overall_size[i] = (int) Math.round(user_screen_size.getWidth() * 0.0025 * i);
		// 數字格式
    	nf = NumberFormat.getInstance();
    	nf.setMaximumFractionDigits(2);
    	
    	// 變數處理
    	graphD = new Rectangle();

    	// 圖形繪製
    	line_cap = 2;
    	char_height = overall_size[5];
    	left_space = overall_size[22];
    	right_space = overall_size[35];
    	start_main = 15;
    	graphD.setLocation(left_space, 10);
    	graphD.setSize(screen_x - left_space - right_space, screen_y - 25); 

    	// 資料處理
    	sourceData = new LinkedList<double[]>();
    	priceNum = 9;
    	quanNum = 3;
    	shift = 0;
    	scale = 10;
    	range_start = 0;
    	range = (int) Math.round(graphD.width / scale);
    	avgNum = 6;
    	avgPrev = new double[avgNum];
    	avgCurr = new double[avgNum];
    	kd_len = 9;
    	kd_data = new double[kd_len+1];
    	k_pvalue = 50.0;
    	d_pvalue = 50.0;
    	bavgPrev = 0.0;
    	bavgCurr = 0.0;
    	
    	connectDB();
		readData();
		paint();
		
		response.setContentType("image/png"); 
		ServletOutputStream out = response.getOutputStream();   
        try {
            ImageIO.write(image, "PNG", out);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
		out.flush();  
        out.close();
	}
	
    private void readData() {
		sourceData.clear();
		
		int dayCount = 0;
		String date1 = "";
		String date2 = "";
		double result_info[] = new double[6];

        try {
            result = stmt.executeQuery("SELECT * FROM info_history WHERE hSid='" + 
            		String.valueOf(stockId) + "' ORDER BY hDate ASC");
            
            while (result.next()) {
            	if (dateType != 0) {
            		date1 = date2;
            		date2 = numToDate(result.getInt(2));
            	}
            	
            	// 週線 or 月線
            	if ((dateType == 1 && !checkDay.isSameWeek(date1, date2)) ||
            			(dateType == 2 && !checkDay.isSameMonth(date1, date2))) {
        			sourceData.add(result_info.clone());
        			dayCount = 0;
        			for (int i = 1; i < 6; i++)
        				result_info[i] = 0;
            	}

            	if (dayCount == 0) {
            		result_info[0] = result.getInt(2);
            		result_info[1] = result.getDouble(3);
            	}
            	
            	if (dateType == 0) {
	            	result_info[5] = result.getInt(7) / 100.0; 
	            	for (int i = 1; i < 5; i++)
		            	result_info[i] = result.getDouble(i+2);
        			sourceData.add(result_info.clone());
            	} else {
            		if (result_info[2] < result.getDouble(4))
            			result_info[2] = result.getDouble(4);
            		if (result_info[3] > result.getDouble(5) || result_info[3] <= 0.0)
            			result_info[3] = result.getDouble(5);
            		result_info[4] = result.getDouble(6);
	            	result_info[5] += result.getInt(7) / 100.0; 
	            	dayCount++;
            	}
            }
            result = stmt.executeQuery("SELECT * FROM stock WHERE Sid='" + 
            		String.valueOf(stockId) + "'");
        } catch (SQLException e) {
        	System.err.println(e);
        }
    }
    
    private void setData() {
    	int tmp = sourceData.size();
    	
    	range_start = tmp - range + shift;
    	if (range_start <= 0)
    		range_start = 0;
    	time = range_start;
    	
    	if (tmp - range_start + 1 < range)
    		range = tmp - range_start;
    	data = new double[range];
    	
    	prev = sourceData.get(time++);
    	
    	// 處理指數部分
    	priceInitial = new double[priceNum + 1];
    	
		for (int i = 0; i < range; i++)
			data[i] = sourceData.get(range_start + i)[4];
		
		Arrays.sort(data);
		
		priceInitial[1] = data[data.length - 1];
		priceInitial[priceNum] = data[0];
		priceSep = (priceInitial[1] - priceInitial[priceNum]) / (priceNum - 1) * 1.0;
		
		for (int i = priceNum - 1; i > 1; i--)
			priceInitial[i] = priceInitial[1] - priceSep * (i-1);
    	
    	// 處理成交量部分
    	quanInitial = new double[quanNum + 1];
    	
		for (int i = 0; i < range; i++)
			data[i] = sourceData.get(range_start + i)[5];
		
    	Arrays.sort(data);
    	
    	quanInitial[1] = data[data.length - 1];
    	quanInitial[quanNum] = 0;
    	
    	quanSep = quanInitial[1] / (quanNum - 1) * 1.0;
    	
    	for (int i = quanNum - 1; i > 1; i--)
    		quanInitial[i] = quanInitial[1] - quanSep * (i-1);
    }
    
	public BufferedImage paint() {
		image = new BufferedImage(screen_x, screen_y, BufferedImage.TYPE_INT_RGB);
    	Graphics g = image.createGraphics();
    	Graphics2D page = (Graphics2D) g;
    	
    	// 重新讀取資料
     	graphD.setLocation(left_space, 10);
    	graphD.setSize(screen_x - left_space - right_space, screen_y - 25); 
    	range = ((int) Math.round((graphD.width - line_cap / 2) / scale)) + 1;
    	height_main = (int) Math.round(graphD.height * 0.4);
    	start_quan = (int) Math.round(graphD.height * 0.5);
    	height_quan = (int) Math.round(graphD.height * 0.16);
    	start_tech1 = (int) Math.round(graphD.height * 0.7);
    	height_tech1 = (int) Math.round(graphD.height * 0.15);
    	start_tech2 = (int) Math.round(graphD.height * 0.85);
    	height_tech2 = (int) Math.round(graphD.height * 0.15 - line_cap / 2);
    	avgPrev = new double[avgNum];
    	avgCurr = new double[avgNum];
    	k_pvalue = 50.0;
    	d_pvalue = 50.0;
    	bavgPrev = 0.0;
    	bavgCurr = 0.0;
    	
    	setData();

    	// 清除背景
		page.setColor(Color.BLACK);
		page.fillRect(0, 0, screen_x, screen_y);
    	
		int x = graphD.x + line_cap / 2;
		int scale_diff = (x + scale < graphD.x + graphD.width && graphD.x + graphD.width - x - scale > scale) ? scale : graphD.x + graphD.width - x;
		boolean drawQuan = sourceData.get(range_start + range - 1)[0] > 19870106 ? true : false;
		
		// 顯示指數資訊
		drawChartLabel(page, 0);

        // 顯示成交量資訊
        if (drawQuan)
        	drawChartLabel(page, 1);

        // 畫線圖、柱圖及顯示時間資訊
        while (time - range_start < range && time < sourceData.size()) {
        	now = sourceData.get(time);
        	if (stockId == 0)
        		drawLineChart(page, prev[1], now[1], x, scale_diff);
        	else if (displayType == TYPE_LINE_CHART )
        		drawLineChart(page, prev[4], now[4], x, scale_diff);
        	else if (displayType == TYPE_KBAR_CHART)
        		drawKBarChart(page, now, x, scale_diff);
        	else {
        		drawKBarChart(page, now, x, scale_diff);
        		drawLineChart(page, prev[4], now[4], x, scale_diff);
        	}

        	if (stockId != 0) {
	        	drawAvgLine(page, 1, 5, 0, time, x, scale_diff, Color.YELLOW);
	        	drawAvgLine(page, 2, 10, 0, time, x, scale_diff, Color.CYAN);
	        	drawAvgLine(page, 3, 20, 0, time, x, scale_diff, Color.MAGENTA);
        	}
        	
            if (drawQuan)
            	drawQuanChart(page, stockId == 0 ? now[3] : now[5], x, scale_diff);

            if (stockId != 0) {
	            drawAvgLine(page, 1, 5, 1, time, x, scale_diff, Color.YELLOW);
	            drawAvgLine(page, 2, 10, 1, time, x, scale_diff, Color.CYAN);
	            drawAvgLine(page, 3, 20, 1, time, x, scale_diff, Color.MAGENTA);
            }

            drawKDIndex(page, time, x, scale_diff, Color.PINK, Color.ORANGE);
//            if (dateType == 0)
            	drawBiasCurve(page, 10, time, x, scale_diff, Color.GREEN);
//            else if (dateType == 1)
//            	drawBiasCurve(page, 25, time, x, scale_diff, Color.GREEN);
//            else if (dateType == 2)
//            	drawBiasCurve(page, 72, time, x, scale_diff, Color.GREEN);
            
            x += scale_diff;

            if ((time - range_start) % (int) Math.round(graphD.width / scale / overall_size[2]) == 0 && (time - range_start)> 0 && x + overall_size[6] < graphD.x + graphD.width - line_cap / 2)
            	drawChartLabel(page, x, now[0]);

            prev = now;
            time++;
        }
        
        // 畫邊框
		drawCanvas(page);
		
		return image;
	}
    
    // 畫虛線
    private void drawBrokenLine (Graphics page, int x0, int y0, int x1, int y1) {
        int b_width;				// 虛線寬
    	int b_height;				// 虛線高
    	int tmpX;					// 曲線圖 x 軸分段繪製坐標
    	int tmpY;					// 曲線圖 y 軸分段繪製坐標
    	
    	b_width = (x1 - x0) / 5;
    	b_height = (y1 - y0) / 5;
    	tmpX = x0;
    	tmpY = y0;
    	
    	while (true) {
    		if (tmpX + (b_width == 0 ? 0 : 5) > x1 || tmpY + (b_height == 0 ? 0 : 5) > y1 || (b_width == 0 && b_height == 0))
    			break;
    		page.drawLine(tmpX, tmpY, tmpX + (b_width == 0 ? 0 : 5), tmpY + (b_height == 0 ? 0 : 5));
    		tmpX += (b_width == 0 ? 0 : 5) * 2;
    		tmpY += (b_height == 0 ? 0 : 5) * 2;
    	}
    }
    
    // 畫折線圖
    private void drawLineChart (Graphics page, double dataPrev, double dataCurr, int x, int scale) {
    	if (dataCurr < dataPrev)
    		page.setColor(Color.CYAN);
//    	else if (dataCurr == dataPrev)
//    		if (dataCurr == priceInitial[1])
//    			page.setColor(Color.RED);
//    		else if (dataCurr == priceInitial[priceNum])
//    			page.setColor(Color.GREEN);
//    		else;
    	else
    		page.setColor(Color.WHITE);
    	
        page.drawLine(x, calculateAxis(dataPrev, 0), x + scale, calculateAxis(dataCurr, 0));
    }
    
    // 畫 K 線圖
    private void drawKBarChart (Graphics page, double[] data, int x, int scale) {
		if (data[4] > data[1]) {
			page.setColor(Color.RED);
	        page.fillRect(x, calculateAxis(data[4], 0), scale, calculateAxis(data[1], 0) - calculateAxis(data[4], 0));
	        page.drawLine(x + (scale - 1) / 2, calculateAxis(data[2], 0), x + (scale - 1) / 2, calculateAxis(data[3], 0));
		}
		else if (data[4] < data[1]) {
			page.setColor(Color.GREEN);
	        page.fillRect(x, calculateAxis(data[1], 0), scale, calculateAxis(data[4], 0) - calculateAxis(data[1], 0));
	        page.drawLine(x + (scale - 1) / 2, calculateAxis(data[2], 0), x + (scale - 1) / 2, calculateAxis(data[3], 0));
		}
		else {
			page.setColor(Color.WHITE);
	        page.drawLine(x, calculateAxis(data[1], 0), x + scale, calculateAxis(data[1], 0));
	        page.drawLine(x + (scale - 1) / 2, calculateAxis(data[2], 0), x + (scale - 1) / 2, calculateAxis(data[3], 0));
		}
	}
    
    // 畫成交量圖
    private void drawQuanChart (Graphics page, double data, int x, int scale) {
    	int y = calculateAxis(data, 1);
        page.setColor(new Color(255, 156, 0));
        page.fillRect(x, y, scale, graphD.y + start_quan + height_quan - y);	// 畫柱圖
        page.setColor(Color.WHITE);
        page.drawLine(x += scale > 1 ? 0 : scale, y, x, graphD.y + start_quan + height_quan - line_cap);	// 畫柱圖分隔線
	}
    
    // 畫均線圖
    private void drawAvgLine (Graphics page, int count, int len, int drawType, int index, int x, int scale, Color color) {
    	int use = drawType * avgNum / 2 + count - 1;
    	if (index - range_start < len)
    		return;
    	else if (avgPrev[use] == 0.0) {
	    	for (int i = 0; i < len; i++)
	    		if (drawType == 0)
	    			avgPrev[use] += sourceData.get(index - i - 1)[4];
	    		else
	    			avgPrev[use] += sourceData.get(index - i - 1)[5];
    	}
    	
		if (drawType == 0)
			avgCurr[use] = avgPrev[use] + sourceData.get(index)[4] - sourceData.get(index - len)[4];
		else
			avgCurr[use] = avgPrev[use] + sourceData.get(index)[5] - sourceData.get(index - len)[5];
		
    	page.setColor(color);
        page.drawLine(x, calculateAxis(avgPrev[use] / len, drawType), x + scale, calculateAxis(avgCurr[use] / len, drawType));
        
        // 文字說明
        if (index  == range_start + range - 1)
	        if (drawType == 0)
	        	page.drawString("Avg" + len + "=" + nf.format(avgCurr[use] / len), graphD.x + graphD.width + line_cap, 
	        			graphD.y + start_main + line_cap + char_height * count);
	        else
	        	page.drawString("Avg" + len + "=" + nf.format(avgCurr[use] / len), graphD.x + graphD.width + line_cap, 
	        			graphD.y + start_quan + line_cap + char_height * count);
        
        avgPrev[use] = avgCurr[use];
    }
    
    // 畫均線圖
    private void drawKDIndex (Graphics page, int index, int x, int scale, Color c1, Color c2) {
    	if (index - range_start < kd_len)
    		return;
    	
    	for (int i = 0; i < kd_len; i++)
			kd_data[i + 1] = sourceData.get(index - i)[4];
    	kd_data[0] = kd_data[1];
    	Arrays.sort(kd_data, 1, kd_len + 1);
    	
    	rsv = (kd_data[0] - kd_data[1]) * 100.0 / (kd_data[kd_len] - kd_data[1]);
    	k_cvalue = (k_pvalue * 2 + rsv) / 3;
    	d_cvalue = (d_pvalue * 2 + k_cvalue) / 3;
    	
    	page.setColor(c1);
        page.drawLine(x, calculateAxis(k_pvalue, 2), x + scale, calculateAxis(k_cvalue, 2));
        page.setColor(c2);
        page.drawLine(x, calculateAxis(d_pvalue, 2), x + scale, calculateAxis(d_cvalue, 2));
        
        if (index  == range_start + range - 1) {
        	int tx;
        	int ty;
        	
        	page.setColor(Color.DARK_GRAY);
        	drawBrokenLine(page, graphD.x, calculateAxis(80, 2), graphD.x + graphD.width, calculateAxis(80, 2)); // 畫橫虛線
        	drawBrokenLine(page, graphD.x, calculateAxis(20, 2), graphD.x + graphD.width, calculateAxis(20, 2)); // 畫橫虛線
        	page.setColor(Color.WHITE);
        	page.drawString("80", 0, calculateAxis(80, 2) + char_height / 2);
        	page.drawString("20", 0, calculateAxis(20, 2) + char_height / 2);
        	
        	tx = graphD.x + graphD.width + line_cap;
        	ty = graphD.y + start_tech1 + line_cap;
        	page.drawString("RSV" + kd_len + "=" + nf.format(rsv), tx, ty);
        	page.drawString("K-D" + "=" + nf.format(k_cvalue - d_cvalue), tx, ty + char_height * 3);
        	page.setColor(c1);
        	page.drawString("K" + (int) Math.sqrt(kd_len) + "=" + nf.format(k_cvalue), tx, ty + char_height * 1);
        	page.setColor(c2);
        	page.drawString("D" + (int) Math.sqrt(kd_len) + "=" + nf.format(d_cvalue), tx, ty + char_height * 2);
        }
        
        k_pvalue = k_cvalue;
        d_pvalue = d_cvalue;
    }

    private void drawBiasCurve (Graphics page, int len, int index, int x, int scale, Color color) {
    	if (index - range_start < len)
    		return;
    	
    	if (bavgPrev == 0.0) {
    		for (int i = 0; i < len; i++)
    			bavgPrev += sourceData.get(index - i - 1)[4];
    		biasPrev = bavgPrev / len * 1.0;
    		biasPrev = (sourceData.get(index)[4] - biasPrev) / biasPrev; 
    	}
    	
		bavgCurr = bavgPrev + sourceData.get(index)[4] - sourceData.get(index - len)[4];
		
		biasCurr = bavgCurr / len * 1.0;
		biasCurr = (sourceData.get(index)[4] - biasCurr) / biasCurr; 
        
        // 文字說明
        if (index  == range_start + range - 1) {
        	int y1;
        	int y2;
        	
        	page.setColor(color);
        	page.drawString("Bias" + len + "=" + nf.format(biasCurr * 100) + "%", graphD.x + graphD.width + line_cap, 
        			graphD.y + start_tech2 + line_cap + char_height);
        	
        	page.setColor(Color.WHITE);
        	if (dateType == 0) {
            	y1 = calculateAxis(0.05, 3);
            	y2 = calculateAxis(-0.045, 3);
        		page.drawString("5%", 0, y1 + char_height / 2);
        		page.drawString("-4.5%", 0, y2 + char_height / 2);
        	} else if (dateType == 1) {
            	y1 = calculateAxis(0.125, 3);
            	y2 = calculateAxis(-0.1125, 3);
        		page.drawString("12.5%", 0, y1 + char_height / 2);
        		page.drawString("-11.25%", 0, y2 + char_height / 2);
        	} else if (dateType == 2) {
            	y1 = calculateAxis(0.25, 3);
            	y2 = calculateAxis(-0.225, 3);
        		page.drawString("25%", 0, y1 + char_height / 2);
        		page.drawString("-22.5%", 0, y2 + char_height / 2);
        	} else {
            	y1 = calculateAxis(0.05, 3);
            	y2 = calculateAxis(-0.045, 3);
        	}

        	page.setColor(Color.DARK_GRAY);
        	drawBrokenLine(page, graphD.x, y1, graphD.x + graphD.width, y1); // 畫橫虛線
        	drawBrokenLine(page, graphD.x, y2, graphD.x + graphD.width, y2); // 畫橫虛線
        }
        
        page.setColor(color);
        page.drawLine(x, calculateAxis(biasPrev, 3), x + scale, calculateAxis(biasCurr, 3));
        
        bavgPrev = bavgCurr;
        biasPrev = biasCurr;
    }
    
    // 計算KD值
    private int calculateAxis (double data, int calType) {	// 0:指數 1:成交量 2:KD 3:Bias
    	switch(calType) {
    	case 0:
    		if (data < 0)
    			return (int) Math.round((height_main) / (priceNum - 1) * -1.0 * (data + 1) + 
    					graphD.y + start_main + char_height / 2);
   			ratio = (priceInitial[1] - data) / priceSep;
        	return (int) Math.round((height_main) / (priceNum - 1) * 1.0 * ratio + graphD.y + start_main);
    	case 1:
    		if (data < 0)
    			return (int) Math.round(height_quan / (quanNum - 1) * -1.0 * (data + 1) + 
        				graphD.y + start_quan + char_height / 2);
       		ratio = (quanInitial[1] - data) / quanSep;
        	return (int) Math.round(height_quan / (quanNum - 1) * ratio + graphD.y + start_quan);
    	case 2:
    		return (int) Math.round(height_tech1 / 100.0 * (100 - data) + graphD.y + start_tech1);
    	case 3:
    		if (dateType == 0)
    			return (int) Math.round(height_tech2 / 20.0 * (10 - data * 100) + graphD.y + start_tech2);
    		else if (dateType == 1)
    			return (int) Math.round(height_tech2 / 50.0 * (25 - data * 100) + graphD.y + start_tech2);
    		else if (dateType == 2)
    			return (int) Math.round(height_tech2 / 100.0 * (50 - data * 100) + graphD.y + start_tech2);
    		else
    			return 0;
		default:
			return 0;
    	}
    }
    
    // 標示線圖資訊 (指數及成交量)
    private void drawChartLabel (Graphics page, int drawType) {	// 0:指數 1:成交量
    	int y;
    	int showNum = drawType == 0 ? priceNum : quanNum;
    	double[] showString = drawType == 0 ? priceInitial : quanInitial;
    	
		for (int i = 0; i < showNum; i++) {
			if (drawType == 0)
	    		if (i == 0 && showString[1] > showString[0] * 1.07 - calculateUnit(showString[0]))
	                page.setColor(Color.RED);
	    		else if (i == priceNum - 1 && showString[priceNum] < showString[0] * 0.93 + calculateUnit(showString[0]))
	                page.setColor(new Color(0, 128, 0));
	    		else
	                page.setColor(Color.WHITE);
			else
				page.setColor(new Color(255, 156, 0));
			
			if (i < showNum - 1 || drawType != 1)
				page.drawString(nf.format(showString[i + 1]), 0, calculateAxis(-1.0 * i - 1, drawType));
			
			if (drawType == 1)
				page.setColor(Color.DARK_GRAY);
			else if (i == 0)
				page.setColor(new Color(128, 0, 0));
			else if (i == (showNum - 1) / 2)
				page.setColor(Color.LIGHT_GRAY);
			else if (i == showNum - 1)
				page.setColor(new Color(0, 128, 0));
			else
				page.setColor(Color.DARK_GRAY);
			
			y = calculateAxis(showString[i + 1], drawType);
			drawBrokenLine(page, graphD.x, y, graphD.x + graphD.width, y); // 畫橫虛線
		}
		
		if (drawType == 1) {
			page.setColor(new Color(255, 156, 0));
			page.drawString(stockId == 1000 || stockId == 0 || stockId == 4000? "億(新台幣)" : "張", 
					0, graphD.y + start_quan + height_quan + char_height / 2);
		}
    }
    
    // 標示線圖資訊 (時間)
    private void drawChartLabel (Graphics page, int x, double date) {
    	page.setColor(Color.LIGHT_GRAY);
    	page.drawString(numToDate(date), x - overall_size[13], screen_y - 1);	// 秀出時間資訊
    	page.setColor(Color.DARK_GRAY);
    	drawBrokenLine(page, x, graphD.y, x, graphD.y + graphD.height);	// 畫縱虛線
    }
    
    private void drawCanvas (Graphics2D page) {
        page.setPaint(Color.BLUE);
        page.setStroke(new BasicStroke(line_cap, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND));
        page.draw(new Line2D.Double(graphD.x, graphD.y, graphD.x, graphD.y + graphD.height));	// 畫左框
        page.draw(new Line2D.Double(graphD.x + graphD.width, graphD.y, graphD.x + graphD.width, graphD.y + graphD.height));	// 畫右框
        page.draw(new Line2D.Double(graphD.x, graphD.y, graphD.x + graphD.width, graphD.y)); 	// 畫上框
        page.draw(new Line2D.Double(graphD.x, graphD.y + graphD.height, graphD.x + graphD.width, graphD.y + graphD.height)); 	// 畫下框
        page.setStroke(new BasicStroke(1, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND));
    }
    
    // 計算升降單位
    private static double calculateUnit (double data) {
    	double unit;
    	
    	if (data < 10.0)
    		unit = 0.01;
    	else if (data < 50.0)
    		unit = 0.05;
    	else if (data < 100.0)
    		unit = 0.1;
    	else if (data < 500.0)
    		unit = 0.5;
    	else if (data < 1000.0)
    		unit = 1.0;
    	else
    		unit = 5.0;
    	
    	return unit;
    }
    
    // 產生日期格式
    private String numToDate (double date) {
    	date_mark[0] = String.valueOf((int) date);
    	date_mark[2] = date_mark[0].substring(6);
    	date_mark[1] = date_mark[0].substring(4, 6);
    	date_mark[0] = date_mark[0].substring(0, 4); 
    	
    	return date_mark[0].concat("/").concat(date_mark[1]).concat("/").concat(date_mark[2]);
    }
    
    public void destroy() {
    	;
    }
}
