<%@ page contentType="text/html; charset=big5" language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5" />
<title>網頁操作說明</title>
<style type="text/css">
<!--
a:link {
	color: #999999;
	text-decoration: none;
}
a:visited {
	color: #999999;
	text-decoration: none;
}
a:hover {
	color: #999999;
	text-decoration: underline;
}
a:active {
	color: #999999;
	text-decoration: none;
}
.style1 {
	color: #FFFFFF;
	font-weight: normal;
}
.style3 {color: #0000FF}
.style6 {color: #999999}
.style7 {font-size: 12}
.style10 {color: #999999; font-size: 12px; }
.style13 {color: #FFFFFF; font-weight: bold; }
-->
</style>
</head>

<body>

<p align="center"><span class="style10"><a name="t" id="t"></a><a href="#1">登入</a> ｜ <a href="#2">頁面說明</a> ｜ <a href="#3">大盤</a> ｜ <a href="#4">個股查詢</a> ｜ <a href="#5">盤後資訊</a> ｜ <a href="#6">交易</a> ｜ <a href="#7">會員資料</a></span></p>
<table width="80%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
  <tbody>
    <tr bgcolor="#CCCCCC">
            <td colspan="6" align="center"><span class="style13">網 頁 操 作 說 明</span></td>
    </tr>
    <tr align="center" bgcolor="#FFFFCC">
      <td colspan="6" align="center">登入<a name="1" id="1"></a></td>
    </tr>
<tr bgcolor="#FFFFFF">
<td colspan="6" align="center"><p><strong>開啟瀏覽器，輸入網址<span class="style3"> http://XDDDDD.twbbs.org/</span></strong><br />
    <img src="/images/doc/1.jpg" width="343" height="246" /><br />
        <strong>第一次登入請先點選「加入會員」，填寫下列資料申請會員</strong><br />
        <img src="/images/doc/2.jpg" width="341" height="222" /><br />
        <strong>成功申請會員之後，用帳號密碼重新登入。</strong></p>
  <p align="right"><a href="#t">top</a><br />
      <br />
  </p></td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center" bgcolor="#FFFFCE"><a name="2" id="2"></a>頁面說明</td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center"><p><strong>登入後即顯示首頁如下：<br />
        <img src="/images/doc/3.jpg" width="544" height="318" /></strong></p>
    <p align="right"><a href="#t">top</a></p></td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center" bgcolor="#FFFFCE"><a name="3" id="3"></a>大盤</td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center"><p><strong>大盤分為「集中市場大盤」以及「上櫃股票大盤」<br />
    </strong>（集中市場：台灣證券交易為提供有價證券之競價買賣所開設之市場，稱之。）<br />
        （上櫃股票：由中華民國證券櫃檯買賣中心所開設之「櫃檯市場」。）<br />
      <br />
        <strong>下圖顯示交易日當天之大盤走勢，圖下的各數值表交易詳細資料，分別有櫃檯指數、最高價最低價、成交金額張數筆數、委買張數筆數以及委賣張數筆數。</strong><br />
    <img src="/images/doc/4.jpg" width="375" height="302" /></p>
    <p align="right"><a href="#t">top</a></p></td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center" bgcolor="#FFFFCE"><a name="4" id="4"></a>個股查詢</td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center"><p><strong>頁面上方之個股查詢下拉選單處，輸入股票代碼即可查詢個股當日股價、個股當日走勢圖、個股當日成交明細、公司基本資料、技術分析的籌碼分析與Ｋ線圖。<br />
    若不知道股票代碼，可點選右邊「股票代碼查詢」進行查詢：<br />
    查詢方式分為兩種，一為關鍵字輸入，二為類股的選擇</strong><br />  
        <img src="/images/doc/5.jpg" width="522" height="243" /><br />
        <strong><br />
        當日個股股價：顯示當日各股即時交易資訊</strong><br />
        <img src="/images/doc/6.jpg" width="465" height="163" /><br />
        <strong>當日個股走勢：顯示當日個股即時走圖線圖</strong><br />
        <img src="/images/doc/7.jpg" width="441" height="250" /><br />
        <strong>個股成交明細：顯示當日個股各個時間的成交資訊，可選擇顯示十筆或五十筆<br />
        <img src="/images/doc/8.jpg" width="447" height="254" /></strong><br />
        <strong>公司基本資料：分三部分，公司資料、股利狀況與營收盈餘表，供使用者做基本分析用</strong><br />
        <img src="/images/doc/9.jpg" width="546" height="405" /><br />
        <strong><br />
        個股籌碼分析：提供個股當日主力進出資訊</strong><br />
        <img src="/images/doc/10.jpg" width="500" height="371" /><br />
        <br />
      </p>
    <p align="right"><a href="#t">top</a></p></td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center" bgcolor="#FFFFCE"><a name="5" id="5"></a>盤後資訊</td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center"><p><strong>歷史股價Ｋ線圖：以歷史股價資料繪成之Ｋ線圖，分為日線周線月線，供使用者做技術分析參考</strong><br />
      <img src="/images/doc/11.jpg" width="412" height="358" /></p>
    <p align="right"><a href="#t">top</a></p></td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center" bgcolor="#FFFFCE"><a name="6" id="6"></a>交易</td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center"><p><strong>點選左方交易專區的「下單」開始進行股票買賣。</strong><br />
    <img src="/images/doc/12.jpg" width="412" height="204" /><br />
          <strong>輸入欲買進或賣出之股票代號、下單價位、張數以及希望交割之期限，最後按下確定送出。</strong><br />
            <br />
              <strong>投資組合：目前使用者所持有之所有股票資訊。<br />
            交易記錄一覽：記錄使用者到目前為止之所有交易記錄，包含交易成功與失敗的記錄。</strong><br />
  </p>
    <p align="right"><a href="#t">top</a></p></td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center" bgcolor="#FFFFCE"><a name="7" id="7"></a>會員資料</td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center"><p><strong>點選左方會員專區的「個人資料修改」可修改使用者資料。<br />
    <img src="/images/doc/13.jpg" width="382" height="277" /></strong><br />
      <strong>若要修改密碼，請另外點選「修改密碼」。</strong></p>
    <p align="right"><a href="#t">top</a></p></td>
</tr>
  </tbody>
</table>
</body>
</html>
