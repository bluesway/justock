<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@ include file="/Connections/justockdb.jsp" %>
<%request.setCharacterEncoding("utf-8");%>
<jsp:useBean id="timeBean" class="common.util.TimeUtil"/>
<jsp:useBean id="stocksearch" class="website.util.StockSearch"/>

<%

int tempid = stocksearch.getStockId(request.getParameter("stockId"));
if (tempid == -1) {
		out.print("<body bgcolor=\"#e7f3ff\">");
 		out.print("<CENTER><font color=\"#ff0000\">沒有這支股票 請重新輸入</font></center>");
		out.print("</body>");
		return;

}
else
	session.putValue("stockId",String.valueOf(tempid));

String stock__MMColParam = "1";
//session.putValue("stockId",request.getParameter("stockId"));
if (session.getValue("stockId") !=null) {stock__MMColParam = (String)session.getValue("stockId");}
%>
<%
Driver Driverstock = (Driver)Class.forName(MM_justockdb_DRIVER).newInstance();
Connection Connstock = DriverManager.getConnection(MM_justockdb_STRING,MM_justockdb_USERNAME,MM_justockdb_PASSWORD);
PreparedStatement Statementstock = Connstock.prepareStatement("SELECT Sid, Sname  FROM justock.stock  WHERE Sid LIKE '" + stock__MMColParam + "'");
ResultSet stock = Statementstock.executeQuery();
boolean stock_isEmpty = !stock.next();
boolean stock_hasData = !stock_isEmpty;
Object stock_data;
int stock_numRows = 0;
%>
<%
String today__MMColParam = "1";
//session.putValue("stockId",request.getParameter("stockId"));
if (session.getValue("stockId") !=null) {today__MMColParam = (String)session.getValue("stockId");}
%>
<%
Driver Drivertoday = (Driver)Class.forName(MM_justockdb_DRIVER).newInstance();
Connection Conntoday = DriverManager.getConnection(MM_justockdb_STRING,MM_justockdb_USERNAME,MM_justockdb_PASSWORD);
PreparedStatement Statementtoday = Conntoday.prepareStatement("SELECT tSid, tTime, tChg, tPrice, tQuan, tBPrice, tSPrice  FROM justock.info_today  WHERE tSid LIKE '" + today__MMColParam + "'  ORDER BY tTime DESC");
ResultSet today = Statementtoday.executeQuery();
boolean today_isEmpty = !today.next();
boolean today_hasData = !today_isEmpty;
Object today_data;
int today_numRows = 0;
	
%>
<%
int Repeat1__numRows = 50;
int Repeat1__index = 0;
today_numRows += Repeat1__numRows;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<title>個股成交明細</title>
<style type="text/css">
<!--
.style1 {color: #FFFFFF}
.style3 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 16px;
}
.style10 {color: #000066}
body {
	background-color: #e7f3ff;
}
.style11 {font-size: 15px}
.style12 {font-family: "新細明體"}
-->
</style>
</head>

<body>
<div>
    <table width="80%" border="0" align="center" cellpadding="2" cellspacing="1" bgcolor="#CCCCCC">
      <tbody>
        <tr bgcolor="#CCCCCC">
          <td colspan="6" align="center" class="style1"> <span class="style3"><span class="style10"><%=(((stock_data = stock.getObject("Sid"))==null || stock.wasNull())?"":stock_data)%> <%=(((stock_data = stock.getObject("Sname"))==null || stock.wasNull())?"":stock_data)%></span>　成 交 明 細</span></td>
        </tr>
        
        <tr align="center" bgcolor="#FFFFCC">
          <td width="20%" align="center"><span class="style11">時間</span></td>
          <td width="16%" align="center"><span class="style11">成交價</span></td>
          <td width="16%" align="center"><span class="style11">買價</span></td>
          <td width="16%" align="center"><span class="style11">賣價</span></td>
          <td width="16%" align="center"><span class="style11">漲跌</span></td>
          <td width="16%" align="center"><span class="style11">單量 (張)</span></td>
        </tr>
			  <% while ((today_hasData)&&(Repeat1__numRows-- != 0)) { 
			  int teddy = Integer.parseInt(String.valueOf(((today_data = today.getObject("tTime"))==null || today.wasNull())?"":today_data));%>
        <tr align="center" bgcolor="#ffffff" height="25">
          <td><span class="style12"><%=timeBean.transTime(teddy)%></span></td>
          <td><span class="style12"><%=(((today_data = today.getObject("tPrice"))==null || today.wasNull())?"":today_data)%></span></td>
          <td><span class="style12"><%=(((today_data = today.getObject("tBPrice"))==null || today.wasNull())?"":today_data)%></span></td>
          <td><span class="style12"><%=(((today_data = today.getObject("tSPrice"))==null || today.wasNull())?"":today_data)%></span></td>
          <td><span class="style12"><%=(((today_data = today.getObject("tChg"))==null || today.wasNull())?"":today_data)%></span></td>
          <td><span class="style12"><%=(((today_data = today.getObject("tQuan"))==null || today.wasNull())?"":today_data)%></span></td>
    </tr>    <%
  Repeat1__index++;
  today_hasData = today.next();
}
%>
      </tbody>
  </table>

</div>
</body>
</html>
<%
stock.close();
Statementstock.close();
Connstock.close();
%>
<%
today.close();
Statementtoday.close();
Conntoday.close();
%>