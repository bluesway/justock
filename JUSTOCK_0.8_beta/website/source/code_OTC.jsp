<%@ page contentType="text/html; charset=big5" language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5" />
<meta http-equiv="Pragma" content="no-cache" />
<title>股票代碼查詢 - 上櫃</title>
<style type="text/css">
<!--
a:hover {
	color: #0000CC;
	font-size: 22px;
	text-decoration: none;
}
a:link {
	color: #0000CC;
	text-decoration: none;
}
a:visited {
	color: #0000CC;
	text-decoration: none;
}
a:active {
	color: #FF0000;
	text-decoration: none;
}
.style1 {
	color: #0000cc;
	font-weight: bold;
	font-size: 16px;
}
.style2 {color: #000000}
.style5 {font-size: 16px; color: #0000cc; }
body {
	background-color: #e7f3ff;
}
-->
</style>
</head>

<body>
<form id="form1" name="form1" method="post" action="">
<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center' height='28'>
<tr align='center' bgcolor='#CCCCCC'>
  <td width='100%' colspan='2' class="style2"><table width='100%' border='0' cellspacing='1' cellpadding='4'>
    <tr align=CENTER bgcolor=#FFFFFF>
      <td height=25 rowspan=3 align=center nowrap="nowrap" class=c3 bgcolor='#FFFFCC'><a href="/code_TSE.jsp" target="_self" class="style5">上市</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=100" target="bottomFrame" class="style5">食品</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=101" target="bottomFrame" class="style5">塑膠</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=102" target="bottomFrame" class="style5">紡織</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=103" target="bottomFrame" class="style5">電機</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=104" target="bottomFrame" class="style5">電器</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=105" target="bottomFrame" class="style5">化工</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=106" target="bottomFrame" class="style5">生技</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=107" target="bottomFrame" class="style5">玻璃</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=108" target="bottomFrame" class="style5">鋼鐵</a></td>
      <td height=25 rowspan=3 align=center nowrap="nowrap" bgcolor='#FFFFCC' class=c3 style1><span class="style1">上櫃</span></td>
    </tr>
    <tr align=CENTER bgcolor=#FFFFFF>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=109" target="bottomFrame" class="style5">橡膠</a></td>
      <td class="c3"  height="25" align="center"><a href="/code1.jsp?class=110" target="bottomFrame" class="style5">半導</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=111" target="bottomFrame" class="style5">電腦</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=112" target="bottomFrame" class="style5">光電</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=113" target="bottomFrame" class="style5">通信</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=114" target="bottomFrame" class="style5">電零</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=115" target="bottomFrame" class="style5">通路</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=116" target="bottomFrame" class="style5">資服</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=117" target="bottomFrame" class="style5">他電</a></td>
    </tr>
    <tr align=CENTER bgcolor=#FFFFFF>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=118" target="bottomFrame" class="style5">營建</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=119" target="bottomFrame" class="style5">航運</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=120" target="bottomFrame" class="style5">觀光</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=121" target="bottomFrame" class="style5">金融</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=122" target="bottomFrame" class="style5">貿易</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=123" target="bottomFrame" class="style5">油電</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=124" target="bottomFrame" class="style5">其他</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3"><a href="/code1.jsp?class=125" target="bottomFrame" class="style5">管理</a></td>
      <td  height="25" align="center" nowrap="nowrap" class="c3 style2 style5">&nbsp;</td>
    </tr>
  </table></td>
</tr>
</table>
</form>
</body>
</html>
