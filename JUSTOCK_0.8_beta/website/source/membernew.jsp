﻿<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<% request.setCharacterEncoding("utf-8"); %>
<%@ include file="Connections/justockdb.jsp" %>
<jsp:useBean id="pm" class="common.util.PwdMD5"/>


<%
// *** Redirect if username exists
String MM_flag="MM_insert";
if (request.getParameter(MM_flag) != null) {
  String MM_dupKeyRedirect="/dmemberpage.jsp";
  String MM_rsKeyConnection=MM_justockdb_STRING;
  String MM_dupKeyUsernameValue = request.getParameter("Uid");
  String MM_dupKeySQL = "SELECT Uid FROM justock.account WHERE Uid='" + MM_dupKeyUsernameValue + "'";
  Driver MM_rsKeyDriver = (Driver)Class.forName(MM_justockdb_DRIVER).newInstance();
  Connection MM_rsKeyConn = DriverManager.getConnection(MM_rsKeyConnection,MM_justockdb_USERNAME,MM_justockdb_PASSWORD);
  PreparedStatement MM_rsKeyStatement = MM_rsKeyConn.prepareStatement(MM_dupKeySQL);
  ResultSet MM_rsKey = MM_rsKeyStatement.executeQuery();
  boolean MM_rsKey_isEmpty = !MM_rsKey.next();
  MM_rsKey.close(); // Close the recordset - we have all the info we need.
  MM_rsKeyConn.close();
  if (!MM_rsKey_isEmpty) {
    // the username was found - can not add the requested username
    String MM_qsChar = "?";
    if (MM_dupKeyRedirect.indexOf("?") >= 0) MM_qsChar = "&";
    MM_dupKeyRedirect = MM_dupKeyRedirect + MM_qsChar + "requsername=" + MM_dupKeyUsernameValue;
    response.sendRedirect(response.encodeRedirectURL(MM_dupKeyRedirect));
    return;
  }
}

%>

<%
// *** Edit Operations: declare variables

// set the form action variable
String MM_editAction = request.getRequestURI();
if (request.getQueryString() != null && request.getQueryString().length() > 0) {
  String queryString = request.getQueryString();
  String tempStr = "";
  for (int i=0; i < queryString.length(); i++) {
    if (queryString.charAt(i) == '<') tempStr = tempStr + "&lt;";
    else if (queryString.charAt(i) == '>') tempStr = tempStr + "&gt;";
    else if (queryString.charAt(i) == '"') tempStr = tempStr +  "&quot;";
    else tempStr = tempStr + queryString.charAt(i);
  }
  MM_editAction += "?" + tempStr;
}

// connection information
String MM_editDriver = null, MM_editConnection = null, MM_editUserName = null, MM_editPassword = null;

// redirect information
String MM_editRedirectUrl = null;

// query string to execute
StringBuffer MM_editQuery = null;

// boolean to abort record edit
boolean MM_abortEdit = false;

// table information
String MM_editTable = null, MM_editColumn = null, MM_recordId = null;

// form field information
String[] MM_fields = null, MM_columns = null;
%>

<%
// *** Insert Record: set variables

if (request.getParameter("MM_insert") != null && request.getParameter("MM_insert").toString().equals("form1")) {
  MM_editDriver     = MM_justockdb_DRIVER;
  MM_editConnection = MM_justockdb_STRING;
  MM_editUserName   = MM_justockdb_USERNAME;
  MM_editPassword   = MM_justockdb_PASSWORD;
  MM_editTable  = "justock.account";
  MM_editRedirectUrl = "welcome.jsp";
  String MM_fieldsStr = "Uid|value|uPwd|value|uName|value|uIdnum|value|uGender|value|uPhone|value|uAddr|value|uEmail|value";
  String MM_columnsStr = "Uid|',none,''|uPwd|',none,''|uName|',none,''|uIdnum|',none,''|uGemder|',none,''|uPhone|',none,''|uAddr|',none,''|uEmail|',none,''";

  // create the MM_fields and MM_columns arrays
  java.util.StringTokenizer tokens = new java.util.StringTokenizer(MM_fieldsStr,"|");
  MM_fields = new String[tokens.countTokens()];
  for (int i=0; tokens.hasMoreTokens(); i++) MM_fields[i] = tokens.nextToken();

  tokens = new java.util.StringTokenizer(MM_columnsStr,"|");
  MM_columns = new String[tokens.countTokens()];
  for (int i=0; tokens.hasMoreTokens(); i++) MM_columns[i] = tokens.nextToken();

  // set the form values
  for (int i=0; i+1 < MM_fields.length; i+=2) {
    MM_fields[i+1] = ((request.getParameter(MM_fields[i])!=null)?(String)request.getParameter(MM_fields[i]):"");
  }
 
  // append the query string to the redirect URL
  if (MM_editRedirectUrl.length() != 0 && request.getQueryString() != null) {
    MM_editRedirectUrl += ((MM_editRedirectUrl.indexOf('?') == -1)?"?":"&") + request.getQueryString();
  }
}
%>

<%
// *** Insert Record: construct a sql insert statement and execute it

if (request.getParameter("MM_insert") != null) {

  // create the insert sql statement
  StringBuffer MM_tableValues = new StringBuffer(), MM_dbValues = new StringBuffer();
  for (int i=0; i+1 < MM_fields.length; i+=2) {
    String formVal = MM_fields[i+1];
    String elem;
    java.util.StringTokenizer tokens = new java.util.StringTokenizer(MM_columns[i+1],",");
    String delim    = ((elem = (String)tokens.nextToken()) != null && elem.compareTo("none")!=0)?elem:"";
    String altVal   = ((elem = (String)tokens.nextToken()) != null && elem.compareTo("none")!=0)?elem:"";
    String emptyVal = ((elem = (String)tokens.nextToken()) != null && elem.compareTo("none")!=0)?elem:"";
	if (i==2) {
	//out.println(formVal);
	  pm.setPass(formVal);
	  formVal = pm.getEncryptedPass();
	}
    if (formVal.length() == 0) {
      formVal = emptyVal;
    } else {
      if (altVal.length() != 0) {
        formVal = altVal;
      } else if (delim.compareTo("'") == 0) {  // escape quotes
        StringBuffer escQuotes = new StringBuffer(formVal);
        for (int j=0; j < escQuotes.length(); j++)
          if (escQuotes.charAt(j) == '\'') escQuotes.insert(j++,'\'');
        formVal = "'" + escQuotes + "'";
      } else {
        formVal = delim + formVal + delim;
      }
    }
    MM_tableValues.append((i!=0)?",":"").append(MM_columns[i]);
    MM_dbValues.append((i!=0)?",":"").append(formVal);
  }
  MM_editQuery = new StringBuffer("insert into " + MM_editTable);
  MM_editQuery.append(" (").append(MM_tableValues.toString()).append(") values (");
  MM_editQuery.append(MM_dbValues.toString()).append(")");
  
  if (!MM_abortEdit) {
    // finish the sql and execute it
    Driver MM_driver = (Driver)Class.forName(MM_editDriver).newInstance();
    Connection MM_connection = DriverManager.getConnection(MM_editConnection,MM_editUserName,MM_editPassword);
    PreparedStatement MM_editStatement = MM_connection.prepareStatement(MM_editQuery.toString());
    MM_editStatement.executeUpdate();

    Statement stmt = MM_connection.createStatement();
	String uid = request.getParameter("Uid");
    stmt.executeUpdate("INSERT INTO bank VALUES ('000', '" + uid + "', '" + String.valueOf(System.currentTimeMillis()).concat(String.valueOf(System.currentTimeMillis())).substring(0, 15) + "', " + 5000000.0 + ")");
    stmt.close();
	
    MM_connection.close();

    // redirect with URL parameters
    if (MM_editRedirectUrl.length() != 0) {
      response.sendRedirect(response.encodeRedirectURL(MM_editRedirectUrl));
      return;
    }
  }
}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>會員申請</title>
<style type="text/css">
<!--
.style11 {
	color: #000000;
	font-weight: bold;
}
.style5 {color: #000000}
.style12 {color: #FF0000}
.style13 {color: #FFFFFF}
.style14 {font-size: 15px}
.style15 {color: #000000; font-size: 15px; }
.style16 {
	font-size: 14px;
	color: #666666;
}
.style17 {color: #FF0000; font-size: 15px; }
.style18 {font-size: 16px}
body {
	background-color: #e7f3ff;
}
a:link {
	color: #0000cc;
}
a:visited {
	color: #0000CC;
}
a:hover {
	color: #0000CC;
}
a:active {
	color: #0000CC;
}
-->
</style>
<script type="text/JavaScript">
<!--

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { 
  	test=args[i+2];
	val=MM_findObj(args[i]);
    if (val) { 
		nm=val.name; 
		if ((val=val.value)!="") {
      		if (test.indexOf('isEmail')!=-1) { 
				p=val.indexOf('@');
        		if (p<1 || p==(val.length-1)) 
					errors+='- '+nm+' must contain an e-mail address.\n';
      		} else if (test!='R') { 
				num = parseFloat(val);
        		if (isNaN(val)) 
					errors+='- '+nm+' must contain a number.\n';
        		if (test.indexOf('inRange') != -1) { 
					p=test.indexOf(':');
          			min=test.substring(8,p); max=test.substring(p+1);
          		if (num<min || max<num) 
					errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    		} 
		} 
	} else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } 
  if (errors) 
  	alert('下列資料未填:\n'+errors);
  
  document.MM_returnValue = (errors == '');
}
//-->
</script>
</head>

<body>

<% request.setCharacterEncoding("UTF-8"); %>
<form action="<%=MM_editAction%>" method="POST" name="form1" id="form1" onsubmit="MM_validateForm('Uid','','R','uName','','R','uIdnum','','R','uPhone','','NisNum','uEmail','','RisEmail','uPwd','','R');return document.MM_returnValue">

  <table width="50%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
    <tr>
      <td colspan="2" class="style11 style13" > <div align="center" class="style18">請 填 寫 下 列 會 員 資 料</div></td>
    </tr>
    <tr>
      <td width="138" align="center" bgcolor="#FFFFCC"><font class="mbody style5 style14" bgcolor="#FFFFCC">帳　　　號</font></td>
      <td width="327" bgcolor="#FFFFFF"><input name="Uid" type="text" id="Uid" onblur="MM_validateForm('m_id','','R','m_pw','','R','m_name','','R','m_idnum','','R','m_tel','','NisNum','m_email','','RisEmail');return document.MM_returnValue" size="20" maxlength="15" />　<span class="style14"><span class="style17">*</span></span><span class="style12"><br />
      </span><span class="style16">（請填入3至20個字元的小寫英文字母以及數字，第一個字元需為英文字母）</span></td>
    </tr>
    <tr>
      <td align="center" bgcolor="#FFFFCC"><font class="mbody style5 style14" bgcolor="#FFFFCC">密　　　碼</font></td>
      <td bgcolor="#FFFFFF"><p><font color="cc3300" class="mbody"><em>
        <input name="uPwd" type="password" id="uPwd" size="20" maxlength="15" />
          </a>　</em></font><span class="style17">*</span><font class="mbody"><span class="style12"><br />
          </span></font><span class="style16">（請輸入6至20個字元的英文、數字組合，不可使用空白鍵及「”」以及「&amp;」）</span> </p>
        </td>
    </tr>

    <tr>
      <td align="center" nowrap="nowrap" bgcolor="#FFFFCC"><font class="mbody style5 style14" bgcolor="#FFFFCC"> 暱　　　稱</font></td>
      <td width="327" bgcolor="#FFFFFF"><input name="uName" type="text" id="uName" size="20" maxlength="10" />　
      <font class="mbody"><span class="style17">*</span></font></td>
    </tr>
    <tr>
      <td align="center" bgcolor="#FFFFCC"><font class="mbody style5 style14" bgcolor="#FFFFCC">身份證字號</font></td>
      <td bgcolor="#FFFFFF"><input name="uIdnum" type="text" id="uIdnum" size="15" maxlength="10" />　<font class="mbody"><span class="style17">*</span></font></td>
    </tr>
    <tr>
      <td align="center" bgcolor="#FFFFCC"><font class="mbody style5 style14" bgcolor="#FFFFCC"> 性　　　別</font></td>
      <td width="327" valign="top" height="19" bgcolor="#FFFFFF"><input name="uGender" type="radio" value="1" checked="checked" />
男
  <input name="uGender" type="radio" value="2" />
女</td>
    </tr>
    <tr>
      <td align="center" bgcolor="#FFFFCC"><span class="style15" bgcolor="#FFFFCC">電　　　話</span></td>
      <td bgcolor="#FFFFFF"><font color="#2C8383">
        <input name="uPhone" type="text" id="uPhone" size="15" maxlength="15" />
      </font></td>
    </tr>
    <tr>
      <td align="center" bgcolor="#FFFFCC"><font class="mbody style5 style14" bgcolor="#FFFFCC">地　　　址</font></td>
      <td bgcolor="#FFFFFF"><input name="uAddr" type="text" id="uAddr" size="42" maxlength="20" /></td>
    </tr>
    <tr>
      <td align="center" bgcolor="#FFFFCC"><span class="style15" bgcolor="#FFFFCC"> E-mail 信箱 </span></td>
      <td bgcolor="#FFFFFF"><font color="#2C8383">
        <input name="uEmail" type="text" id="uEmail" size="35" maxlength="30" />　
        </font><font class="mbody"><span class="style17">*</span></font></td>
    </tr>
    

<td height="28" colspan="2" align="center" bgcolor="#FFFFFF" ><input name="submit" type="submit" onclick="MM_validateForm('m_id','','R','m_pw','','R','m_name','','R','m_idnum','','R','m_tel','','NisNum','m_email','','RisEmail');return document.MM_returnValue" value=" 完 成 " />
      <input name="reset" type="reset" id="reset" value="重新填寫 " /> <a href="/index.jsp"><span class="style14">回登入頁</span></a></td>
  </table>


    <input type="hidden" name="MM_insert" value="form1">
</form>
</form>

<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
