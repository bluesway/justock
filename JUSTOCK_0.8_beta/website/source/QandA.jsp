<%@ page contentType="text/html; charset=big5" language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5" />
<title>無標題文件</title>
</head>
      <script language="javascript">
			function showlayer(id){
				for(var i=1;i<=14;i++){
					if(document.all("q"+i).style.display=='inline'){
						document.all("q"+i).style.display='none';
						document.all("q"+i+"_title").style.fontWeight ='normal';
						break;
					}
				}
				document.all("q"+id).style.display='inline';
				document.all("q"+id+"_title").style.fontWeight ='bold';
				document.all("q"+id+"_title").style.fontColor ='#CC3300';
			}
	  </script>
<body>
<p>&nbsp;</p>
<table width="583" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="62" valign="top"><div align="right">
      <p>Q1：</p>
      </div></td>
    <td width="519" valign="top" background="images/QnA_bg.gif"><div id="q4_title" align="left">
      <p><strong>個股價量明細為何13:24後就不再更新資料，而直接接13:30的收盤價格？</strong></p>
    </div></td>
  </tr>
  <tr id="q4">
    <td align="left" valign="top">&nbsp;</td>
    <td valign="top">
      <p>Ans：台灣證券交易所於91年7月1日公布新措施「收盤價改採五分鐘集合競價」，規定自下午1:25起至1:30止暫停撮合，但電腦持續接受買賣申報的輸入、改 量及取消作業，直至1:30停止上述委託作業，再依集合競價決定收盤價格並執行撮合。為配合交易所的新措施，股市報價的價量明細在13:24後就停止了， 直到13:30才出現最後的收盤價。此項措拖的相關細節，建議你可至交易所網站查詢：http://www.tse.com.tw/</p>
      </td>
  </tr>
  <tr id="q4">
    <td valign="top"><div align="right">Q2：</div></td>
    <td valign="top"><strong>就是股的報價資訊是從哪來？</strong></td>
  </tr>
  <tr id="q4">
    <td valign="top">&nbsp;</td>
    <td valign="top">Ans：所有股票報價資料均來自「台灣證券交易所」。 <br />
    本資料僅供參考，所有資料以台灣證券交易所、櫃買中心公告為準。</td>
  </tr>
  <tr id="q4">
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr id="q4">
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr id="q4">
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr id="q4">
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr id="q4">
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr id="q4">
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
