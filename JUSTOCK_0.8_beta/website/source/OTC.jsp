<%@ page contentType="text/html; charset=big5" language="java" import="java.text.*,java.util.*,java.sql.*" errorPage="" %>
<%@ include file="Connections/justockdb.jsp" %>
<jsp:useBean id="dateBean" class="common.util.DateUtil"/>
<jsp:useBean id="timeBean" class="common.util.TimeUtil"/>
<%
Driver DriverOTC = (Driver)Class.forName(MM_justockdb_DRIVER).newInstance();
Connection ConnOTC = DriverManager.getConnection(MM_justockdb_STRING,MM_justockdb_USERNAME,MM_justockdb_PASSWORD);
PreparedStatement StatementOTC = ConnOTC.prepareStatement("SELECT * FROM justock.info_otc ORDER BY oTime DESC");
ResultSet OTC = StatementOTC.executeQuery();
boolean OTC_isEmpty = !OTC.next();
boolean OTC_hasData = !OTC_isEmpty;
Object OTC_data;
int OTC_numRows = 0;
int teddy = Integer.parseInt(String.valueOf((((OTC_data = OTC.getObject("oTime"))==null || OTC.wasNull())?"":OTC_data)));
%>
<%
Driver DriverOTC1 = (Driver)Class.forName(MM_justockdb_DRIVER).newInstance();
Connection ConnOTC1 = DriverManager.getConnection(MM_justockdb_STRING,MM_justockdb_USERNAME,MM_justockdb_PASSWORD);
PreparedStatement StatementOTC1 = ConnOTC1.prepareStatement("SELECT oIndex FROM justock.info_otc ORDER BY oIndex ASC");
ResultSet OTC1 = StatementOTC1.executeQuery();
boolean OTC1_isEmpty = !OTC1.next();
boolean OTC1_hasData = !OTC1_isEmpty;
Object OTC1_data;
int OTC1_numRows = 0;
%>
<%
Driver DriverOTC2 = (Driver)Class.forName(MM_justockdb_DRIVER).newInstance();
Connection ConnOTC2 = DriverManager.getConnection(MM_justockdb_STRING,MM_justockdb_USERNAME,MM_justockdb_PASSWORD);
PreparedStatement StatementOTC2 = ConnOTC2.prepareStatement("SELECT oIndex FROM justock.info_otc ORDER BY oIndex DESC");
ResultSet OTC2 = StatementOTC2.executeQuery();
boolean OTC2_isEmpty = !OTC2.next();
boolean OTC2_hasData = !OTC2_isEmpty;
Object OTC2_data;
int OTC2_numRows = 0;
%>
<%
// *** Recordset Stats, Move To Record, and Go To Record: declare stats variables

int OTC1_first = 1;
int OTC1_last  = 1;
int OTC1_total = -1;

if (OTC1_isEmpty) {
  OTC1_total = OTC1_first = OTC1_last = 0;
}

//set the number of rows displayed on this page
if (OTC1_numRows == 0) {
  OTC1_numRows = 1;
}
%>

<%
// *** Recordset Stats: if we don't know the record count, manually count them

if (OTC1_total == -1) {

  // count the total records by iterating through the recordset
    for (OTC1_total = 1; OTC1.next(); OTC1_total++);

  // reset the cursor to the beginning
  OTC1.close();
  OTC1 = StatementOTC1.executeQuery();
  OTC1_hasData = OTC1.next();

  // set the number of rows displayed on this page
  if (OTC1_numRows < 0 || OTC1_numRows > OTC1_total) {
    OTC1_numRows = OTC1_total;
  }

  // set the first and last displayed record
  OTC1_first = Math.min(OTC1_first, OTC1_total);
  OTC1_last  = Math.min(OTC1_first + OTC1_numRows - 1, OTC1_total);
}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5" />
<title>上櫃股票大盤走勢</title>
<style type="text/css">
<!--
.style3 {color: #353842;
	font-weight: bold;
}
.style26 {font-family: "新細明體"; font-size: 15px; }
.style28 {font-family: "新細明體"; font-size: 15px; color: #8b0041; }
.style30 {font-family: "新細明體"; font-size: 15px; color: #006583; }
.style6 {font-family: "新細明體";font-size: 15px; }
.style31 {
	color: #666666;
	font-size: 20px;
}
body {
	background-color: #e7f3ff;
}
.style32 {font-size: 12px}
a:link {
	color: #0000CC;
}
a:visited {
	color: #0000CC;
}
a:hover {
	color: #0000CC;
}
a:active {
	color: #0000CC;
}
.style33 {font-size: 15px}
-->
</style>
</head>

<body>
<p align="center"><font class="style3"><span class="style31">
<%
  String today = "";
  Calendar cal = Calendar.getInstance();
  cal.setTime(new java.util.Date(System.currentTimeMillis()));

  if (cal.get(Calendar.DAY_OF_WEEK) == 7) {
  	cal.roll(Calendar.DAY_OF_YEAR, -1);
	today = new java.text.SimpleDateFormat("yyyy/MM/dd").format(cal.getTime());
  } else if (cal.get(Calendar.DAY_OF_WEEK) == 1) {
  	cal.roll(Calendar.DAY_OF_YEAR, -2);
	today = new java.text.SimpleDateFormat("yyyy/MM/dd").format(cal.getTime());
  } else if (cal.get(Calendar.HOUR_OF_DAY) < 9) {
  	cal.roll(Calendar.DAY_OF_YEAR, -1);
	today = new java.text.SimpleDateFormat("yyyy/MM/dd").format(cal.getTime());
  } else  
  	today = dateBean.show(1).substring(0,10);;

  out.print(today);
%> 上櫃股票大盤走勢圖</span></font>　<a href="/TSE.jsp" target="_self" class="style32">集中市場大盤走勢圖</a><br />
<img src="overallGraph.jsp?stockId=0&amp;otc=true&amp;screen_x=600&amp;screen_y=300" alt="上櫃" /><br />
<table width="50%" border="0" align="center" cellpadding="1" cellspacing="1">
  <tr>
    <td width="82" align="right" nowrap="nowrap"><span class="style26"><font color="#0000FF">加權指數：</font></span></td>
    <td width="106" align="left" class="style6"><%=(((OTC_data = OTC.getObject("oIndex"))==null || OTC.wasNull())?"":OTC_data)%></td>
    <td width="85" align="right" nowrap="nowrap"><span class="style33">時間：</span></td>
    <td width="98" align="left" class="style6"><span class="style26"><span class="style27"><span class="style6"><%=timeBean.transTime(teddy)%></span></span></span></td>
  </tr>
  <tr>
    <td align="right" nowrap="nowrap"><span class="style26"><font color="#0000FF">最高：</font></span></td>
    <td align="left" class="style6"><%=(((OTC2_data = OTC2.getObject("oIndex"))==null || OTC2.wasNull())?"":OTC2_data)%></td>
    <td align="right" nowrap="nowrap"><span class="style26"><font color="#0000FF">　　最低：</font></span></td>
    <td align="left" class="style6"><%=(((OTC1_data = OTC1.getObject("oIndex"))==null || OTC1.wasNull())?"":OTC1_data)%></td>
  </tr>
  <tr>
    <td align="right" nowrap="nowrap"><span class="style26"><font color="#FF0000">成交金額：</font></span></td>
    <td align="left" class="style6">
	<%
	String num = String.valueOf(((OTC_data = OTC.getObject("oAmount"))==null || OTC.wasNull())?"":OTC_data);
	double quan = Long.parseLong(num) / 100000000.0;
	DecimalFormat df = new DecimalFormat("000.00");
	out.print(df.format(quan));
	%>
	億	</td>
    <td align="left">&nbsp;</td>
    <td align="left" class="style6"></td>
  </tr>
  <tr>
    <td align="right" nowrap="nowrap"><span class="style26"><font color="#FF0000">成交張數：</font></span></td>
    <td align="left" class="style6"><%=(((OTC_data = OTC.getObject("oShare"))==null || OTC.wasNull())?"":OTC_data)%></td>
    <td align="right" nowrap="nowrap"><span class="style26"><font color="#FF0000">成交筆數：</font></span></td>
    <td align="left" class="style6"><%=(((OTC_data = OTC.getObject("oCnt"))==null || OTC.wasNull())?"":OTC_data)%></td>
  </tr>
  <tr>
    <td align="right" nowrap="nowrap"><span class="style28">委買張數：</span></td>
    <td align="left" class="style6"><%=(((OTC_data = OTC.getObject("oBQuan"))==null || OTC.wasNull())?"":OTC_data)%></td>
    <td align="right" nowrap="nowrap"><span class="style28">委買筆數：</span></td>
    <td align="left" class="style6"><%=(((OTC_data = OTC.getObject("oBCnt"))==null || OTC.wasNull())?"":OTC_data)%></td>
  </tr>
  <tr>
    <td align="right" nowrap="nowrap"><span class="style30">委賣張數：</span></td>
    <td align="left" class="style6"><%=(((OTC_data = OTC.getObject("oSQuan"))==null || OTC.wasNull())?"":OTC_data)%></td>
    <td align="right" nowrap="nowrap"><span class="style30">委賣筆數：</span></td>
    <td align="left" class="style6"><%=(((OTC_data = OTC.getObject("oSCnt"))==null || OTC.wasNull())?"":OTC_data)%></td>
  </tr>
</table>
<p align="center">

</body>
</html>
<%
OTC.close();
StatementOTC.close();
ConnOTC.close();
%>
<%
OTC1.close();
StatementOTC1.close();
ConnOTC1.close();
%>
<%
OTC2.close();
StatementOTC2.close();
ConnOTC2.close();
%>
