<%@ page contentType="text/html; charset=big5" language="java" import="java.sql.*,java.util.*" errorPage="" %>
<%@ include file="Connections/justockdb.jsp" %>

<jsp:useBean id="pm" class="common.util.PwdMD5"/>

<%
// *** Validate request to log in to this site.
String MM_LoginAction = request.getRequestURI();
if (request.getQueryString() != null && request.getQueryString().length() > 0) {
	String queryString = request.getQueryString();
  	String tempStr = "";
  	for (int i=0; i < queryString.length(); i++) {
    	if (queryString.charAt(i) == '<') 
			tempStr = tempStr + "&lt;";
    	else if (queryString.charAt(i) == '>') 
			tempStr = tempStr + "&gt;";
    	else if (queryString.charAt(i) == '"') 
			tempStr = tempStr +  "&quot;";
    	else tempStr = tempStr + queryString.charAt(i);
  	}
  	MM_LoginAction += "?" + tempStr;
}
String MM_valUsername=request.getParameter("Uid");
if (MM_valUsername != null) {
  	String MM_fldUserAuthorization="";
  	String MM_redirectLoginSuccess="index1.jsp";
  	String MM_redirectLoginFailed="index.jsp";
  	String MM_redirectLogin=MM_redirectLoginFailed;
  	Driver MM_driverUser = (Driver)Class.forName(MM_justockdb_DRIVER).newInstance();
  	Connection MM_connUser = DriverManager.getConnection(MM_justockdb_STRING,MM_justockdb_USERNAME,MM_justockdb_PASSWORD);
  	String MM_pSQL = "SELECT Uid, uPwd";
  	if (!MM_fldUserAuthorization.equals("")) 
		MM_pSQL += "," + MM_fldUserAuthorization;
  	pm.setPass(request.getParameter("pw").toString());
  	MM_pSQL += " FROM justock.account WHERE Uid=\'" + MM_valUsername.replace('\'', ' ') + "\' AND uPwd=\'" + pm.getEncryptedPass() + "\'";
  	PreparedStatement MM_statementUser = MM_connUser.prepareStatement(MM_pSQL);
  	ResultSet MM_rsUser = MM_statementUser.executeQuery();
  	boolean MM_rsUser_isNotEmpty = MM_rsUser.next();
  	if (MM_rsUser_isNotEmpty) {
    	// username and password match - this is a valid user
    	session.putValue("MM_Username", MM_valUsername);
    	if (!MM_fldUserAuthorization.equals("")) {
      		session.putValue("MM_UserAuthorization", MM_rsUser.getString(MM_fldUserAuthorization).trim());
    	} else {
     		session.putValue("MM_UserAuthorization", "");
    	}	
    	if ((request.getParameter("accessdenied") != null) && false) {
      		MM_redirectLoginSuccess = request.getParameter("accessdenied");
    	}
    	MM_redirectLogin=MM_redirectLoginSuccess;
  	}else{
  		MM_redirectLogin=MM_redirectLoginFailed + "?loginfail=true";
  	}
  	MM_rsUser.close();
  	MM_connUser.close();
  	response.sendRedirect(response.encodeRedirectURL(MM_redirectLogin));
  	return;
}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5" />
<title>JUSTOCK - 登入</title>
<script type="text/JavaScript">
<!--
function MM_goToURL() { //v3.0
  	var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  	for (i=0; i<(args.length-1); i+=2) 
		eval(args[i]+".location='"+args[i+1]+"'");
}
//-->
</script>
<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
.style3 {
	color: #181063;
	line-height: 1.5;
	font-weight: bold;
}
a:link {
	color: #0000FF;
}
a:visited {
	color: #0000FF;
}
a:hover {
	color: #0000FF;
}
a:active {
	color: #0000FF;
}
.style7 { 
	color: #181063; 
	font-size: 12px; 
	line-height: 1.5;
}
.style8 {
	color: #FF0000;
	line-height: 1.5;
}

.style10 {
	color: #181063; 
	line-height: 1.5; 
}
.style9 {
	color: #181063
}
img {
	behavior:url(png.htc)
}
-->
</style>
</head>

<body>
<div align="center">
	<p>&nbsp;</p>
  	<p><img src="images/0809-1-2.png" width="432" height="158" /></p>
</div>
<form id="form1" name="form1" method="POST" action="<%=MM_LoginAction%>">

  	<div align="left">
		<table width="200" border="0" align="center" cellpadding="0" cellspacing="0">
      		<tr>
        		<td><div align="center"><span class="style3">帳號:</span></div></td>
        		<td colspan="3">
					<div align="center">
          				<input name="Uid" type="text" id="Uid" size="15" maxlength="15" />
        			</div>
				</td>
      		</tr>
      		<tr>
        		<td><div align="center"><span class="style3">密碼:</span></div></td>
        		<td colspan="3">
					<div align="center">
          				<input name="pw" type="password" id="pw" size="15" />
     				</div>
				</td>
      		</tr>
      		<tr><td>&nbsp;</td>
        		<td colspan="3"><div align="center">
          			<input name="Submit" type="submit" value="登入" />
          			<input type="reset" name="Submit2" value="取消" />
        		</div></td>
      		</tr>      
      		<tr>
        		<td colspan="4"><div align="center">
					<a href="membernew.jsp">加入會員</a> │ <a href="mail.jsp">忘記密碼</a>
				</div></td>
      		</tr>
		</table>
	<div align="center">
    	<% 
			if(request.getParameter("loginfail") !=null && request.getParameter("loginfail").equals("true")){ 
		%>
    	<span class="style8">您輸入的帳號或密碼有誤!!</span>
		<% } %>
	</div>
</form>
<p>&nbsp;</p>
<form id="form2" name="form2" method="post" action="" >
 	<div align="center" class="style7">
    	<table cellspacing="0" cellpadding="0">
      		<tr>
        		<td colspan="5">
					<div align="center">國立中正大學97級大學部專題XDDDDD組版權所有</div>
				</td>
      		</tr>
      		<tr>
        		<td colspan="5">
					<div align="center">Copyright c  2007  BDKY XDDDDD Group. All rights reserved.</div>
				</td>
      		</tr>
      		<tr>
        		<td colspan="5">
					<div align="center">若有任何問題請洽『<a href="mailto:BDKY.JUSTOCK@gmail.com">系統管理員</a>』</div>
				</td>
      		</tr>
      		<tr>
        		<td colspan="5" height="10"><div align="center"></div></td>
      		</tr>
      		<tr>
        		<td colspan="5"><div align="center">
            		<p>本網站最佳解析度1024*768建議使用Microsoft IE 7.0以上版本或Mozilla Firefox瀏覽</p>
        		</div></td>
      		</tr>
    	</table>
    </div>
</form>
</body>
</html>
