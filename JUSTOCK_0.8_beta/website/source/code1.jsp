<%@ page contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" %>
<%@ include file="/Connections/justockdb.jsp" %>
<%
String stockcode__MMColParam = "1000";
if (request.getParameter("class") !=null) {stockcode__MMColParam = (String)request.getParameter("class");}
%>
<%
Driver Driverstockcode = (Driver)Class.forName(MM_justockdb_DRIVER).newInstance();
Connection Connstockcode = DriverManager.getConnection(MM_justockdb_STRING,MM_justockdb_USERNAME,MM_justockdb_PASSWORD);
PreparedStatement Statementstockcode = Connstockcode.prepareStatement("SELECT *  FROM justock.stock  WHERE class = " + stockcode__MMColParam + "  ORDER BY Sid ASC");
ResultSet stockcode = Statementstockcode.executeQuery();
boolean stockcode_isEmpty = !stockcode.next();
boolean stockcode_hasData = !stockcode_isEmpty;
Object stockcode_data;
int stockcode_numRows = 0;
%>
<%
int Repeat1__numRows = 10;
int Repeat1__index = 0;
stockcode_numRows += Repeat1__numRows;
%>
<%
// *** Recordset Stats, Move To Record, and Go To Record: declare stats variables

int stockcode_first = 1;
int stockcode_last  = 1;
int stockcode_total = -1;

if (stockcode_isEmpty) {
  stockcode_total = stockcode_first = stockcode_last = 0;
}

//set the number of rows displayed on this page
if (stockcode_numRows == 0) {
  stockcode_numRows = 1;
}
%>
<% String MM_paramName = ""; %>
<%
// *** Move To Record and Go To Record: declare variables

ResultSet MM_rs = stockcode;
int       MM_rsCount = stockcode_total;
int       MM_size = stockcode_numRows;
String    MM_uniqueCol = "";
          MM_paramName = "";
int       MM_offset = 0;
boolean   MM_atTotal = false;
boolean   MM_paramIsDefined = (MM_paramName.length() != 0 && request.getParameter(MM_paramName) != null);
%>
<%
// *** Move To Record: handle 'index' or 'offset' parameter

if (!MM_paramIsDefined && MM_rsCount != 0) {

  //use index parameter if defined, otherwise use offset parameter
  String r = request.getParameter("index");
  if (r==null) r = request.getParameter("offset");
  if (r!=null) MM_offset = Integer.parseInt(r);

  // if we have a record count, check if we are past the end of the recordset
  if (MM_rsCount != -1) {
    if (MM_offset >= MM_rsCount || MM_offset == -1) {  // past end or move last
      if (MM_rsCount % MM_size != 0)    // last page not a full repeat region
        MM_offset = MM_rsCount - MM_rsCount % MM_size;
      else
        MM_offset = MM_rsCount - MM_size;
    }
  }

  //move the cursor to the selected record
  int i;
  for (i=0; stockcode_hasData && (i < MM_offset || MM_offset == -1); i++) {
    stockcode_hasData = MM_rs.next();
  }
  if (!stockcode_hasData) MM_offset = i;  // set MM_offset to the last possible record
}
%>
<%
// *** Move To Record: if we dont know the record count, check the display range

if (MM_rsCount == -1) {

  // walk to the end of the display range for this page
  int i;
  for (i=MM_offset; stockcode_hasData && (MM_size < 0 || i < MM_offset + MM_size); i++) {
    stockcode_hasData = MM_rs.next();
  }

  // if we walked off the end of the recordset, set MM_rsCount and MM_size
  if (!stockcode_hasData) {
    MM_rsCount = i;
    if (MM_size < 0 || MM_size > MM_rsCount) MM_size = MM_rsCount;
  }

  // if we walked off the end, set the offset based on page size
  if (!stockcode_hasData && !MM_paramIsDefined) {
    if (MM_offset > MM_rsCount - MM_size || MM_offset == -1) { //check if past end or last
      if (MM_rsCount % MM_size != 0)  //last page has less records than MM_size
        MM_offset = MM_rsCount - MM_rsCount % MM_size;
      else
        MM_offset = MM_rsCount - MM_size;
    }
  }

  // reset the cursor to the beginning
  stockcode.close();
  stockcode = Statementstockcode.executeQuery();
  stockcode_hasData = stockcode.next();
  MM_rs = stockcode;

  // move the cursor to the selected record
  for (i=0; stockcode_hasData && i < MM_offset; i++) {
    stockcode_hasData = MM_rs.next();
  }
}
%>
<%
// *** Move To Record: update recordset stats

// set the first and last displayed record
stockcode_first = MM_offset + 1;
stockcode_last  = MM_offset + MM_size;
if (MM_rsCount != -1) {
  stockcode_first = Math.min(stockcode_first, MM_rsCount);
  stockcode_last  = Math.min(stockcode_last, MM_rsCount);
}

// set the boolean used by hide region to check if we are on the last record
MM_atTotal  = (MM_rsCount != -1 && MM_offset + MM_size >= MM_rsCount);
%>
<%
// *** Go To Record and Move To Record: create strings for maintaining URL and Form parameters

String MM_keepBoth,MM_keepURL="",MM_keepForm="",MM_keepNone="";
String[] MM_removeList = { "index", MM_paramName };

// create the MM_keepURL string
if (request.getQueryString() != null) {
  MM_keepURL = '&' + request.getQueryString();
  for (int i=0; i < MM_removeList.length && MM_removeList[i].length() != 0; i++) {
  int start = MM_keepURL.indexOf(MM_removeList[i]) - 1;
    if (start >= 0 && MM_keepURL.charAt(start) == '&' &&
        MM_keepURL.charAt(start + MM_removeList[i].length() + 1) == '=') {
      int stop = MM_keepURL.indexOf('&', start + 1);
      if (stop == -1) stop = MM_keepURL.length();
      MM_keepURL = MM_keepURL.substring(0,start) + MM_keepURL.substring(stop);
    }
  }
}

// add the Form variables to the MM_keepForm string
if (request.getParameterNames().hasMoreElements()) {
  java.util.Enumeration items = request.getParameterNames();
  while (items.hasMoreElements()) {
    String nextItem = (String)items.nextElement();
    boolean found = false;
    for (int i=0; !found && i < MM_removeList.length; i++) {
      if (MM_removeList[i].equals(nextItem)) found = true;
    }
    if (!found && MM_keepURL.indexOf('&' + nextItem + '=') == -1) {
      MM_keepForm = MM_keepForm + '&' + nextItem + '=' + java.net.URLEncoder.encode(request.getParameter(nextItem));
    }
  }
}

String tempStr = "";
for (int i=0; i < MM_keepURL.length(); i++) {
  if (MM_keepURL.charAt(i) == '<') tempStr = tempStr + "&lt;";
  else if (MM_keepURL.charAt(i) == '>') tempStr = tempStr + "&gt;";
  else if (MM_keepURL.charAt(i) == '"') tempStr = tempStr +  "&quot;";
  else tempStr = tempStr + MM_keepURL.charAt(i);
}
MM_keepURL = tempStr;

tempStr = "";
for (int i=0; i < MM_keepForm.length(); i++) {
  if (MM_keepForm.charAt(i) == '<') tempStr = tempStr + "&lt;";
  else if (MM_keepForm.charAt(i) == '>') tempStr = tempStr + "&gt;";
  else if (MM_keepForm.charAt(i) == '"') tempStr = tempStr +  "&quot;";
  else tempStr = tempStr + MM_keepForm.charAt(i);
}
MM_keepForm = tempStr;

// create the Form + URL string and remove the intial '&' from each of the strings
MM_keepBoth = MM_keepURL + MM_keepForm;
if (MM_keepBoth.length() > 0) MM_keepBoth = MM_keepBoth.substring(1);
if (MM_keepURL.length() > 0)  MM_keepURL = MM_keepURL.substring(1);
if (MM_keepForm.length() > 0) MM_keepForm = MM_keepForm.substring(1);
%>
<%
// *** Move To Record: set the strings for the first, last, next, and previous links

String MM_moveFirst,MM_moveLast,MM_moveNext,MM_movePrev;
{
  String MM_keepMove = MM_keepBoth;  // keep both Form and URL parameters for moves
  String MM_moveParam = "index=";

  // if the page has a repeated region, remove 'offset' from the maintained parameters
  if (MM_size > 1) {
    MM_moveParam = "offset=";
    int start = MM_keepMove.indexOf(MM_moveParam);
    if (start != -1 && (start == 0 || MM_keepMove.charAt(start-1) == '&')) {
      int stop = MM_keepMove.indexOf('&', start);
      if (start == 0 && stop != -1) stop++;
      if (stop == -1) stop = MM_keepMove.length();
      if (start > 0) start--;
      MM_keepMove = MM_keepMove.substring(0,start) + MM_keepMove.substring(stop);
    }
  }

  // set the strings for the move to links
  StringBuffer urlStr = new StringBuffer(request.getRequestURI()).append('?').append(MM_keepMove);
  if (MM_keepMove.length() > 0) urlStr.append('&');
  urlStr.append(MM_moveParam);
  MM_moveFirst = urlStr + "0";
  MM_moveLast  = urlStr + "-1";
  MM_moveNext  = urlStr + Integer.toString(MM_offset+MM_size);
  MM_movePrev  = urlStr + Integer.toString(Math.max(MM_offset-MM_size,0));
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache">
<title>股市代碼</title>
<style type="text/css">
<!--
body {
	background-color: #E7F3FF;
}
a:link {
	text-decoration: none;
	color: #000066;
}
a:visited {
	text-decoration: none;
	color: #000066;
}
a:hover {
	text-decoration: none;
	color: #000066;
}
a:active {
	text-decoration: none;
	color: #000066;
}
.style1 {font-size: 14px}
.style4 {font-family: "新細明體"; font-size: 16px; }
-->
</style></head>

<body>
<table width="12%" border="0" align="center" cellpadding="4" cellspacing="1" bordercolor="#FFFFFF" bgcolor="#CCCCCC">
  <tr>
    <% while ((stockcode_hasData)&&(Repeat1__numRows-- != 0)) { %>
    <td width="42%" align="center" bgcolor="#FFFFFF"><span class="style4"><%=(((stockcode_data = stockcode.getObject("Sid"))==null || stockcode.wasNull())?"":stockcode_data)%></span></td>
    <td width="58%" align="center" bgcolor="#FFFFFF"><span class="style4"><%=(((stockcode_data = stockcode.getObject("Sname"))==null || stockcode.wasNull())?"":stockcode_data)%></span></td>
  </tr>
  <%
  Repeat1__index++;
  stockcode_hasData = stockcode.next();
}
%>
</table>

<p align="center"><span class="style1"><A HREF="<%=MM_moveFirst%>">第一頁</A> <A HREF="<%=MM_movePrev%>">上一頁</A> <A HREF="<%=MM_moveNext%>">下一頁</A> <A HREF="<%=MM_moveLast%>">最末頁</A></span></p>
</body>

</html>
<%
stockcode.close();
Statementstockcode.close();
Connstockcode.close();
%>
