package software;

import javax.swing.*;

/* Used by InternalFrameDemo.java. */
@SuppressWarnings("serial")
public class MyInternalFrame extends JInternalFrame
{
	static int openFrameCount = 0;
    static final int xOffset = 30 ;
    static final int yOffset = 30 ;
    
    static JInternalFrame frame = new JInternalFrame();

    public MyInternalFrame(String actionName) 
    {
//    	super(transActionName(actionName),
    	super(actionName,
    			true, //resizable
    			true, //closable
    			true, //maximizable
    			true);//iconifiable
    	
    	if (openFrameCount++ > 10)
    		openFrameCount = 0;
    	//Set the window's location.
    	setLocation(xOffset*openFrameCount, yOffset*openFrameCount);
    }

    // Translate actionCommand to string name
//     deprecated
//    private static String transActionName(String actName)
//    {
//    	if(actName.equals("realPrice0"))
//     		return GUI.Real[0];
//    	else if(actName.equals("realPrice1"))
//    		return GUI.Real[1];
//    	else if(actName.equals("transItem0"))
//    		return GUI.Trans[0];
//    	else if(actName.equals("transItem1"))
//    		return GUI.Trans[1];
//    	else if(actName.equals("analysisItem0"))
//    		return GUI.Analysis[0];
//    	else if(actName.equals("analysisItem1"))
//    		return GUI.Analysis[1];
//    	else if(actName.equals("analysisItem2"))
//    		return GUI.Analysis[2];
//    	else if(actName.equals("analysisItem3"))
//    		return GUI.Analysis[3];
//    	else if(actName.equals("memberItem0"))
//    		return GUI.Member[0];
//    	else if(actName.equals("memberItem1"))
//    		return GUI.Member[1];
//    	else if(actName.equals("memberItem2"))
//    		return GUI.Member[2];
//    	else if(actName.equals("documentItem0"))
//    		return GUI.Document[0];
//    	else if(actName.equals("documentItem1"))
//    		return GUI.Document[1];
//    	else if(actName.equals("documentItem2"))
//    		return GUI.Document[2];
//    	else
//    		return actName;
//    }
    
}