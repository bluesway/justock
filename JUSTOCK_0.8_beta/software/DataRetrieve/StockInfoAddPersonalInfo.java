package software.DataRetrieve;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
import java.util.Vector;

import javax.swing.JOptionPane;

import software.DB;

public class StockInfoAddPersonalInfo {
	public int maxData = 50;
	private Vector<String[]> psInfo;
	private String[] buffer;
	private File file;
	
	public StockInfoAddPersonalInfo() {
		buffer = new String[8];
		file = new File("personalStockInfo");

		setToNull();
		checkFile();
		readData();
	}
	
	private void readData() {
		Scanner scn = null;
		
		try {
			scn = new Scanner(file, "UTF-8");

			while (scn.hasNext()) {
				addData(scn.nextLine());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
    // 將指定的個股加入自選股
    public void addData(String stockId) {
    	int index;
    	Connection conn = null;
    	Statement stmt = null;
    	ResultSet result = null;
    	
    	index = psInfo.size();
    	
    	if(index > maxData - 1) 
    		JOptionPane.showMessageDialog(null, "自選股已滿，請刪除現有自選股！");
    	else {
    		buffer[0] = stockId;
    		try {
    			conn = DB.db.getConnection();
    			stmt = conn.createStatement();
    			
				result = stmt.executeQuery("SELECT * from info_current where cSid='" + stockId + "'");
				
				if(result.next()) {
					buffer[3] = result.getString(2); 
					buffer[4] = result.getString(3); 
					buffer[5] = result.getString(4); 
					buffer[6] = result.getString(5); 
					buffer[7] = result.getString(6); 
				}
				
				result = conn.createStatement().executeQuery("SELECT tPrice from info_today where tSid='" + stockId  +"'");
				if(result.next());
				if(result.next())
					buffer[2] = result.getString(1);
				
				result = conn.createStatement().executeQuery("SELECT * from stock where Sid='" + stockId + "'");
				if(result.next())
					buffer[1] = result.getString("sName");
    			
    			psInfo.add(buffer.clone());
    			saveFile();
    		}catch(SQLException e) {
    			System.err.println(e);
    		} finally {
    			try {
    				if (result != null)
    					result.close();
    				if (stmt != null)
    					stmt.close();
    				if (conn != null)
    					DB.db.closeConnection(conn);
    			} catch (SQLException e) {
    				e.printStackTrace();
    			}
    		}
    	}
    }
    
    private void saveFile() {
    	try {
			PrintStream ps = new PrintStream(file);
			
			for (int i = 0; i < psInfo.size(); i++)
				ps.println(psInfo.get(i)[0]);
				
			ps.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    }
    
    public void delData(int index) {
    	psInfo.remove(index);
    	saveFile();
    }
    
    public void delFile() {
		file.delete();
		setToNull();
    }
    
    private void checkFile() {
    	if (!file.exists())
    		createNewFile();
    }
    
    private void createNewFile() {
    	try {
			if(!file.createNewFile()) {
				JOptionPane.showMessageDialog(null, "自選股檔案建立失敗，系統即將關閉...", "系統狀態", JOptionPane.ERROR_MESSAGE);
				System.exit(0);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    private void setToNull() {
    	psInfo = new Vector<String[]>();
    }

    public Vector<String[]> getData() {
    	return psInfo;
    }
}