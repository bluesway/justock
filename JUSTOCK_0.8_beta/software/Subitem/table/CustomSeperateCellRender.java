package software.Subitem.table;

import java.awt.Component;
import java.awt.Color;
import java.awt.Font;
import java.io.UnsupportedEncodingException;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class CustomSeperateCellRender extends DefaultTableCellRenderer {

	private static final long serialVersionUID = 1L;
	
	public CustomSeperateCellRender() {

	}

	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {

		try {
			Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				if(column == 5 && ((!String.valueOf(value).startsWith("��")) ||(String.valueOf(value).startsWith("+")))) {
					table.setOpaque(true);
					cell.setBackground(Color.PINK);
					cell.setForeground(Color.BLACK);
					cell.setFont(new Font("�з���", Font.PLAIN, 14));
				}
				else if(column == 5  && (String.valueOf(value).startsWith("-") || String.valueOf(value).startsWith("��"))) {
					table.setOpaque(true);
					cell.setBackground(Color.GREEN);
					cell.setForeground(Color.BLACK);
				} else{
					cell.setBackground(Color.BLACK);
					cell.setForeground(Color.WHITE);
				}
		} catch(IndexOutOfBoundsException  e) {
			e.printStackTrace();
		}
		return super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
	}
	
	public boolean checkChinese(Object value) throws UnsupportedEncodingException {
		String   sentence = String.valueOf(value);
		byte[]   temp=sentence.getBytes("big5");
		boolean   chinese=false;                                      
		int   i;  
		   
         for(i=0;i<temp.length;i++)  
            if(temp[i]<0) {   
            	  chinese=true;  
                  i=temp.length;
            }  
		   
          if(chinese)  
                return false;
          else  
                return true;
	}
}