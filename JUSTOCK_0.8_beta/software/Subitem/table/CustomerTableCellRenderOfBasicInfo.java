package software.Subitem.table;

import java.awt.Component;
import java.awt.Color;
import java.awt.Font;
import java.io.UnsupportedEncodingException;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class CustomerTableCellRenderOfBasicInfo extends DefaultTableCellRenderer {

	private static final long serialVersionUID = 1L;

	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {

		try {
			Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			if(String.valueOf(value).length() > 1) {
				if(column % 2 == 1) {
					table.setOpaque(true);
					cell.setBackground(Color.LIGHT_GRAY);
					cell.setForeground(Color.BLACK);
				}
				else if(column % 2 == 0) {
					table.setOpaque(true);
					cell.setBackground(Color.CYAN);
					cell.setForeground(Color.black);
				}
			}
		} catch(IndexOutOfBoundsException  e) {
			e.printStackTrace();
		}
		return super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
	}
	
	public boolean checkChinese(Object value) throws UnsupportedEncodingException {
		String   sentence = String.valueOf(value);
		byte[]   temp=sentence.getBytes("big5");
		boolean   chinese=false;                                      
		int   i;  
		   
         for(i=0;i<temp.length;i++)  
            if(temp[i]<0) {   
            	  chinese=true;  
                  i=temp.length;
            }  
		   
          if(chinese)  
                return false;
          else  
                return true;
	}
}