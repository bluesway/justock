/*
*      author by Kevingo
*      Data @ 2007/09/02
*      顯示價量分析資料 
*      yahoo version
*      
*/

package software.Subitem.table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import common.util.MyTableModel_7;
import common.util.StockBox;
import software.GUI;
import software.DataRetrieve.StockInfoPriceAndVolumnParser_Pchome;

public class PriceAndVolumnInfo implements ActionListener {
	private static final long serialVersionUID = 1L;
	public boolean DONE = false;
	private int TABLE_HEIGHT, TABLE_WIDTH, DATA_ROW, DATA_COLUMN, vColIndex, width;
	private Object [][] data;
	private Container container;
    private JButton okBtn,cancelBtn; // 確定 取消按鈕
    private JComboBox stockBox;
    private JPanel panel_Table; // 放置表格的panel_Table
    private JTable table; // 表格
    private JTextField stockField, statusField; // 使用者輸入股票代碼
    private MyTableModel_7 model; // tableModel
    private Border compound;
    private TitledBorder border;
    private Scanner scan; // 讀取股票代碼
	private StockInfoPriceAndVolumnParser_Pchome spp;
	private String stockID;
	private String tmpProcess;
	private String [] stockList;
	private String [] columnNames = new String[] {
			"時間",
			"買價",
			"買量",
			"賣價",
			"賣量",
			"成交價",
			"漲跌",
			"分量(張)",
			"累積量(張)"
	};
	
	private JButton callTecDiagramBtn;
	private JButton callIniDiagramBtn;

    public PriceAndVolumnInfo() {
    	String tmp = "";
    	try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		container = new Container();
		container.setLayout(new BorderLayout());
		
		DATA_ROW = StockInfoPriceAndVolumnParser_Pchome.ROW;
		DATA_COLUMN = StockInfoPriceAndVolumnParser_Pchome.COLUMN;
		
		vColIndex = 0; // 決定column 的 index
    	width = 80; // column index 的寬度
		
    	panel_Table = new JPanel();
    	stockList = new String[2054];
    	data = new Object[DATA_ROW][DATA_COLUMN];
    	
    	// 讀取stockList到String陣列中 ，JComboBox要用
    	readData(); 
    	int i = 0;
    	while(scan.hasNext()) {
			tmp = scan.nextLine();
			if (tmp.startsWith("#"))
				continue;
    		stockList[i] = tmp;
    		i++;
    	}
    	
    	TABLE_HEIGHT = 500;
    	TABLE_WIDTH = 350;
    	
    	callTecDiagramBtn = new JButton("查詢個股技術分析圖表");
    	callTecDiagramBtn.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			GUI.gui.setStockId(Integer.valueOf(String.valueOf(StockBox.getStockId(stockBox))));
    	    			GUI.gui.createFrame("技術分析");
    	    		}
    	    	});
    	panel_Table.add(callTecDiagramBtn);
    	
    	callIniDiagramBtn = new JButton("查詢個股即時行情圖表");
    	callIniDiagramBtn.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			GUI.gui.setStockId(Integer.valueOf(String.valueOf(StockBox.getStockId(stockBox))));
    	    			GUI.gui.createFrame("個股即時行情");
    	    		}
    	    	});
    	panel_Table.add(callIniDiagramBtn);
    	
       	// 股票代碼下拉式選單
    	stockBox = StockBox.create();    	
    	Border redline = BorderFactory.createLineBorder(Color.darkGray, 1);
        compound = BorderFactory.createCompoundBorder(
                redline, compound);
        border = BorderFactory.createTitledBorder(
                compound, "請選擇股票代碼",
                TitledBorder.CENTER,
                TitledBorder.DEFAULT_POSITION);
    	stockBox.setBorder(border);
    	stockBox.getEditor().addActionListener(this);
    	enterPressesWhenFocused(stockBox);
    	panel_Table.add(stockBox);
    	
    	// 確定按鈕
    	okBtn = new JButton("確定");
    	enterPressesWhenFocused(okBtn);
    	okBtn.addActionListener(
    		new ActionListener() {
    			public void actionPerformed(ActionEvent ae) {
    					updataData();
        		        model.fireTableDataChanged(); 
        			}
    			}
    	);
    	panel_Table.add(okBtn);
    	
    	// 取消按鈕
    	cancelBtn = new JButton("取消");
    	cancelBtn.addActionListener(new ActionListener() {    		 
    		public void actionPerformed(ActionEvent arg0) {
    			stockField.setText("");
    		}
    	});
    	panel_Table.add(cancelBtn);
    	
    	// 表格
		model = new MyTableModel_7(DATA_ROW, DATA_COLUMN);
		model.setColumnNames(columnNames);
		
    	table = new JTable(model);
		TableCellRenderer renderer = new CustomTableCellOfWisdom();
		table.setDefaultRenderer( Object.class, renderer );
        table.setColumnSelectionAllowed(true);
        table.clearSelection();
        table.revalidate();
        table.repaint();
    	table.setPreferredScrollableViewportSize(new Dimension(TABLE_HEIGHT, TABLE_WIDTH));
    	table.setFillsViewportHeight(true);   
    	table.setRowSelectionAllowed(true);
    	table.setColumnSelectionAllowed(true);
        model.addTableModelListener(new TableModelListener(){
            public void tableChanged(TableModelEvent e) {
                table.updateUI();
            }});
        
        // 設定column的寬度
        TableColumn col = table.getColumnModel().getColumn(vColIndex);
        col.setPreferredWidth(width);
        
        // 拉動式捲軸 和 主要Frame
    	JScrollPane scrollPane = new JScrollPane(table);
        
        RowHeaderer rowHeader= new RowHeaderer(table);
        scrollPane.setRowHeaderView(rowHeader);
        scrollPane.getRowHeader().setPreferredSize(new Dimension(35,100));
        
        table.setBackground(Color.BLACK);
        table.setForeground(Color.WHITE);
        table.setFont(new Font("標楷體", Font.PLAIN, 14));
        container.add(panel_Table, BorderLayout.NORTH);
        container.add(scrollPane, BorderLayout.WEST);
        container.setVisible(true);
    }
    
    public Container getFrame() {
    	return container;
    }
    
    // 從檔案讀取股票資料
    private void readData() {
        File tmpfile;
        tmpfile = new File("common/source/stockId.list");
        try {
            scan = new Scanner(tmpfile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    // 股票代碼ComboBox的確認動作
    public void actionPerformed(ActionEvent e) {
    	stockID = String.valueOf(StockBox.getStockId(stockBox));
    	updataData();
    }

    // 回傳找尋到的ID
    public String searchName(String id) {
    	id = common.util.StockBox.searchStockId(id);
    	return id;
    }
    
    public void updataData() {
    	if(StockBox.getStockId(stockBox) != -1) {
			DONE = true;
			spp = new StockInfoPriceAndVolumnParser_Pchome();
			spp.retrieveData(spp, String.valueOf(StockBox.getStockId(stockBox)), DONE);
			data = spp.getData();
		
	    	for(int i=0 ; i<DATA_ROW ; i++) {
	    		for(int j=0 ; j<DATA_COLUMN ; j++) {
	    			tmpProcess = String.valueOf(data[i][j]);
	    			if(tmpProcess.startsWith("+") && tmpProcess.length() != 1) {
	    				tmpProcess = "▲ " + tmpProcess.substring(1, tmpProcess.length());
	    				table.setSelectionForeground(Color.YELLOW);
	    			}
	    			else if(tmpProcess.startsWith("-") && tmpProcess.length() != 1)
	    				tmpProcess = "▼ " + tmpProcess.substring(1, tmpProcess.length());
	    			else if(tmpProcess == "null")
	    				tmpProcess = "";
	
	    			model.setValueAt(tmpProcess, i, j);
	    		}		
	    	}
    	}
    }
    
    // 處理快速鍵的問題   enter 確定 esc 取消
    public void enterPressesWhenFocused(JComponent object) {
    		object.registerKeyboardAction(
    				object.getActionForKeyStroke(
    						KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)), 
    						KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false), 
    						JComponent.WHEN_IN_FOCUSED_WINDOW);
    		
    		object.registerKeyboardAction(
    				object.getActionForKeyStroke(
    						KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)), 
    						KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), 
    						JComponent.WHEN_IN_FOCUSED_WINDOW);
    }
}