/**
 * @author bluesway
 * TARGET 圖形繪製的基礎模型
 */

package software.Subitem;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Arrays;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import common.util.DateUtil;
import common.util.StockBox;
import software.DB;
import software.GUI;
import software.Subitem.Listener.TradeListener;

@SuppressWarnings("serial")
public abstract class GeneralGraph extends JPanel {
	public static final int TYPE_LINE_CHART = 0;
	public static final int TYPE_KBAR_CHART = 1;
	public static final int TYPE_BIN_CHART = 2;
	public static final int TYPE_LIMITED = 0;
	public static final int TYPE_UNLIMITED = 1;
	
    // 繪圖用變數
    protected float screen_x;
    protected float screen_y;
    protected Rectangle2D.Float graphD;	// 線圖邊框範圍
    protected Rectangle2D.Float rect;	// 2D矩形物件
    protected Line2D.Float line;		// 2D直線物件
    protected float line_cap;			// 框線寬度
    protected float start_main;			// 主線圖上緣
    protected float height_main;		// 主線圖高度
    protected float start_quan;			// 成交量上緣
    protected float height_quan;		// 成交量高度
    protected float start_tech1;		// 技術分析1上緣
    protected float height_tech1;		// 技術分析1高度
    protected float start_tech2;		// 技術分析2上緣
    protected float height_tech2;		// 技術分析2高度
    protected float left_space;			// 顯示指數、成交量資訊的寬度
    protected float right_space;		// 顯示指標資訊的寬度
    protected float char_height;		// 預設字高
    protected int limiteType;			// 限制類別 0:有7%限制 1:無7%限制 
	protected int displayType;			// 顯示型態 0:折線圖 1:K線圖 2:兩者同時顯示
    protected float scale;				// 線圖縮放級距
    protected int dateRange;			// 線圖日期/時間單位
    protected int dateType;				// 線圖日期型式 0:日線 1:週線 2:週線 3:5分線
	protected float ratio;				// 數值在圖表中的相對比例
	protected NumberFormat nf;			// 數字格式化工具
	protected int mouse_x;				// 滑鼠游標x軸座標
	protected int mouse_y;				// 滑鼠游標y軸座標
	
	// container 元件變數
	protected JPanel controlPanel;					// 控制面板
	protected float cpl_height;						// 控制面板高度
	protected JButton[] defaultBtn;					// 預設按鈕
	protected JComboBox stockBox;					// 股票選擇
	protected MouseListener[] defaultML;
	protected JPopupMenu metaMenu;
	protected JMenuItem[] menuItem;

	// 資料處理用變數
	protected int stockId;							// 繪圖資料來源 - 0:大盤 id:個股
	protected LinkedList<float[]> sourceData;		// 原始資料
	protected HashMap<Integer, float[]> sourceMap;	// 時間對應原始資料
	protected boolean isOverall;					// 是否為當日資料
    protected String[] date_mark = new String[3];	// 日期字串
    protected String[] time_mark = new String[2];	// 時間字串
    protected int priceNum;							// 指數參考資料顯示筆數 (須為單數筆且大於3)
    protected int quanNum;							// 成交量參考資料顯示筆數 (須為單數筆)
    protected float[] data;						// 圖表繪製range資料
	protected float[] priceInitial;				// 指數參考資料值
	protected float[] quanInitial;					// 成交量參考資料值
	protected float priceSep;						// 指數參考資料區間
	protected float quanSep;						// 成交量參考資料區間
	protected float priceUnit;						// 股價升降單位
	protected float[] prev;						// 前項指標
	protected float[] now;							// 當項指標
	protected int avgNum;							// 均線數量 (包含指數及成交量，須為雙數)
	protected float[] avgPrev;						// 前項均值
	protected float[] avgCurr;						// 當項均值
	protected int time;								// 當前處理時間
	protected String sName;							// 股票名稱
	protected int range_start		;				// 資料讀取起點
	protected int range;							// 資料讀取區間
	protected int shift;							// 資料移動區間
	
    public GeneralGraph() {
		// 數字格式
    	nf = NumberFormat.getInstance();
    	nf.setMaximumFractionDigits(2);
    	
    	// 變數處理
    	graphD = new Rectangle2D.Float();
    	rect = new Rectangle2D.Float();
    	line = new Line2D.Float();
    	
    	
    	// 控制台配置
    	defaultBtn = new JButton[6];
    	defaultML = new MouseListener[4];
    	defaultML[0] = new scaleListener();
    	defaultML[1] = new shiftListener();
    	defaultML[2] = new metaMenuListener();
    	defaultML[3] = new outputListener();
    	defaultBtn[0] = new JButton("+");		// 放大線圖
    	defaultBtn[1] = new JButton("-");		// 縮小線圖
    	defaultBtn[0].addMouseListener(defaultML[0]);
    	defaultBtn[1].addMouseListener(defaultML[0]);
    	defaultBtn[2] = new JButton("<");		// 向前平移
    	defaultBtn[3] = new JButton(">");		// 向後平移
    	defaultBtn[2].addMouseListener(defaultML[1]);
    	defaultBtn[3].addMouseListener(defaultML[1]);
    	defaultBtn[4] = new JButton("下單");		// 股票下單
    	defaultBtn[4].setActionCommand(String.valueOf(stockId));
    	defaultBtn[4].addActionListener(new TradeListener());
    	defaultBtn[5] = new JButton("輸出");		// 圖檔輸出
    	defaultBtn[5].addMouseListener(defaultML[3]);

    	metaMenu = new JPopupMenu("右鍵選單");
    	menuItem = new JMenuItem[4];
    	metaMenu.add(menuItem[3] = new JMenuItem("5分線"));
    	metaMenu.add(menuItem[0] = new JMenuItem("日線"));
    	metaMenu.add(menuItem[1] = new JMenuItem("週線"));
    	metaMenu.add(menuItem[2] = new JMenuItem("月線"));
    	for (int i = 0; i < 4; i++) {
    		menuItem[i].setActionCommand(String.valueOf(i));
    		menuItem[i].addActionListener(new menuItemListener());
    	}
    	
    	// 其它設定
		addMouseListener(defaultML[2]);
        addMouseMotionListener(new timeLineListener());
        
    	setBackground(Color.BLACK);
		setPreferredSize(GUI.screen);
		setVisible(true);
    }
    
    protected void createBox() {
    	stockBox = StockBox.create();
		stockBox.getEditor().addActionListener(new stockAction());
    }
    
    protected void readData() {
    	Connection conn = null;
    	Statement stmt = null;
    	ResultSet result = null;
		long a = System.currentTimeMillis();
		
		sourceData.clear();
		
		int dayCount = 0;
		String date1 = "";
		String date2 = "";
		/* 0:日期時間 1:開始價 2:最高 3:最低 4:結束價 5:量 */
		float result_info[] = new float[6];

        try {
        	conn = DB.db.getConnection();
        	stmt = conn.createStatement();
        	
        	if (dateType == 3) {
        		result = stmt.executeQuery("SELECT * FROM info_today WHERE tSid='" + 
        				String.valueOf(stockId) + "' ORDER BY tTime ASC");
        		result.next();
        	} else
        		result = stmt.executeQuery("SELECT * FROM info_history WHERE hSid='" + 
        				String.valueOf(stockId) + "' ORDER BY hDate ASC");
            
            while (result.next()) {
            	if (dateType != 0) {
            		date1 = date2;
            		if (dateType == 3)
            			date2 = numToTime(result.getInt(2));
            		else
            			date2 = numToDate(result.getInt(2));
            	}
            	
        		// 5分線 or 週線 or 月線的結束
        		if ((dateType == 1 && !DateUtil.isSameWeek(date1, date2)) ||
        				(dateType == 2 && !DateUtil.isSameMonth(date1, date2)) ||
        					dateType == 3 && !DateUtil.isSameFivePeriod(date1, date2)) {
        			sourceData.add(result_info.clone());

        			if (dateType == 3) {
            			numToTime(result_info[0]);
            			sourceMap.put(((Integer.parseInt(time_mark[0]) - 9) * 60 + Integer.parseInt(time_mark[1])) / 5, result_info.clone());
        			}
        			
        			dayCount = 0;
        			for (int i = 1; i < 6; i++)
        				result_info[i] = 0;
        		}

        		if (dayCount == 0) {
        			result_info[0] = result.getInt(2);
        			
        			if (dateType == 3)
        				result_info[1] = result.getFloat(4);
        			else
        				result_info[1] = result.getFloat(3);
        		}
            	
            	if (dateType == 0) {			// 日線
	            	result_info[5] = result.getInt(7) / 100.0f; 
	            	for (int i = 1; i < 5; i++)
		            	result_info[i] = result.getFloat(i+2);
        			sourceData.add(result_info.clone());
            	} else if (dateType == 3){		// 5分線
            		if (result_info[2] < result.getFloat(4))
            			result_info[2] = result.getFloat(4);
            		if (result_info[3] > result.getFloat(4) || result_info[3] <= 0.0)
            			result_info[3] = result.getFloat(4);
            		result_info[4] = result.getFloat(4);
            		result_info[5] += result.getInt(5) / 100.0;
            		dayCount++;
            	} else {						// 週線、月線
            		if (result_info[2] < result.getFloat(4))
            			result_info[2] = result.getFloat(4);
            		if (result_info[3] > result.getFloat(5) || result_info[3] <= 0.0)
            			result_info[3] = result.getFloat(5);
            		result_info[4] = result.getFloat(6);
	            	result_info[5] += result.getInt(7) / 100.0; 
	            	dayCount++;
            	}
            }
            result = stmt.executeQuery("SELECT * FROM stock WHERE Sid='" + 
            		String.valueOf(stockId) + "'");
        	sName = result.next() ? result.getString(2) : "";
        } catch (SQLException e) {
        	System.err.println(e);
		} finally {
			try {
				if (result != null)
					result.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					DB.db.closeConnection(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
        
		a = System.currentTimeMillis() - a;
		System.out.println("資料已讀取，共花費" + a / 1000.0 + "秒");
    }
    
	protected void setData() {
		long a = System.currentTimeMillis();
    	int tmp = sourceData.size();

    	if (!isOverall) {
    		if (dateType == 3) {
    			range = tmp;
    			time = range_start = 0;
    			shift = 0;
    			scale = (graphD.width - line_cap) / range * 1.0f;
    		} else {
    			range_start = tmp - range + shift;
    			if (range_start <= 0)
    				range_start = 0;
    			time = range_start;
			
    			if (tmp - range_start < range)
    				range = tmp - range_start;
    		}

			data = new float[range];
    	}
    	else {
    		time = 0;
    		range = tmp;
    		data = new float[tmp];
    	}
    	
    	prev = sourceData.get(time++);
    	
    	// 處理指數部分
    	priceInitial = new float[priceNum + 1];
    	
		for (int i = 0; i < range; i++)
			if (isOverall)
				data[i] = sourceData.get(i)[1];
			else
				data[i] = sourceData.get(range_start + i)[4];
		
		if (limiteType == TYPE_LIMITED) {
			priceInitial[0] = data[0];
			Arrays.sort(data);
			if (priceInitial[0] - data[0] > data[data.length - 1] - priceInitial[0]) {
				priceInitial[1] = priceInitial[0] * 2 - data[0];
				priceInitial[priceNum] = data[0];
			} else {
				priceInitial[1] = data[data.length - 1];
				priceInitial[priceNum] = priceInitial[0] * 2 - data[data.length - 1];
			}
			priceSep = (priceInitial[1] - priceInitial[priceNum]) / (priceNum - 1) * 1.0f;
			
			for (int i = priceNum - 1; i > 1; i--)
				priceInitial[i] = priceInitial[1] - priceSep * (i-1);
		} else {
			Arrays.sort(data);
    	
			priceInitial[1] = data[data.length - 1];
			priceInitial[priceNum] = data[0];
			priceSep = (priceInitial[1] - priceInitial[priceNum]) / (priceNum - 1) * 1.0f;
			
			for (int i = priceNum - 1; i > 1; i--)
				priceInitial[i] = priceInitial[1] - priceSep * (i-1);
		}
    	
    	// 處理成交量部分
    	quanInitial = new float[quanNum + 1];
    	
		for (int i = 0; i < range; i++)
			if (isOverall)
				if (stockId == 0)
					data[i] = sourceData.get(i)[3] / 100000000.0f;
				else
					data[i] = sourceData.get(i)[3];
			else
				data[i] = sourceData.get(range_start + i)[5];
		
    	Arrays.sort(data);
    	
    	quanInitial[1] = data[data.length - 1];
    	quanInitial[quanNum] = 0;
    	
    	quanSep = quanInitial[1] / (quanNum - 1) * 1.0f;
    	
    	for (int i = quanNum - 1; i > 1; i--) {
    		quanInitial[i] = quanInitial[1] - quanSep * (i-1);
    	}
    	
		a = System.currentTimeMillis() - a;
    }
    
    public void paintComponent(Graphics g) {
    	Graphics2D page = (Graphics2D) g;
    	
    	setData();

    	// 清除背景
		page.setColor(Color.BLACK);
		rect.setRect(0, 0, screen_x, screen_y);
		page.fill(rect);

//        page.dispose();
//        System.out.println(range);
//        System.out.println(range_start);
//        System.out.println(shift);
//        page.drawImage(image, getInsets().left, getInsets().top, this);
    }
    
//    public void update(Graphics g)
//    {
//    		paintComponent(g);
//    }
    
    // 畫虛線
    protected void drawBrokenLine (Graphics2D page, float x0, float y0, float x1, float y1) {
        float b_width;				// 虛線寬
    	float b_height;				// 虛線高
    	float tmpX;					// 曲線圖 x 軸分段繪製坐標
    	float tmpY;					// 曲線圖 y 軸分段繪製坐標
    	
    	b_width = (x1 - x0) / 5;
    	b_height = (y1 - y0) / 5;
    	tmpX = x0;
    	tmpY = y0;
    	
    	while (true) {
    		if (tmpX + (b_width == 0 ? 0 : 5) > x1 || tmpY + (b_height == 0 ? 0 : 5) > y1 || (b_width == 0 && b_height == 0))
    			break;
    		line.setLine(tmpX, tmpY, tmpX + (b_width == 0 ? 0 : 5), tmpY + (b_height == 0 ? 0 : 5));
    		page.draw(line);
    		tmpX += (b_width == 0 ? 0 : 5) * 2;
    		tmpY += (b_height == 0 ? 0 : 5) * 2;
    	}
    }
    
    // 畫折線圖
    protected void drawLineChart (Graphics2D page, float dataPrev, float dataCurr, float x, float scale) {
    	if (dataCurr < dataPrev)
    		page.setColor(Color.CYAN);
    	else
    		page.setColor(Color.WHITE);
    	
    	line.setLine(x, calculateAxis(dataPrev, 0), x + scale, calculateAxis(dataCurr, 0));
        page.draw(line);
    }
    
    // 畫 K 線圖
    protected void drawKBarChart (Graphics2D page, float[] data, float x, float scale) {
		if (data[4] > data[1]) {
			page.setColor(Color.RED);
			rect.setRect(x, calculateAxis(data[4], 0), scale, calculateAxis(data[1], 0) - calculateAxis(data[4], 0));
	        page.fill(rect);
	        line.setLine(x + scale / 2, calculateAxis(data[2], 0), x + scale / 2, calculateAxis(data[3], 0));
	        page.draw(line);
		}
		else if (data[4] < data[1]) {
			page.setColor(Color.GREEN);
			rect.setRect(x, calculateAxis(data[1], 0), scale, calculateAxis(data[4], 0) - calculateAxis(data[1], 0));
	        page.fill(rect);
	        line.setLine(x + scale / 2, calculateAxis(data[2], 0), x + scale / 2, calculateAxis(data[3], 0));
	        page.draw(line);
		}
		else {
			page.setColor(Color.WHITE);
			line.setLine(x, calculateAxis(data[1], 0), x + scale, calculateAxis(data[1], 0));
	        page.draw(line);
	        line.setLine(x + scale / 2, calculateAxis(data[2], 0), x + scale / 2, calculateAxis(data[3], 0));
	        page.draw(line);
		}
	}
    
    // 畫成交量圖
    protected void drawQuanChart (Graphics2D page, float data, float x, float scale) {
    	float y = calculateAxis(data, 1);
        page.setColor(new Color(255, 156, 0));
        rect.setRect(x, y, scale, graphD.y + start_quan + height_quan - y);
        page.fill(rect);	// 畫柱圖
        page.setColor(Color.WHITE);
        line.setLine(x += scale > 1 ? 0 : scale, y, x, graphD.y + start_quan + height_quan - line_cap);
        page.draw(line);	// 畫柱圖分隔線
	}
    
    // 畫均線圖
    protected void drawAvgLine (Graphics2D page, int count, int len, int drawType, int index, float x, float scale, Color color) {
    	int use = drawType * avgNum / 2 + count - 1;
    	if (index - range_start < len)
    		return;
    	else if (avgPrev[use] == 0.0) {
	    	for (int i = 0; i < len; i++)
	    		if (drawType == 0)
	    			avgPrev[use] += sourceData.get(index - i - 1)[4];
	    		else
	    			avgPrev[use] += sourceData.get(index - i - 1)[5];
	    	
    	}
    	
		if (drawType == 0)
			avgCurr[use] = avgPrev[use] + sourceData.get(index)[4] - sourceData.get(index - len)[4];
		else
			avgCurr[use] = avgPrev[use] + sourceData.get(index)[5] - sourceData.get(index - len)[5];
		
    	page.setColor(color);
    	line.setLine(x, calculateAxis(avgPrev[use] / len, drawType), x + scale, calculateAxis(avgCurr[use] / len, drawType));
        page.draw(line);
        
        // 文字說明
        if (index  == range_start + range - 1)
	        if (drawType == 0)
	        	page.drawString("Avg" + len + "=" + nf.format(avgCurr[use] / len), graphD.x + graphD.width + line_cap, 
	        			graphD.y + start_main + line_cap + char_height * count);
	        else
	        	page.drawString("Avg" + len + "=" + nf.format(avgCurr[use] / len), graphD.x + graphD.width + line_cap, 
	        			graphD.y + start_quan + line_cap + char_height * count);
        
        avgPrev[use] = avgCurr[use];
    }
    
    // 標示線圖資訊 (指數及成交量)
    protected void drawChartLabel (Graphics2D page, int drawType) {	// 0:指數 1:成交量
    	float y;
    	int showNum = drawType == 0 ? priceNum : quanNum;
    	float[] showString = drawType == 0 ? priceInitial : quanInitial;
    	
		for (int i = 0; i < showNum; i++) {
			if (limiteType == TYPE_UNLIMITED)
				if (drawType == 0)
					page.setColor(Color.WHITE);
				else
					page.setColor(new Color(255, 156, 0));
			else if (drawType == 0)
	    		if (i == 0 && showString[1] > showString[0] * 1.07 - calculateUnit(showString[0]))
	                page.setColor(Color.RED);
	    		else if (i == priceNum - 1 && showString[priceNum] < showString[0] * 0.93 + calculateUnit(showString[0]))
	                page.setColor(new Color(0, 128, 0));
	    		else
	                page.setColor(Color.WHITE);
			else
				page.setColor(new Color(255, 156, 0));
			
			if (i < showNum - 1 || drawType != 1)
				page.drawString(nf.format(showString[i + 1]), 0, calculateAxis(-1.0f * i - 1, drawType));
			
			if (limiteType == TYPE_UNLIMITED)
	        	if (drawType == 0 && i == (showNum + 1) / 2)
	        		page.setColor(Color.LIGHT_GRAY);
	        	else
	        		page.setColor(Color.DARK_GRAY);
			else if (drawType == 1)
				page.setColor(Color.DARK_GRAY);
			else if (i == 0)
				page.setColor(new Color(128, 0, 0));
			else if (i == (showNum - 1) / 2)
				page.setColor(Color.LIGHT_GRAY);
			else if (i == showNum - 1)
				page.setColor(new Color(0, 128, 0));
			else
				page.setColor(Color.DARK_GRAY);
			
			y = calculateAxis(showString[i + 1], drawType);
			drawBrokenLine(page, graphD.x, y, graphD.x + graphD.width, y); // 畫橫虛線
		}
		
		if (drawType == 1) {
			page.setColor(new Color(255, 156, 0));
			page.drawString(stockId == 1000 || stockId == 0 || stockId == 4000 ? "億(新台幣)" : "張", 
					0, graphD.y + start_quan + height_quan + char_height / 2);
		}
    }
    
    // 標示線圖資訊 (時間)
    protected void drawChartLabel (Graphics2D page, int time, float x) {
    	page.setColor(Color.LIGHT_GRAY);
    	page.drawString(numToTime(time), x - GUI.overall_size[6], screen_y - 1);	// 秀出時間資訊
    	page.setColor(Color.DARK_GRAY);
    	drawBrokenLine(page, x, graphD.y, x, graphD.y + graphD.height);	// 畫縱虛線
    }
    
    // 標示線圖資訊 (時間)
    protected void drawChartLabel (Graphics2D page, float x, int date) {
    	page.setColor(Color.LIGHT_GRAY);
    	page.drawString(numToDate(date), x - GUI.overall_size[13], screen_y - 1);	// 秀出時間資訊
    	page.setColor(Color.DARK_GRAY);
    	drawBrokenLine(page, x, graphD.y, x, graphD.y + graphD.height);	// 畫縱虛線
    }
    
    // 標示滑鼠指標資訊
    protected void drawMouseInfo (Graphics2D page, boolean drawQuan) {
        try {
			int data;
			float[] currData;

			if (mouse_x > graphD.x && mouse_x < graphD.x + graphD.width &&	// 判斷滑鼠有沒有在作動範圍內
					mouse_y >= graphD.y + line_cap / 2 && mouse_y <= graphD.y + graphD.height - line_cap / 2) {
				// 顯示個股基本資料
				currData = mouse_x == -1 ?
						sourceData.getLast() : sourceData.get((int) Math.round((mouse_x - graphD.x - line_cap / 2) / scale) + range_start + 1);

				page.setColor(Color.YELLOW);
				line.setLine(graphD.x - line_cap / 2, mouse_y, graphD.x + graphD.width - line_cap / 2, mouse_y);
				page.draw(line);	// 畫滑鼠指數線
				line.setLine(mouse_x, graphD.y + line_cap / 2, mouse_x, graphD.y + graphD.height - line_cap / 2);
				page.draw(line); 	// 畫滑鼠時間線
				
				page.setColor(Color.WHITE);
				if (mouse_y < graphD.y + start_quan + height_quan) {
					rect.setRect(0, mouse_y - char_height / 2 - 1, graphD.x - line_cap / 2, char_height + 2);
					page.fill(rect);		// 畫指數軸欄位
				}
				if (mouse_x + GUI.overall_size[14] > graphD.x + graphD.width) {			// 避免圖形繪出範圍外
					rect.setRect(mouse_x - GUI.overall_size[26], graphD.y + graphD.height + line_cap / 2 + 1, 
							GUI.overall_size[26], char_height + 2);
					page.fill(rect);		// 畫時間軸欄位
					page.setColor(Color.BLACK);
					page.drawString(numToDate(currData[0]), mouse_x - GUI.overall_size[25], screen_y - 1);		// 秀出時間軸時間
				} else if (mouse_x - GUI.overall_size[14] < GUI.overall_size[20]) {
					rect.setRect(mouse_x, graphD.y + graphD.height + line_cap / 2 + 1, GUI.overall_size[26], 
							char_height + 2);
					page.fill(rect);		// 畫時間軸欄位
					page.setColor(Color.BLACK);
					page.drawString(numToDate(currData[0]), mouse_x + GUI.overall_size[1], screen_y - 1);		// 秀出時間軸時間
				} else {
					rect.setRect(mouse_x - GUI.overall_size[13], graphD.y + graphD.height + line_cap / 2 + 1, 
							GUI.overall_size[26], char_height + 2);
					page.fill(rect);		// 畫時間軸欄位
					page.setColor(Color.BLACK);
					page.drawString(numToDate(currData[0]), mouse_x - GUI.overall_size[12], screen_y - 1);		// 秀出時間軸時間
				}

				if (drawQuan && mouse_y > graphD.y + (start_main + height_main + start_quan) / 2) {	// 判斷是價還是量
					data = 5;
					
					if (mouse_y < graphD.y + start_quan + height_quan) {
//						return;
						page.setColor(new Color(0, 99, 255));
						page.drawString(nf.format(calculateQuan(mouse_y)), 0, mouse_y + char_height / 2);	// 秀出成交量軸資訊
						page.setColor(Color.WHITE);
					}
				}
				else {
					data = 4;
					page.setColor(Color.BLACK);
					page.drawString(nf.format(calculatePrice(mouse_y)), 0, mouse_y + char_height / 2);	// 秀出指數軸資訊
					page.setColor(Color.YELLOW);
				}
				
				// 顯示十字游標上的資訊
				if (mouse_y < graphD.y + start_quan + height_quan) {
					String currdata = nf.format(currData[data]);
					if (mouse_x + GUI.overall_size[18]< graphD.x + graphD.width)			// 避免字串繪出範圍外
						if (mouse_y < graphD.y + line_cap / 2 + char_height)
							page.drawString(currdata, mouse_x - GUI.overall_size[18] - 1, mouse_y + char_height + 1);	// 秀出股價資訊
						else
							page.drawString(currdata, mouse_x + 1, mouse_y - 1);		// 秀出股價資訊
					else
						if (mouse_y < graphD.y + line_cap / 2 + char_height)
							page.drawString(currdata, mouse_x - GUI.overall_size[18] - 1, mouse_y + char_height + 1);	// 秀出股價資訊
						else
							page.drawString(currdata, mouse_x - GUI.overall_size[18] - 1, mouse_y - 1);	// 秀出股價資訊
				}
			}
			else
				currData = sourceData.getLast();

			page.setColor(Color.CYAN);
			page.drawString(String.valueOf(stockId), screen_x * 0.01f, GUI.overall_size[19]);
			page.drawString(sName, screen_x * 0.01f, GUI.overall_size[24]);
			page.setColor(Color.WHITE);
			page.drawString("日期 " + numToDate(currData[0]), screen_x * 0.17f, GUI.overall_size[19]);
			page.drawString("開盤 " + nf.format(currData[1]), screen_x * 0.47f, GUI.overall_size[19]);
			if (currData[4] > currData[1])
				if (currData[4] > currData[1] * 1.07 - calculateUnit(currData[1])) {
					page.setColor(Color.RED);
					rect.setRect(screen_x * 0.82f, GUI.overall_size[15], GUI.overall_size[19], GUI.overall_size[5]);
					page.fill(rect);
					page.setColor(Color.WHITE);
				}
				else
					page.setColor(Color.RED);
			else if (currData[4] < currData[1])
				if (currData[4] < currData[1] * 0.93 + calculateUnit(currData[1])) {
					page.setColor(new Color(0, 128, 0));
					rect.setRect(screen_x * 0.82f, GUI.overall_size[15], GUI.overall_size[19], GUI.overall_size[5]);
					page.fill(rect);
					page.setColor(Color.WHITE);
				}
				else
					page.setColor(Color.GREEN);
			page.drawString("收盤 " + nf.format(currData[4]), screen_x * 0.77f, GUI.overall_size[19]);
			page.setColor(Color.WHITE);
			page.drawString("成交量 " + nf.format(currData[5]) + "(億元)", screen_x * 0.17f, GUI.overall_size[19] + char_height);
			page.setColor(Color.RED);
			page.drawString("最高 " + nf.format(currData[2]), screen_x * 0.47f, GUI.overall_size[19] + char_height);
			page.setColor(Color.GREEN);
			page.drawString("最低 " + nf.format(currData[3]), screen_x * 0.77f, GUI.overall_size[19] + char_height);
		} catch (IndexOutOfBoundsException e) {
			mouse_x = (int) Math.round(graphD.x + graphD.width - line_cap / 2);
			repaint();
		}
    }
    
    protected void drawCanvas (Graphics2D page) {
        page.setPaint(Color.BLUE);
        page.setStroke(new BasicStroke(line_cap, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND));
        line.setLine(graphD.x, graphD.y, graphD.x, graphD.y + graphD.height);
        page.draw(line);	// 畫左框
        line.setLine(graphD.x + graphD.width, graphD.y, graphD.x + graphD.width, graphD.y + graphD.height);
        page.draw(line);	// 畫右框
        line.setLine(graphD.x, graphD.y, graphD.x + graphD.width, graphD.y);
        page.draw(line); 	// 畫上框
        line.setLine(graphD.x, graphD.y + graphD.height, graphD.x + graphD.width, graphD.y + graphD.height);
        page.draw(line); 	// 畫下框
        page.setStroke(new BasicStroke(1, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND));
    }
    
    // 由數值計算座標
    protected float calculateAxis (float data, int calType) {	// 0:指數 1:成交量
    	if (data < 0)
    		if (calType == 0)
    			return (height_main) / (priceNum - 1) * -1.0f * (data + 1) + 
    					graphD.y + start_main + char_height / 2;
    		else
        		return height_quan / (quanNum - 1) * -1.0f * (data + 1) + 
        				graphD.y + start_quan + char_height / 2;
    	
    	if (calType == 0) {
    		ratio = (priceInitial[1] - data) / priceSep;
        	return (height_main) / (priceNum - 1) * 1.0f * ratio + graphD.y + start_main;
    	}
    	else {
    		ratio = (quanInitial[1] - data) / quanSep;
        	return height_quan / (quanNum - 1) * ratio + graphD.y + start_quan;
    	}
    }
    
    // 由座標計算成交價
    protected float calculatePrice (int point_y) {
    	return priceInitial[1] - (priceNum - 1) * (point_y - graphD.y - start_main) * priceSep / height_main;
    }
    
    // 由座標計算成交量
    protected float calculateQuan (int point_y) {
    	float initialAxis = calculateAxis(quanInitial[1], 1);
    	return quanInitial[1] - (point_y - initialAxis) * 1.0f / (calculateAxis(quanInitial[2], 1) - initialAxis) * quanSep;
    }
    
    // 計算升降單位
    public static float calculateUnit (float data) {
    	float unit;
    	
    	if (data < 10.0)
    		unit = 0.01f;
    	else if (data < 50.0)
    		unit = 0.05f;
    	else if (data < 100.0)
    		unit = 0.1f;
    	else if (data < 500.0)
    		unit = 0.5f;
    	else if (data < 1000.0)
    		unit = 1.0f;
    	else
    		unit = 5.0f;
    	
    	return unit;
    }
    
    // 產生日期格式
    protected String numToDate (float date) {
    	date_mark[0] = String.valueOf((int) date);
    	date_mark[2] = date_mark[0].substring(6);
    	date_mark[1] = date_mark[0].substring(4, 6);
    	date_mark[0] = date_mark[0].substring(0, 4); 
    	
    	return date_mark[0].concat("/").concat(date_mark[1]).concat("/").concat(date_mark[2]);
    }
    
    // 產生時間格式
    protected String numToTime (float time) {
    	time_mark[0] = String.valueOf((int) time);
    	if (time_mark[0].length() == 5) {
    		time_mark[1] = time_mark[0].substring(1, 3);
    		time_mark[0] = time_mark[0].substring(0, 1);
    	} else {
    		time_mark[1] = time_mark[0].substring(2, 4);
    		time_mark[0] = time_mark[0].substring(0, 2);
    	}
    	
    	return time_mark[0].concat(":").concat(time_mark[1]);
    }
    
    // 將線圖輸出圖檔
    public void outputImage () {
    	BufferedImage image = new BufferedImage((int) Math.round(screen_x), (int) Math.round(screen_y - cpl_height), BufferedImage.TYPE_INT_RGB);
		Graphics bufferGraph = image.createGraphics();
		String imageFormat = "png";
		JPEGImageWriteParam jpgIWP = null;
		
		bufferGraph.translate(0, -1 * (int) Math.round(cpl_height));
		getRootPane().paintAll(bufferGraph);

		Date date = new Date(System.currentTimeMillis());
		DateFormat df= new SimpleDateFormat("yyyyMMdd-HHmmss");
        ImageOutputStream ios;
        Iterator<ImageWriter> ite = ImageIO.getImageWritersByFormatName(imageFormat);
        ImageWriter imageWriter = ite.next();

        if (imageFormat.equals("jpeg")) {
        	jpgIWP = (JPEGImageWriteParam) imageWriter.getDefaultWriteParam();
        	jpgIWP.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        	jpgIWP.setCompressionQuality(1.0f);
        }
        
        
		try {
			ios = ImageIO.createImageOutputStream(new File("common/source/" + stockId + "_" + df.format(date) + "." + imageFormat));
            imageWriter.setOutput(ios);
            if (imageFormat.equals("jpeg"))
            	imageWriter.write(null, new IIOImage(image, null, null), jpgIWP);
            else
            	imageWriter.write(image);
            ios.flush();
			ios.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
    }
    
    public class outputListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			outputImage();
		}
    }
    
    public class stockAction implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			int sid;
//			System.out.println("item changed!" + System.currentTimeMillis());
			sid = StockBox.getStockId(stockBox);
			if (sid != -1) {
				stockId = sid;
				defaultBtn[4].setActionCommand(String.valueOf(stockId));
				readData();
				repaint();
			}
		}
    }
    
    public class metaMenuListener extends MouseAdapter {
		public void mouseClicked (MouseEvent event) {
			if (event.isMetaDown()) {
				metaMenu.show(event.getComponent(), event.getX(), event.getY());
			}
		}
    }
    
    public class menuItemListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			dateType = Integer.parseInt(event.getActionCommand());
			
			if (dateType == 3)
	    		for (int i = 0; i < 4; i++)
	    			defaultBtn[i].setVisible(false);
	    	else {
	    		for (int i = 0; i < 4; i++)
	    			defaultBtn[i].setVisible(true);
	    		range = (int) ((graphD.width - line_cap) / scale);
	    	}

			readData();
			repaint();
		}
    }
    
    public class scaleListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			if (e.getComponent().equals(defaultBtn[0])) {
				if (!defaultBtn[1].isVisible())
					defaultBtn[1].setVisible(true);
				scale += 1;
				if (scale >= 30)
					defaultBtn[0].setVisible(false);

				repaint();
			} else if (e.getComponent().equals(defaultBtn[1])) {
				if (!defaultBtn[0].isVisible())
					defaultBtn[0].setVisible(true);
				scale -= 1;
				if (scale <= 1)
					defaultBtn[1].setVisible(false);
				
				repaint();
			}
		}
    }
    
    public class shiftListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			if (e.getComponent().equals(defaultBtn[2]))
				shift += range_start - range > 0 ? -1 * range : -1 * (sourceData.size() + shift - range);
			else
				shift += range_start + range < sourceData.size() ? range : -1 * shift;
			
			repaint();
		}
    }
    
    public class timeLineListener implements MouseMotionListener {
		public void mouseDragged(MouseEvent e) {
			mouse_x = e.getX();
			mouse_y = e.getY();
			
			repaint();
		}

		public void mouseMoved(MouseEvent e) {

			mouse_x = e.getX();
			mouse_y = e.getY();
			
			repaint();
		}
    }
}