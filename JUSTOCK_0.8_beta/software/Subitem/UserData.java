/**
 * @author kevingo
 * @modify bluesway
 * TARGET 使用者資料
 */

package software.Subitem;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import software.DB;
import software.GUI;

public class UserData {
    // 顯示使用者畫面
    private JLabel idLabel;
    private JLabel pwdLabel;
	private JLabel nameLabel;
	private JLabel phoneLabel;
	private JLabel addrLabel;
	private JLabel idnumLabel;
	private JLabel genderLabel;
	private JLabel emailLabel;
	private JLabel loginLabel;
	public Container container;
	public JTextField idField;
	public JPanel pwdPane;
	public CardLayout pwdCard;
	public JTextField pwdMaskField;
	public JPasswordField pwdField;
	public JTextField nameField;
	public JTextField phoneField;
	public JTextField addrField;
	public JTextField idnumField;
	public JTextField genderField;
	public JTextField emailField;
	public JTextField loginField;
	public JButton okBtn;
	public JButton modifyBtn;

	private String[] pwdMask = {"********", "(請輸入新密碼，不修改請跳過)"};
	
	public UserData() {
		setFrame();
    }

    private void setFrame() {
    	GridBagConstraints g = new GridBagConstraints();
    	buttonListener bl = new buttonListener();
    	
    	container = new Container();
		okBtn = new JButton("確認");
		modifyBtn = new JButton();
		
		container.setLayout(new GridBagLayout());
		container.setSize(400, 300);

		g.weighty = 1; // 設定各個元件之間的分散程度

		idLabel = new JLabel("使用者帳號：");
		idField = new JTextField(20);
		idField.setEditable(false);
		g.gridx = 0;
		g.gridy = 0;
		container.add(idLabel, g);
		g.gridx = 1;
		g.gridy = 0;			
		container.add(idField, g);
		
		pwdCard = new CardLayout();
		pwdPane = new JPanel();
		pwdPane.setLayout(pwdCard);
		pwdLabel = new JLabel("使用者密碼：");
		pwdMaskField = new JTextField(20);
		pwdField = new JPasswordField(20);
		pwdMaskField.setEditable(false);
		pwdField.setEditable(true);
//		pwdCard.addLayoutComponent(pwdMaskField, "0");
//		pwdCard.addLayoutComponent(pwdField, "1");
		pwdPane.add("0", pwdMaskField);
		pwdPane.add("1", pwdField);
		g.gridx = 0;
		g.gridy = 1;
		container.add(pwdLabel, g);
		g.gridx = 1;
		g.gridy = 1;
		container.add(pwdPane, g);
		
		nameLabel = new JLabel("姓名：");
		nameField = new JTextField(20);
		nameField.setEditable(false);
		g.gridx = 0;
		g.gridy = 2;
		container.add(nameLabel, g);
		g.gridx = 1;
		g.gridy = 2;
		container.add(nameField, g);

		phoneLabel = new JLabel("電話：");
		phoneField = new JTextField(20);
		phoneField.setEditable(false);
		g.gridx = 0;
		g.gridy = 3;
		container.add(phoneLabel, g);
		g.gridx = 1;
		g.gridy = 3;
		container.add(phoneField, g);
		
		addrLabel = new JLabel("地址：");
		addrField = new JTextField(20);
		addrField.setEditable(false);
		g.gridx = 0;
		g.gridy = 4;
		container.add(addrLabel, g);
		g.gridx = 1;
		g.gridy = 4;
		container.add(addrField, g);
		
		idnumLabel = new JLabel("身分證字號：");
		idnumField = new JTextField(20);
		idnumField.setEditable(false);
		g.gridx = 0;
		g.gridy = 5;
		container.add(idnumLabel, g);
		g.gridx = 1;
		g.gridy = 5;
		container.add(idnumField, g);
		
		genderLabel = new JLabel("性別：");
		genderField = new JTextField(20);
		genderField.setEditable(false);
		g.gridx = 0;
		g.gridy = 6;				
		container.add(genderLabel, g);
		g.gridx = 1;
		g.gridy = 6;
		container.add(genderField, g);
		
		emailLabel = new JLabel("電子郵件：");
		emailField = new JTextField(20);
		emailField.setEditable(false);
		g.gridx = 0;
		g.gridy = 7;
		container.add(emailLabel, g);
		g.gridx = 1;
		g.gridy = 7;
		container.add(emailField, g);
		
		loginLabel = new JLabel("登入次數：");
		loginField = new JTextField(20);
		loginField.setEditable(false);
		g.gridx = 0;
		g.gridy = 8;
		container.add(loginLabel, g);
		g.gridx = 1;
		g.gridy = 8;
		container.add(loginField, g);
		
		g.gridx = 0;
		g.gridy = 9;
		container.add(okBtn, g);
		g.gridx = 1;
		g.gridy = 9;
		container.add(modifyBtn, g);
		
		pwdMaskField.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent arg0) {
				if (pwdMaskField.isEditable()) {
					pwdCard.last(pwdPane);
					pwdField.requestFocusInWindow();
				}
			}
			
			public void focusLost(FocusEvent arg0) {
				if (pwdMaskField.isEditable())
					pwdMaskField.setText(pwdMask[1]);
			}
		});
		pwdField.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent arg0) {
			}

			public void focusLost(FocusEvent arg0) {
				if (String.valueOf(pwdField.getPassword()).equals(""))
					pwdCard.first(pwdPane);
			}
		});
		modifyBtn.addActionListener(bl);
		
		setData();
	}
    
    private void setData() {
    	pwdCard.first(pwdPane);
    	pwdMaskField.setText(pwdMask[0]);
    	modifyBtn.setText("修改");
    	idField.setText(GUI.userData[0]);
    	nameField.setText(GUI.userData[2]);
    	phoneField.setText(GUI.userData[3]);
    	addrField.setText(GUI.userData[4]);
    	idnumField.setText(GUI.userData[5]);
    	genderField.setText(GUI.userData[6]);
    	emailField.setText(GUI.userData[7]);
    	loginField.setText(GUI.userData[8]);
    }
    
    private void updateData() {
    	String newPwd;
    	String encryptPasswd;
    	newPwd = String.valueOf(pwdField.getPassword());
    	encryptPasswd = GUI.userData[1];
    	Connection conn = null;
    	Statement stmt = null;

    	if (!String.valueOf(pwdField.getPassword()).equals(""))
    		if (!common.util.PwdMD5.encrypt(newPwd).equals(GUI.userData[1])) {
    			JOptionPane pane = new JOptionPane();
    			JDialog dialog = pane.createDialog(GUI.gui, "密碼修改");
    			pane.setOptionType(JOptionPane.YES_NO_OPTION);
    			pane.setMessageType(JOptionPane.QUESTION_MESSAGE);
    			JPanel verifyPane = new JPanel();
    			JLabel verifyLabel = new JLabel("請輸入原密碼");
    			final JPasswordField verifyField = new JPasswordField(20);
    			verifyPane.setLayout(new BorderLayout());
    			verifyPane.add(verifyLabel, BorderLayout.NORTH);
    			verifyPane.add(verifyField, BorderLayout.SOUTH);
    			pane.setMessage(verifyPane);
    			dialog.addComponentListener(new ComponentAdapter() {
    	            public void componentShown(ComponentEvent ce) {
    	                verifyField.requestFocusInWindow();
    	            }
    	        });

    			dialog.setVisible(true);
    			
    			if (!common.util.PwdMD5.encrypt(String.valueOf(verifyField.getPassword())).equals(encryptPasswd)) {
    				JOptionPane.showMessageDialog(null, "原密碼不符！資料修改失敗..", "系統狀態", JOptionPane.ERROR_MESSAGE);
    				return;
    			}

    			verifyLabel.setText("請確認新密碼");
    			verifyField.setText("");
    			dialog.setVisible(true);
    			
    			if (!String.valueOf(verifyField.getPassword()).equals(String.valueOf(pwdField.getPassword()))) {
    				JOptionPane.showMessageDialog(null, "新密碼不符！資料修改失敗..", "系統狀態", JOptionPane.ERROR_MESSAGE);
    				return;
    			}
    			else
    				encryptPasswd = common.util.PwdMD5.encrypt(String.valueOf(verifyField.getPassword()));
    		}
    	
        try {
        	conn = DB.db.getConnection();
        	stmt = conn.createStatement();
        	
			stmt.executeUpdate("UPDATE account SET uPwd=\"" + encryptPasswd + "\", uPhone=\"" + phoneField.getText() + 
					"\", uAddr=\"" + addrField.getText() + "\", uEmail=\"" + emailField.getText() + 
					"\" WHERE Uid=\"" + GUI.userData[0] + "\"");
			
			setUserData(GUI.userData[0]);
			JOptionPane.showMessageDialog(null, "資料修改成功", "系統狀態", JOptionPane.PLAIN_MESSAGE);
		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "資料修改失敗！", "系統狀態", JOptionPane.ERROR_MESSAGE);
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null)
					DB.db.closeConnection(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
    }
    
    public Container getFrame() {
		return container;
    }
    
    public static void setUserData(String userId) {
    	GUI.userData = getUserData(userId);
    }
    
    private static String[] getUserData(String userId) {
    	Connection conn = null;
    	Statement stmt = null;
    	ResultSet result = null;
    	String[] userData = new String[9];
    	
        try {
        	conn = DB.db.getConnection();
        	stmt = conn.createStatement();
        	result = stmt.executeQuery("SELECT * from account where Uid='" + userId + "'");
        	
        	if (result.next())
        		for (int i = 0; i < 9; i++)
        			userData[i] = result.getString(i + 1);
        } catch (SQLException e) {
            System.err.println(e);
		} finally {
			try {
				if (result != null)
					result.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					DB.db.closeConnection(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
        
        return userData.clone();
    }
    
    public static String[] getUserData(String userId, String passwd) {
    	Connection conn = null;
    	Statement stmt = null;
    	ResultSet result = null;
    	String[] userData = new String[9];
    	
        try {
        	conn = DB.db.getConnection();
        	stmt = conn.createStatement();
        	result = stmt.executeQuery("SELECT * from account where Uid='" + userId + "'");
        	String encryptPasswd;
        	encryptPasswd = common.util.PwdMD5.encrypt(passwd);
        	
        	if(result.next() && result.getString("uPwd").equals(encryptPasswd)) // 帳號、密碼正確
        		for (int i = 0; i < 9; i++)
        			userData[i] = result.getString(i + 1);
        } catch (SQLException e) {
            System.err.println(e);
		} finally {
			try {
				if (result != null)
					result.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					DB.db.closeConnection(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
        
        return userData.clone();
    }
    
    public static boolean chkPwd (String userId, String passwd) {
    	ResultSet result;
    	String encryptPasswd = common.util.PwdMD5.encrypt(passwd);
    	
        try {
        	result = DB.db.getConnection().createStatement().executeQuery("SELECT * from account where Uid='" + userId + "'");
        	return (result.next() && result.getString("uPwd").equals(encryptPasswd)); // 帳號、密碼正確
        } catch (SQLException e) {
            System.err.println(e);
            return false;
        }
        
    }

    public class buttonListener implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			if (pwdMaskField.isEditable()) {
				if (JOptionPane.showConfirmDialog(null, "是否更新資料？", "資料修改", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
					updateData();
				pwdField.setText("");
				pwdMaskField.setEditable(false);
				setData();
			} else {
				pwdMaskField.setEditable(true);
				pwdMaskField.setText(pwdMask[1]);
				phoneField.requestFocusInWindow();
				modifyBtn.setText("更新");
			}
			
			phoneField.setEditable(!phoneField.isEditable()); 
			addrField.setEditable(!addrField.isEditable()); 
			emailField.setEditable(!emailField.isEditable()); 
		}
    }
}