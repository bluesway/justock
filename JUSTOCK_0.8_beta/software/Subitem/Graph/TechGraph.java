/**
 * @author bluesway
 * TARGET 技術分析圖
 */

package software.Subitem.Graph;

import java.awt.Color;
//import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import javax.swing.JFrame;

import common.util.StockBox;
import software.GUI;
import software.Subitem.GeneralGraph;

@SuppressWarnings("serial")
public class TechGraph extends GeneralGraph {
	// KD指標
	private int kd_len;
	private float rsv;
	private float k_pvalue;
	private float k_cvalue;
	private float d_pvalue;
	private float d_cvalue;
	private float[] kd_data;
	
	// Bias乖離率
	private float bavgPrev;
	private float bavgCurr;
	private float biasPrev;
	private float biasCurr;
	
    public TechGraph () {
    	this(1000, 0, 1);
    }
    
	public TechGraph (int stockId, int dType, int lType) {
		this.stockId = stockId == 0 ? 1000 : stockId;
    	isOverall = false;
		
    	// 顯示型態
    	limiteType = lType;
    	displayType = dType;
    	
    	// 圖形繪製
    	line_cap = 2;
    	char_height = GUI.overall_size[5];
    	cpl_height = GUI.overall_size[25];
    	left_space = GUI.overall_size[22];
    	right_space = GUI.overall_size[35];
    	start_main = 15;

    	// 資料處理
    	sourceData = new LinkedList<float[]>();
    	sourceMap = new HashMap<Integer, float[]>();
    	priceNum = 9;
    	quanNum = 3;
    	scale = 3.0f;
    	shift = 0;
    	range = 0;
    	range_start = 0;
    	mouse_x = -1;
    	mouse_y = -1;
    	avgNum = 6;
    	avgPrev = new float[avgNum];
    	avgCurr = new float[avgNum];
    	kd_len = 9;
    	kd_data = new float[kd_len+1];
    	k_pvalue = 50.0f;
    	d_pvalue = 50.0f;
    	bavgPrev = 0.0f;
    	bavgCurr = 0.0f;
    	
    	// 控制台配置
    	if (this.stockId != 1000 && this.stockId != 4000) {
    		createBox();
    		add(stockBox);
    	}
    	
		add(defaultBtn[2]);
		add(defaultBtn[1]);
		add(defaultBtn[0]);
		add(defaultBtn[3]);
    	
		readData();
		
    	if (this.stockId != 1000 && this.stockId != 4000) {
    		add(defaultBtn[4]);
        	defaultBtn[4].setActionCommand(String.valueOf(this.stockId));
    		stockBox.setSelectedIndex(StockBox.searchIndexOfStockId(this.stockId));
    		GUI.gui.setStockId(this.stockId);
    	}
	}
	
	public void paintComponent(Graphics g) {
		long a = System.currentTimeMillis();
    	Graphics2D page = (Graphics2D) g;
    	
    	// 重新讀取資料
     	screen_x = getWidth();
     	screen_y = getHeight();
    	graphD.setRect(getX() + left_space, getY() + cpl_height + 10, screen_x - left_space - right_space, screen_y - cpl_height - 25);
    	height_main = graphD.height * 0.4f;
    	start_quan = graphD.height * 0.5f;
    	height_quan = graphD.height * 0.16f;
    	start_tech1 = graphD.height * 0.7f;
    	height_tech1 = graphD.height * 0.15f;
    	start_tech2 = graphD.height * 0.85f;
    	height_tech2 = graphD.height * 0.15f - line_cap / 2;
    	avgPrev = new float[avgNum];
    	avgCurr = new float[avgNum];
    	k_pvalue = 50.0f;
    	d_pvalue = 50.0f;
    	bavgPrev = 0.0f;
    	bavgCurr = 0.0f;
    	
    	if (dateType != 3)
    		range = (int) ((graphD.width - line_cap) / scale) + 1;
    	
    	super.paintComponent(page);
    	
    	float scale_tmp = scale;
    	float x = graphD.x + line_cap / 2;
    	boolean drawQuan = sourceData.get(range_start + range - 1)[0] > 19870106 ? true : dateType == 3 ? true : false;

    	// 使圖形貼齊外框
    	scale = (graphD.width - line_cap) / (range - 1);
    	
    	// 顯示指數資訊
    	drawChartLabel(page, 0);
    	
    	// 顯示成交量資訊
    	if (drawQuan)
    		drawChartLabel(page, 1);
    	
    	// 畫線圖、柱圖及顯示時間資訊
    	while (time - range_start < range && time < sourceData.size()) {
    		now = sourceData.get(time);
    		if (stockId == 0)
    			drawLineChart(page, prev[1], now[1], x, scale);
    		else if (displayType == TYPE_LINE_CHART )
    			drawLineChart(page, prev[4], now[4], x, scale);
    		else if (displayType == TYPE_KBAR_CHART)
    			drawKBarChart(page, now, x, scale);
    		else {
    			drawKBarChart(page, now, x, scale);
    			drawLineChart(page, prev[4], now[4], x, scale);
    		}
    		
    		if (stockId != 0) {
    			drawAvgLine(page, 1, 5, 0, time, x, scale, Color.YELLOW);
    			drawAvgLine(page, 2, 10, 0, time, x, scale, Color.CYAN);
    			drawAvgLine(page, 3, 20, 0, time, x, scale, Color.MAGENTA);
    		}
    		
    		if (drawQuan)
    			drawQuanChart(page, stockId == 0 ? now[3] : now[5], x, scale);
    		
    		if (stockId != 0) {
    			drawAvgLine(page, 1, 5, 1, time, x, scale, Color.YELLOW);
    			drawAvgLine(page, 2, 10, 1, time, x, scale, Color.CYAN);
    			drawAvgLine(page, 3, 20, 1, time, x, scale, Color.MAGENTA);
    		}
    		
    		drawKDIndex(page, time, x, scale, Color.PINK, Color.ORANGE);
//            if (dateType == 0)
    		drawBiasCurve(page, 10, time, x, scale, Color.GREEN);
//            else if (dateType == 1)
//            	drawBiasCurve(page, 25, time, x, scale, Color.GREEN);
//            else if (dateType == 2)
//            	drawBiasCurve(page, 72, time, x, scale, Color.GREEN);
    		
    		x += scale;
    		
    		// 標示日期
    		if (dateType != 3)
    			if ((time - range_start) % (int) Math.round(graphD.width / scale / GUI.overall_size[2]) == 0 && (time - range_start)> 0 && x + GUI.overall_size[6] < graphD.x + graphD.width - line_cap / 2)
    				drawChartLabel(page, x, (int) now[0]);
    		
    		prev = now;
    		time++;
    	}
    	
    	if (dateType == 3) {
    		// 顯示時間資訊
    		drawChartLabel(page, 100000, numToAxis(100000));
    		drawChartLabel(page, 110000, numToAxis(110000));
    		drawChartLabel(page, 120000, numToAxis(120000));
    		drawChartLabel(page, 130000, numToAxis(130000));
    		
    		// 處理滑鼠指標
    		drawMouseInfo(page);
    	} else
    		drawMouseInfo(page, drawQuan);
    	
        // 畫邊框
		drawCanvas(page);
		a = System.currentTimeMillis() - a;
//		System.out.println("繪圖完成，共花費" + a / 1000.0 + "秒");
		
        // 復原 scale 使其不致於愈繪愈小 
        scale = scale_tmp;
	}
	
    // 畫個股當日成交量圖
	protected void drawQuanChart (Graphics2D page, float[] data, int x1) {
    	float y = calculateAxis(data[3], 1);
    	if (data[1] == data[4])
    		page.setColor(Color.RED);
    	else
    		page.setColor(Color.GREEN);
    	line.setLine(x1, y, x1, graphD.y + start_quan + height_quan - line_cap);
    	page.draw(line);	// 畫柱圖分隔線
	}

    // 畫均線圖
    private void drawKDIndex (Graphics2D page, int index, float x, float scale, Color c1, Color c2) {
    	if (index - range_start < kd_len)
    		return;
    	
    	for (int i = 0; i < kd_len; i++)
			kd_data[i + 1] = sourceData.get(index - i)[4];
    	kd_data[0] = kd_data[1];
    	Arrays.sort(kd_data, 1, kd_len + 1);
    	
    	rsv = (kd_data[0] - kd_data[1]) * 100.0f / (kd_data[kd_len] - kd_data[1]);
    	k_cvalue = (k_pvalue * 2 + rsv) / 3;
    	d_cvalue = (d_pvalue * 2 + k_cvalue) / 3;
    	
    	page.setColor(c1);
    	line.setLine(x, calculateAxis(k_pvalue, 2), x + scale, calculateAxis(k_cvalue, 2));
        page.draw(line);
        page.setColor(c2);
        line.setLine(x, calculateAxis(d_pvalue, 2), x + scale, calculateAxis(d_cvalue, 2));
        page.draw(line);
        
        if (index  == range_start + range - 1) {
        	float tx;
        	float ty;
        	
        	page.setColor(Color.DARK_GRAY);
        	drawBrokenLine(page, graphD.x, calculateAxis(80, 2), graphD.x + graphD.width, calculateAxis(80, 2)); // 畫橫虛線
        	drawBrokenLine(page, graphD.x, calculateAxis(20, 2), graphD.x + graphD.width, calculateAxis(20, 2)); // 畫橫虛線
        	page.setColor(Color.WHITE);
        	page.drawString("80", 0, calculateAxis(80, 2) + char_height / 2);
        	page.drawString("20", 0, calculateAxis(20, 2) + char_height / 2);
        	
        	tx = graphD.x + graphD.width + line_cap;
        	ty = graphD.y + start_tech1 + line_cap;
        	page.drawString("RSV" + kd_len + "=" + nf.format(rsv), tx, ty);
        	page.drawString("K-D" + "=" + nf.format(k_cvalue - d_cvalue), tx, ty + char_height * 3);
        	page.setColor(c1);
        	page.drawString("K" + Math.sqrt(kd_len) + "=" + nf.format(k_cvalue), tx, ty + char_height * 1);
        	page.setColor(c2);
        	page.drawString("D" + Math.sqrt(kd_len) + "=" + nf.format(d_cvalue), tx, ty + char_height * 2);
        }
        
        k_pvalue = k_cvalue;
        d_pvalue = d_cvalue;
    }

    private void drawBiasCurve (Graphics2D page, int len, int index, float x, float scale, Color color) {
    	if (index - range_start < len)
    		return;
    	
    	if (bavgPrev == 0.0) {
    		for (int i = 0; i < len; i++)
    			bavgPrev += sourceData.get(index - i - 1)[4];
    		biasPrev = bavgPrev / len * 1.0f;
    		biasPrev = (sourceData.get(index)[4] - biasPrev) / biasPrev; 
    	}
    	
		bavgCurr = bavgPrev + sourceData.get(index)[4] - sourceData.get(index - len)[4];
		
		biasCurr = bavgCurr / len * 1.0f;
		biasCurr = (sourceData.get(index)[4] - biasCurr) / biasCurr; 
        
        // 文字說明
        if (index  == range_start + range - 1) {
        	float y1;
        	float y2;
        	
        	page.setColor(color);
        	page.drawString("Bias" + len + "=" + nf.format(biasCurr * 100) + "%", graphD.x + graphD.width + line_cap, 
        			graphD.y + start_tech2 + line_cap + char_height);
        	
        	page.setColor(Color.WHITE);
        	if (dateType == 0 || dateType == 3) {
            	y1 = calculateAxis(0.05f, 3);
            	y2 = calculateAxis(-0.045f, 3);
        		page.drawString("5%", 0, y1 + char_height / 2);
        		page.drawString("-4.5%", 0, y2 + char_height / 2);
        	} else if (dateType == 1) {
            	y1 = calculateAxis(0.125f, 3);
            	y2 = calculateAxis(-0.1125f, 3);
        		page.drawString("12.5%", 0, y1 + char_height / 2);
        		page.drawString("-11.25%", 0, y2 + char_height / 2);
        	} else if (dateType == 2) {
            	y1 = calculateAxis(0.25f, 3);
            	y2 = calculateAxis(-0.225f, 3);
        		page.drawString("25%", 0, y1 + char_height / 2);
        		page.drawString("-22.5%", 0, y2 + char_height / 2);
        	} else {
            	y1 = calculateAxis(0.05f, 3);
            	y2 = calculateAxis(-0.045f, 3);
        	}

        	page.setColor(Color.DARK_GRAY);
        	drawBrokenLine(page, graphD.x, y1, graphD.x + graphD.width, y1); // 畫橫虛線
        	drawBrokenLine(page, graphD.x, y2, graphD.x + graphD.width, y2); // 畫橫虛線
        }
        
        page.setColor(color);
        line.setLine(x, calculateAxis(biasPrev, 3), x + scale, calculateAxis(biasCurr, 3));
        page.draw(line);
        
        bavgPrev = bavgCurr;
        biasPrev = biasCurr;
    }
    
    // 計算KD值
    protected float calculateAxis (float data, int calType) {	// 2:KD 3:Bias
    	switch(calType) {
    	case 2:
    		return height_tech1 / 100.0f * (100 - data) + graphD.y + start_tech1;
    	case 3:
    		if (dateType == 0 || dateType == 3)
    			return height_tech2 / 20.0f * (10 - data * 100) + graphD.y + start_tech2;
    		else if (dateType == 1)
    			return height_tech2 / 50.0f * (25 - data * 100) + graphD.y + start_tech2;
    		else if (dateType == 2)
    			return height_tech2 / 100.0f * (50 - data * 100) + graphD.y + start_tech2;
    		else
    			return 0;
    	default:
    		return super.calculateAxis(data, calType);
    	}
    }
    
    // 標示滑鼠指標資訊
    protected void drawMouseInfo (Graphics2D page) {
        try {
			int data;
			float[] currData;
			String currdata;

			if (mouse_x > graphD.x && mouse_x < graphD.x + graphD.width &&	// 判斷滑鼠有沒有在作動範圍內
					mouse_y >= graphD.y + line_cap / 2 && mouse_y <= graphD.y + graphD.height - line_cap / 2) {
				
				page.setColor(Color.YELLOW);
				line.setLine(graphD.x - line_cap / 2, mouse_y, graphD.x + graphD.width - line_cap / 2, mouse_y);
				page.draw(line);	// 畫滑鼠指數線
				line.setLine(mouse_x, graphD.y + line_cap / 2, mouse_x, graphD.y + graphD.height - line_cap / 2);
				page.draw(line); 	// 畫滑鼠時間線

				// 顯示個股基本資料
				data = (int) Math.round((mouse_x - graphD.x - line_cap / 2) / scale) + 1;
				if (sourceMap.containsKey(data))
					currData = sourceMap.get(data);
				else
					currData = sourceData.getLast();
				
				page.setColor(Color.WHITE);
				if (mouse_y < graphD.y + start_quan + height_quan) {
					rect.setRect(0, mouse_y - char_height / 2 - 1, graphD.x - line_cap / 2, char_height + 2);
					page.fill(rect);		// 畫指數軸欄位
				}
				if (mouse_x + GUI.overall_size[7] > graphD.x + graphD.width) {			// 避免圖形繪出範圍外
					rect.setRect(mouse_x - GUI.overall_size[12], graphD.y + graphD.height + line_cap / 2 + 1, 
							GUI.overall_size[12], char_height + 2);
					page.fill(rect);	// 畫時間軸欄位
					page.setColor(Color.BLACK);
					page.drawString(numToTime(currData[0]), mouse_x - GUI.overall_size[12], screen_y - 1);		// 秀出時間軸時間
				} else if (mouse_x - GUI.overall_size[7] < graphD.x) {
					rect.setRect(mouse_x, graphD.y + graphD.height + line_cap / 2 + 1, GUI.overall_size[13], char_height + 2);
					page.fill(rect);	// 畫時間軸欄位
					page.setColor(Color.BLACK);
					page.drawString(numToTime(currData[0]), mouse_x, screen_y - 1);		// 秀出時間軸時間
				} else {
					rect.setRect(mouse_x - GUI.overall_size[6], graphD.y + graphD.height + line_cap / 2 + 1, 
							GUI.overall_size[12], char_height + 2);
					page.fill(rect);	// 畫時間軸欄位
					page.setColor(Color.BLACK);
					page.drawString(numToTime(currData[0]), mouse_x - GUI.overall_size[6], screen_y - 1);		// 秀出時間軸時間
				}
				
				if (mouse_y > graphD.y + (start_main + height_main + start_quan) / 2) {	// 判斷是價還是量
					if (mouse_y >= graphD.y + start_quan + height_quan)
						return;
					data = 3;
					page.setColor(new Color(0, 99, 255));
					page.drawString(nf.format(calculateQuan(mouse_y)), 0, mouse_y + char_height / 2);	// 秀出成交量軸資訊
					page.setColor(Color.WHITE);
				}
				else {
					data = 1;
					page.setColor(Color.BLACK);
					page.drawString(nf.format(calculatePrice(mouse_y)), 0, mouse_y + char_height / 2);	// 秀出指數軸資訊
					page.setColor(Color.YELLOW);
				}
				
				// 顯示十字游標上的資訊
				if (data == 1)
					currdata = nf.format(currData[4]);
				else
					currdata = nf.format(currData[5]);
				if (mouse_x + GUI.overall_size[18]< graphD.x + graphD.width)			// 避免字串繪出範圍外
					if (mouse_y < graphD.y + line_cap / 2 + char_height)
						page.drawString(currdata, mouse_x - GUI.overall_size[18] - 1, mouse_y + char_height + 1);	// 秀出股價資訊
					else
						page.drawString(currdata, mouse_x + 1, mouse_y - 1);		// 秀出股價資訊
				else
					if (mouse_y < graphD.y + line_cap / 2 + char_height)
						page.drawString(currdata, mouse_x - GUI.overall_size[18] - 1, mouse_y + char_height + 1);	// 秀出股價資訊
					else
						page.drawString(currdata, mouse_x - GUI.overall_size[18] - 1, mouse_y - 1);	// 秀出股價資訊
			} else {	// 滑鼠不在範圍內，就只重繪最近的資訊
				currData = sourceData.getLast();
			}
			
			page.setColor(Color.CYAN);
			if (stockId == 1000)
				page.drawString("集中市場", screen_x * 0.01f, GUI.overall_size[19]);
			else if (stockId == 4000)
				page.drawString("店頭市場", screen_x * 0.01f, GUI.overall_size[19]);
			else {
				page.drawString(String.valueOf(stockId), screen_x * 0.01f, GUI.overall_size[19]);
				page.drawString(sName, screen_x * 0.01f, GUI.overall_size[24]);
			}
			
			page.setColor(Color.CYAN);
			page.drawString(String.valueOf(stockId), screen_x * 0.01f, GUI.overall_size[19]);
			page.drawString(sName, screen_x * 0.01f, GUI.overall_size[24]);
			page.setColor(Color.WHITE);
			page.drawString("日期 " + numToTime(currData[0]), screen_x * 0.17f, GUI.overall_size[19]);
			page.drawString("開盤 " + nf.format(currData[1]), screen_x * 0.47f, GUI.overall_size[19]);
			if (currData[4] > currData[1])
				page.setColor(Color.RED);
			else if (currData[4] < currData[1])
				page.setColor(Color.GREEN);
			page.drawString("收盤 " + nf.format(currData[4]), screen_x * 0.77f, GUI.overall_size[19]);
			page.setColor(Color.WHITE);
			page.drawString("成交量 " + nf.format(currData[5]) + "(億元)", screen_x * 0.17f, GUI.overall_size[19] + char_height);
			page.setColor(Color.RED);
			page.drawString("最高 " + nf.format(currData[2]), screen_x * 0.47f, GUI.overall_size[19] + char_height);
			page.setColor(Color.GREEN);
			page.drawString("最低 " + nf.format(currData[3]), screen_x * 0.77f, GUI.overall_size[19] + char_height);
		} catch (IndexOutOfBoundsException e) {
//			mouse_x = graphD.x + graphD.width - line_cap / 2;
//			repaint();
		}
    }

    private float numToAxis (float time) {
    	numToTime(time);
    	if (dateType == 3)
    		return graphD.x + line_cap + ((
        			Integer.parseInt(time_mark[0]) - 9) * 60 + Integer.parseInt(time_mark[1])) * scale / 5;
    	else
    		return graphD.x + line_cap + ((
    				Integer.parseInt(time_mark[0]) - 9) * 60 + Integer.parseInt(time_mark[1])) * scale;
    }

    public static void main (String[] args)
    {
    	GeneralGraph mg = new TechGraph (1101, TYPE_KBAR_CHART, TYPE_UNLIMITED);
    	JFrame jf = new JFrame ("- Graph Demo -");
    	jf.getContentPane().add(mg);
       
    	jf.setVisible(true);
    	jf.toFront();
    	jf.setResizable(true);
    	jf.pack();
       
    	jf.setLocationRelativeTo(null);
    	jf.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
    }
}