/**
 * @author bluesway
 * TARGET 顯示個股及大盤的當日資訊
 */

package software.Subitem.Graph;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFrame;

import common.util.StockBox;
import software.DB;
import software.GUI;
import software.Subitem.GeneralGraph;

@SuppressWarnings("serial")
public class OverallGraph extends GeneralGraph {
//	private float yesterday;		// 昨日收盤價
	private float[] tmp;
	private float[] bestFive;
	private boolean otc;
	private JButton techButton;

    public OverallGraph () {
    	this (0, false);
    }
    
    public OverallGraph (boolean otc) {
    	this (0, otc);
    }
    
    public OverallGraph (int stockId) {
    	this (stockId, false);
    }
    public OverallGraph (final int stockId, boolean otc) {
    	this.stockId = stockId;
    	this.otc = otc;
    	isOverall = true;
    	
    	// 顯示型態
		limiteType = GeneralGraph.TYPE_LIMITED;
    	displayType = GeneralGraph.TYPE_LINE_CHART;
    	
    	// 圖形繪製
    	line_cap = 2;
    	char_height = GUI.overall_size[5];
    	left_space = GUI.overall_size[22];
		cpl_height = GUI.overall_size[30];
		if (stockId == 0)
    		right_space = 10;
    	else
    		right_space = GUI.overall_size[30];
    	start_main = 15;

    	// 資料處理
    	sourceData = new LinkedList<float[]>();
    	sourceMap = new HashMap<Integer, float[]>();
    	priceNum = 5;
    	quanNum = 3;
    	mouse_x = -1;
    	mouse_y = -1;
    	
    	removeMouseListener(defaultML[2]);
    	
    	techButton = new JButton("技術");		// 技術分析
    	techButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GUI.gui.setStockId(getStockId());
				GUI.gui.createFrame(GUI.Analysis[0]);
			}
    	});
    	
    	// 控制台配置
    	if (stockId != 0) {
    		createBox();
    		add(stockBox);
    		add(defaultBtn[4]);
    		stockBox.setSelectedIndex(StockBox.searchIndexOfStockId(stockId));
        	defaultBtn[4].setActionCommand(String.valueOf(stockId));
        	GUI.gui.setStockId(this.stockId);
    	}
    	
		add(techButton);
		
		readData();
	}
	
    protected void readData() {
    	Connection conn = null;
    	Statement stmt = null;
    	ResultSet result = null;
		long a = System.currentTimeMillis();
		float[] result_info;
		
		sourceData.clear();
		sourceMap.clear();
		try {
			conn = DB.db.getConnection();
			stmt = conn.createStatement();
			
			if (stockId == 0) {
				result_info = new float[10];
				
				if (otc)
					result = stmt.executeQuery("SELECT * FROM info_otc ORDER BY oTime ASC");
				else
					result = stmt.executeQuery("SELECT * FROM info_tse ORDER BY oTime ASC");
				
				if (result.next())
//					System.out.println(result.getfloat(5));
					;
//					yesterday = result.getfloat(2);
				
				if (result.next())
					for (int i = 0; i < 10; i++){
						result_info[i] = result.getFloat(i + 1);
					}
				tmp = result_info.clone();
				sourceData.add(result_info.clone());
				
				numToTime(result_info[0]);
				sourceMap.put((Integer.parseInt(time_mark[0]) - 9) * 60 + Integer.parseInt(time_mark[1]), result_info.clone());

				while (result.next()) {
					for (int i = 0; i < 3; i++) {
						result_info[i] = result.getFloat(i + 1);
						tmp[i] = result_info[i];
					}
					for (int i = 3; i < 10; i++) {
						result_info[i] = result.getFloat(i + 1) - tmp[i];
						tmp[i] = result.getFloat(i + 1);
					}
					
					sourceData.add(result_info.clone());
					numToTime(result_info[0]);
					sourceMap.put(new Integer((Integer.parseInt(time_mark[0]) - 9) * 60 + Integer.parseInt(time_mark[1])), result_info.clone());
				}
			} else {
				result_info = new float[8];
				bestFive = new float[20];
				
				// 取當盤資料
				result = stmt.executeQuery("SELECT * FROM info_today WHERE tSid='" + 
            		String.valueOf(stockId) + "' ORDER BY tTime ASC");

				if (result.next());
				
				while (result.next()) {
					// 為了和大盤資料相容
					result_info[0] = result.getFloat(2);
					result_info[1] = result.getFloat(4);
					result_info[2] = result.getFloat(3);
					for (int i = 3; i < 8; i++)
						result_info[i] = result.getFloat(i + 2);

					sourceData.add(result_info.clone());
					numToTime(result_info[0]);
					sourceMap.put(new Integer((Integer.parseInt(time_mark[0]) - 9) * 60 + Integer.parseInt(time_mark[1])), result_info.clone());
				}

				// 取當日資料
				result = stmt.executeQuery("SELECT * FROM info_current WHERE cSid='" + 
	            		String.valueOf(stockId) + "'");

				if (result.next()) {
					for (int i = 0; i <5; i++)
						result_info[i] = result.getFloat(i + 2);
					
					tmp = result_info.clone();
				}
				
				// 取得公司名稱
	            result = stmt.executeQuery("SELECT * FROM stock WHERE Sid='" + 
	            		String.valueOf(stockId) + "'");
	        	sName = result.next() ? result.getString(2) : "";
	        	
	        	// 取得最佳五檔資訊
	            result = stmt.executeQuery("SELECT * FROM best_five WHERE bSid='" + 
	            		String.valueOf(stockId) + "'");
				if (result.next())
					for (int i = 0; i <20; i++)
						bestFive[i] = result.getFloat(i + 2);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (result != null)
					result.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					DB.db.closeConnection(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		a = System.currentTimeMillis() - a;
//		System.out.println("資料已讀取，共花費" + a / 1000.0 + "秒");
    }
    
	public void paintComponent(Graphics g) {
		long a = System.currentTimeMillis();
    	Graphics2D page = (Graphics2D) g;
    	
    	// 重新讀取資料
     	screen_x = getWidth();
     	screen_y = getHeight();
    	graphD.setRect(getX() + left_space, getY() + cpl_height + 10, screen_x - left_space - right_space, screen_y - cpl_height - 25);
    	scale = graphD.width / 270.0f;
    	height_main = graphD.height * 0.55f;
    	start_quan = graphD.height * 0.75f;
    	height_quan = graphD.height * 0.25f;
    	
    	super.paintComponent(page);
    	
		float x1, x2;
		
		// 顯示指數資訊
		drawChartLabel(page, 0);

        // 顯示成交量資訊
    	drawChartLabel(page, 1);

        // 畫線圖、柱圖
        while (time < sourceData.size()) {
        	now = sourceData.get(time);

        	drawLineChart(page, prev[1], now[1], x1 = numToAxis(prev[0]), x2 = numToAxis(now[0]) - x1);
        	if (stockId == 0)
        		drawQuanChart(page, now[3] / 100000000.0f, x1, x2);
        	else
        		drawQuanChart(page, now, x1);

            prev = now;
            time++;
        }
        
        // 顯示時間資訊
        drawChartLabel(page, 100000, numToAxis(100000));
        drawChartLabel(page, 110000, numToAxis(110000));
        drawChartLabel(page, 120000, numToAxis(120000));
        drawChartLabel(page, 130000, numToAxis(130000));
        
        // 顯示最佳五檔及當日資訊 for 個股顯示
        if (stockId != 0)
        	drawStockInfo(page);
        
        // 處理滑鼠指標
        drawMouseInfo(page);

        // 畫邊框
		drawCanvas(page);
		
		a = System.currentTimeMillis() - a;
//		System.out.println("繪圖完成，共花費" + a / 1000.0 + "秒");
	}
    
    // 畫個股當日成交量圖
    protected void drawQuanChart (Graphics2D page, float[] data, float x1) {
    	float y = calculateAxis(data[3], 1);
    	if (data[1] == data[4])
    		page.setColor(Color.RED);
    	else
    		page.setColor(Color.GREEN);
    	line.setLine(x1, y, x1, graphD.y + start_quan + height_quan - line_cap);
    	page.draw(line);	// 畫柱圖分隔線
	}
    
    // 標示最佳五檔、當日資訊
    private void drawStockInfo (Graphics2D page) {
		page.setColor(Color.RED);
		page.drawString("委買五檔", graphD.x + graphD.width + GUI.overall_size[1], graphD.y + char_height);
		page.drawString("價格", graphD.x + graphD.width + GUI.overall_size[1], graphD.y + char_height * 2);
		page.drawString("張數", graphD.x + graphD.width + GUI.overall_size[15], graphD.y + char_height * 2);
		page.setColor(Color.GREEN);
		page.drawString("委賣五檔", graphD.x + graphD.width + GUI.overall_size[1], graphD.y + char_height * 9);
		page.drawString("價格", graphD.x + graphD.width + GUI.overall_size[1], graphD.y + char_height * 10);
		page.drawString("張數", graphD.x + graphD.width + GUI.overall_size[15], graphD.y + char_height * 10);
		page.setColor(Color.ORANGE);
		for (int i = 0; i < 5; i++) {
			page.drawString(nf.format(bestFive[i * 2]), graphD.x + graphD.width + GUI.overall_size[1], graphD.y + char_height * (i + 3));
			page.drawString(nf.format(bestFive[i * 2 + 10]), graphD.x + graphD.width + GUI.overall_size[1], graphD.y + char_height * (i + 11));
		}
		page.setColor(Color.CYAN);
		for (int i = 0; i < 5; i++) {
		page.drawString(nf.format(bestFive[i * 2 + 1]), graphD.x + graphD.width + GUI.overall_size[15], graphD.y + char_height * (i + 3));
		page.drawString(nf.format(bestFive[i * 2 + 11]), graphD.x + graphD.width + GUI.overall_size[15], graphD.y + char_height * (i + 11));
		}
    }
    
    // 標示滑鼠指標資訊
    protected void drawMouseInfo (Graphics2D page) {
        try {
			int data;
			float[] currData;
			String currdata;

			if (mouse_x > graphD.x && mouse_x < graphD.x + graphD.width &&	// 判斷滑鼠有沒有在作動範圍內
					mouse_y >= graphD.y + line_cap / 2 && mouse_y <= graphD.y + graphD.height - line_cap / 2) {
				
				page.setColor(Color.YELLOW);
				line.setLine(graphD.x - line_cap / 2, mouse_y, graphD.x + graphD.width - line_cap / 2, mouse_y);
				page.draw(line);	// 畫滑鼠指數線
				line.setLine(mouse_x, graphD.y + line_cap / 2, mouse_x, graphD.y + graphD.height - line_cap / 2);
				page.draw(line); 	// 畫滑鼠時間線

				// 顯示個股基本資料
				data = (int) Math.round((mouse_x - graphD.x - line_cap / 2) / scale);
				if (sourceMap.containsKey(data))
					currData = sourceMap.get(data);
				else
					currData = sourceData.getLast();
				
				page.setColor(Color.WHITE);
				if (mouse_y < graphD.y + start_quan + height_quan) {
					rect.setRect(0, mouse_y - char_height / 2 - 1, graphD.x - line_cap / 2, char_height + 2);
					page.fill(rect);		// 畫指數軸欄位
				}
				if (mouse_x + GUI.overall_size[7] > graphD.x + graphD.width) {			// 避免圖形繪出範圍外
					rect.setRect(mouse_x - GUI.overall_size[12], graphD.y + graphD.height + line_cap / 2 + 1, 
							GUI.overall_size[12], char_height + 2);
					page.fill(rect);	// 畫時間軸欄位
					page.setColor(Color.BLACK);
					page.drawString(numToTime(currData[0]), mouse_x - GUI.overall_size[12], screen_y - 1);		// 秀出時間軸時間
				} else if (mouse_x - GUI.overall_size[7] < graphD.x) {
					rect.setRect(mouse_x, graphD.y + graphD.height + line_cap / 2 + 1, GUI.overall_size[13], char_height + 2);
					page.fill(rect);	// 畫時間軸欄位
					page.setColor(Color.BLACK);
					page.drawString(numToTime(currData[0]), mouse_x, screen_y - 1);		// 秀出時間軸時間
				} else {
					rect.setRect(mouse_x - GUI.overall_size[6], graphD.y + graphD.height + line_cap / 2 + 1, 
							GUI.overall_size[12], char_height + 2);
					page.fill(rect);	// 畫時間軸欄位
					page.setColor(Color.BLACK);
					page.drawString(numToTime(currData[0]), mouse_x - GUI.overall_size[6], screen_y - 1);		// 秀出時間軸時間
				}
				
				if (mouse_y > graphD.y + (start_main + height_main + start_quan) / 2) {	// 判斷是價還是量
					if (mouse_y >= graphD.y + start_quan + height_quan)
						return;
					data = 3;
					page.setColor(new Color(0, 99, 255));
					page.drawString(nf.format(calculateQuan(mouse_y)), 0, mouse_y + char_height / 2);	// 秀出成交量軸資訊
					page.setColor(Color.WHITE);
				}
				else {
					data = 1;
					page.setColor(Color.BLACK);
					page.drawString(nf.format(calculatePrice(mouse_y)), 0, mouse_y + char_height / 2);	// 秀出指數軸資訊
					page.setColor(Color.YELLOW);
				}
				
				// 顯示十字游標上的資訊
				if (data == 1)
					currdata = nf.format(currData[1]);
				else
					currdata = nf.format(currData[3] / (stockId == 0 ? 100000000.0 : 1.0));
				if (mouse_x + GUI.overall_size[18]< graphD.x + graphD.width)			// 避免字串繪出範圍外
					if (mouse_y < graphD.y + line_cap / 2 + char_height)
						page.drawString(currdata, mouse_x - GUI.overall_size[18] - 1, mouse_y + char_height + 1);	// 秀出股價資訊
					else
						page.drawString(currdata, mouse_x + 1, mouse_y - 1);		// 秀出股價資訊
				else
					if (mouse_y < graphD.y + line_cap / 2 + char_height)
						page.drawString(currdata, mouse_x - GUI.overall_size[18] - 1, mouse_y + char_height + 1);	// 秀出股價資訊
					else
						page.drawString(currdata, mouse_x - GUI.overall_size[18] - 1, mouse_y - 1);	// 秀出股價資訊
			} else {	// 滑鼠不在範圍內，就只重繪最近的資訊
				currData = sourceData.getLast();
			}
			
			page.setColor(Color.CYAN);
			if (stockId == 0)
				page.drawString(otc ? "店頭市場" : "集中市場", screen_x * 0.01f, GUI.overall_size[19]);
			else {
				page.drawString(String.valueOf(stockId), screen_x * 0.01f, GUI.overall_size[19]);
				page.drawString(sName, screen_x * 0.01f, GUI.overall_size[24]);
			}
			page.drawString("時間 " + numToTime(currData[0]), screen_x * 0.01f, GUI.overall_size[29]);
			if (currData[2] > 0)
				if (currData[2] > currData[1] * 0.07 - calculateUnit(currData[1])) {
					page.setColor(Color.RED);
					rect.setRect(screen_x * 0.82f, GUI.overall_size[15], GUI.overall_size[19], GUI.overall_size[5]);
					page.fill(rect);
					page.setColor(Color.WHITE);
				}
				else
					page.setColor(Color.RED);
			else if (currData[2] < 0)
				if (currData[2] < currData[1] * -0.07 + calculateUnit(currData[1])) {
					page.setColor(new Color(0, 128, 0));
					rect.setRect(screen_x * 0.82f, GUI.overall_size[15], GUI.overall_size[19], GUI.overall_size[5]);
					page.fill(rect);
					page.setColor(Color.WHITE);
				}
				else
					page.setColor(Color.GREEN);
			page.drawString("漲跌 " + nf.format(currData[2]), screen_x * 0.42f, GUI.overall_size[19]);

			page.setColor(Color.WHITE);
			if (stockId == 0) {
				page.drawString("指數 " + nf.format(currData[1]), screen_x * 0.17f, GUI.overall_size[19]);
				page.drawString("成交量 " + nf.format(currData[3] / 100000000.0) + "(億元)", screen_x * 0.70f, GUI.overall_size[19]);
				page.drawString("成交張 " + nf.format(currData[4]), screen_x * 0.17f, GUI.overall_size[19] + char_height);
				page.drawString("委買張 " + nf.format(currData[6]), screen_x * 0.42f, GUI.overall_size[19] + char_height);
				page.drawString("委賣張 " + nf.format(currData[8]), screen_x * 0.70f, GUI.overall_size[19] + char_height);
				page.drawString("成交筆 " + nf.format(currData[5]), screen_x * 0.17f, GUI.overall_size[19] + char_height * 2);
				page.drawString("委買筆 " + nf.format(currData[7]), screen_x * 0.42f, GUI.overall_size[19] + char_height * 2);
				page.drawString("委賣筆 " + nf.format(currData[9]), screen_x * 0.70f, GUI.overall_size[19] + char_height * 2);
			} else {
				page.drawString("成交價 " + nf.format(currData[1]), screen_x * 0.17f, GUI.overall_size[19]);
				page.drawString("成交量 " + nf.format(currData[3]) + "張", screen_x * 0.70f, GUI.overall_size[19]);
				page.drawString("委買價 " + nf.format(currData[4]), screen_x * 0.17f, GUI.overall_size[19] + char_height);
				page.drawString("委買量 " + nf.format(currData[5]), screen_x * 0.42f, GUI.overall_size[19] + char_height);
				page.drawString("委賣價 " + nf.format(currData[6]), screen_x * 0.17f, GUI.overall_size[19] + char_height * 2);
				page.drawString("委賣量 " + nf.format(currData[7]), screen_x * 0.42f, GUI.overall_size[19] + char_height * 2);
			}
			
		} catch (IndexOutOfBoundsException e) {
//			mouse_x = graphD.x + graphD.width - line_cap / 2;
//			repaint();
		}
    }
    
    private float numToAxis (float time) {
    	numToTime(time);
    	return graphD.x + line_cap + ((
    			Integer.parseInt(time_mark[0]) - 9) * 60 + Integer.parseInt(time_mark[1])) * scale;
    }
    
    public static LinkedList<float[]> getStockData(int stockId) {
    	OverallGraph og = new OverallGraph(stockId);
    	og.readData();
    	
    	return og.sourceData;
    }
    
    private int getStockId () {
    	if (otc)
    		return 4000;
    	else
    		return stockId;
    }
    
    public static void main (String[] args)
    {
    	GeneralGraph mg = new OverallGraph (2330);
    	JFrame jf = new JFrame ("- Graph Demo -");
    	jf.getContentPane().add(mg);
       
    	jf.setVisible(true);
    	jf.toFront();
    	jf.setResizable(true);
    	jf.pack();
       
    	jf.setLocationRelativeTo(null);
    	jf.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
    }
}
