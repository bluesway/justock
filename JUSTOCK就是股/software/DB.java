/**
 * @author bluesway
 * TARGET 專門建立 DB 連線
 */

package software;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;
import common.source.JUSTOCKDBSource;

public class DB {
	public static JUSTOCKDBSource db;
	public static Connection conn;
	public static Statement stmt;
	
	static {
	    try {
			db = new JUSTOCKDBSource("common/source/JUSTOCKDB.properties");
//	    	db = new JUSTOCKDBSource("common/source/local.properties");
			long a = System.currentTimeMillis();
			conn = db.getConnection();
			stmt = conn.createStatement();
			a = System.currentTimeMillis() - a;
			System.out.println("資料庫已連線，共花費" + a / 1000.0 + "秒");
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "資料庫參數讀取失敗，請洽系統人員。", "系統狀態", JOptionPane.WARNING_MESSAGE);
			e.printStackTrace();
			System.exit(0);
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "資料庫參數讀取失敗，請洽系統人員。", "系統狀態", JOptionPane.WARNING_MESSAGE);
			e.printStackTrace();
			System.exit(0);
		} catch (ClassNotFoundException e) {
			JOptionPane.showMessageDialog(null, "資料庫連線失敗，請檢查網路組態。", "系統狀態", JOptionPane.WARNING_MESSAGE);
			e.printStackTrace();
			System.exit(0);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "資料庫無法建立查詢，請稍後再試。", "系統狀態", JOptionPane.WARNING_MESSAGE);
			e.printStackTrace();
			System.exit(0);
		}
	}
}
