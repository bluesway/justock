package software.DataRetrieve;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import common.util.DateUtil;

import software.DB;
import software.GUI;

public class StockInfoInvestCombination
{
	String id;
	String [][] data;
	String [][] queryData;
	int row = 0;
	int column = 0; 
	
	
	public StockInfoInvestCombination() {
		try {
			getInvestData();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void getInvestData() throws SQLException {
    	ResultSet result = null;
    	ResultSet result2 = null;
    	Statement stmt1 = null;
    	Statement stmt2 = null;
    	double hprice, tprice;
    	int hquan;
    	
        try {
        	NumberFormat nf = new DecimalFormat("0.00");
        	
        	stmt1 = DB.conn.createStatement();
        	result = stmt1.executeQuery("SELECT * from holding where hUid='" + GUI.userData[0] + "'");
        	if(result.last())
        		row = result.getRow();
        	
        	result.beforeFirst();
        	column = result.getMetaData().getColumnCount(); 

        	queryData = new String [row][column + 2];
        	int j = 0;
        	while(result.next()) {
        		j++;
        		for(int i=1 ; i<=column ; i++) {
        			if(i == 5) 
        				queryData[j-1][i-1] = String.valueOf(divdouble(Double.valueOf(result.getString(5)),1,2));
        			else if(i == 3)
        				queryData[j-1][i-1] = DateUtil.transDate(Integer.valueOf(result.getString(i)));
        			else
        				queryData[j-1][i-1] = result.getString(i);
        		}
        		stmt2 = DB.conn.createStatement();
        		result2 = stmt2.executeQuery("SELECT tPrice from info_today WHERE tSid='" + queryData[j-1][1] + "' ORDER BY tTime DESC");
        		if (result2.next()) {
        			hprice = Double.parseDouble(queryData[j-1][column-2]);
        			hquan = Integer.parseInt(queryData[j-1][column-1]);
        			tprice = result2.getDouble(1);
        			queryData[j-1][column] = nf.format((tprice - hprice) * hquan);
        			queryData[j-1][column+1] = nf.format(Double.parseDouble(queryData[j-1][column]) / (hprice * hquan) * 100) + "%";
        		}
        	}

        } catch (SQLException e) {
            System.err.println(e);
        }
        
        result.close();
    }
	
	public int getRow() {
		return row;
	}
	
	public int getColumn() {
		return column + 2;
	}
	
	public String[][] getData() {
		return queryData;
	}
	
    public static double divdouble(double d1,double d2,int len)  { //浮點除運算，len小數位數-沒有四捨五入 
            BigDecimal b1 = new BigDecimal(Double.toString(d1));
            BigDecimal b2 = new BigDecimal(Double.toString(d2));
            return b1.divide(b2,len, BigDecimal.ROUND_CEILING).doubleValue();
    }
}