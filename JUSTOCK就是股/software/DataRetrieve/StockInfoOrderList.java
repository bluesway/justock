package software.DataRetrieve;

import java.sql.ResultSet;
import java.sql.SQLException;

import common.util.DateUtil;
import common.util.TimeUtil;
import software.DB;
import software.GUI;

public class StockInfoOrderList
{
	String id;
	String [][] data;
	String [][] queryData;
	int row = 0;
	int column = 0; 
	
	public StockInfoOrderList() {
		try {
			getOrderData();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void getOrderData() throws SQLException {
    	ResultSet result = null;
        try {
        		result = DB.stmt.executeQuery("SELECT * from transaction where tUid='" + GUI.userData[0] + "' AND tDate='" + DateUtil.getDBDate(System.currentTimeMillis()) + "' GROUP BY tId DESC");

        		if(result.last())
        		row = result.getRow();
        	
        	result.beforeFirst();
        	column = result.getMetaData().getColumnCount(); 

        	queryData = new String [row][column];
        	int j = 0;
        	TimeUtil tu = new TimeUtil();
        	while(result.next()) {
        		j++;
        		for(int i=1 ; i<=column ; i++) {
        			if(i == column - 1 && result.getInt(i) == 0) {
        				queryData[j-1][i-1] = "不限制";
        			} else if(i == column) {
        				switch(Integer.valueOf(result.getString(i))) {
        					case -1: 
        						queryData[j-1][i-1] = "下單成功";
        						break;
        					case 0:
        						queryData[j-1][i-1] = "交易成功";
        						break;
        					case 1:
        						queryData[j-1][i-1] = "違約交割";
        						break;
        					case 2:
        						queryData[j-1][i-1] = "已失效";
        						break;
        					case 3:
        						queryData[j-1][i-1] = "限價錯誤";
        						break;
        					case 4:
        						queryData[j-1][i-1] = "持股不足";
        						break;
        					case 5:
        						queryData[j-1][i-1] = "權限不足";
        						break;
        					case 6:
        						queryData[j-1][i-1] = "限制交易";
        						break;
        					case 127:
        						queryData[j-1][i-1] = "交割完成";
        						break;
        				}
        			}else if(i == 6) {
        				queryData[j-1][i-1] = DateUtil.transDate(Integer.valueOf(result.getString(i)));
        			} else if(i ==7) {
        				queryData[j-1][i-1] = tu.transTime(Integer.valueOf(result.getString(i)));
        			}
        			else
        				queryData[j-1][i-1] = result.getString(i);
        		}
        	}

        } catch (SQLException e) {
            System.err.println(e);
        }
        
        result.close();
    }
	
	public int getRow() {
		return row;
	}
	
	public int getColumn() {
		return column;
	}
	
	public String[][] getData() {
		return queryData;
	}
}