/*

*/

package software.DataRetrieve;

import java.sql.ResultSet;
import java.sql.SQLException;

import software.DB;

public class QueryTransaction
{
	String id;
	static String [][] data;
	static String [][] queryData;
	static int row = 0;
	static int column = 0; 
	
	public static String[][] getUserData(String userId) throws SQLException {
    	ResultSet result = null;
        try {
        	result = DB.stmt.executeQuery("SELECT * from transaction where tUid='" + userId + "'");
        	if(result.last())
        		row = result.getRow();
        	
        	result.beforeFirst();
        	column = result.getMetaData().getColumnCount(); 

        	queryData = new String [row][column];
        	int j = 0;
        	while(result.next()) {
        		j++;
        		for(int i=1 ; i<column ; i++) {
        			queryData[j-1][i-1] = result.getString(i);
        		}
        	}

        } catch (SQLException e) {
            System.err.println(e);
        }
        
        result.close();
        return queryData;
    }
	
	public int getRow() {
		return row;
	}
	
	public int getColumn() {
		return column;
	}
	
    public static void main(String [] args) throws SQLException {
    	data = getUserData("XDDDDD");
    	for(int i=0 ; i<data.length ; i++)
    		for(int j=0 ; j<data[0].length ; j++)
    			System.out.println(data[i][j]);
    }
}