package software.DataRetrieve;

/*
*      author by Kevingo
*      Data @ 2007/07/30
*      抓取籌碼分析資料  
*      
*/

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.swing.JOptionPane;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;

public class StockInfoBasicParser_Pchome extends HTMLEditorKit.ParserCallback
{
    private boolean inHeader = false;
    private HTML.Tag tag;
    private int count,i,j;
    public static int COLUMN = 6;
    public static int ROW = 8;
    private Object [][] data = new Object[ROW][COLUMN];

    
    // 建構式
    public StockInfoBasicParser_Pchome() {      
    	setTag(HTML.Tag.TD);
    }
    
    public void retrieveData (StockInfoBasicParser_Pchome sby, String stockId, boolean READY) {
    	try{	
	    		if(READY) {
    				ParserGetter kit = new ParserGetter();
	    			HTMLEditorKit.Parser parser = kit.getParser();
	    			HTMLEditorKit.ParserCallback callback = sby;
	    			URL u = new URL("http://tw.stock.yahoo.com/d/s/company_" + stockId + ".html");
	    			InputStream in = u.openStream();
	    			InputStreamReader reader = new InputStreamReader(in);
	    			parser.parse(reader, callback, true);
	    		}
    	} catch (IOException e) {
    			JOptionPane.showMessageDialog(null, "查無資料！請重新選擇");
    	}
    }
    
    public void setTag(HTML.Tag tag) {
    	this.tag = tag;
    }
    
    public void handleText(char[] text, int position) {
        if(inHeader) {
            String tmp = String.valueOf(text); 
            count++;
			if(count>68 && count<113) {
				i++;
				setData(tmp, i);
			}
        }
    }
    
    public void handleStartTag(HTML.Tag tag, MutableAttributeSet attributes, int position) {
        if(this.tag == tag) {
            this.inHeader = true;
        }
    }

    public void handleEndTag(HTML.Tag tag, int position) {
        if(this.tag == tag) {
            inHeader = false;
        }
    }
    
    public void setData(String tmp,int count) {
    	if(count%6 == 1 && count!=1)
    		j++;
    	data[j][count-(6*j)-1] = tmp;
    }
    
    public Object[][] getData() {
    	return data;
    }
}