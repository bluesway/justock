/*
*      author by bluesway
*      Date @ 7/6
*      For Retrieve Stock info from TSEC
*/

package software.DataRetrieve;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Scanner;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import software.DB;
import software.Routine;
    
public class StockInfoParser_TSEC extends Thread implements Job
{
	private boolean getStock;	// true:抓個股, false:抓大盤
    private boolean start;		// 開始抓資料
    private String stockId;		// 股票代號
    private String [] info;		// 資料欄位
    private String [] info1;	// 資料欄位1
    private Scanner scan;
    private URL url;
    private int start_line;		// 資料讀取起始行
    private int end_line;		// 資料讀取終止行
    private int run_line;		// 資料執行行次
    private int ite_count;		// 執行完成次數
    
    private Statement stmt;		// SQL查詢
    
    public StockInfoParser_TSEC() {
    	this(-1);
    }
    
    public StockInfoParser_TSEC(int begin) {
    	this(begin, 0);
    }
    
    public StockInfoParser_TSEC(int begin, int end) {
        start = false;
        stockId = "";
        start_line = begin;
        end_line = end;
        run_line = start_line;
        ite_count = 0;
//        sleep_time = 0;
        getStock = begin != -1; 

        try {
			stmt = DB.conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
    
    private void getData() {
        try {
            start = true;
            
            if (getStock) {
	            url = new URL("http://mis.tse.com.tw/data/" + stockId + ".csv");
	            info = new Scanner(url.openStream()).nextLine().replace("\"", "").replace(" ", "").split(",");
	            info[2] = info[2].replace(":", "");
	            
            } else {
            	// 集中市場
            	url = new URL("http://mis.tse.com.tw/data/TSEIndex.csv");
            	scan = new Scanner(url.openStream());
            	
            	while (scan.hasNext())
            		stockId += scan.nextLine() + ",";	// 借來用
            	
            	info = stockId.split("\",\"", -2);
	            info[5] = info[5].replace(":", "");
//	            info[198] = String.valueOf(Math.round(Integer.parseInt(info[198].substring(0, 5)) / 10));
	            
	            // 店頭市場
            	url = new URL("http://mis.tse.com.tw/data/OTCIndex.csv");
            	scan = new Scanner(url.openStream());
            	stockId = "";
            	
            	while (scan.hasNext())
            		stockId += scan.nextLine() + ",";	// 借來用
            	
            	info1 = stockId.split("\",\"", -2);
	            info1[5] = info1[5].replace(":", "");
            }
        } catch (IOException e) {
            start = false;
            System.err.println(e);
        }
            
        if (start) {
            writeToDB();
            start = false;
            if (getStock)
            	System.out.println("已完成" + ite_count);
            else
            	System.out.println("已完成");
        }
        else
            System.out.println("略過");

//        try {
//        	if (ite_count % 100 == 0)
//        		Thread.sleep(sleep_time);
//        } catch (InterruptedException ex) {
//            ex.printStackTrace();
//        }

        if (getStock)
        	run_line++;
    }
    
    private void writeToDB() {
    	String sql = "";
    	
        try {
        	if (getStock) {
	        	// 抓個股名稱
	            if (!stmt.executeQuery("SELECT Sid FROM stock WHERE Sid='" + stockId + "'").next())
	                stmt.addBatch("INSERT INTO stock VALUES('" + stockId + "', '" + info[32] + "')");
	            
	            // 抓當日資料
	            if (stmt.executeQuery("SELECT cSid FROM info_current WHERE cSid='" + stockId + "'").next()) {
	            	sql = "UPDATE info_current SET cHigh=" + info[6] + ", cLow=" + info[7] + ", cChg=" + 
            			info[1] + ", cOpen=" + info[5] + ", cQuan=" + info[9] + " WHERE cSid='" + stockId + "'";
	                stmt.addBatch(sql);
//	                System.out.println(sql);
	            } else {
	            	sql = "INSERT INTO info_current VALUES('" + stockId + "', " + info[6] + ", " + info[7] + ", " + 
	            		info[1] + ", " + info[5] + ", " + info[9] + ")";
	            	stmt.addBatch(sql);
//	            	System.out.println(sql);
	            }
	            
	            // 抓當盤資料
	            if (stmt.executeQuery("SELECT tSid, tTime FROM info_today WHERE tSid='" + stockId + "' AND tTime=" + info[2]).next())
	            	stmt.addBatch("DELETE FROM info_today WHERE tSid='" + stockId + "' AND tTime=" + info[2]);

	            sql = "INSERT INTO info_today VALUES('" + stockId + "', " + info[2] + ", " + info[1] + ", " +
                	info[8] + ", " + info[10] + ", " + info[11] + ", " + info[12] + ", " + info[21] + ", " + info[22] + ")";
	            stmt.addBatch(sql);
//            	System.out.println(sql);
	            
	            // 抓最佳五檔
	            if (stmt.executeQuery("SELECT bSid FROM best_five WHERE bSid='" + stockId + "'").next())
	            	stmt.addBatch("DELETE FROM best_five WHERE bSid='" + stockId + "'");
	            
	            sql = "INSERT INTO best_five VALUES('" + stockId + "', " + info[11] + ", " + info[12] + ", " + 
        			info[13] + ", " + info[14] + ", " + info[15] + ", " + info[16] + ", " + info[17] + ", " + info[18] + ", " + 
        			info[19] + ", " + info[20] + ", " + info[21] + ", " + info[22] + ", " + info[23] + ", " + info[24] + ", " + 
        			info[25] + ", " + info[26] + ", " + info[27] + ", " + info[28] + ", " + info[29] + ", " + info[30] + ")";
	            stmt.addBatch(sql);
//            	System.out.println(sql);

	            ite_count++;
        	} else {
        		// 集中市場
        		if (stmt.executeQuery("SELECT oTime FROM info_tse WHERE oTime=" + info[5]).next())
        			stmt.addBatch("DELETE FROM info_tse WHERE oTime=" + info[5]);

        		sql = "INSERT INTO info_tse VALUES(" + info[5] + ", " + info[6] + ", " + info[7] + ", '" + 
					info[198].replace(",", "") + "', " + info[202].replace(",", "") + ", " + info[203].replace(",", "") + ", " + 
					info[206].replace(",", "") + ", " + info[207].replace(",", "") + ", " + info[210].replace(",", "") + ", " + 
					info[211].replace(",", "") + ")";
        		stmt.addBatch(sql);
//            	System.out.println(sql);
        		
        		// 店頭市場
        		if (stmt.executeQuery("SELECT oTime FROM info_otc WHERE oTime=" + info[5]).next())
        			stmt.addBatch("DELETE FROM info_otc WHERE oTime=" + info[5]);

        		sql = "INSERT INTO info_otc VALUES(" + info1[5] + ", " + info1[6] + ", " + info1[7] + ", '" + 
					info1[106].replace(",", "") + "', " + info1[110].replace(",", "") + ", " + info1[111].replace(",", "") + ", " + 
					info1[114].replace(",", "") + ", " + info1[115].replace(",", "") + ", " + info1[118].replace(",", "") + ", " + 
					info1[119].replace(",", "").replace("\"", "") + ")";
        		stmt.addBatch(sql);
//            	System.out.println(sql);
        	}
        } catch (SQLException e) {
            System.err.println(e);
            System.out.println("錯誤:" + sql);
        }
    }
    
    private void readData(int begin) {
        InputStream tmpfile;
        
        tmpfile = getClass().getClassLoader().getResourceAsStream("common/source/stockId.list");
        try {
            scan = new Scanner(tmpfile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        for (int i = 0; i < begin; i++) {
        	scan.nextLine();
        }
    }
    
    public void run() {
		long[] thread_time = new long[3];

		thread_time[0] = System.currentTimeMillis();

		if (getStock) {
			readData(start_line);

			// try { // 怕被 ban
			// sleep(2000 - start_line);
			// } catch (InterruptedException e1) {
			// e1.printStackTrace();
			// }

			while (scan.hasNext()) {
				stockId = scan.nextLine().split(" ")[0];
				if (stockId.startsWith("#"))
					continue;
				System.out.print(stockId + ": ");

				getData();

				if (end_line > 0 && run_line >= end_line)
					break;
			}
		} else {
			getData();
		}

		try {
			stmt.setQueryTimeout(5);
			stmt.executeBatch();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		thread_time[1] = System.currentTimeMillis();

		System.out.println("花費時間："
				+ (thread_time[2] = (thread_time[1] - thread_time[0])) / 1000);

		if (!getStock) {
			start = false;
			stockId = "";
		} else {
			start = false;
			stockId = "";
			run_line = start_line;
			ite_count = 0;
		}
	}

    public void execute(JobExecutionContext context) throws JobExecutionException {
    	String jobName = context.getJobDetail().getFullName();
    	
        StockInfoParser_TSEC sip0 = new StockInfoParser_TSEC(0, 122);
        StockInfoParser_TSEC sip1 = new StockInfoParser_TSEC(122, 244);
        StockInfoParser_TSEC sip2 = new StockInfoParser_TSEC(244, 366);
        StockInfoParser_TSEC sip3 = new StockInfoParser_TSEC(366, 488);
        StockInfoParser_TSEC sip4 = new StockInfoParser_TSEC(488, 610);
        StockInfoParser_TSEC sip5 = new StockInfoParser_TSEC(610, 732);
        StockInfoParser_TSEC sip6 = new StockInfoParser_TSEC(732, 854);
        StockInfoParser_TSEC sip7 = new StockInfoParser_TSEC(854, 976);
        StockInfoParser_TSEC sip8 = new StockInfoParser_TSEC(976, 1098);
        StockInfoParser_TSEC sip9 = new StockInfoParser_TSEC(1098);
    	StockInfoParser_TSEC sip10 = new StockInfoParser_TSEC();

    	Routine.log.info("StockInfoParser_TSEC says: " + jobName + " executing at " + new Date());
        
        sip0.start();
        sip1.start();
        sip2.start();
        sip3.start();
        sip4.start();
        sip5.start();
        sip6.start();
        sip7.start();
        sip8.start();
        sip9.start();
    	sip10.start();
    }
    
    public static void main(String[] args) {
        StockInfoParser_TSEC sip0 = new StockInfoParser_TSEC(0, 122);
        StockInfoParser_TSEC sip1 = new StockInfoParser_TSEC(122, 244);
        StockInfoParser_TSEC sip2 = new StockInfoParser_TSEC(244, 366);
        StockInfoParser_TSEC sip3 = new StockInfoParser_TSEC(366, 488);
        StockInfoParser_TSEC sip4 = new StockInfoParser_TSEC(488, 610);
        StockInfoParser_TSEC sip5 = new StockInfoParser_TSEC(610, 732);
        StockInfoParser_TSEC sip6 = new StockInfoParser_TSEC(732, 854);
        StockInfoParser_TSEC sip7 = new StockInfoParser_TSEC(854, 976);
        StockInfoParser_TSEC sip8 = new StockInfoParser_TSEC(976, 1098);
        StockInfoParser_TSEC sip9 = new StockInfoParser_TSEC(1098);
    	StockInfoParser_TSEC sip10 = new StockInfoParser_TSEC();
        
        sip0.start();
        sip1.start();
        sip2.start();
        sip3.start();
        sip4.start();
        sip5.start();
        sip6.start();
        sip7.start();
        sip8.start();
        sip9.start();
    	sip10.start();
    }
}
