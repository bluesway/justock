package software.DataRetrieve;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Vector;

import javax.swing.JComboBox;

import software.DB;
import software.GUI;

public class StockInfoWisdomChoice
{
	String id;
	String [][] data;
	String [][] queryData;
	int row = 0;
	int column = 8; 
	int type;	// �d�ߺ���
	
	public StockInfoWisdomChoice(int type) {
		try {
			this.type = type;
			getWisdomData();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void getWisdomData() throws SQLException {
    	ResultSet result = null;
    	String sql = "";
    	String tmp = "";
		Scanner scan;
		String[] data = new String[9];
		ArrayList<String[]> dataList = new ArrayList<String[]>();

        try {
        	scan = new Scanner(new FileInputStream("common/source/stockId.list"));
        	while (scan.hasNext()) {
        		tmp = scan.nextLine().split(" ")[0];
        		if (tmp.startsWith("#"))
        			continue;
        		
        		if (type == 0)
        			sql = "SELECT tSid, tChg, tPrice, tQuan, tBPrice, tBQuan, tSPrice, tSQuan FROM info_today WHERE tSid = '" + tmp + "' AND tTime = ( SELECT MAX( tTime ) FROM info_today WHERE tSid = '" + tmp + "' ) AND tprice >= ( SELECT MAX( hPrice ) AS price FROM ( SELECT * FROM info_history WHERE hSid = '" + tmp + "' ORDER BY hDate DESC LIMIT 0 , 20 )tmp )";
        		else
        			sql = "SELECT tSid, tChg, tPrice, tQuan, tBPrice, tBQuan, tSPrice, tSQuan FROM info_today WHERE tSid = '" + tmp + "' AND tTime = ( SELECT MAX( tTime ) FROM info_today WHERE tSid = '" + tmp + "' ) AND tprice <= ( SELECT MIN( hPrice ) AS price FROM ( SELECT * FROM info_history WHERE hSid = '" + tmp + "' ORDER BY hDate DESC LIMIT 0 , 20 )tmp )";
        		result = DB.stmt.executeQuery(sql);
        		
        		if(result.next()) {
        			if ((type == 0 && result.getDouble("tChg") < 0.0) || (type == 1 && result.getDouble("tChg") > 0.0))
        				continue;
        			
        			
        			for (int i = 0; i < column; i++) {
        				data[i] = result.getString(i+1);
        			}
        			
        			dataList.add(data.clone());
        			row++;
        		}
        	}

        	queryData = new String [row][column];
        	for(int j = 0; j < row; j++) {
        		data = dataList.remove(0);
        		for(int i = 0; i < column; i++) {
        			queryData[j][i] = data[i];
        		}
        	}
        } catch (SQLException e) {
            System.err.println(e);
        } catch (FileNotFoundException e) {
			e.printStackTrace();
		}
        
        result.close();
    }
	
	public int getRow() {
		return row;
	}
	
	public int getColumn() {
		return column;
	}
	
	public String[][] getData() {
		return queryData;
	}
}