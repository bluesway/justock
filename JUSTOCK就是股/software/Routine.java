/**
 * @author bluesway
 * TARGET 常駐程式，執行撮合、交割……等常態性任務
 */

package software;

import java.awt.AWTException;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.swing.ImageIcon;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;


public class Routine implements Job {
	public static Logger log;
	FileHandler fileHandler;
	Scheduler sched;
	StdSchedulerFactory sf;
	
    public void run() throws Exception {
    	Date date;
    	JobDetail execJob1, execJob2;	// 撮合作業
    	JobDetail tiktJob1, tiktJob2;	// 交割作業
    	JobDetail tsecJob1, tsecJob2;	// 即時資訊擷取作業
    	JobDetail hisJob;				// 歷史資料擷取作業
    	JobDetail tranJob;				// 交易清算作業
    	JobDetail manJob;				// 資料庫維護作業
    	JobDetail testJob;				// 測試作業
    	CronTrigger trigger;
    	
    	log = Logger.getLogger("Routine");
    	fileHandler = new FileHandler("routine.log");
    	fileHandler.setFormatter(new SimpleFormatter());
    	log.addHandler(fileHandler);
    	sf = new StdSchedulerFactory();
    	sched = sf.getScheduler();
    	
        log.info("------- Scheduling Jobs ----------------");

        // jobs can be scheduled before sched.start() has been called
        // execJob will run during market opening
        manJob = new JobDetail("manJob", Scheduler.DEFAULT_GROUP, software.Subitem.Transaction.Maintain.class);
        tsecJob1 = new JobDetail("tsecJob1", Scheduler.DEFAULT_GROUP, software.DataRetrieve.StockInfoParser_TSEC.class);
        execJob1 = new JobDetail("execJob1", Scheduler.DEFAULT_GROUP, software.Subitem.Transaction.Execution.class);
        tiktJob1 = new JobDetail("tiktJob1", Scheduler.DEFAULT_GROUP, software.Subitem.Transaction.Ticket.class);
        tsecJob2 = new JobDetail("tsecJob2", Scheduler.DEFAULT_GROUP, software.DataRetrieve.StockInfoParser_TSEC.class);
        execJob2 = new JobDetail("execJob2", Scheduler.DEFAULT_GROUP, software.Subitem.Transaction.Execution.class);
        tiktJob2 = new JobDetail("tiktJob2", Scheduler.DEFAULT_GROUP, software.Subitem.Transaction.Ticket.class);
        tranJob = new JobDetail("tranJob", Scheduler.DEFAULT_GROUP, software.Subitem.Transaction.Execution.class);
        hisJob = new JobDetail("hisJob", Scheduler.DEFAULT_GROUP, software.DataRetrieve.StockInfoParser_DBMaker.class);
if (GUI.TESTMODE) {
		testJob = new JobDetail("testJob", Scheduler.DEFAULT_GROUP, software.DataRetrieve.StockInfoParser_TSEC.class);
		
        // test every 5 seconds
        trigger = new CronTrigger("test", Scheduler.DEFAULT_GROUP, "30 * * * * ?");
        date = sched.scheduleJob(testJob, trigger);
        log.info(testJob.getFullName() + " will run at: " + date + " and repeat on: " + trigger.getCronExpression());
} else {
	    // mantain database @ 8:55
	    trigger = new CronTrigger("mantain", Scheduler.DEFAULT_GROUP, "0 55 8 ? * MON-FRI");
	    date = sched.scheduleJob(manJob, trigger);
	    log.info(manJob.getFullName() + " will run at: " + date + " and repeat on: " + trigger.getCronExpression());

        // fetch TSEC info @ 9:00 ~ 13:59
        trigger = new CronTrigger("retrieve1", Scheduler.DEFAULT_GROUP, "10,40 * 8-13 ? * MON-FRI");
        date = sched.scheduleJob(tsecJob1, trigger);
        log.info(tsecJob1.getFullName() + " will run at: " + date + " and repeat on: " + trigger.getCronExpression());
        
        // fetch TSEC info @ 14:00 ~ 14:30
        trigger = new CronTrigger("retrieve2", Scheduler.DEFAULT_GROUP, "10,40 0-30 14 ? * MON-FRI");
        date = sched.scheduleJob(tsecJob2, trigger);
        log.info(tsecJob2.getFullName() + " will run at: " + date + " and repeat on: " + trigger.getCronExpression());
        
        // do transaction @ 9:00 ~ 12:59
        trigger = new CronTrigger("transaction1_1", Scheduler.DEFAULT_GROUP, "30 * 9-16 ? * MON-FRI");
        date = sched.scheduleJob(execJob1, trigger);
        log.info(execJob1.getFullName() + " will run at: " + date + " and repeat on: " + trigger.getCronExpression());
        trigger = new CronTrigger("transaction1_2", Scheduler.DEFAULT_GROUP, "30 * 9-16 ? * MON-FRI");
        date = sched.scheduleJob(tiktJob1, trigger); 
        log.info(tiktJob1.getFullName() + " will run at: " + date + " and repeat on: " + trigger.getCronExpression());
        
        // do transaction @ 13:00 ~ 13:30
        trigger = new CronTrigger("transaction2_1", Scheduler.DEFAULT_GROUP, "30 0-30 17 ? * MON-FRI");
        date = sched.scheduleJob(execJob2, trigger);
        log.info(execJob2.getFullName() + " will run at: " + date + " and repeat on: " + trigger.getCronExpression());
        trigger = new CronTrigger("transaction2_2", Scheduler.DEFAULT_GROUP, "30 0-30 17 ? * MON-FRI");
        date = sched.scheduleJob(tiktJob2, trigger);
        log.info(tiktJob2.getFullName() + " will run at: " + date + " and repeat on: " + trigger.getCronExpression());

        // clean unhandled transaction @ 14:35
        trigger = new CronTrigger("transaction3", Scheduler.DEFAULT_GROUP, "0 35 17 ? * MON-FRI");
        date = sched.scheduleJob(tranJob, trigger);
        log.info(tranJob.getFullName() + " will run at: " + date + " and repeat on: " + trigger.getCronExpression());

        // fetch DBMaker after-market info @ 15:30
        trigger = new CronTrigger("transaction4", Scheduler.DEFAULT_GROUP, "0 30 18 ? * MON-FRI");
        date = sched.scheduleJob(hisJob, trigger);
        log.info(hisJob.getFullName() + " will run at: " + date + " and repeat on: " + trigger.getCronExpression());
}	//end of TESTMODE

        log.info("------- Starting Scheduler ----------------");

        // All of the jobs have been added to the scheduler, but none of the
        // jobs
        // will run until the scheduler has been started
        sched.start();

        log.info("------- Scheduler Started and Waiting ...--");
    }

    public void stop() throws SchedulerException {
        log.info("------- Shutting Down ---------------------");
        sched.shutdown(true);
        log.info("------- Shutdown Complete -----------------");
        log.info("Executed " + sched.getMetaData().numJobsExecuted() + " jobs.");
    }

    public void execute(JobExecutionContext arg0) throws JobExecutionException {
    	System.out.println(System.currentTimeMillis());
    }

    public static void main(String[] args) throws Exception {
    	final Routine routine = new Routine();
    	final PopupMenu popup = new PopupMenu();
    	TrayIcon trayIcon;
    	MenuItem exitItem;
    	ActionListener al = new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		synchronized (routine) {
        			routine.notify();
				}
        	}
        };
        
        if (SystemTray.isSupported()) {
            SystemTray tray = SystemTray.getSystemTray();

            // load an image
            ImageIcon icon = new ImageIcon("common/source/images/favicon.gif");
            exitItem = new MenuItem("Quit");
            exitItem.addActionListener(al);
            popup.add(exitItem);

            trayIcon = new TrayIcon(icon.getImage(), "Justock 常駐程式", popup);
            
            try {
                tray.add(trayIcon);
            } catch (AWTException e) {
                System.err.println(e);
            }
        }

        routine.run();
        synchronized (routine) {
    		try {
				routine.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
        routine.stop();
        System.exit(0);
    }
}