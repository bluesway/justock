/**
 * @author bluesway
 * TARGET ��Ʈw�C����@
 */

package software.Subitem.Transaction;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;


import software.DB;
import software.Routine;

public class Maintain implements Job {
	public void maintain() {
		Statement stmt;
		try {
			stmt = DB.conn.createStatement();
			
			stmt.addBatch("TRUNCATE TABLE `info_tse`");
			stmt.addBatch("TRUNCATE TABLE `info_otc`");
			stmt.addBatch("TRUNCATE TABLE `info_today`");
			stmt.addBatch("TRUNCATE TABLE `info_current`");
			stmt.addBatch("TRUNCATE TABLE `best_five`");
			stmt.addBatch("OPTIMIZE TABLE `transaction`");
			stmt.addBatch("OPTIMIZE TABLE `transaction_five`");
			stmt.addBatch("OPTIMIZE TABLE `holding`");
			stmt.addBatch("OPTIMIZE TABLE `login_record`");
			
			stmt.executeBatch();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}
	
    public void execute(JobExecutionContext context) throws JobExecutionException {
    	String jobName = context.getJobDetail().getFullName();
    	
		Maintain man = new Maintain();
		
		Routine.log.info("Maintain: " + jobName + " executing at " + new Date());
		
		man.maintain();
    }
}
