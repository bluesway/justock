/**
 * @author bluesway
 * TARGET 執行撮合作業 (五分鐘一次)
 */

package software.Subitem.Transaction;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Random;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import common.util.TimeUtil;

import software.DB;
import software.Routine;

public class Execution implements Job {
	// 交易機率計算
	private double score;				// 交易成功機率
	private double score_price;			// 成交價分數
	private double score_quan;			// 成交量分數
	private double score_five;			// 五檔分數
	private final double wPrice = 0.3;	// 成交價分數比重
	private final double wQuan = 0.3;	// 成交量分數比重
	private final double wFive = 0.4;	// 五檔分數比重
	private final double pScore = 40;	// 撮合成功分數下限
	
	// 交易單資訊
	private int id;						// 交易單流水號
	private int stockId;				// 交易股票代號
	private double price;				// 交易價格
	private int quan;					// 交易數量
	private double oldFive[];			// 掛單之五檔資訊
	private double currFive[];			// 目前之五檔資訊
	private int fivePos;				// 掛單之五檔位置
	private boolean buy;				// 交易類型
	private int time;					// 下單時間
	private int due;					// 有效期
	
	// 資料庫用變數
	private Statement tranStmt = null;
	private Statement priceStmt = null;
	private Statement quanStmt = null;
	private Statement fiveStmt = null;
	private Statement doStmt = null;

	private ResultSet tranResult = null;
	private ResultSet priceResult = null;
	private ResultSet quanResult = null;
	private ResultSet fiveResult = null;
	
	public Execution(){
		try {
			tranStmt = DB.conn.createStatement();
			priceStmt = DB.conn.createStatement();
			quanStmt = DB.conn.createStatement();
			fiveStmt = DB.conn.createStatement();
			doStmt = DB.conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		oldFive = new double[20];
		currFive = new double[20];
		score = 0.0;
	}

	// 開始撮合作業
	private void execute() {
		try {
			tranResult = tranStmt.executeQuery("SELECT * FROM transaction WHERE tStatus=-1");
			
			while (tranResult.next()) {
				id = tranResult.getInt("tId");
				stockId = Integer.parseInt(tranResult.getString("tSid"));
				price = tranResult.getDouble("tPrice");
				quan = tranResult.getInt("tQuan");
				buy = price > 0 ? true : false;
				time = tranResult.getInt("tTime");
				due = tranResult.getInt("tDue");
				
				if (!checkDue())					// 檢查有效期
					continue;
				
				if (requestBestFiveInfo())	{		// 比較價格部份
					if (checkPrice()) {
						analyzePriceInfo();			// 比較成交價資訊
						analyzeQuanInfo();			// 比較成交量資訊
						analyzeBestFiveInfo();		// 取得五檔資訊
						calculateScore();			// 計算最後分數
					}
				}
				
				doExecution(false);
			}
			
			doExecution(true);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (tranResult != null)
					tranResult.close();
				if (quanResult != null)
					quanResult.close();
				if (priceResult != null)
					priceResult.close();
				if (fiveResult != null)
					fiveResult.close();
				if (tranStmt != null)
					tranStmt.close();
				if (quanStmt != null)
					quanStmt.close();
				if (priceStmt != null)
					priceStmt.close();
				if (fiveStmt != null)
					fiveStmt.close();
				if (doStmt != null)
					doStmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return;
	}

	private boolean checkDue() {
		int limit;
		TimeUtil tu;
		
		if (due == 0)
			return true;
		
		tu = new TimeUtil(time);
		
		limit = (Integer.parseInt(TimeUtil.getCurrentHour()) - Integer.parseInt(tu.getHour())) * 60 + 
				(Integer.parseInt(TimeUtil.getCurrentMinute()) - Integer.parseInt(tu.getMinute()));
		
		if (limit > due) {	// 超過有效期
			try {
				doStmt.addBatch("UPDATE transaction SET tStatus=2 WHERE tId=" + id);
				return false;
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}
		} else
			return true;
	}

	// 取得五檔資訊
	private boolean requestBestFiveInfo() {
		try {
			fiveResult = fiveStmt.executeQuery("SELECT * FROM transaction_five WHERE tId='" + id + "'");
			if (fiveResult.next())
				for (int i = 0; i < 20; i++)
					oldFive[i] = fiveResult.getDouble(i + 2);
			else
				return false;

			fiveResult = fiveStmt.executeQuery("SELECT * FROM best_five WHERE bSid=\"" + stockId + "\"");
			if (fiveResult.next())
				for (int i = 0; i < 20; i++)
					currFive[i] = fiveResult.getDouble(i + 2);
			else
				return false;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return true;
	}

	// 檢查價格
	private boolean checkPrice() {
		int i;
		boolean undealt = false;
		
		if (buy)
			for (i = 0; i < 10; i += 2)
				if (undealt = price == currFive[i])
					break;
				else;
		else
			for (i = 10; i < 20; i += 2)
				if (undealt = price == currFive[i])
					break;
		
		fivePos = i;
		score = 100;
		
		return undealt;
	}

	// 分析成交價資訊 (score_price 最大值 1.0)
	private void analyzePriceInfo() {
		double open = 1.0;
		double chg = 0.0;
		try {
			priceResult = priceStmt.executeQuery("SELECT tPrice FROM info_today WHERE tSid='" + id + "' AND tChg=0");
			if (priceResult.next())
				open = priceResult.getDouble(1);
			
			priceResult = priceStmt.executeQuery("SELECT tChg FROM info_today WHERE tSid='" + id + "' ORDER BY tTime DESC");
			if (priceResult.next())
				chg = priceResult.getDouble(1);
			
			chg /= 1.0 * open;		// 漲跌幅
			
			score_price = 0.5 + chg * (buy ? 7.14 : -7.14);	// 14% 轉成 100%
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	// 分析成交量資訊 (score_quan 最大值 1.0)
	private void analyzeQuanInfo() {
		int max = 1;
		int curr = 0;
		try {
			quanResult = quanStmt.executeQuery("SELECT MAX(tQuan) FROM info_today WHERE tSid='" + stockId + "'");
			if (quanResult.next())
				max = quanResult.getInt(1) * 1000;
			
			quanResult = quanStmt.executeQuery("SELECT tQuan FROM info_today WHERE tSid='" + stockId + "' ORDER BY tTime DESC");
			if (quanResult.next())
				curr = quanResult.getInt(1) * 1000;
			
			if (quan > curr)// 掛單量不可能比當盤成交量大
				score_quan = 0.0;
			else
				score_quan = 1.0 * curr / max;
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// 分析五檔資訊 (score_five 最大值 1.0)
	private void analyzeBestFiveInfo() {
		int quan = 1;
		int diff = 0;

		try {
			quanResult = quanStmt.executeQuery("SELECT tQuan FROM info_today WHERE tSid='" + stockId + "' ORDER BY tTime DESC");
			if (quanResult.next())
				quan = quanResult.getInt(1) * 1000;
			
			diff = (int) Math.abs(currFive[fivePos + 1] * 1000 - quan);

			score_five = 1.0 * diff / quan;
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	// 計算最後分數
	private void calculateScore() {
		double seed;
		Random rnd;

		rnd= new Random(System.currentTimeMillis());
		seed = 50 + 2 * rnd.nextGaussian();	// μ=50, σ=2 的常態分配亂數
		System.out.println(score_price * wPrice + "," + score_quan * wQuan + "," + score_five * wFive + "," + seed);
		score = score_price * wPrice + score_quan * wQuan + score_five * wFive;
		score *= seed;
	}
	
	
	// 執行撮合寫入資料庫
	private int doExecution(boolean done) {
		try {
			if (done)
				return doStmt.executeBatch().length;
			
			System.out.println("Id: " + id + ", score: " + score);
			
			if (score < pScore)
				return 0;
			
			doStmt.addBatch("UPDATE transaction SET tStatus=0 WHERE tId=" + id);
			
			return 1;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	// 整理未撮合之掛單
	public void maintain() {
		ResultSet list;
		Statement stmt;
		try {
			stmt = DB.conn.createStatement();
			list = stmt.executeQuery("SELECT * FROM transaction WHERE tStatus=-1");
			
			while (list.next())
				stmt.addBatch("UPDATE transaction SET tStatus=0 WHERE tId=" + list.getInt("tId"));
			
			stmt.executeBatch();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}
	
    public void execute(JobExecutionContext context) throws JobExecutionException {
    	String jobName = context.getJobDetail().getFullName();
    	
		Execution exe = new Execution();

		if (Integer.parseInt(TimeUtil.getCurrentHour()) > 13 && Integer.parseInt(TimeUtil.getCurrentMinute()) > 30) {
			Routine.log.info("Maintain: " + jobName + " executing at " + new Date());
			exe.maintain();
		} else {
			Routine.log.info("Execution: " + jobName + " executing at " + new Date());
			exe.execute();
		}
    }
    
	public static void main(String[] args) {
		Execution exe = new Execution();
		exe.execute();
	}
}
