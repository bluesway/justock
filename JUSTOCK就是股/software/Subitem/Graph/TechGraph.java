/**
 * @author bluesway
 * TARGET 技術分析圖
 */

package software.Subitem.Graph;

import java.awt.Color;
//import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.Arrays;
import java.util.LinkedList;
import javax.swing.JFrame;

import common.util.StockBox;
import software.GUI;
import software.Subitem.GeneralGraph;

@SuppressWarnings("serial")
public class TechGraph extends GeneralGraph {
	// KD指標
	private int kd_len;
	private double rsv;
	private double k_pvalue;
	private double k_cvalue;
	private double d_pvalue;
	private double d_cvalue;
	private double[] kd_data;
	
	// Bias乖離率
	private double bavgPrev;
	private double bavgCurr;
	private double biasPrev;
	private double biasCurr;
	
    public TechGraph () {
    	this(1000, 0, 1);
    }
    
	public TechGraph (int stockId, int dType, int lType) {
		this.stockId = stockId == 0 ? 1000 : stockId;
    	isOverall = false;
		
    	// 顯示型態
    	limiteType = lType;
    	displayType = dType;
    	
    	// 圖形繪製
    	line_cap = 2;
    	char_height = GUI.overall_size[5];
    	cpl_height = GUI.overall_size[25];
    	left_space = GUI.overall_size[22];
    	right_space = GUI.overall_size[35];
    	graphD = new Rectangle();
    	start_main = 15;

    	// 資料處理
    	sourceData = new LinkedList<double[]>();
    	priceNum = 9;
    	quanNum = 3;
    	shift = 0;
    	scale = 3;
    	range_start = 0;
    	range = (int) Math.round(graphD.width / scale);
    	mouse_x = -1;
    	mouse_y = -1;
    	avgNum = 6;
    	avgPrev = new double[avgNum];
    	avgCurr = new double[avgNum];
    	kd_len = 9;
    	kd_data = new double[kd_len+1];
    	k_pvalue = 50.0;
    	d_pvalue = 50.0;
    	bavgPrev = 0.0;
    	bavgCurr = 0.0;
    	
    	// 控制台配置
    	if (this.stockId != 1000 && this.stockId != 4000) {
    		createBox();
    		add(stockBox);
    	}
    	
    	add(defaultBtn[2]);
    	add(defaultBtn[1]);
    	add(defaultBtn[0]);
    	add(defaultBtn[3]);
    	
		readData();
		
    	if (this.stockId != 1000 && this.stockId != 4000) {
    		add(defaultBtn[4]);
        	defaultBtn[4].setActionCommand(String.valueOf(this.stockId));
    		stockBox.setSelectedIndex(StockBox.searchIndexOfStockId(this.stockId));
    		GUI.gui.setStockId(this.stockId);
    	}
	}
	
	public void paintComponent(Graphics g) {
		long a = System.currentTimeMillis();
    	Graphics2D page = (Graphics2D) g;
    	
    	// 重新讀取資料
     	screen_x = getWidth();
     	screen_y = getHeight();
     	graphD.setLocation(getX() + left_space, getY() + cpl_height + 10);
    	graphD.setSize(screen_x - left_space - right_space, screen_y - cpl_height - 25); 
    	range = ((int) Math.round((graphD.width - line_cap / 2) / scale)) + 1;
    	height_main = (int) Math.round(graphD.height * 0.4);
    	start_quan = (int) Math.round(graphD.height * 0.5);
    	height_quan = (int) Math.round(graphD.height * 0.16);
    	start_tech1 = (int) Math.round(graphD.height * 0.7);
    	height_tech1 = (int) Math.round(graphD.height * 0.15);
    	start_tech2 = (int) Math.round(graphD.height * 0.85);
    	height_tech2 = (int) Math.round(graphD.height * 0.15 - line_cap / 2);
    	avgPrev = new double[avgNum];
    	avgCurr = new double[avgNum];
    	k_pvalue = 50.0;
    	d_pvalue = 50.0;
    	bavgPrev = 0.0;
    	bavgCurr = 0.0;
    	
    	super.paintComponent(page);
    	
		int x = graphD.x + line_cap / 2;
		int scale_diff = (x + scale < graphD.x + graphD.width && graphD.x + graphD.width - x - scale > scale) ? scale : graphD.x + graphD.width - x;
		boolean drawQuan = sourceData.get(range_start + range - 1)[0] > 19870106 ? true : false;
		
		// 顯示指數資訊
		drawChartLabel(page, 0);

        // 顯示成交量資訊
        if (drawQuan)
        	drawChartLabel(page, 1);

        // 畫線圖、柱圖及顯示時間資訊
        while (time - range_start < range && time < sourceData.size()) {
        	now = sourceData.get(time);
        	if (stockId == 0)
        		drawLineChart(page, prev[1], now[1], x, scale_diff);
        	else if (displayType == TYPE_LINE_CHART )
        		drawLineChart(page, prev[4], now[4], x, scale_diff);
        	else if (displayType == TYPE_KBAR_CHART)
        		drawKBarChart(page, now, x, scale_diff);
        	else {
        		drawKBarChart(page, now, x, scale_diff);
        		drawLineChart(page, prev[4], now[4], x, scale_diff);
        	}

        	if (stockId != 0) {
	        	drawAvgLine(page, 1, 5, 0, time, x, scale_diff, Color.YELLOW);
	        	drawAvgLine(page, 2, 10, 0, time, x, scale_diff, Color.CYAN);
	        	drawAvgLine(page, 3, 20, 0, time, x, scale_diff, Color.MAGENTA);
        	}
        	
            if (drawQuan)
            	drawQuanChart(page, stockId == 0 ? now[3] : now[5], x, scale_diff);

            if (stockId != 0) {
	            drawAvgLine(page, 1, 5, 1, time, x, scale_diff, Color.YELLOW);
	            drawAvgLine(page, 2, 10, 1, time, x, scale_diff, Color.CYAN);
	            drawAvgLine(page, 3, 20, 1, time, x, scale_diff, Color.MAGENTA);
            }

            drawKDIndex(page, time, x, scale_diff, Color.PINK, Color.ORANGE);
//            if (dateType == 0)
            	drawBiasCurve(page, 10, time, x, scale_diff, Color.GREEN);
//            else if (dateType == 1)
//            	drawBiasCurve(page, 25, time, x, scale_diff, Color.GREEN);
//            else if (dateType == 2)
//            	drawBiasCurve(page, 72, time, x, scale_diff, Color.GREEN);
            
            x += scale_diff;

            if ((time - range_start) % (int) Math.round(graphD.width / scale / GUI.overall_size[2]) == 0 && (time - range_start)> 0 && x + GUI.overall_size[6] < graphD.x + graphD.width - line_cap / 2)
            	if (dateType == 3)
            		drawChartLabel(page, (int) now[0], x);
            	else
            		drawChartLabel(page, x, now[0]);

            prev = now;
            time++;
        }
        
        // 處理滑鼠指標
        drawMouseInfo(page, drawQuan);

        // 畫邊框
		drawCanvas(page);
		
		a = System.currentTimeMillis() - a;
//		System.out.println("繪圖完成，共花費" + a / 1000.0 + "秒");
	}
	
    // 畫均線圖
    private void drawKDIndex (Graphics page, int index, int x, int scale, Color c1, Color c2) {
    	if (index - range_start < kd_len)
    		return;
    	
    	for (int i = 0; i < kd_len; i++)
			kd_data[i + 1] = sourceData.get(index - i)[4];
    	kd_data[0] = kd_data[1];
    	Arrays.sort(kd_data, 1, kd_len + 1);
    	
    	rsv = (kd_data[0] - kd_data[1]) * 100.0 / (kd_data[kd_len] - kd_data[1]);
    	k_cvalue = (k_pvalue * 2 + rsv) / 3;
    	d_cvalue = (d_pvalue * 2 + k_cvalue) / 3;
    	
    	page.setColor(c1);
        page.drawLine(x, calculateAxis(k_pvalue, 2), x + scale, calculateAxis(k_cvalue, 2));
        page.setColor(c2);
        page.drawLine(x, calculateAxis(d_pvalue, 2), x + scale, calculateAxis(d_cvalue, 2));
        
        if (index  == range_start + range - 1) {
        	int tx;
        	int ty;
        	
        	page.setColor(Color.DARK_GRAY);
        	drawBrokenLine(page, graphD.x, calculateAxis(80, 2), graphD.x + graphD.width, calculateAxis(80, 2)); // 畫橫虛線
        	drawBrokenLine(page, graphD.x, calculateAxis(20, 2), graphD.x + graphD.width, calculateAxis(20, 2)); // 畫橫虛線
        	page.setColor(Color.WHITE);
        	page.drawString("80", 0, calculateAxis(80, 2) + char_height / 2);
        	page.drawString("20", 0, calculateAxis(20, 2) + char_height / 2);
        	
        	tx = graphD.x + graphD.width + line_cap;
        	ty = graphD.y + start_tech1 + line_cap;
        	page.drawString("RSV" + kd_len + "=" + nf.format(rsv), tx, ty);
        	page.drawString("K-D" + "=" + nf.format(k_cvalue - d_cvalue), tx, ty + char_height * 3);
        	page.setColor(c1);
        	page.drawString("K" + (int) Math.sqrt(kd_len) + "=" + nf.format(k_cvalue), tx, ty + char_height * 1);
        	page.setColor(c2);
        	page.drawString("D" + (int) Math.sqrt(kd_len) + "=" + nf.format(d_cvalue), tx, ty + char_height * 2);
        }
        
        k_pvalue = k_cvalue;
        d_pvalue = d_cvalue;
    }

    private void drawBiasCurve (Graphics page, int len, int index, int x, int scale, Color color) {
    	if (index - range_start < len)
    		return;
    	
    	if (bavgPrev == 0.0) {
    		for (int i = 0; i < len; i++)
    			bavgPrev += sourceData.get(index - i - 1)[4];
    		biasPrev = bavgPrev / len * 1.0;
    		biasPrev = (sourceData.get(index)[4] - biasPrev) / biasPrev; 
    	}
    	
		bavgCurr = bavgPrev + sourceData.get(index)[4] - sourceData.get(index - len)[4];
		
		biasCurr = bavgCurr / len * 1.0;
		biasCurr = (sourceData.get(index)[4] - biasCurr) / biasCurr; 
        
        // 文字說明
        if (index  == range_start + range - 1) {
        	int y1;
        	int y2;
        	
        	page.setColor(color);
        	page.drawString("Bias" + len + "=" + nf.format(biasCurr * 100) + "%", graphD.x + graphD.width + line_cap, 
        			graphD.y + start_tech2 + line_cap + char_height);
        	
        	page.setColor(Color.WHITE);
        	if (dateType == 0) {
            	y1 = calculateAxis(0.05, 3);
            	y2 = calculateAxis(-0.045, 3);
        		page.drawString("5%", 0, y1 + char_height / 2);
        		page.drawString("-4.5%", 0, y2 + char_height / 2);
        	} else if (dateType == 1) {
            	y1 = calculateAxis(0.125, 3);
            	y2 = calculateAxis(-0.1125, 3);
        		page.drawString("12.5%", 0, y1 + char_height / 2);
        		page.drawString("-11.25%", 0, y2 + char_height / 2);
        	} else if (dateType == 2) {
            	y1 = calculateAxis(0.25, 3);
            	y2 = calculateAxis(-0.225, 3);
        		page.drawString("25%", 0, y1 + char_height / 2);
        		page.drawString("-22.5%", 0, y2 + char_height / 2);
        	} else {
            	y1 = calculateAxis(0.05, 3);
            	y2 = calculateAxis(-0.045, 3);
        	}

        	page.setColor(Color.DARK_GRAY);
        	drawBrokenLine(page, graphD.x, y1, graphD.x + graphD.width, y1); // 畫橫虛線
        	drawBrokenLine(page, graphD.x, y2, graphD.x + graphD.width, y2); // 畫橫虛線
        }
        
        page.setColor(color);
        page.drawLine(x, calculateAxis(biasPrev, 3), x + scale, calculateAxis(biasCurr, 3));
        
        bavgPrev = bavgCurr;
        biasPrev = biasCurr;
    }
    
    // 計算KD值
    protected int calculateAxis (double data, int calType) {	// 2:KD 3:Bias
    	switch(calType) {
    	case 2:
    		return (int) Math.round(height_tech1 / 100.0 * (100 - data) + graphD.y + start_tech1);
    	case 3:
    		if (dateType == 0)
    			return (int) Math.round(height_tech2 / 20.0 * (10 - data * 100) + graphD.y + start_tech2);
    		else if (dateType == 1)
    			return (int) Math.round(height_tech2 / 50.0 * (25 - data * 100) + graphD.y + start_tech2);
    		else if (dateType == 2)
    			return (int) Math.round(height_tech2 / 100.0 * (50 - data * 100) + graphD.y + start_tech2);
    		else
    			return 0;
    	default:
    		return super.calculateAxis(data, calType);
    	}
    }
    
    public static void main (String[] args)
    {
    	GeneralGraph mg = new TechGraph (1101, TYPE_KBAR_CHART, TYPE_UNLIMITED);
    	JFrame jf = new JFrame ("- Graph Demo -");
    	jf.getContentPane().add(mg);
       
    	jf.setVisible(true);
    	jf.toFront();
    	jf.setResizable(true);
    	jf.pack();
       
    	jf.setLocationRelativeTo(null);
    	jf.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
    }
}