/*
 *  設置彈出式選單的介面
 *  20070829
 *  Author @ kevingo
 * 
 * */

package software.Subitem.Listener;

import java.awt.PopupMenu;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PopupListener extends MouseAdapter {
	PopupMenu popup;

    public PopupListener(PopupMenu popupMenu) {
    	popup = popupMenu;
    }

    public void mousePressed(MouseEvent e) {
        showPopup(e);
    }

    public void mouseReleased(MouseEvent e) {
        showPopup(e);
    }

    private void showPopup(MouseEvent e) {
        if (e.isPopupTrigger()) {
            popup.show(e.getComponent(),
                       e.getX(), e.getY());
        }
    }
}