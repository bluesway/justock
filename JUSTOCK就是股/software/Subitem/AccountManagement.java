/**
 * @author kevingo
 * @modify bluesway
 * TARGET 帳戶資料
 */

package software.Subitem;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JLabel;
import javax.swing.JTextField;

import software.DB;
import software.GUI;

public class AccountManagement {
    // 顯示使用者畫面
	private JLabel bankLabel;
    private JLabel idLabel;
    private JLabel accountLabel;
	private JLabel bankAcctLabel;
	private JLabel assetLabel;

	public Container container;
	public JTextField idField;
	public JTextField bankAcctField;
	public JTextField assetField;
	private JTextField bankField;
	private JTextField accountField;
	private String [] data;
	private double asset;

	public AccountManagement() {
		data  = getAccountData(GUI.userData[0]);
		asset = getAsset(GUI.userData[0]);
		setFrame();
    }

	private void setFrame() {
    	GridBagConstraints g = new GridBagConstraints();
    	
    	container = new Container();
		
		container.setLayout(new GridBagLayout());
		container.setSize(400, 300);

		g.weighty = 1; // 設定各個元件之間的分散程度
		
		bankLabel = new JLabel("銀行代碼：");
		bankField = new JTextField(20);
		bankField.setEditable(false);
		g.gridx = 0;
		g.gridy = 0;
		container.add(bankLabel, g);
		g.gridx = 1;
		g.gridy = 0;			
		bankField.setText(data[0]);
		container.add(bankField, g);

		idLabel = new JLabel("使用者帳號：");
		idField = new JTextField(20);
		idField.setEditable(false);
		g.gridx = 0;
		g.gridy = 1;
		container.add(idLabel, g);
		g.gridx = 1;
		g.gridy = 1;			
		idField.setText(data[1]);
		container.add(idField, g);
		
		accountLabel = new JLabel("銀行帳號：");
		accountField = new JTextField(20);
		accountField.setEditable(false);
		g.gridx = 0;
		g.gridy = 2;
		container.add(accountLabel, g);
		g.gridx = 1;
		g.gridy = 2;			
		accountField.setText(data[2]);
		container.add(accountField, g);

		bankAcctLabel = new JLabel("帳戶餘額：");
		bankAcctField = new JTextField(20);
		bankAcctField.setEditable(false);
		g.gridx = 0;
		g.gridy = 3;
		container.add(bankAcctLabel, g);
		g.gridx = 1;
		g.gridy = 3;
		bankAcctField.setText(data[3]);
		container.add(bankAcctField, g);
		
		assetLabel = new JLabel("持股總值：");
		assetField = new JTextField(20);
		assetField.setEditable(false);
		g.gridx = 0;
		g.gridy = 4;
		container.add(assetLabel, g);
		g.gridx = 1;
		g.gridy = 4;
		assetField.setText(String.valueOf(asset));
		container.add(assetField, g);
	}
    
    public Container getFrame() {
		return container;
    }
    
    private double getAsset(String userId) {
    	ResultSet result, result2;
    	Statement stmt, stmt2;
    	double total = 0.0;
    	String sid;
    	int hquan = 0;
    	double tprice = 0.0;
    	
    	try {
			stmt = DB.conn.createStatement();
			stmt2 = DB.conn.createStatement();
			result = stmt.executeQuery("SELECT * FROM holding WHERE hUid='" + userId + "'");
			
			while(result.next()) {
				 sid = result.getString("hSid");
				 hquan = result.getInt("hQuan");
				 result2 = stmt2.executeQuery("SELECT tPrice FROM info_today WHERE tSid='" + sid + "' ORDER BY tTime DESC");
				 
				 if(result2.next()) {
					 tprice = result2.getDouble("tPrice");
					 total += tprice * hquan;
				 }
			}
			
			return total;
		} catch (SQLException e) {
			e.printStackTrace();
		}

    	return 0;
	}
    
    private static String[] getAccountData(String userId) {
    	ResultSet result;
    	String[] userData = new String[5];
    	
        try {
        	result = DB.stmt.executeQuery("SELECT * from bank where bUid='" + userId + "'");
        	
        	if (result.next())
        		for (int i = 0; i < 4; i++)
        			userData[i] = result.getString(i + 1);
        } catch (SQLException e) {
            System.err.println(e);
        }
        
        return userData.clone();
    }
}