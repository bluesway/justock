package software.Subitem.table;

import java.awt.Component;
import java.awt.Color;
import java.awt.Font;
import java.io.UnsupportedEncodingException;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class CustomTableCellOfWisdom extends DefaultTableCellRenderer {

	private static final long serialVersionUID = 1L;
	
	public CustomTableCellOfWisdom() {

	}

	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {

		try {
			Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				if(row %2 == 1) {
					table.setOpaque(true);
					cell.setBackground(Color.DARK_GRAY);
					cell.setForeground(Color.WHITE);
					cell.setFont(new Font("�з���", Font.PLAIN, 14));
				}
				else {
					table.setOpaque(true);
					cell.setBackground(Color.BLACK);
					cell.setForeground(Color.WHITE);
				}
		} catch(IndexOutOfBoundsException  e) {
			e.printStackTrace();
		}
		return super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
	}
	
	public boolean checkChinese(Object value) throws UnsupportedEncodingException {
		String   sentence = String.valueOf(value);
		byte[]   temp=sentence.getBytes("big5");
		boolean   chinese=false;                                      
		int   i;  
		   
         for(i=0;i<temp.length;i++)  
            if(temp[i]<0) {   
            	  chinese=true;  
                  i=temp.length;
            }  
		   
          if(chinese)  
                return false;
          else  
                return true;
	}
}