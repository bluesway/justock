/*
 * 產生店頭市場即時資訊圖 
 * */
package software.Subitem.table;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import software.Subitem.Graph.OverallGraph;

public class WidgetOfMainGraph extends JFrame{
	private static final long serialVersionUID = 1L;
	private JPanel panel;
	private OverallGraph ogt;
	private Container container;
	
	public WidgetOfMainGraph() {
		createPage();
	}
	
	public void createPage() {
		ogt = new OverallGraph(true);
		container = new Container();
		container.setLayout(new BorderLayout());
    	panel= new JPanel(new GridLayout(1,1));
    	
		try {
			ogt = new OverallGraph(true);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}		
		panel.add(ogt);
		
        container.add(panel, BorderLayout.CENTER);
        container.setVisible(true);
	}
	
    public Container getFrame() {
		return container;
    }
}