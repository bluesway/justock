package common.source;

import common.util.CryptAES;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class JUSTOCKDBSource implements DBSource {
    private Properties props;
    private String url;
    private String user;
    private String passwd;
    private int max;
    private List<Connection> connections;
    
    public JUSTOCKDBSource() throws IOException, ClassNotFoundException {
        this("JUSTOCKDB.properties");
    }
    
    public JUSTOCKDBSource(String configFile) throws IOException, ClassNotFoundException {
        props = new Properties();
        try {
			props.load(new StringReader((String) CryptAES.encryptfile(null, configFile, CryptAES.DECRYPT, CryptAES.AES_ALGO, CryptAES.TEXT_OUTPUT)));
		} catch (Exception e) {
			e.printStackTrace();
		}
        url = props.getProperty("url");
        user = props.getProperty("user");
        passwd = props.getProperty("password");
        max = Integer.parseInt(props.getProperty("poolmax"));
        Class.forName(props.getProperty("driver"));
        
        connections = new ArrayList<Connection>();
    }
    
    public synchronized Connection getConnection() throws SQLException {
        if(connections.size() == 0)
            return DriverManager.getConnection(url, user, passwd);
        else
            return connections.remove(connections.size() - 1);
    }
    
    public synchronized void closeConnection(Connection conn) throws SQLException {
        if(connections.size() == max)
            conn.close();
        else
            connections.add(conn);
    }
}