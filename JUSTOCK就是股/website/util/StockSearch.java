package website.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class StockSearch {
    private static Connection conn;
    private static Statement stmt;
    
    public StockSearch() {
    	connectDB();
    }
    
	private void connectDB() {
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://JUSTOCK.twbbs.org:3306/justock";
		String user = "XDDDDD";
		String pwd = "specification";
		
		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url + "?user=" + user + "&password=" + pwd);
			stmt = conn.createStatement();
		} catch (InstantiationException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}
	
	// 以 stockBox 的內容搜尋股票，傳回股票代碼
	public int getStockId(String stockItem) {
		int stockId;
    	ResultSet result = null;
    	
		try {
			result = stmt.executeQuery("SELECT * FROM stock WHERE Sid LIKE \'" + stockItem + "\' OR Sname LIKE \'" + stockItem + "\'");
			
			if (result.next())
				stockId = Integer.parseInt(result.getString("Sid"));
			else
				stockId = -1;
		} catch (SQLException e1) {
			e1.printStackTrace();
			stockId = -1;
		} finally {
			try {
				if (result != null)
					result.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return stockId;
	}
	
	public static void main(String[] args) {
		StockSearch ss = new StockSearch();
		System.out.println(ss.getStockId("東泥"));
	}
}
