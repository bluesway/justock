<%@ page contentType="text/html; charset=UTF-8" language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>證券辭典</title>
<style type="text/css">
<!--
a:link {
	color: #0000CE;
}
a:visited {
	color: #0000CE;
}
a:hover {
	color: #0000CE;
}
a:active {
	color: #0000CE;
}
.style5 {font-size: 12px}
-->
</style></head>

<body>

<form id="form1" name="form1" method="post" action="">
  <table width="90%" border="0" align="center">
    <tr>
      <td width="5%">&nbsp;</td>
      <td width="19%"><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=1" target="rightFrame">集中市場</a></span></td>
      <td width="19%"><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=2" target="rightFrame">電腦自動交易</a></span></td>
      <td width="19%"><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=3" target="rightFrame">價格優先、時間優先</a></span></td>
      <td width="19%"><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=4" target="rightFrame">成交價格決定方式</a></span></td>
      <td width="19%"><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=5" target="rightFrame">集合競價</a></span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=6" target="rightFrame">開盤價</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=7" target="rightFrame">收盤價</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=8" target="rightFrame">買賣揭示價格</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=9" target="rightFrame">揭示買賣數量</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=10" target="rightFrame">最佳買價</a></span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=11" target="rightFrame">最佳賣價</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=12" target="rightFrame">最佳五檔</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="/noun2.jsp" target="rightFrame">檔位</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=14" target="rightFrame">盤中瞬間價格穩定措施</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=15" target="rightFrame">限價委託</a></span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=16" target="rightFrame">鉅額交易</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=17" target="rightFrame">零股交易</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=18" target="rightFrame">盤後定價交易</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=19" target="rightFrame">全權委託代客操作</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=20" target="rightFrame">拍賣</a></span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=21" target="rightFrame">標購</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=22" target="rightFrame">除息交易日</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=23" target="rightFrame">除權交易日</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="/noun1.jsp" target="rightFrame">升降單位</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="/noun3.jsp" target="rightFrame">漲跌幅度</a></span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=26" target="rightFrame">有價證券抵繳</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=27" target="rightFrame">假除權</a><a href="http://www.tse.com.tw/ch/dict_right.php?id=58" target="rightFrame">(</a><a href="http://www.tse.com.tw/ch/dict_right.php?id=27" target="rightFrame">新制</a><a href="http://www.tse.com.tw/ch/dict_right.php?id=58" target="rightFrame">)</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=28" target="rightFrame">標借、議借</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=29" target="rightFrame">融資比率</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=30" target="rightFrame">融資券分配</a></span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=31" target="rightFrame">融券回補</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=32" target="rightFrame">融資融券契約期間</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=33" target="rightFrame">資券互抵</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=34" target="rightFrame">擔保維持率</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=35" target="rightFrame">融券保證金</a></span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=36" target="rightFrame">網路下單認證</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=37" target="rightFrame">全額交割股</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=38" target="rightFrame">處置股票</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=39" target="rightFrame">證券經紀商</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=40" target="rightFrame">證券自營商</a></span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=41" target="rightFrame">證券承銷商</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=42" target="rightFrame">投資人違約</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=43" target="rightFrame">錯帳</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=44" target="rightFrame">更正帳號</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=45" target="rightFrame">限價交易</a></span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=46" target="rightFrame">投資人買賣委託方式</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=47" target="rightFrame">預收款券</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=48" target="rightFrame">認購</a><a href="http://www.tse.com.tw/ch/dict_right.php?id=58" target="rightFrame">(</a><a href="http://www.tse.com.tw/ch/dict_right.php?id=48" target="rightFrame">售</a><a href="http://www.tse.com.tw/ch/dict_right.php?id=58" target="rightFrame">)</a><a href="http://www.tse.com.tw/ch/dict_right.php?id=48" target="rightFrame">權證</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=49" target="rightFrame">存續期間</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=50" target="rightFrame">履約價格</a></span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=51" target="rightFrame">行使比例</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=52" target="rightFrame">權利金</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=53" target="rightFrame">溢價比</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=54" target="rightFrame">槓桿比率</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=55" target="rightFrame">個股型權證</a></span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=56" target="rightFrame">組合型權證</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=57" target="rightFrame">指數型權證</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=58" target="rightFrame">價內認購(售)權證</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=59" target="rightFrame">價外認購(售)權證</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=60" target="rightFrame">價平認購(售)權證</a></span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=61" target="rightFrame">美式權證</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=62" target="rightFrame">歐式權證</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=63" target="rightFrame">內含價值</a></span></td>
      <td><span class="style5"><img src="http://www.tse.com.tw/ch/images/dict_arr2.gif" height="5" width="3" /> <a href="http://www.tse.com.tw/ch/dict_right.php?id=64" target="rightFrame">時間價值</a></span></td>
      <td>&nbsp;</td>
    </tr>
  </table>
  
  <hr align="center" width="90%" />
</form>
</body>
</html>
