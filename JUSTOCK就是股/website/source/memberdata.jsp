<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@ include file="Connections/justockdb.jsp" %>
<%
// *** Restrict Access To Page: Grant or deny access to this page
String MM_authorizedUsers="";
String MM_authFailedURL="/relogin.jsp";
boolean MM_grantAccess=false;
if (session.getValue("MM_Username") != null && !session.getValue("MM_Username").equals("")) {
  if (true || (session.getValue("MM_UserAuthorization")=="") || 
          (MM_authorizedUsers.indexOf((String)session.getValue("MM_UserAuthorization")) >=0)) {
    MM_grantAccess = true;
  }
}
if (!MM_grantAccess) {
  String MM_qsChar = "?";
  if (MM_authFailedURL.indexOf("?") >= 0) MM_qsChar = "&";
  String MM_referrer = request.getRequestURI();
  if (request.getQueryString() != null) MM_referrer = MM_referrer + "?" + request.getQueryString();
  MM_authFailedURL = MM_authFailedURL + MM_qsChar + "accessdenied=" + java.net.URLEncoder.encode(MM_referrer);
  response.sendRedirect(response.encodeRedirectURL(MM_authFailedURL));
  return;
}
%>
<%
String rsmemberd__MMColParam = "1";
if (session.getValue("Uid") !=null) {rsmemberd__MMColParam = (String)session.getValue("Uid");}
%>
<%
Driver Driverrsmemberd = (Driver)Class.forName(MM_justockdb_DRIVER).newInstance();
Connection Connrsmemberd = DriverManager.getConnection(MM_justockdb_STRING,MM_justockdb_USERNAME,MM_justockdb_PASSWORD);
PreparedStatement Statementrsmemberd = Connrsmemberd.prepareStatement("SELECT * FROM justock.account WHERE Uid = '" + rsmemberd__MMColParam + "'");
ResultSet rsmemberd = Statementrsmemberd.executeQuery();
boolean rsmemberd_isEmpty = !rsmemberd.next();
boolean rsmemberd_hasData = !rsmemberd_isEmpty;
Object rsmemberd_data;
int rsmemberd_numRows = 0;
%>
<% request.setCharacterEncoding("utf-8"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>會員資料</title>
<style type="text/css">
<!--
a:link {
	color: aba000;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: #ABA000;
}
a:hover {
	text-decoration: underline;
	color: #ABA000;
}
a:active {
	text-decoration: none;
	color: #ABA000;
}
.style12 {
	color: #CCCCCC;
	font-family: "新細明體";
}
.style14 {
	color: #FFFFFF;
	font-weight: bold;
	font-family: "新細明體";
	font-size: 16px;
}
.style16 {font-family: "新細明體"}
body {
	background-color: #e7f3ff;
}
.style19 {color: #000000; font-family: "新細明體"; font-size: 15px; }
-->
</style>

<script type="text/JavaScript">
<!--
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//-->
</script>
</head>

<body>
<form id="form1" name="form1" method="post" action="">
  <div align="center">
<table width="75%" align="center" bgcolor="#CCCCCC" border="0" cellpadding="4" cellspacing="1" >

	      <tr bgcolor="#ffffff">
        <td colspan="4" bgcolor="#CCCCCC" align="center"><span class="style14">會 員 資 料</span></td>
      <tr>
        <td width="30%" bgcolor="#FFFFCC"><div align="center" class="style19"><font class="mbody ">帳　　　號</font></div></td>
        <td width="70%" bgcolor="#FFFFFF"><span class="style16"><%=(((rsmemberd_data = rsmemberd.getObject("Uid"))==null || rsmemberd.wasNull())?"":rsmemberd_data)%></span></td>
      </tr>
      <tr>
        <td bgcolor="#FFFFCC"><div align="center" class="style19"><font class="mbody ">密　　　碼</font></div></td>
        <td bgcolor="#FFFFFF"><span class="style12">********</span></td>
      </tr>
      <tr>
        <td align="right" nowrap="nowrap" bgcolor="#FFFFCC"><div align="center" class="style19"><font class="mbody ">暱　　　稱</font></div></td>
        <td bgcolor="#FFFFFF"><span class="style16"><%=(((rsmemberd_data = rsmemberd.getObject("uName"))==null || rsmemberd.wasNull())?"":rsmemberd_data)%></span></td>
      </tr>
      <tr>
        <td align="right" bgcolor="#FFFFCC"><div align="center" class="style19"><font class="mbody ">身份證字號</font></div></td>
        <td bgcolor="#FFFFFF"><span class="style16"><%=(((rsmemberd_data = rsmemberd.getObject("uIdnum"))==null || rsmemberd.wasNull())?"":rsmemberd_data)%></span></td>
      </tr>
      <tr>
        <td align="right" bgcolor="#FFFFCC"><div align="center" class="style19"><font class="mbody ">性　　　別</font></div></td>
        <td valign="top" height="19" bgcolor="#FFFFFF"><span class="style16"><%=(((rsmemberd_data = rsmemberd.getObject("uGemder"))==null || rsmemberd.wasNull())?"":rsmemberd_data)%></span></td>
      </tr>
      <tr>
        <td height="19" align="right" bgcolor="#FFFFCC"><div align="center" class="style19">電　　　話</div></td>
        <td height="19" bgcolor="#FFFFFF"><span class="style16"><%=(((rsmemberd_data = rsmemberd.getObject("uPhone"))==null || rsmemberd.wasNull())?"":rsmemberd_data)%></span></td>
      </tr>
      <tr>
        <td align="right" bgcolor="#FFFFCC"><div align="center" class="style19"><font class="mbody ">地　　　址</font></div></td>
        <td bgcolor="#FFFFFF"><span class="style16"><%=(((rsmemberd_data = rsmemberd.getObject("uAddr"))==null || rsmemberd.wasNull())?"":rsmemberd_data)%></span></td>
      </tr>
      <tr>
        <td align="right" bgcolor="#FFFFCC"><div align="center" class="style19">E-mail 信箱</div></td>
        <td bgcolor="#FFFFFF"><span class="style16"><%=(((rsmemberd_data = rsmemberd.getObject("uEmail"))==null || rsmemberd.wasNull())?"":rsmemberd_data)%></span></td>
      </tr>


      <tr>
        <td align="center" bgcolor="#FFFFFF" colspan="2"><input name="submit" type="button" onclick="MM_goToURL('self','modify.jsp');return document.MM_returnValue" value=" 修 改 " /></td>
      </tr>
    </table>
  </div>
</form>





</body>
</html>
<%
rsmemberd.close();
Statementrsmemberd.close();
Connrsmemberd.close();
%>
