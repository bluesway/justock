<%@ page contentType="text/html; charset=big5" language="java" import="java.sql.*" errorPage="" %>
<%@ include file="Connections/test.jsp" %>
<%@ include file="/Connections/justockdb.jsp" %>
<jsp:useBean id="dateBean" class="common.util.DateUtil"/>

<jsp:useBean id="timeBean" class="common.util.TimeUtil"/>
<%
// *** Restrict Access To Page: Grant or deny access to this page
String MM_authorizedUsers="";
String MM_authFailedURL="/relogin.jsp";
boolean MM_grantAccess=false;
if (session.getValue("MM_Username") != null && !session.getValue("MM_Username").equals("")) {
  if (true || (session.getValue("MM_UserAuthorization")=="") || 
          (MM_authorizedUsers.indexOf((String)session.getValue("MM_UserAuthorization")) >=0)) {
    MM_grantAccess = true;
  }
}
if (!MM_grantAccess) {
  String MM_qsChar = "?";
  if (MM_authFailedURL.indexOf("?") >= 0) MM_qsChar = "&";
  String MM_referrer = request.getRequestURI();
  if (request.getQueryString() != null) MM_referrer = MM_referrer + "?" + request.getQueryString();
  MM_authFailedURL = MM_authFailedURL + MM_qsChar + "accessdenied=" + java.net.URLEncoder.encode(MM_referrer);
  response.sendRedirect(response.encodeRedirectURL(MM_authFailedURL));
  return;
}
%>
<%
Driver Driverholding = (Driver)Class.forName(MM_test_DRIVER).newInstance();
Connection Connholding = DriverManager.getConnection(MM_test_STRING,MM_test_USERNAME,MM_test_PASSWORD);
PreparedStatement Statementholding = Connholding.prepareStatement("SELECT * FROM justock.holding");
ResultSet holding = Statementholding.executeQuery();
boolean holding_isEmpty = !holding.next();
boolean holding_hasData = !holding_isEmpty;
Object holding_data;
int holding_numRows = 0;
%>
<%
String stock__MMColParam = "1";
if (session.getValue("Sid") !=null) {stock__MMColParam = (String)session.getValue("Sid");}
%>
<%
Driver Driverstock = (Driver)Class.forName(MM_justockdb_DRIVER).newInstance();
Connection Connstock = DriverManager.getConnection(MM_justockdb_STRING,MM_justockdb_USERNAME,MM_justockdb_PASSWORD);
PreparedStatement Statementstock = Connstock.prepareStatement("SELECT Sid, Sname  FROM justock.stock  WHERE Sid = '" + stock__MMColParam + "'");
ResultSet stock = Statementstock.executeQuery();
boolean stock_isEmpty = !stock.next();
boolean stock_hasData = !stock_isEmpty;
Object stock_data;
int stock_numRows = 0;
%>
<%
String traderecord_jsp__MMColParam = "1";
if (session.getValue("Uid") !=null) {traderecord_jsp__MMColParam = (String)session.getValue("Uid");}
%>
<%
Driver Drivertraderecord_jsp = (Driver)Class.forName(MM_justockdb_DRIVER).newInstance();
Connection Conntraderecord_jsp = DriverManager.getConnection(MM_justockdb_STRING,MM_justockdb_USERNAME,MM_justockdb_PASSWORD);
PreparedStatement Statementtraderecord_jsp = Conntraderecord_jsp.prepareStatement("SELECT * FROM justock.transaction WHERE tUid = '" + traderecord_jsp__MMColParam + "' ORDER BY tDate DESC,tTime DESC");
ResultSet traderecord_jsp = Statementtraderecord_jsp.executeQuery();
boolean traderecord_jsp_isEmpty = !traderecord_jsp.next();
boolean traderecord_jsp_hasData = !traderecord_jsp_isEmpty;
Object traderecord_jsp_data;
int traderecord_jsp_numRows = 0;

%>
<%
int Repeat1__numRows = 10;
int Repeat1__index = 0;
traderecord_jsp_numRows += Repeat1__numRows;
%>
<%
// *** Recordset Stats, Move To Record, and Go To Record: declare stats variables

int traderecord_jsp_first = 1;
int traderecord_jsp_last  = 1;
int traderecord_jsp_total = -1;

if (traderecord_jsp_isEmpty) {
  traderecord_jsp_total = traderecord_jsp_first = traderecord_jsp_last = 0;
}

//set the number of rows displayed on this page
if (traderecord_jsp_numRows == 0) {
  traderecord_jsp_numRows = 1;
}
%>
<% String MM_paramName = ""; %>
<%
// *** Move To Record and Go To Record: declare variables

ResultSet MM_rs = traderecord_jsp;
int       MM_rsCount = traderecord_jsp_total;
int       MM_size = traderecord_jsp_numRows;
String    MM_uniqueCol = "";
          MM_paramName = "";
int       MM_offset = 0;
boolean   MM_atTotal = false;
boolean   MM_paramIsDefined = (MM_paramName.length() != 0 && request.getParameter(MM_paramName) != null);
%>
<%
// *** Move To Record: handle 'index' or 'offset' parameter

if (!MM_paramIsDefined && MM_rsCount != 0) {

  //use index parameter if defined, otherwise use offset parameter
  String r = request.getParameter("index");
  if (r==null) r = request.getParameter("offset");
  if (r!=null) MM_offset = Integer.parseInt(r);

  // if we have a record count, check if we are past the end of the recordset
  if (MM_rsCount != -1) {
    if (MM_offset >= MM_rsCount || MM_offset == -1) {  // past end or move last
      if (MM_rsCount % MM_size != 0)    // last page not a full repeat region
        MM_offset = MM_rsCount - MM_rsCount % MM_size;
      else
        MM_offset = MM_rsCount - MM_size;
    }
  }

  //move the cursor to the selected record
  int i;
  for (i=0; traderecord_jsp_hasData && (i < MM_offset || MM_offset == -1); i++) {
    traderecord_jsp_hasData = MM_rs.next();
  }
  if (!traderecord_jsp_hasData) MM_offset = i;  // set MM_offset to the last possible record
}
%>
<%
// *** Move To Record: if we dont know the record count, check the display range

if (MM_rsCount == -1) {

  // walk to the end of the display range for this page
  int i;
  for (i=MM_offset; traderecord_jsp_hasData && (MM_size < 0 || i < MM_offset + MM_size); i++) {
    traderecord_jsp_hasData = MM_rs.next();
  }

  // if we walked off the end of the recordset, set MM_rsCount and MM_size
  if (!traderecord_jsp_hasData) {
    MM_rsCount = i;
    if (MM_size < 0 || MM_size > MM_rsCount) MM_size = MM_rsCount;
  }

  // if we walked off the end, set the offset based on page size
  if (!traderecord_jsp_hasData && !MM_paramIsDefined) {
    if (MM_offset > MM_rsCount - MM_size || MM_offset == -1) { //check if past end or last
      if (MM_rsCount % MM_size != 0)  //last page has less records than MM_size
        MM_offset = MM_rsCount - MM_rsCount % MM_size;
      else
        MM_offset = MM_rsCount - MM_size;
    }
  }

  // reset the cursor to the beginning
  traderecord_jsp.close();
  traderecord_jsp = Statementtraderecord_jsp.executeQuery();
  traderecord_jsp_hasData = traderecord_jsp.next();
  MM_rs = traderecord_jsp;

  // move the cursor to the selected record
  for (i=0; traderecord_jsp_hasData && i < MM_offset; i++) {
    traderecord_jsp_hasData = MM_rs.next();
  }
}
%>
<%
// *** Move To Record: update recordset stats

// set the first and last displayed record
traderecord_jsp_first = MM_offset + 1;
traderecord_jsp_last  = MM_offset + MM_size;
if (MM_rsCount != -1) {
  traderecord_jsp_first = Math.min(traderecord_jsp_first, MM_rsCount);
  traderecord_jsp_last  = Math.min(traderecord_jsp_last, MM_rsCount);
}

// set the boolean used by hide region to check if we are on the last record
MM_atTotal  = (MM_rsCount != -1 && MM_offset + MM_size >= MM_rsCount);
%>
<%
// *** Go To Record and Move To Record: create strings for maintaining URL and Form parameters

String MM_keepBoth,MM_keepURL="",MM_keepForm="",MM_keepNone="";
String[] MM_removeList = { "index", MM_paramName };

// create the MM_keepURL string
if (request.getQueryString() != null) {
  MM_keepURL = '&' + request.getQueryString();
  for (int i=0; i < MM_removeList.length && MM_removeList[i].length() != 0; i++) {
  int start = MM_keepURL.indexOf(MM_removeList[i]) - 1;
    if (start >= 0 && MM_keepURL.charAt(start) == '&' &&
        MM_keepURL.charAt(start + MM_removeList[i].length() + 1) == '=') {
      int stop = MM_keepURL.indexOf('&', start + 1);
      if (stop == -1) stop = MM_keepURL.length();
      MM_keepURL = MM_keepURL.substring(0,start) + MM_keepURL.substring(stop);
    }
  }
}

// add the Form variables to the MM_keepForm string
if (request.getParameterNames().hasMoreElements()) {
  java.util.Enumeration items = request.getParameterNames();
  while (items.hasMoreElements()) {
    String nextItem = (String)items.nextElement();
    boolean found = false;
    for (int i=0; !found && i < MM_removeList.length; i++) {
      if (MM_removeList[i].equals(nextItem)) found = true;
    }
    if (!found && MM_keepURL.indexOf('&' + nextItem + '=') == -1) {
      MM_keepForm = MM_keepForm + '&' + nextItem + '=' + java.net.URLEncoder.encode(request.getParameter(nextItem));
    }
  }
}

String tempStr = "";
for (int i=0; i < MM_keepURL.length(); i++) {
  if (MM_keepURL.charAt(i) == '<') tempStr = tempStr + "&lt;";
  else if (MM_keepURL.charAt(i) == '>') tempStr = tempStr + "&gt;";
  else if (MM_keepURL.charAt(i) == '"') tempStr = tempStr +  "&quot;";
  else tempStr = tempStr + MM_keepURL.charAt(i);
}
MM_keepURL = tempStr;

tempStr = "";
for (int i=0; i < MM_keepForm.length(); i++) {
  if (MM_keepForm.charAt(i) == '<') tempStr = tempStr + "&lt;";
  else if (MM_keepForm.charAt(i) == '>') tempStr = tempStr + "&gt;";
  else if (MM_keepForm.charAt(i) == '"') tempStr = tempStr +  "&quot;";
  else tempStr = tempStr + MM_keepForm.charAt(i);
}
MM_keepForm = tempStr;

// create the Form + URL string and remove the intial '&' from each of the strings
MM_keepBoth = MM_keepURL + MM_keepForm;
if (MM_keepBoth.length() > 0) MM_keepBoth = MM_keepBoth.substring(1);
if (MM_keepURL.length() > 0)  MM_keepURL = MM_keepURL.substring(1);
if (MM_keepForm.length() > 0) MM_keepForm = MM_keepForm.substring(1);
%>
<%
// *** Move To Record: set the strings for the first, last, next, and previous links

String MM_moveFirst,MM_moveLast,MM_moveNext,MM_movePrev;
{
  String MM_keepMove = MM_keepBoth;  // keep both Form and URL parameters for moves
  String MM_moveParam = "index=";

  // if the page has a repeated region, remove 'offset' from the maintained parameters
  if (MM_size > 1) {
    MM_moveParam = "offset=";
    int start = MM_keepMove.indexOf(MM_moveParam);
    if (start != -1 && (start == 0 || MM_keepMove.charAt(start-1) == '&')) {
      int stop = MM_keepMove.indexOf('&', start);
      if (start == 0 && stop != -1) stop++;
      if (stop == -1) stop = MM_keepMove.length();
      if (start > 0) start--;
      MM_keepMove = MM_keepMove.substring(0,start) + MM_keepMove.substring(stop);
    }
  }

  // set the strings for the move to links
  StringBuffer urlStr = new StringBuffer(request.getRequestURI()).append('?').append(MM_keepMove);
  if (MM_keepMove.length() > 0) urlStr.append('&');
  urlStr.append(MM_moveParam);
  MM_moveFirst = urlStr + "0";
  MM_moveLast  = urlStr + "-1";
  MM_moveNext  = urlStr + Integer.toString(MM_offset+MM_size);
  MM_movePrev  = urlStr + Integer.toString(Math.max(MM_offset-MM_size,0));
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5" />
<meta http-equiv="Pragma" content="no-cache" />
<title>交易紀錄一覽</title>
<style type="text/css">
<!--
a:link {
	color: 000066;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: 000066;
}
a:hover {
	text-decoration: none;
	color: 000066;
}
a:active {
	text-decoration: none;
	color: 000066;
}
.style4 {color: #FF3300}
.style5 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 16px;
}
.style6 {
	color: #FF0000;
	font-size: 14px;
}
.style7 {color: #000066}
.style8 {
	font-size: 14px;
	color: #cccccc;
}
.style9 {font-size: 14px;color: #000066;}
body {
	background-color: #e7f3ff;
}
.style10 {font-size: 15px}
.style11 {font-size: 15px; color: #000066; }
.style12 {color: #000000}
.style13 {font-size: 15px; color: #000000; }
-->
</style>
</head>

<body>
<%/**
	Connection con=null;
	Statement stmt1=null;
	ResultSet rs1=null;

Class.forName("com.mysql.jdbc.Driver").newInstance();
			con =DriverManager.getConnection(		 "jdbc:mysql://localhost:3306/justock?user=XDDDDD&password=specification&useUnicode=true&characterEncoding=Big5") ;
			stmt1=con.createStatement (ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			
			String query="SELECT * FROM transaction WHERE tUid='XDDDDD'  ORDER by tid ASC";
			rs1=stmt1.executeQuery(query);
	rs1.next();
	String new_tDate=rs1.getString("tDate");
	out.println(new_tDate);
**/
%>

<div>
  <table width="85%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
    <tbody>
      <tr bgcolor="#CCCCCC">
        <td colspan="7" bgcolor="#CCCCCC"><div align="center" class="style5">交 易 記 錄 一 覽</div></td>
      </tr>
      <tr>
        <td bgcolor="#FFFFCC" width="18%"><a href="/traderecord.jsp">
        <p align="center" class="style11">日期(排序)</p>
        </a></td>
        <td width="15%" align="center" bgcolor="#FFFFCC" class="style11 style12">時間</td>
        <td width="14%" bgcolor="#FFFFCC" class="style10"><p align="center" class="style13">股票代碼</p></td>
        <td width="11%" bgcolor="#FFFFCC" class="style10"><p align="center" class="style13">價位</p></td>
        <td width="11%" bgcolor="#FFFFCC" class="style10"><p align="center" class="style13">交易量</p></td>
        <td width="17%" bgcolor="#FFFFCC" class="style10"><p align="center" class="style13">記錄訊息</p></td>
        <td width="14%" bgcolor="#FFFFCC" class="style11"><div align="center" class="style12">交易方式</div></td>
      </tr>
      <tr bgcolor="#FFFFFF">
        <% while ((traderecord_jsp_hasData)&&(Repeat1__numRows-- != 0)) { %>
        <td align="center" bgcolor="#FFFFFF">
		<% 
		String gdate=traderecord_jsp.getString("tDate");
		int in_gdate=Integer.parseInt(gdate);
		out.println(dateBean.transDate(in_gdate));
		%></td>
        <td align="center" bgcolor="#FFFFFF"><%
		String gtime=traderecord_jsp.getString("tTime");
		int in_gtime=Integer.parseInt(gtime);
		out.println(timeBean.transTime(in_gtime));
		%></td>
        <td align="center" bgcolor="#FFFFFF"><%=(((traderecord_jsp_data = traderecord_jsp.getObject("tSid"))==null || traderecord_jsp.wasNull())?"":traderecord_jsp_data)%></td>
        <td align="center" bgcolor="#FFFFFF"><%
	  String get2_tPrice=traderecord_jsp.getString("tPrice");
		double dou_get2_tPrice=Double.valueOf(get2_tPrice);
		if(dou_get2_tPrice>=0)
		{out.println(dou_get2_tPrice);}
		else
		{double a =dou_get2_tPrice*(-1);
		out.println(a);
		}
	%>        </td>
        <td align="center" bgcolor="#FFFFFF"><%=(((traderecord_jsp_data = traderecord_jsp.getObject("tQuan"))==null || traderecord_jsp.wasNull())?"":traderecord_jsp_data)%></td>
        <td align="center" bgcolor="#FFFFFF"><% 
	  
	  String get_tStatus=traderecord_jsp.getString("tStatus");
		double dou_get_tStatus=Double.valueOf(get_tStatus);
		if(dou_get_tStatus==(-1))
		{
		out.println("下單成功");
		}
		else if(dou_get_tStatus==(0))
		{
		out.println("交易成功");
		}
		else if(dou_get_tStatus==(1))
		{
		out.println("帳戶餘額不足");
		}
		else if(dou_get_tStatus==(2))
		{
		out.println("已失效");
		}
		else if(dou_get_tStatus==(3))
		{
		out.println("限價超出範圍");
		}
		else if(dou_get_tStatus==(4))
		{
		out.println("持有餘股不足");
		}
		else if(dou_get_tStatus==(5))
		{
		out.println("權限不足");
		}
		else if(dou_get_tStatus==(6))
		{
		out.println("權限不足");
		}
		else if(dou_get_tStatus==(127))
		{
		out.println("交割完成");
		}
		%></td>
        <td align="center" bgcolor="#FFFFFF"><%
	  String get_tPrice=traderecord_jsp.getString("tPrice");
		double dou_get_tPrice=Double.valueOf(get_tPrice);
		if(dou_get_tPrice >=0)
		{out.println("買");	}
		else
		{out.println("賣");}
	  %></td>
      </tr>
    </tbody>
    <%
  Repeat1__index++;
  traderecord_jsp_hasData = traderecord_jsp.next();
}
%>
  </table>
  <% if (traderecord_jsp_isEmpty ) { %>
    <div align="center"><span class="style6"><br />
    您還沒有交易記錄</span></div>
    <% } /* end traderecord_jsp_isEmpty */ %>
  <p align="center"><A HREF="<%=MM_moveFirst%>"><span class="style9">第一頁</span></A> <A HREF="<%=MM_movePrev%>"><span class="style9">上一頁</span></A> <A HREF="<%=MM_moveNext%>"><span class="style9">下一頁</span></A> <A HREF="<%=MM_moveLast%>"><span class="style9">最末頁</span></A></p>
</div>
<form action="" method="post" name="form1" class="style4" id="form1">
  
  <div align="left" class="style7">
    <div align="center" class="style8">
      <% if (MM_offset !=0) { %>
        <% if (MM_atTotal) { %>最末頁<% } /* end MM_atTotal */ %>
        <% } /* end MM_offset != 0 */ %>
    </div>
  </div>
</form>

<div align="center">
  <% if (MM_offset == 0) { %>
    <% if (!MM_atTotal) { %>
</div>
    <form action="" method="post" name="form2" class="style7" id="form2">
      <div align="center" class="style8">第一頁</div>
    </form>
    <div align="center">
      <% } /* end !MM_atTotal */ %>
    <% } /* end MM_offset == 0 */ %>
</div>
</body>
</html>
<%
traderecord_jsp.close();
Statementtraderecord_jsp.close();
Conntraderecord_jsp.close();
%>
