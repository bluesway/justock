<%@ page contentType="text/html; charset=big5" language="java" import="java.sql.*" errorPage="" %>
<%@ include file="/Connections/justockdb.jsp" %>
<jsp:useBean id="timeBean" class="common.util.TimeUtil"/>
<%
String stock__MMColParam = "1";

if (request.getParameter("stockId") !=null) {stock__MMColParam = (String)request.getParameter("stockId");}
%>
<%
Driver Driverstock = (Driver)Class.forName(MM_justockdb_DRIVER).newInstance();
Connection Connstock = DriverManager.getConnection(MM_justockdb_STRING,MM_justockdb_USERNAME,MM_justockdb_PASSWORD);
PreparedStatement Statementstock = Connstock.prepareStatement("SELECT Sid, Sname FROM justock.stock WHERE Sid = '" + stock__MMColParam + "'");
ResultSet stock = Statementstock.executeQuery();
boolean stock_isEmpty = !stock.next();
boolean stock_hasData = !stock_isEmpty;
Object stock_data;
int stock_numRows = 0;
%>
<%
String today__MMColParam = "1";
session.putValue("stockId",request.getParameter("stockId"));
if (session.getValue("stockId") !=null) {today__MMColParam = (String)session.getValue("stockId");}
%>
<%
Driver Drivertoday = (Driver)Class.forName(MM_justockdb_DRIVER).newInstance();
Connection Conntoday = DriverManager.getConnection(MM_justockdb_STRING,MM_justockdb_USERNAME,MM_justockdb_PASSWORD);
PreparedStatement Statementtoday = Conntoday.prepareStatement("SELECT tSid, tTime, tChg, tPrice, tQuan, tBPrice, tSPrice  FROM justock.info_today  WHERE tSid LIKE '" + today__MMColParam + "'  ORDER BY tTime DESC");
ResultSet today = Statementtoday.executeQuery();
boolean today_isEmpty = !today.next();
boolean today_hasData = !today_isEmpty;
Object today_data;
int today_numRows = 0;
	
%>
<%
int Repeat1__numRows = 50;
int Repeat1__index = 0;
today_numRows += Repeat1__numRows;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5" />
<title>個股成交明細</title>
<style type="text/css">
<!--
.style1 {color: #FFFFFF}
.style3 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 16px;
}
.style6 {font-size: 13px}
.style9 {color: #FFFFFF; font-size: 13px; }
.style10 {color: #181063}
-->
</style>
</head>

<body>
<div><p>&nbsp;</p>


    <table width="80%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
      <tbody>
        <tr bgcolor="#CCCCCC">
          <td colspan="2" class="style1">資料日期：</td>
          <td align="center" colspan="2"> <span class="style3"><span class="style10"><%=(((stock_data = stock.getObject("Sid"))==null || stock.wasNull())?"":stock_data)%> <%=(((stock_data = stock.getObject("Sname"))==null || stock.wasNull())?"":stock_data)%></span>　成 交 明 細</span></td>
          <td align="right" colspan="2" width="180"><div align="right"><span class="style9"><a href="/quire_deallist50.jsp">列出前十筆成交明細</a></span></div></td>
        </tr>
        
        <tr align="center" bgcolor="#FFFFCC">
          <td align="center"><span class="style6">時間</span></td>
          <td align="center"><span class="style6">成交價</span></td>
          <td align="center"><span class="style6">買價</span></td>
          <td align="center"><span class="style6">賣價</span></td>
          <td align="center"><span class="style6">漲跌</span></td>
          <td align="center"><span class="style6">單量 (張)</span></td>
        </tr>
			  <% while ((today_hasData)&&(Repeat1__numRows-- != 0)) { 
			  int teddy = Integer.parseInt(String.valueOf(((today_data = today.getObject("tTime"))==null || today.wasNull())?"":today_data));%>
        <tr align="center" bgcolor="#ffffff" height="25">
          <td><%=timeBean.transTime(teddy)%></td>
          <td><%=(((today_data = today.getObject("tPrice"))==null || today.wasNull())?"":today_data)%></td>
          <td><%=(((today_data = today.getObject("tBPrice"))==null || today.wasNull())?"":today_data)%></td>
          <td><%=(((today_data = today.getObject("tSPrice"))==null || today.wasNull())?"":today_data)%></td>
          <td><%=(((today_data = today.getObject("tChg"))==null || today.wasNull())?"":today_data)%></td>
          <td><%=(((today_data = today.getObject("tQuan"))==null || today.wasNull())?"":today_data)%></td>
    </tr>    <%
  Repeat1__index++;
  today_hasData = today.next();
}
%>
      </tbody>
    </table>

<p>&nbsp;</p>
</div>


</body>
</html>
<%
stock.close();
Statementstock.close();
Connstock.close();
%>
<%
today.close();
Statementtoday.close();
Conntoday.close();
%>