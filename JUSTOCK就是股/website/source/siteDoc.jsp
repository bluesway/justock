<%@ page contentType="text/html; charset=big5" language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5" />
<title>網頁操作說明</title>
<style type="text/css">
<!--
a:link {
	color: #999999;
	text-decoration: none;
}
a:visited {
	color: #999999;
	text-decoration: none;
}
a:hover {
	color: #999999;
	text-decoration: underline;
}
a:active {
	color: #999999;
	text-decoration: none;
}
.style3 {color: #0000FF}
.style10 {color: #999999; font-size: 13px; }
.style13 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 16px;
}
body {
	background-color: #e7f3ff;
}
.style14 {font-size: 15px}
.style17 {font-size: 14px}
-->
</style>
</head>

<body>
<p align="center"><span class="style10"><a name="t" id="t"></a><a href="#1">登入</a> ｜ <a href="#2">頁面說明</a> ｜ <a href="#3">大盤</a> ｜ <a href="#4">個股查詢</a> ｜ <a href="#5">盤後資訊</a> ｜ <a href="#6">交易</a> ｜ <a href="#7">會員資料</a></span></p>
<table width="80%" border="0" align="center" cellpadding="4" cellspacing="1" bgcolor="#CCCCCC">
  <tbody>
    <tr bgcolor="#CCCCCC">
            <td colspan="6" align="center"><span class="style13">網 頁 操 作 說 明</span></td>
    </tr>
    <tr align="center" bgcolor="#FFFFCC">
      <td colspan="6" align="center"><span class="style14">登入</span><a name="1" id="1"></a></td>
    </tr>
<tr bgcolor="#FFFFFF">
<td colspan="6" align="center"><p><span class="style17">開啟瀏覽器，輸入網址<span class="style3"> http://XDDDDD.twbbs.org/</span></span><br />
    <img src="/images/siteDoc - login.jpg" width="500" height="372" /><br />
    <br />
        <span class="style17">第一次登入請先點選「加入會員」，填寫下列資料申請會員。</span><br />
        <img src="/images/siteDoc - modify.jpg" width="248" height="155" /><br />
        <span class="style17">成功申請會員之後，用帳號密碼重新登入。</span></p>
  <p align="right"><a href="#t">top</a><br />
      <br />
  </p></td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center" bgcolor="#FFFFCE"><a name="2" id="2"></a><span class="style14">頁面說明</span></td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center"><p><span class="style17">登入後即顯示首頁如下：</span><strong><br />
        <img src="/images/siteDoc - TSE.jpg" width="500" height="372" /></strong></p>
    <table width="80%" border="0" cellspacing="3" cellpadding="1">
      <tr>
        <td><span class="style17">A. 本系統logo圖示：為<u>首頁</u>連結</span></td>
      </tr>
      <tr>
        <td><span class="style17">B. 各項功能選單：分為大盤行情、交易專區、會員專區以及使用說明。點選最下方之<u>聯絡系統管理員</u>可寄發e-mail向本系統管理者做問題的回報。 </span></td>
      </tr>
      <tr>
        <td><span class="style17">C. 個股查詢選單：輸入股票代碼或名稱，即可選擇即時行情、盤後分析之個股查詢。若不清楚股票代碼或股票名稱，可點選後方<u>股票代碼查詢</u>的連結來搜尋。 </span></td>
      </tr>
      <tr>
        <td><span class="style17">D. 現在時間顯示 </span></td>
      </tr>
      <tr>
        <td><span class="style17">E. 使用者暱稱與登入狀況顯示 </span></td>
      </tr>
      <tr>
        <td><span class="style17">F. 登出 </span></td>
      </tr>
    </table>
    <br />
    
    <p align="right"><a href="#t">top</a></p></td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center" bgcolor="#FFFFCE"><a name="3" id="3"></a><span class="style14">大盤</span></td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center"><p><span class="style17">大盤分為「集中市場大盤」以及「上櫃股票大盤」<strong><br />
    </strong>（集中市場：台灣證券交易為提供有價證券之競價買賣所開設之市場，稱之。）<br />
        （上櫃股票：由中華民國證券櫃檯買賣中心所開設之「櫃檯市場」。）<br />
        <br />
        下圖顯示交易日當天之大盤走勢，圖下的各數值表交易詳細資料，分別有櫃檯指數、最高價最低價、成交金額張數筆數、委買張數筆數以及委賣張數筆數。</span><br />
    <img src="/images/siteDoc - OTC.jpg" width="309" height="228" /></p>
    <p align="right"><a href="#t">top</a></p></td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center" bgcolor="#FFFFCE"><a name="4" id="4"></a><span class="style14">個股查詢</span></td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center"><p><span class="style17">頁面上方之個股查詢下拉選單處，輸入股票代碼即可查詢個股當日股價、個股當日走勢圖、個股當日成交明細、公司基本資料、技術分析的籌碼分析與Ｋ線圖。<br />
    若不知道股票代碼，可點選右邊「股票代碼查詢」進行查詢：<br />
    查詢方式分為兩種，一為關鍵字輸入，二為類股的選擇。</span><br />  
        <img src="/images/siteDoc - stockid.jpg" width="500" height="372" /><br />
        <strong><br />
        </strong><span class="style17">當日個股股價：顯示當日各股即時交易資訊</span><br />
        <img src="/images/siteDoc - price.jpg" width="295" height="48" /><br />
        <br />
        <span class="style17">當日個股走勢：顯示當日個股即時走圖線圖</span><br />
        <img src="/images/siteDoc - diagram.jpg" width="500" height="372" /><br />
        <br />
        <span class="style17">個股成交明細：顯示當日個股各個時間的成交資訊</span><strong><br />
        </strong><img src="/images/siteDoc - dealist.jpg" width="500" height="372" /><br />
        <br />
        <span class="style17">公司基本資料：分三部分，公司資料、股利狀況與營收盈餘表，供使用者做基本分析用</span><br />
        <img src="/images/siteDoc - company.jpg" width="295" height="133" /><br />
        <img src="/images/siteDoc - company2.jpg" width="295" height="207" /><br />
        <img src="/images/siteDoc - company3.jpg" width="295" height="180" /><br />
        <strong><br />
        </strong><span class="style17">個股籌碼分析：提供個股當日主力進出資訊</span><br />
        <img src="/images/siteDoc - analys.jpg" width="500" height="372" /><br />
        <br />
      </p>
    <p align="right"><a href="#t">top</a></p></td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center" bgcolor="#FFFFCE"><a name="5" id="5"></a><span class="style14">盤後資訊</span></td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center"><p><span class="style17">歷史股價Ｋ線圖：以歷史股價資料繪成之Ｋ線圖，分為日線周線月線，供使用者做技術分析參考</span><br />
      <img src="/images/siteDoc - kline.jpg" width="500" height="372" /></p>
    <p align="right"><a href="#t">top</a></p></td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center" bgcolor="#FFFFCE"><a name="6" id="6"></a><span class="style14">交易</span></td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center"><p><span class="style17">點選左方交易專區的「下單」開始進行股票買賣。</span><br />
      <img src="/images/siteDoc - purchase.jpg" width="500" height="372" /><br />
          <span class="style17">輸入欲買進或賣出之股票代號、下單價位、張數以及希望交割之期限，最後按下確定送出。<br />
            <br />
              投資組合：目前使用者所持有之所有股票資訊。</span><br />
              <img src="/images/siteDoc - holding.jpg" width="228" height="147" /><br />
              <br />
              <span class="style17">交易記錄一覽：記錄使用者到目前為止之所有交易記錄，包含交易成功與失敗的記錄。</span><br />
          <img src="/images/siteDoc - traderecord.jpg" width="500" height="372" /><br />
          </p>
    <p align="right"><a href="#t">top</a></p></td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center" bgcolor="#FFFFCE"><a name="7" id="7"></a><span class="style14">會員資料</span></td>
</tr>
<tr bgcolor="#FFFFFF">
  <td colspan="6" align="center"><p><span class="style17">點選左方會員專區的「個人資料修改」可修改使用者資料。</span><strong><br />
        <img src="/images/siteDoc - modify.jpg" width="248" height="155" /></strong></p>
    <p align="right"><a href="#t">top</a></p></td>
</tr>
  </tbody>
</table>
</body>
</html>
