<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@ include file="Connections/justockdb.jsp" %>
<% request.setCharacterEncoding("utf-8"); %>
<%
// *** Edit Operations: declare variables

// set the form action variable
String MM_editAction = request.getRequestURI();
if (request.getQueryString() != null && request.getQueryString().length() > 0) {
  String queryString = request.getQueryString();
  String tempStr = "";
  for (int i=0; i < queryString.length(); i++) {
    if (queryString.charAt(i) == '<') tempStr = tempStr + "&lt;";
    else if (queryString.charAt(i) == '>') tempStr = tempStr + "&gt;";
    else if (queryString.charAt(i) == '"') tempStr = tempStr +  "&quot;";
    else tempStr = tempStr + queryString.charAt(i);
  }
  MM_editAction += "?" + tempStr;
}

// connection information
String MM_editDriver = null, MM_editConnection = null, MM_editUserName = null, MM_editPassword = null;

// redirect information
String MM_editRedirectUrl = null;

// query string to execute
StringBuffer MM_editQuery = null;

// boolean to abort record edit
boolean MM_abortEdit = false;

// table information
String MM_editTable = null, MM_editColumn = null, MM_recordId = null;

// form field information
String[] MM_fields = null, MM_columns = null;
%>
<%
// *** Update Record: set variables

if (request.getParameter("MM_update") != null &&
    request.getParameter("MM_update").toString().equals("form1") &&
    request.getParameter("MM_recordId") != null) {

  MM_editDriver     = MM_justockdb_DRIVER;
  MM_editConnection = MM_justockdb_STRING;
  MM_editUserName   = MM_justockdb_USERNAME;
  MM_editPassword   = MM_justockdb_PASSWORD;
  MM_editTable  = "justock.account";
  MM_editColumn = "Uid";
  MM_recordId   = "'" + request.getParameter("MM_recordId") + "'";
  MM_editRedirectUrl = "modifydone.jsp";
  String MM_fieldsStr = "uName|value|uGender|value|uPhone|value|uAddr|value|uEmail|value";
  String MM_columnsStr = "uName|',none,''|uGemder|none,none,NULL|uPhone|',none,''|uAddr|',none,''|uEmail|',none,''";

  // create the MM_fields and MM_columns arrays
  java.util.StringTokenizer tokens = new java.util.StringTokenizer(MM_fieldsStr,"|");
  MM_fields = new String[tokens.countTokens()];
  for (int i=0; tokens.hasMoreTokens(); i++) MM_fields[i] = tokens.nextToken();

  tokens = new java.util.StringTokenizer(MM_columnsStr,"|");
  MM_columns = new String[tokens.countTokens()];
  for (int i=0; tokens.hasMoreTokens(); i++) MM_columns[i] = tokens.nextToken();

  // set the form values
  for (int i=0; i+1 < MM_fields.length; i+=2) {
    MM_fields[i+1] = ((request.getParameter(MM_fields[i])!=null)?(String)request.getParameter(MM_fields[i]):"");
  }

  // append the query string to the redirect URL
  if (MM_editRedirectUrl.length() != 0 && request.getQueryString() != null) {
    MM_editRedirectUrl += ((MM_editRedirectUrl.indexOf('?') == -1)?"?":"&") + request.getQueryString();
  }
}
%>
<%
// *** Update Record: construct a sql update statement and execute it

if (request.getParameter("MM_update") != null &&
    request.getParameter("MM_recordId") != null) {

  // create the update sql statement
  MM_editQuery = new StringBuffer("update ").append(MM_editTable).append(" set ");
  for (int i=0; i+1 < MM_fields.length; i+=2) {
    String formVal = MM_fields[i+1];
    String elem;
    java.util.StringTokenizer tokens = new java.util.StringTokenizer(MM_columns[i+1],",");
    String delim    = ((elem = (String)tokens.nextToken()) != null && elem.compareTo("none")!=0)?elem:"";
    String altVal   = ((elem = (String)tokens.nextToken()) != null && elem.compareTo("none")!=0)?elem:"";
    String emptyVal = ((elem = (String)tokens.nextToken()) != null && elem.compareTo("none")!=0)?elem:"";
    if (formVal.length() == 0) {
      formVal = emptyVal;
    } else {
      if (altVal.length() != 0) {
        formVal = altVal;
      } else if (delim.compareTo("'") == 0) {  // escape quotes
        StringBuffer escQuotes = new StringBuffer(formVal);
        for (int j=0; j < escQuotes.length(); j++)
          if (escQuotes.charAt(j) == '\'') escQuotes.insert(j++,'\'');
        formVal = "'" + escQuotes + "'";
      } else {
        formVal = delim + formVal + delim;
      }
    }
    MM_editQuery.append((i!=0)?",":"").append(MM_columns[i]).append(" = ").append(formVal);
  }
  MM_editQuery.append(" where ").append(MM_editColumn).append(" = ").append(MM_recordId);
  
  if (!MM_abortEdit) {
    // finish the sql and execute it
    Driver MM_driver = (Driver)Class.forName(MM_editDriver).newInstance();
    Connection MM_connection = DriverManager.getConnection(MM_editConnection,MM_editUserName,MM_editPassword);
    PreparedStatement MM_editStatement = MM_connection.prepareStatement(MM_editQuery.toString());
    MM_editStatement.executeUpdate();
    MM_connection.close();

    // redirect with URL parameters
    if (MM_editRedirectUrl.length() != 0) {
      response.sendRedirect(response.encodeRedirectURL(MM_editRedirectUrl));
      return;
    }
  }
}
%>
<%
String rsmember__MMColParam = "1";
if (session.getValue("Uid") !=null) {rsmember__MMColParam = (String)session.getValue("Uid");}
%>
<%
Driver Driverrsmember = (Driver)Class.forName(MM_justockdb_DRIVER).newInstance();
Connection Connrsmember = DriverManager.getConnection(MM_justockdb_STRING,MM_justockdb_USERNAME,MM_justockdb_PASSWORD);
PreparedStatement Statementrsmember = Connrsmember.prepareStatement("SELECT * FROM justock.account WHERE Uid = '" + rsmember__MMColParam + "'");
ResultSet rsmember = Statementrsmember.executeQuery();
boolean rsmember_isEmpty = !rsmember.next();
boolean rsmember_hasData = !rsmember_isEmpty;
Object rsmember_data;
int rsmember_numRows = 0;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>資料修改</title>
<style type="text/css">
<!--
.style2 {color: #353842}
.style11 {color: #000000; }
a:link {
	color: #ABA000;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: #ABA000;
}
a:hover {
	text-decoration: underline;
	color: #ABA000;
}
a:active {
	text-decoration: none;
	color: #ABA000;
}
.style13 {
	color: #FFFFFF;
	font-size: 16px;
	font-family: "新細明體";
}
.style14 {color: #000000; font-family: "新細明體"; }
.style15 {font-family: "新細明體"}
.style16 {font-size: 15px}
.style17 {color: #000000; font-family: "新細明體"; font-size: 15px; }
.style18 {font-family: "新細明體"; font-size: 15px; }
body {
	background-color: #e7f3ff;
}

-->
</style>

<script type="text/JavaScript">
<!--
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//-->
</script>
</head>

<body>
<form ACTION="<%=MM_editAction%>" id="form1" name="form1" method="POST">
<table width="75%" align="center" bgcolor="#CCCCCC" border="0" cellpadding="4" cellspacing="1" >

	      <tr bgcolor="#ffffff">
        <td colspan="4" bgcolor="#CCCCCC" align="center"><span class="style10 style13"><strong>會 員 資 料</strong></span></td>
     <tr>
       <td width="30%" bgcolor="#FFFFCC"><div align="center" class="style14 style16"><font class="mbody ">帳　　　號</font></div></td>
      <td bgcolor="#FFFFFF"><span class="style15"><%=(((rsmember_data = rsmember.getObject("Uid"))==null || rsmember.wasNull())?"":rsmember_data)%></span></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFCC"><div align="center" class="style17"><font class="mbody ">密　　　碼</font></div></td>
      <td bgcolor="#FFFFFF"><input name="Submit" type="submit" onclick="MM_goToURL('self','/modifypw.jsp');return document.MM_returnValue" value=" 修改密碼 " /></td>
    </tr>
    <tr>
      <td align="right" nowrap="nowrap" bgcolor="#FFFFCC"><div align="center" class="style18"><font class="mbody style11"> 暱　　　稱</font></div></td>
      <td bgcolor="#FFFFFF"><input name="uName" type="text" id="uName" value="<%=(((rsmember_data = rsmember.getObject("uName"))==null || rsmember.wasNull())?"":rsmember_data)%>" size="20" /></td>
    </tr>
    <tr>
      <td align="right" bgcolor="#FFFFCC"><div align="center" class="style18"><font class="mbody style11">身份證字號</font></div></td>
      <td bgcolor="#FFFFFF" class="style15"><%=(((rsmember_data = rsmember.getObject("uIdnum"))==null || rsmember.wasNull())?"":rsmember_data)%></td>
    </tr>
    <tr>
      <td align="right" bgcolor="#FFFFCC"><div align="center" class="style18"><font class="mbody style11"> 性　　　別</font></div></td>
      <td valign="top" bgcolor="#FFFFFF">
        <span class="style15">
        <input <%=(((((rsmember_data = rsmember.getObject("uGemder"))==null || rsmember.wasNull())?"":rsmember_data).toString().equals(("男性").toString()))?"checked=\"checked\"":"")%> value="1" name="uGender" type="radio" />
        男
        <input <%=(((((rsmember_data = rsmember.getObject("uGemder"))==null || rsmember.wasNull())?"":rsmember_data).toString().equals(("女性").toString()))?"checked=\"checked\"":"")%> name="uGender" type="radio" value="2" />
        女</span></td>
    </tr>
    <tr>
      <td align="right" bgcolor="#FFFFCC"><div align="center" class="style18"><span class="style11">電　　　話</span></div></td>
      <td bgcolor="#FFFFFF"><span class="style15"><font color="#2C8383">
        <input name="uPhone" type="text" id="uPhone" value="<%=(((rsmember_data = rsmember.getObject("uPhone"))==null || rsmember.wasNull())?"":rsmember_data)%>" size="15" maxlength="15" />
      </font></span></td>
    </tr>
    <tr>
      <td align="right" bgcolor="#FFFFCC"><div align="center" class="style18"><font class="mbody style11">地　　　址</font></div></td>
      <td bgcolor="#FFFFFF"><input name="uAddr" type="text" id="uAddr" value="<%=(((rsmember_data = rsmember.getObject("uAddr"))==null || rsmember.wasNull())?"":rsmember_data)%>" size="40" maxlength="150" /></td>
    </tr>
    <tr>
      <td align="right" bgcolor="#FFFFCC"><div align="center" class="style18"><span class="style11"> E-mail 信箱 </span></div></td>
      <td bgcolor="#FFFFFF"><span class="style15"><font color="#2C8383">
        <input name="uEmail" type="text" id="uEmail" value="<%=(((rsmember_data = rsmember.getObject("uEmail"))==null || rsmember.wasNull())?"":rsmember_data)%>" size="35" />
      </font></span></td>
    </tr>

    <tr>
      <td align="center" bgcolor="#FFFFFF" colspan="2"><input name="submit" type="submit" value=" 完 成 " />
      <input name="cancel" type="button" id="cancel" onclick="MM_goToURL('self','memberdata.jsp');return document.MM_returnValue" value=" 取 消 " /></td>
    </tr>
  </table>
  

  <input type="hidden" name="MM_update" value="form1">
  <input type="hidden" name="MM_recordId" value="<%=(((rsmember_data = rsmember.getObject("Uid"))==null || rsmember.wasNull())?"":rsmember_data)%>">
</form>
<p class="style2">&nbsp;</p>
<p class="style2">&nbsp; 	</p>
</body>
</html>
<%
rsmember.close();
Statementrsmember.close();
Connrsmember.close();
%>
