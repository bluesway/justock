<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>

<%
// *** Restrict Access To Page: Grant or deny access to this page
String MM_authorizedUsers="";
String MM_authFailedURL="/relogin.jsp";
boolean MM_grantAccess=false;
if (session.getValue("MM_Username") != null && !session.getValue("MM_Username").equals("")) {
  if (true || (session.getValue("MM_UserAuthorization")=="") || 
          (MM_authorizedUsers.indexOf((String)session.getValue("MM_UserAuthorization")) >=0)) {
    MM_grantAccess = true;
  }
}
if (!MM_grantAccess) {
  String MM_qsChar = "?";
  if (MM_authFailedURL.indexOf("?") >= 0) MM_qsChar = "&";
  String MM_referrer = request.getRequestURI();
  if (request.getQueryString() != null) MM_referrer = MM_referrer + "?" + request.getQueryString();
  MM_authFailedURL = MM_authFailedURL + MM_qsChar + "accessdenied=" + java.net.URLEncoder.encode(MM_referrer);
  response.sendRedirect(response.encodeRedirectURL(MM_authFailedURL));
  return;
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<title>下單一覽</title>
<style type="text/css">
<!--
.style1 {
	color: #FF3300;
	font-family: "新細明體";
}
.style2 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 16px;
}
.style6 {font-family: "新細明體"}
body {
	background-color: #e7f3ff;
}
.style12 {font-size: 15px; font-family: "新細明體"; color: #000000; }
-->
</style>
<script language="javascript">
function sendMsg()
{
ans=window.confirm("確認刪除此筆下單資料?")
if(ans)
document.forms[0].submit()
}
</script>
</head>
<%@ include file="/Connections/justockdb.jsp" %>

<jsp:useBean id="dateBean" class="common.util.DateUtil"/>
<jsp:useBean id="timeBean" class="common.util.TimeUtil"/>
<%long time = System.currentTimeMillis();
int dateStr = dateBean.getDBDate(time);
int timeStr = timeBean.getDBTime(time);%>
<%
String order_list__MMColParam = "1";
if (session.getValue("Uid") !=null) {order_list__MMColParam = (String)session.getValue("Uid");}
%>
<%
Driver Driverorder_list = (Driver)Class.forName(MM_justockdb_DRIVER).newInstance();
Connection Connorder_list = DriverManager.getConnection(MM_justockdb_STRING,MM_justockdb_USERNAME,MM_justockdb_PASSWORD);
PreparedStatement Statementorder_list = Connorder_list.prepareStatement("SELECT * FROM justock.transaction WHERE tUid = '" + order_list__MMColParam + "' AND tDate='"+dateStr+"'   ORDER BY tId ASC");
ResultSet order_list = Statementorder_list.executeQuery();
boolean order_list_isEmpty = !order_list.next();
boolean order_list_hasData = !order_list_isEmpty;
Object order_list_data;
int order_list_numRows = 0;
%>
<%
int Repeat1__numRows = -1;
int Repeat1__index = 0;
order_list_numRows += Repeat1__numRows;
%>
<body>
<form id="form1" name="form1" method="post" action="">
  <div align="center">
    <table width="95%" border="0" align="center" cellpadding="4" cellspacing="1" bgcolor="#CCCCCC">
      <tr>
        <td colspan="10" align="center" bgcolor="#CCCCCC"><span class="style2">下 單 一 覽</span></td>
      </tr>
      <tr>
        <td align="center" width="9%" bgcolor="#FFFFCC"><span class="style12">交易<br />
        序號</span></td>
        <td align="center" width="10%" bgcolor="#FFFFCC"><span class="style12">下單<br />日期</span></td>
        <td align="center" width="10%" bgcolor="#FFFFCC"><span class="style12">下單<br />
        時間</span></td>
        <td align="center" width="9%" bgcolor="#FFFFCC"><span class="style12">股票<br />代碼</span></td>
        <td align="center" width="9%" bgcolor="#FFFFCC"><span class="style12">下單<br />價格</span></td>
        <td align="center" width="8%" bgcolor="#FFFFCC"><span class="style12">下單<br />股數</span></td>
        <td align="center" width="8%" bgcolor="#FFFFCC"><span class="style12">下單<br />
          期限(分)</span></td>
        <td align="center" width="15%" bgcolor="#FFFFCC"><span class="style12">交易<br />狀態</span></td>
        <td align="center" width="15%" bgcolor="#FFFFCC"><span class="style12">刪除<br />
        資料</span></td>
        <td align="center" width="7%" bgcolor="#FFFFCC"><span class="style12">買賣<br />方式</span></td>
      </tr>
      <tr>
        <% while ((order_list_hasData)&&(Repeat1__numRows-- != 0)) { %>
        <td align="center" bgcolor="#FFFFFF"><span class="style6"><%=(((order_list_data = order_list.getObject("tId"))==null || order_list.wasNull())?"":order_list_data)%></span></td>
          <td align="center" bgcolor="#FFFFFF"><span class="style6">
        <%
		  	String gdate2=order_list.getString("tDate");
		int in_gdate2=Integer.parseInt(gdate2);
		out.println(dateBean.transDate(in_gdate2));
		  
		  %></span></td>
          <td align="center" bgcolor="#FFFFFF"><span class="style6">
        <%
		  	String gtime=order_list.getString("tTime");
		int in_gtime=Integer.parseInt(gtime);
		out.println(timeBean.transTime(in_gtime));
		  
		  %></span></td>
          <td align="center" bgcolor="#FFFFFF"><span class="style6"><%=(((order_list_data = order_list.getObject("tSid"))==null || order_list.wasNull())?"":order_list_data) %></span></td>
          <td align="center" bgcolor="#FFFFFF"><span class="style6"><%=(((order_list_data = order_list.getObject("tPrice"))==null || order_list.wasNull())?"":order_list_data)%></span></td>
          <td align="center" bgcolor="#FFFFFF"><span class="style6"><%=(((order_list_data = order_list.getObject("tQuan"))==null || order_list.wasNull())?"":order_list_data)%></span></td>
          <td align="center" bgcolor="#FFFFFF"><span class="style6">
		  <%  
		String s_tDue=order_list.getString("tDue");
		int in_tDue=Integer.parseInt(s_tDue);
		if(in_tDue==0)
		{out.println("不限制時間");
		}
		else
		{
		out.println(in_tDue);
		
		}
		  
		  %></span></td>
          <td align="center" bgcolor="#FFFFFF"><span class="style6">
          <%
		String get_tStatus=order_list.getString("tStatus");
		double dou_get_tStatus=Double.valueOf(get_tStatus);
		if(dou_get_tStatus==(-1))
		{
		out.println("完成下單");
		}
		else if(dou_get_tStatus==(0))
		{
		out.println("交易成功");
		}
		else if(dou_get_tStatus==(1))
		{
		out.println("帳戶餘額不足");
		}
		else if(dou_get_tStatus==(2))
		{
		out.println("已失效");
		}
		else if(dou_get_tStatus==(3))
		{
		out.println("限價超出範圍");
		}
		else if(dou_get_tStatus==(4))
		{
		out.println("持有餘股不足");
		}
		else if(dou_get_tStatus==(5))
		{
		out.println("權限不足");
		}
		else if(dou_get_tStatus==(6))
		{
		out.println("權限不足");
		}
		else if(dou_get_tStatus==(127))
		{
		out.println("交割完成");
		}
		
		%>
        </span></td>
          <td align="center" bgcolor="#FFFFFF">
	 	  <span class="style1" ><%if (dou_get_tStatus == (-1)) {%><a href="/orderlist2.jsp?class=<%=order_list_data = order_list.getObject("tId")%>">刪除下單</a><%}%></span></td>
          <td align="center" bgcolor="#FFFFFF"><span class="style6">
          <%
	  String get_tPrice=order_list.getString("tPrice");
		double dou_get_tPrice=Double.valueOf(get_tPrice);
		if(dou_get_tPrice >=0)
		{out.println("買");	}
		else
		{out.println("賣");}
	  %>
        </span></td>
      </tr>   <%
  Repeat1__index++;
  order_list_hasData = order_list.next();
}
%>
    </table>
  </div>
</form>
<p align="center">&nbsp;</p>
</body>
</html>
<%
order_list.close();
Statementorder_list.close();
Connorder_list.close();
%>
