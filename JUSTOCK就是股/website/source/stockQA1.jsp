<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />

<title>股市問答 - 入門篇</title>
<style type="text/css">
<!--
body {
	overflow-x:hidden;
	background-color: #e7f3ff;
}
.style1 {color: #9c9a9c}
a:link {
	color: #999999;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: #999999;
}
a:hover {
	text-decoration: underline;
	color: #999999;
}
a:active {
	text-decoration: none;
	color: #999999;
}
.style2 {color: #9C9A9C}
.style3 {color: #000000}
.style4 {
	color: #FFFFFF;
	font-weight: bold;
	font-size: 16px;
}
.style5 {color: #999999}
.style6 {font-size: 15px}

-->
</style>
<script language="javascript">
<!--
function showlayer(id){
	for(var i=1;i<=20;i++){
		if(document.all("q"+i).style.display=='inline'){
		    document.all("q"+i).style.display='none';
			document.all("q"+i+"_title").style.fontWeight ='normal';
			break;
		}
	}
		document.all("q"+id).style.display='inline';
		document.all("q"+id+"_title").style.fontWeight ='bold';
		document.all("q"+id+"_title").style.fontColor ='#CC3300';
	}

function MM_openBrWindow(theURL,winName,features) { //v2.0
	window.open(theURL,winName,features);
}
//-->
</script>
</head>

<body>
<form id="form1" name="form1" method="post" action="">
  <div align="center" class="style1"><span class="style5"><span class="style6"><a href="/noun.jsp" target="_blank">證券辭典</a> ｜ </span></span><span class="style6">入門股市問答 │<a href="/stockQA2.jsp" target="_self">進階股市問答</a></span></div>
</form>

<table width="80%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
<tr class="content_title">
	<td colspan="2" align="center" valign="top" border="0"><span class="style4">【入門股市問答】</span></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td border="0" valign="top"><div align="right" class="style6">01：</div></td>
	<td width="95%" valign="top"><div align="left" class="style6" id="q1_title"><a href="javascript:showlayer(1)" class="mt style3"> 本益比代表之意義為何？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q1" style="display:inline" class="content_answer">
	<td valign="top"><div align="right" class="style6">Ans：</div></td>
	<td valign="top"><div align="left" class="style6"> 本益比中之「本」乃指股價，此即欲從市場上購進股票的成本。「益」係指每股稅後純益。本益比係股票價格除其每股稅後純益之比。例如，甲股票的本益比為30表示甲股票之市價為其每股稅後純益的30倍。在計算本益比時，每股稅後純益通常採用最近一年的每股稅後純益，亦有採用預估的每股稅後純益。由於每股稅後純益為公司每年之獲利能力，故甲股票的本益比30，表示投資人必須付出該公司每年獲利能力30倍之價格才能擁有甲股票。所以本益比愈高。通常成長型的公司其未來發展較具潛力，因此本益比會高於成熟型公司之本益比。除了個股本益比外，亦有產業別及市場本益比，有興趣之投資人可至台灣證券交易所公關室索取「本益比簡介」參閱。</div></td>
</tr>

<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style6">02：</div></td>
	<td valign="top"><div align="left" class="style6" id="q2_title"><a href="javascript:showlayer(2)" class="mt style3"> 投資人買賣股票或受益憑證，需要負擔何種費用？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q2" style="display:none" class="content_answer">
  	<td valign="top"><div align="right" class="style6">Ans：</div></td>
  	<td valign="top"><div align="left" class="style6"> 買賣股票或受益憑證須支付給受託證券商該股票成交金額千分之一‧四二五的手續費，但賣出股票時除了上述的手續費外，還需繳交成交金額千分之三的證券交易稅，受益憑證則為成交金額之千分之一。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style6">03：</div></td>
	<td valign="top"><div align="left" class="style6" id="q3_title"><a href="javascript:showlayer(3)" class="mt style3"> 電話委託買賣有價證券應於何時補簽章？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q3" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style6">Ans：</div></td>
  	<td valign="top"><div align="left" class="style6"> 應由委託人于成交後交割時補簽章。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style6">04：</div></td>
	<td valign="top"><div align="left" class="style6" id="q4_title"><a href="javascript:showlayer(4)" class="mt style3"> 上市（櫃）股票與未上市（櫃）股票之區分時點為何？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q4" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style6">Ans：</div></td>
  	<td valign="top"><div align="left" class="style6"> 依台灣證券交易所股份有限公司營業細則第四十三條第一項規定：「發行公司申請有價證券上市案經本公司審定，檢具上市契約報奉主管機關核准後生效，該公司即列為上市公司」。櫃檯買賣中心之業務規則雖無類似明文規定，亦應作相同的解釋，故上市（櫃）股票與未上市（櫃）股票應以主管機關核准上市（櫃）契約為區分的時點。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style6">05：</div></td>
	<td valign="top"><div align="left" class="style6" id="q5_title"><a href="javascript:showlayer(5)" class="mt style3"> 申報買賣有價證券價格之升降單位及升降漲跌幅度為何？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q5" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style6">Ans：</div></td>
  	<td valign="top"><div align="left" class="style6"> 股票每股或受益憑證每一受益權單位市價未滿五元為一分；五元以上未滿十五元者為五分；十五元以上未滿五十元者為一角；五十元以上未滿一百五十元者為五 角；一百五十元以上未滿一千元者一元，一千元以上者為五元。至於公債及公司債申報買賣之價格其升降單位則一律為五分。股票轉換公司債及受益憑證當日市價升 降幅度以漲至或跌至前一日收盤價格百分之七為限，債券則以百分之五為限。如前一日無收盤價格，但有連續二日漲至或跌至限度之揭示價格時，得以之作為升降幅 度之基準。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style6">06：</div></td>
	<td valign="top"><div align="left" class="style6" id="q6_title"><a href="javascript:showlayer(6)" class="mt style3"> 颱風侵襲時，台灣證券交易所有價證券集中交易市場是否停止交易？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q6" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style6">Ans：</div></td>
  	<td valign="top"><div align="left" class="style6"> 一、天然災害侵襲時，本公司有價證券集中交易市場是否休市，應以台北市市長宣布當日台北市公教機關是否停止上班為準據。<br />
二、本公司有價證券集中交易市場休市時，則全體證券商當日停止營業，且當日應屆交割事務順延辦理。<br />
三、本公司有價證券集中交易市場不休市時：<br />
 1.證券商正常營業，且當日應屆交割事務正常辦理。<br />
 2.若台北市以外證券商所在區域因遭受天然災害侵襲致當地府首長宣布當日公教機關停止上班時，當地所有證券商應全部停止營業；其應行交割之款券均順延辦理。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style6">07：</div></td>
	<td valign="top"><div align="left" class="style6" id="q7_title"><a href="javascript:showlayer(7)" class="mt style3">
投資人買股票或受益憑證後，與證券商交割時應以何種方式繳付價款？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q7" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style6">Ans：</div></td>
  	<td valign="top"><div align="left" class="style6">
應於證券商指定之金融機構開立款項劃撥帳戶，辦理收付價款。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style6">08：</div></td>
	<td valign="top"><div align="left" class="style6" id="q8_title"><a href="javascript:showlayer(8)" class="mt style3"> 有價證券交易之交易單位為何？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q8" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style6">Ans：</div></td>
  	<td valign="top"><div align="left" class="style6">
買賣有價證券時，每次申報數量規定不得少於一個交易單位或其倍數，股票以每股面額十元並以一千股為一交易單位，公債及公司債則以面額十萬元為一交易單位，受益憑證其每一受益權為十元者，亦以一千受益權為一交易單位。轉換公司債以面額十萬元為一交易單位。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style6">09：</div></td>
	<td valign="top"><div align="left" class="style6" id="q9_title"><a href="javascript:showlayer(9)" class="mt style3"> 投資人委託買賣股票及受益憑證時，可否要求證券經紀商減少手續費？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q9" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style6">Ans：</div></td>
  	<td valign="top"><div align="left" class="style6"> 不可以，證券經紀商不得任意增減手續費。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style6">10：</div></td>
	<td valign="top"><div align="left" class="style6" id="q10_title"><a href="javascript:showlayer(10)" class="mt style3"> 何謂證券交易稅？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q10" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style6">Ans：</div></td>
  	<td valign="top"><div align="left" class="style6"> 依證券交易稅條例規定，證券交易稅係向出賣有價證券人按每次交易成交價格，如為一般公司發行之股票及表明股票權利之證書或憑證部分，課徵千分之三，如為一般公司債及其他經政府核准有價證券部分，課徵千分之一。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style6">11：</div></td>
	<td valign="top"><div align="left" class="style6" id="q11_title"><a href="javascript:showlayer(11)" class="mt style3"> 何謂手續費？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q11" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style6">Ans：</div></td>
  	<td valign="top"><div align="left" class="style6"> 證券經紀商受託買賣股票及受益憑證成交後向委託人收取之費用，稱為手續費，目前之手續費率為成交金額的千分之一點四二五。但可轉換公司債之手續費率為千分之一點二五。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style6">12：</div></td>
	<td valign="top"><div align="left" class="style6" id="q12_title"><a href="javascript:showlayer(12)" class="mt style3"> 如何註銷帳戶？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q12" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style6">Ans：</div></td>
  	<td valign="top"><div align="left" class="style6"> 投資人已開立之帳戶辦理註銷時，須在當初辦理開戶之證券經紀商填寫註銷帳戶申請書，並攜帶本人國民身分證等有關文件辦理。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style6">13：</div></td>
	<td valign="top"><div align="left" class="style6" id="q13_title"><a href="javascript:showlayer(13)" class="mt style3"> 同一投資人可否在同一家證券經紀商開立二個帳戶？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q13" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style6">Ans：</div></td>
  	<td valign="top"><div align="left" class="style6"> 同一人在同一家證券經紀商僅能開立一個帳戶。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style6">14：</div></td>
	<td valign="top"><div align="left" class="style6" id="q14_title"><a href="javascript:showlayer(14)" class="mt style3"> 華僑及外國人可否開戶委託買賣股票？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q14" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style6">Ans：</div></td>
  	<td valign="top"><div align="left" class="style6"> 可以。但須持經濟部投資審議委員會或科學工業園區管理局專案核准之相關文件，並由證券經紀商專函送經證交所同意開戶後一律只准委託賣出，不得委託買入。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style6">15：</div></td>
	<td valign="top"><div align="left" class="style6" id="q15_title"><a href="javascript:showlayer(15)" class="mt style3"> 未成年人可否在證券經紀商開戶委託買賣證券？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q15" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style6">Ans：</div></td>
  	<td valign="top"><div align="left" class="style6"> 可以。但應由法定代理人親持其本人及委託人之國民身分證正本，並在委託契約上簽章，註明其親屬關係。另對委託買賣證券之業務憑證均須由法定代理人簽章。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style6">16：</div></td>
	<td valign="top"><div align="left" class="style6" id="q16_title"><a href="javascript:showlayer(16)" class="mt style3"> 何種人不得辦理開戶？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q16" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style6">Ans：</div></td>
  	<td valign="top"><div align="left" class="style6"> 1. 未成年人未經法定代理人之代理或允許者。<br />
2. 證券主管機關及證券交易所職員、雇員。<br />
3. 受破產之宣告未經復權者。<br />
4. 受禁治產之宣告未經法定代理人之代理者。<br />
5. 法人委託開戶未能提出該法人授權開戶之證明者。<br />
6. 證券經紀商之股東或受僱人、法人股東之董事監察人及經理人可辦理開戶，惟限在所屬證券商開戶委託買賣。<br />
7. 證券自營商未經主管機關許可者。<br />
8. 證券經紀商未經主管機關許可或本公司同意者。<br />
9. 曾因證券交易違背契約，經本公司通函各證券經紀商有案未滿三年，或雖滿三年但未結案者。<br />
10. 曾經違反證券交易法規定，經司法機關有罪之刑事判決確定，或經主管機關通知停止買賣證券有案，其期限未滿五年者。</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style6">17：</div></td>
	<td valign="top"><div align="left" class="style6" id="q17_title"><a href="javascript:showlayer(17)" class="mt style3"> 如何計算獲利？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q17" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style6">Ans：</div></td>
  	<td valign="top"><div align="left" class="style6"> 普通交易<br />
假設今年初小方買了一張股價80元的股票，一個月後當股價上漲至100元時，小方將股票賣出。表面上看起來賺了現金20,000元，事實上是如此嗎？小方是不是少考慮了一些成本呢？讓我們一起來幫小方算一算到底賺了多少錢？ <br />
一、股票買賣淨利潤〈會計利潤〉=賣出股票所得-買進股票成本<br />
<span class="style2"><img src="/images/1.bmp" /></span><br />
二、股票買賣淨利潤 -- 機會成本 = 經濟利潤<br />
<span class="style2"><img src="/images/2.bmp" /></span><br />
  	</div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style6">18：</div></td>
	<td valign="top"><div align="left" class="style6" id="q18_title"><a href="javascript:showlayer(18)" class="mt style3"> 如何買賣股票？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q18" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style6">Ans：</div></td>
  	<td valign="top"><div align="left" class="style6"> 
<span class="style2"><img src="/images/howtobuystock.bmp" /></span></div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style6">19：</div></td>
	<td valign="top"><div align="left" class="style6" id="q19_title"><a href="javascript:showlayer(19)" class="mt style3"> 如何開戶？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q19" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style6">Ans：</div></td>
  	<td valign="top"><div align="left" class="style6"> <strong>(一)選擇證券公司</strong><br />
選擇一家大規模的公司：大規模公司提供較多的服務如自辦融資融券等服務離家近：對於以後辦理一些手續比較方便多樣理財工具：提供許多先進的理財工具，如網路下單、語音下單、行動下單等下單工具<br />
<strong>(二)選擇營業員</strong><br />
可以請親朋好友介紹或者請開戶人員介紹。一個好的營業員應該具備以下幾個條件：親切有耐心、操守良好而且可以給予投資者一些投資理財建議。<br />
<strong>(三)開戶時攜帶物品</strong><br />
銀行開戶金額100元、印章、身份證<br />
<strong>(四)開戶時要辦理哪些手續</strong><br />
<em>1.到證券公司開戶櫃檯填寫以下表格</em><br />
印鑑卡：以後有關證券業務使用印章，需和印鑑卡上的印章同樣式樣。<br />
開戶基本資料表：填寫有關投資人的基本資料。<br />
客戶自填徵信資料表：提供證券公司作為財務狀況的調查。<br />
八種契約書<br />
委託買賣證券受託契約書：此契約書是委託證券公司買賣上市股票。<br />
櫃檯買賣有價證券開戶契約：此契約書是委託證券公司買賣上櫃股票。<br />
聲明書：投資人對於印鑑、款項、存摺應妥善保管的契約書。<br />
櫃檯買賣確認書：投資人充分了解櫃檯買賣規則的契約書。<br />
集中保管同意書：此申請書是讓以後買賣股票不需親自拿股票去交割，由集中保管公司自動移轉買賣的股票。<br />
交割款券轉撥同意書：交割款券轉撥同意書是投資人委託證券公司辦理交割、集保及在指定銀行扣款匯錢。<br />
認購〈售〉權證風險預告書：告知投資人必須自付股票理財風險。<br />
電子交易帳戶委託買賣同意書：客戶可以利用網路、電話語音進行下單及一些相關風險承擔及責任歸屬的契約書。<br />
第二類票據退票資料電腦查詢申請單：證券公司可以利用此申請單查詢投資人是否有退票紀錄。<br />
<em>2. 到指定銀行開戶</em><br />
客戶必須選擇證券公司合作的銀行作為交割帳戶。注意，每一家證券分公司搭配銀行不同。銀行現在也有推出網路銀行服務，上網即可查詢帳戶相關資料，建議投資人在銀行開戶時，順便辦理網路銀行功能。<br />
<em>3.開戶後你會拿到集保存摺及開戶卡之後，即可下單集保存摺上登錄股票買賣的紀錄。</em></div></td>
</tr>
<tr bgcolor="#ffffff" class="content_title">
	<td valign="top"><div align="right" class="style6">20：</div></td>
	<td valign="top"><div align="left" class="style6" id="q20_title"><a href="javascript:showlayer(20)" class="mt style3"> 何謂股票？</a></div></td>
</tr>
<tr bgcolor="#ffffcc" id="q20" style="display:none" class="content_answer">
   	<td valign="top"><div align="right" class="style6">Ans：</div></td>
  	<td valign="top"><div align="left" class="style6"> 股票是一種憑證。當你決定投資一家公司，並取得這家公司的股票之後，你就是該公司的股東，有權利分享公司的利潤及資產。一般而言，在證券市場上股票的交易單位是以「張」來計算，1張股票=1000股。你所能分配的利潤有多少，必須視公司盈餘的狀況，舉例來說，如果一家公司股票總共1000股，那麼當公司每賺1000元時，每一股就可分得1元。</div></td>
</tr>
</table>

</body>

</html>
