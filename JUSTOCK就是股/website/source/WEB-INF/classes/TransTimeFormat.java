/**
 * @author bluesway
 * TARGET 分析 info_tse 資料表取得的時間資料
 */

package common.util;

public class TransTimeFormat {
	String h;	// 時
	String m;	// 分
	String s;	// 秒

	// null Constructor
	public TransTimeFormat() {
	}

	// 轉換 info_tse 取得的時間數字資料
	public void transTime(int time) {
    	String time_mark = String.valueOf(time);
    	if (time_mark.length() == 5) {
    		h = time_mark.substring(0, 1);
    		m = time_mark.substring(1, 3);
    		s = time_mark.substring(3, 5);
    	} else {
    		h = time_mark.substring(0, 2);
    		s = time_mark.substring(2, 4);
    		s = time_mark.substring(4, 6);
    	}
	}
	
	// 取得小時
	public String getHour() {
		return h;
	}
	
	// 取得小時，若小時數字只有一位，則在最前面附加 prefix 字串，ex: getHour("0");
	public String getHour(String prefix) {
		if (h.length() == 1)
			return prefix + h;
		else 
			return h;
	}
	
	// 取得分鐘
	public String getMinute() {
		return m;
	}
	
	// 取得秒
	public String getSecond() {
		return s;
	}
	
	// 取得時間，使用sep字串當作分隔符號，ex: getTime(":");
	public String getTime(String sep) {
		return h + sep + m + sep + s;
	}
	
	// 取得時間，使用prefix作為小時位數為1時的前綴，並使用 sep 字串當作分隔符號，ex: getTime(" ", "-");
	public String getTime(String prefix, String sep) {
		return getHour(prefix) + sep + m + sep + s;
	}
	
	public static void main(String[] args) {
		TransTimeFormat ttf = new TransTimeFormat();
		ttf.transTime(23456);
		System.out.println(ttf.getTime("0", ":"));
	}

}
