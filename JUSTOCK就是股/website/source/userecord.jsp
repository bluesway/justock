<%@ page contentType="text/html; charset=big5" language="java" import="java.util.*, java.text.*, java.sql.*" errorPage="" %>
<%@ include file="/Connections/justockdb.jsp" %>
<%
// *** Restrict Access To Page: Grant or deny access to this page
String MM_authorizedUsers="";
String MM_authFailedURL="/relogin.jsp";
boolean MM_grantAccess=false;
if (session.getValue("MM_Username") != null && !session.getValue("MM_Username").equals("")) {
  if (true || (session.getValue("MM_UserAuthorization")=="") || 
          (MM_authorizedUsers.indexOf((String)session.getValue("MM_UserAuthorization")) >=0)) {
    MM_grantAccess = true;
  }
}
if (!MM_grantAccess) {
  String MM_qsChar = "?";
  if (MM_authFailedURL.indexOf("?") >= 0) MM_qsChar = "&";
  String MM_referrer = request.getRequestURI();
  if (request.getQueryString() != null) MM_referrer = MM_referrer + "?" + request.getQueryString();
  MM_authFailedURL = MM_authFailedURL + MM_qsChar + "accessdenied=" + java.net.URLEncoder.encode(MM_referrer);
  response.sendRedirect(response.encodeRedirectURL(MM_authFailedURL));
  return;
}
%>
<jsp:useBean id="dateBean" class="common.util.DateUtil"/>
<jsp:useBean id="timeBean" class="common.util.TimeUtil"/>
<%
String userrecord__MMColParam = "1";
if (session.getValue("Uid")  !=null) {userrecord__MMColParam = (String)session.getValue("Uid") ;}
%>
<%

Driver Driveruserrecord = (Driver)Class.forName(MM_justockdb_DRIVER).newInstance();
Connection Connuserrecord = DriverManager.getConnection(MM_justockdb_STRING,MM_justockdb_USERNAME,MM_justockdb_PASSWORD);
PreparedStatement Statementuserrecord = Connuserrecord.prepareStatement("SELECT *  FROM justock.login_record  WHERE rUid = '" + userrecord__MMColParam + "'  ORDER BY rDate DESC, rTime DESC");
ResultSet userrecord = Statementuserrecord.executeQuery();
boolean userrecord_isEmpty = !userrecord.next();
boolean userrecord_hasData = !userrecord_isEmpty;
Object userrecord_data;
int userrecord_numRows = 0;

%>

<%
int Repeat1__numRows = 25;
int Repeat1__index = 0;
userrecord_numRows += Repeat1__numRows;
%>
<%
// *** Recordset Stats, Move To Record, and Go To Record: declare stats variables

int userrecord_first = 1;
int userrecord_last  = 1;
int userrecord_total = -1;

if (userrecord_isEmpty) {
  userrecord_total = userrecord_first = userrecord_last = 0;
}

//set the number of rows displayed on this page
if (userrecord_numRows == 0) {
  userrecord_numRows = 1;
}
%>

<%
// *** Recordset Stats: if we don't know the record count, manually count them

if (userrecord_total == -1) {

  // count the total records by iterating through the recordset
    for (userrecord_total = 1; userrecord.next(); userrecord_total++);

  // reset the cursor to the beginning
  userrecord.close();
  userrecord = Statementuserrecord.executeQuery();
  userrecord_hasData = userrecord.next();

  // set the number of rows displayed on this page
  if (userrecord_numRows < 0 || userrecord_numRows > userrecord_total) {
    userrecord_numRows = userrecord_total;
  }

  // set the first and last displayed record
  userrecord_first = Math.min(userrecord_first, userrecord_total);
  userrecord_last  = Math.min(userrecord_first + userrecord_numRows - 1, userrecord_total);
}
%>
<% String MM_paramName = ""; %>
<%
// *** Move To Record and Go To Record: declare variables

ResultSet MM_rs = userrecord;
int       MM_rsCount = userrecord_total;
int       MM_size = userrecord_numRows;
String    MM_uniqueCol = "";
          MM_paramName = "";
int       MM_offset = 0;
boolean   MM_atTotal = false;
boolean   MM_paramIsDefined = (MM_paramName.length() != 0 && request.getParameter(MM_paramName) != null);
%>
<%
// *** Move To Record: handle 'index' or 'offset' parameter

if (!MM_paramIsDefined && MM_rsCount != 0) {

  //use index parameter if defined, otherwise use offset parameter
  String r = request.getParameter("index");
  if (r==null) r = request.getParameter("offset");
  if (r!=null) MM_offset = Integer.parseInt(r);

  // if we have a record count, check if we are past the end of the recordset
  if (MM_rsCount != -1) {
    if (MM_offset >= MM_rsCount || MM_offset == -1) {  // past end or move last
      if (MM_rsCount % MM_size != 0)    // last page not a full repeat region
        MM_offset = MM_rsCount - MM_rsCount % MM_size;
      else
        MM_offset = MM_rsCount - MM_size;
    }
  }

  //move the cursor to the selected record
  int i;
  for (i=0; userrecord_hasData && (i < MM_offset || MM_offset == -1); i++) {
    userrecord_hasData = MM_rs.next();
  }
  if (!userrecord_hasData) MM_offset = i;  // set MM_offset to the last possible record
}
%>
<%
// *** Move To Record: if we dont know the record count, check the display range

if (MM_rsCount == -1) {

  // walk to the end of the display range for this page
  int i;
  for (i=MM_offset; userrecord_hasData && (MM_size < 0 || i < MM_offset + MM_size); i++) {
    userrecord_hasData = MM_rs.next();
  }

  // if we walked off the end of the recordset, set MM_rsCount and MM_size
  if (!userrecord_hasData) {
    MM_rsCount = i;
    if (MM_size < 0 || MM_size > MM_rsCount) MM_size = MM_rsCount;
  }

  // if we walked off the end, set the offset based on page size
  if (!userrecord_hasData && !MM_paramIsDefined) {
    if (MM_offset > MM_rsCount - MM_size || MM_offset == -1) { //check if past end or last
      if (MM_rsCount % MM_size != 0)  //last page has less records than MM_size
        MM_offset = MM_rsCount - MM_rsCount % MM_size;
      else
        MM_offset = MM_rsCount - MM_size;
    }
  }

  // reset the cursor to the beginning
  userrecord.close();
  userrecord = Statementuserrecord.executeQuery();
  userrecord_hasData = userrecord.next();
  MM_rs = userrecord;

  // move the cursor to the selected record
  for (i=0; userrecord_hasData && i < MM_offset; i++) {
    userrecord_hasData = MM_rs.next();
  }
}
%>
<%
// *** Move To Record: update recordset stats

// set the first and last displayed record
userrecord_first = MM_offset + 1;
userrecord_last  = MM_offset + MM_size;
if (MM_rsCount != -1) {
  userrecord_first = Math.min(userrecord_first, MM_rsCount);
  userrecord_last  = Math.min(userrecord_last, MM_rsCount);
}

// set the boolean used by hide region to check if we are on the last record
MM_atTotal  = (MM_rsCount != -1 && MM_offset + MM_size >= MM_rsCount);
%>
<%
// *** Go To Record and Move To Record: create strings for maintaining URL and Form parameters

String MM_keepBoth,MM_keepURL="",MM_keepForm="",MM_keepNone="";
String[] MM_removeList = { "index", MM_paramName };

// create the MM_keepURL string
if (request.getQueryString() != null) {
  MM_keepURL = '&' + request.getQueryString();
  for (int i=0; i < MM_removeList.length && MM_removeList[i].length() != 0; i++) {
  int start = MM_keepURL.indexOf(MM_removeList[i]) - 1;
    if (start >= 0 && MM_keepURL.charAt(start) == '&' &&
        MM_keepURL.charAt(start + MM_removeList[i].length() + 1) == '=') {
      int stop = MM_keepURL.indexOf('&', start + 1);
      if (stop == -1) stop = MM_keepURL.length();
      MM_keepURL = MM_keepURL.substring(0,start) + MM_keepURL.substring(stop);
    }
  }
}

// add the Form variables to the MM_keepForm string
if (request.getParameterNames().hasMoreElements()) {
  java.util.Enumeration items = request.getParameterNames();
  while (items.hasMoreElements()) {
    String nextItem = (String)items.nextElement();
    boolean found = false;
    for (int i=0; !found && i < MM_removeList.length; i++) {
      if (MM_removeList[i].equals(nextItem)) found = true;
    }
    if (!found && MM_keepURL.indexOf('&' + nextItem + '=') == -1) {
      MM_keepForm = MM_keepForm + '&' + nextItem + '=' + java.net.URLEncoder.encode(request.getParameter(nextItem));
    }
  }
}

String tempStr = "";
for (int i=0; i < MM_keepURL.length(); i++) {
  if (MM_keepURL.charAt(i) == '<') tempStr = tempStr + "&lt;";
  else if (MM_keepURL.charAt(i) == '>') tempStr = tempStr + "&gt;";
  else if (MM_keepURL.charAt(i) == '"') tempStr = tempStr +  "&quot;";
  else tempStr = tempStr + MM_keepURL.charAt(i);
}
MM_keepURL = tempStr;

tempStr = "";
for (int i=0; i < MM_keepForm.length(); i++) {
  if (MM_keepForm.charAt(i) == '<') tempStr = tempStr + "&lt;";
  else if (MM_keepForm.charAt(i) == '>') tempStr = tempStr + "&gt;";
  else if (MM_keepForm.charAt(i) == '"') tempStr = tempStr +  "&quot;";
  else tempStr = tempStr + MM_keepForm.charAt(i);
}
MM_keepForm = tempStr;

// create the Form + URL string and remove the intial '&' from each of the strings
MM_keepBoth = MM_keepURL + MM_keepForm;
if (MM_keepBoth.length() > 0) MM_keepBoth = MM_keepBoth.substring(1);
if (MM_keepURL.length() > 0)  MM_keepURL = MM_keepURL.substring(1);
if (MM_keepForm.length() > 0) MM_keepForm = MM_keepForm.substring(1);
%>
<%
// *** Move To Record: set the strings for the first, last, next, and previous links

String MM_moveFirst,MM_moveLast,MM_moveNext,MM_movePrev;
{
  String MM_keepMove = MM_keepBoth;  // keep both Form and URL parameters for moves
  String MM_moveParam = "index=";

  // if the page has a repeated region, remove 'offset' from the maintained parameters
  if (MM_size > 1) {
    MM_moveParam = "offset=";
    int start = MM_keepMove.indexOf(MM_moveParam);
    if (start != -1 && (start == 0 || MM_keepMove.charAt(start-1) == '&')) {
      int stop = MM_keepMove.indexOf('&', start);
      if (start == 0 && stop != -1) stop++;
      if (stop == -1) stop = MM_keepMove.length();
      if (start > 0) start--;
      MM_keepMove = MM_keepMove.substring(0,start) + MM_keepMove.substring(stop);
    }
  }

  // set the strings for the move to links
  StringBuffer urlStr = new StringBuffer(request.getRequestURI()).append('?').append(MM_keepMove);
  if (MM_keepMove.length() > 0) urlStr.append('&');
  urlStr.append(MM_moveParam);
  MM_moveFirst = urlStr + "0";
  MM_moveLast  = urlStr + "-1";
  MM_moveNext  = urlStr + Integer.toString(MM_offset+MM_size);
  MM_movePrev  = urlStr + Integer.toString(Math.max(MM_offset-MM_size,0));
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5" />
<title>使用者記錄列表</title>
<style type="text/css">
<!--
a:link {
	color: 000066;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: 000066;
}
a:hover {
	text-decoration: none;
	color: 000066;
}
a:active {
	text-decoration: none;
	color: 000066;
}
.style4 {color: #000000}
.style5 {color: #FFFFFF}
.style6 {color: #999900}
body {
	background-color: #e7f3ff;
}
.style8 {color: #999900; font-size: 14px; }
.style9 {
	font-size: 14px;
	font-family: "新細明體";
	color: #000000;
}
.style13 {font-size: 15px; color: #000000; font-family: "新細明體"; }
.style15 {
	color: #FFFFFF;
	font-size: 16px;
	font-weight: bold;
}
.style17 {
	font-size: 14px;
	color: #000000;
}
.style14 {
	font-size: 14px;
	font-family: "新細明體";
	color: #000066;
}
-->
</style>
</head>

<body>

<div>
  <table width="70%" border="0" align="center" cellpadding="4" cellspacing="1" bgcolor="#CCCCCC">
            <tbody>

              <tr bgcolor="#FFFFCC">
                <td colspan="3" align="center" bgcolor="#CCCCCC"><span class="style15">使 用 者 記 錄 列 表 </span></td>
              </tr>
              <tr bgcolor="#FFFFCC">
                <td colspan="3" align="center" bgcolor="#FFFFCC"><span class="style17">這是您第 <%=(userrecord_total)%> 次登入</span></td>
              </tr>
              <tr bgcolor="#FFFFCC">
                <td width="28%" align="center"><span class="style13">登入日期</span></td>
                <td width="28%" align="center"><span class="style13">登入時間</span></td>
                <td width="44%" align="center"><span class="style13">登入 I P 位址</span></td>
              </tr>
 <% while ((userrecord_hasData)&&(Repeat1__numRows-- != 0)) { 
int yaoyi = Integer.parseInt(String.valueOf((((userrecord_data = userrecord.getObject("rDate"))==null || userrecord.wasNull())?"":userrecord_data)));
int teddy = Integer.parseInt(String.valueOf((((userrecord_data = userrecord.getObject("rTime"))==null || userrecord.wasNull())?"":userrecord_data)));
 %>
              <tr bgcolor="#ffffff">
                <td align="center"><%=dateBean.transDate(yaoyi)%></td>
                <td align="center"><%=timeBean.transTime(teddy)%></td>
                <td align="center"><%=(((userrecord_data = userrecord.getObject("rIP"))==null || userrecord.wasNull())?"":userrecord_data)%></td></tr>
<%
  Repeat1__index++;
  userrecord_hasData = userrecord.next();
}
%>
</tbody>
</table>
  
  <p align="center">&nbsp;	
    <span class="style17">記錄 <%=(userrecord_first)%> 到 <%=(userrecord_last)%> 共 <%=(userrecord_total)%> 筆</span>　<A HREF="<%=MM_moveFirst%>"><span class="style14">第一頁</span></A> <A HREF="<%=MM_movePrev%>"><span class="style14">上一頁</span></A> <A HREF="<%=MM_moveNext%>"><span class="style14">下一頁</span></A> <A HREF="<%=MM_moveLast%>"><span class="style14">最末頁</span></A></p>
</div>
</body>
</html>
<%
userrecord.close();
Statementuserrecord.close();
Connuserrecord.close();
%>
