/**
 * @author kevingo
 * @modify bluesway
 * TARGET 主要使用者介面
 */

package software;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;
import javax.swing.JWindow;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EtchedBorder;

import software.DataRetrieve.StockInfoAddPersonalInfo;
import software.Subitem.GeneralGraph;
import software.Subitem.Login;
import software.Subitem.UserData;
import software.Subitem.Graph.OverallGraph;
import software.Subitem.Graph.TechGraph;
import software.Subitem.Listener.StockBoxListener;
import software.Subitem.Transaction.Trade;
import software.Subitem.table.BasicStockInfo;
import software.Subitem.table.FinancialBonusStockInfo;
import software.Subitem.table.FinancialRatioStockInfo;
import software.Subitem.table.FinancialStockInfo;
import software.Subitem.table.FinancialStockInfo_Ver2;
import software.Subitem.table.MajorStockInfo;
import software.Subitem.table.OrderList;
import software.Subitem.table.PersonalStockInfo;
import software.Subitem.table.PriceAndVolumnInfo;
import software.Subitem.table.PriceInfo;
import software.Subitem.table.PriceLow;
import software.Subitem.table.ProfitAndStatementStockInfo;
import software.Subitem.table.ReturnFinancialStockInfo;
import software.Subitem.table.SeperateStockInfo;
import software.Subitem.table.TradePriceInfo;
import software.Subitem.table.TradeVolumnPerDayInfo;
import software.Subitem.table.TransStockInfo;
import software.Subitem.table.WidgetOfDiagram;
import software.Subitem.table.WidgetOfMainGraph;
import software.Subitem.table.InvestCombinationList;

import common.util.StockBox;

@SuppressWarnings("serial")
public class GUI extends JFrame implements ActionListener
{
	// 視窗元件大小變數
    public static final Dimension user_screen_size = Toolkit.getDefaultToolkit().getScreenSize();
    public static final double scale = 0.5; // scale of frame and screen
    public static  double screen_x = user_screen_size.getWidth() * scale;
    public static  double screen_y = user_screen_size.getHeight() * scale;
    public static final Dimension screen = 	new Dimension((int) Math.round(screen_x), (int) Math.round(screen_y));
    public static int[] overall_size;
    
    // 使用者變數
    public static String[] userData;
    
    // 資料庫類別
    public static DB db;
    
    // 資料處理變數
    public int stockId;

    // 視窗元件變數
    public static GUI gui;
    public static final boolean TESTMODE = false; 
    private static int typeLNF = 4;
    private static String[] lnfName = {"javax.swing.plaf.basic.BasicLookAndFeel", 
    															"javax.swing.plaf.metal.MetalLookAndFeel", 
    															"javax.swing.plaf.synth.SynthLookAndFeel", 
    															"com.sun.java.swing.plaf.motif.MotifLookAndFeel", 
    															"com.sun.java.swing.plaf.windows.WindowsLookAndFeel",
    															"com.sun.java.swing.plaf.gtk.GTKLookAndFeel"};
    public static JWindow msgWindow = new JWindow();
    private static Icon splashImage = new ImageIcon("common/source/images/splash2.png");
    private static ImageIcon frameIcon = new ImageIcon("common/source/images/frameicon.gif");
    private static JLabel msg = new JLabel(splashImage);
	private Icon logo; // Logo
	private  JDesktopPane desktop;
	private  JMenuBar menubar;
	private  JMenu real;
	private  JMenu trans;
	private  JMenu analysis;
	private  JMenu FinanMenu;
	private  JMenu EachStockList;
	private  JMenu member;
	private  JMenu document;
	private  JMenu refresh;
	private  JMenu order;
	private  JMenuItem item;
	private  JToolBar toolbar;
	public  JComboBox stockBox; // 工具列股票代碼欄位
	private  JLabel stockLabel;
	private  JLabel logoLabel;
	private static JButton addPersonalStockBtn;
	private MyInternalFrame frame;
	
	// 工具列選單變數
	public static String [] Real = new String[]{"自選。集中市場綜合看盤","集中市場即時行情","店頭市場即時行情","個股即時行情","分類報價看盤"};
	public static String [] Trans = new String[]{"線上下單"};
	public static String [] Analysis = new String[]{"技術分析","籌碼分析","基本分析"};
	public static String [] Member = new String[]{"個人基本資料","修改基本資料","帳戶管理"};
	public static String [] Document = new String[]{"系統說明","新手上路","版權宣告"};
	public static String [] menuOfAnalysis = new String[]{"財務分析","個股當日明細表"};
	public static String [] subFinancialItem = new String[]{"資產負債表","每月營收變化表","財務比例","股利狀況","營收盈餘","價量明細","損益表"};
	public static String [] eachStockList = new String[]{"當日成交量排行","當日成交明細","當日成交金額排行","當日成交價排行","當日漲幅排行","當日跌幅排行","當日漲停股票","當日跌停股票"};
	public static String [] orderList = new String[]{"下單一覽表","投資組合","交易紀錄一覽表"};

    // 軟體功能變數
	public static Trade trade;	// 交易系統
    private UserData ud;		// 使用者資料
    private OverallGraph og;	// 大盤走勢
    private TechGraph tg;		// 技術分析
    private MajorStockInfo ms; // 籌碼分析
    private BasicStockInfo bsi;// 公司基本資料
    private FinancialStockInfo fs;  // 財務分析表 I
    private FinancialStockInfo_Ver2 fs2; // 財務分析表 II
    private FinancialRatioStockInfo fr; // 財務比例表
    private FinancialBonusStockInfo fb; // 股利狀況表
    private ReturnFinancialStockInfo rf; // 營收盈餘表
    private PriceAndVolumnInfo pav; // 價量明細表
    private PersonalStockInfo psi;
    private ProfitAndStatementStockInfo pas;
    public StockInfoAddPersonalInfo siap; // 自選股表
    private TradeVolumnPerDayInfo tvpd; // 當日成交量排行表
    private TransStockInfo tsi; // 當日成交明細表
    private TradePriceInfo tpi; // 當日成交金額排行表
    private PriceInfo pi; // 當日成交價排行表
//    private PriceUpInfo pui; // 當日漲幅排行
    private PriceLow pL; // 當日跌幅排行
    private WidgetOfDiagram wd;
    private WidgetOfMainGraph mg;
    private SeperateStockInfo ssi;
    private OrderList oL; // 下單一覽表
    private InvestCombinationList icL; // 投資組合一覽表

    static {
    	try {
    		UIManager.setLookAndFeel(lnfName[typeLNF]);
    		SwingUtilities.updateComponentTreeUI(msg);
    	}
    	catch (UnsupportedLookAndFeelException ex1) {
    		msg.setText("Unsupported LookAndFeel: " + lnfName);
    	}
    	catch (ClassNotFoundException ex2) {
    		msg.setText("LookAndFeel class not found: " + lnfName);
    	}
    	catch (InstantiationException ex3) {
    		msg.setText("Could not load LookAndFeel: " + lnfName);
    	}
    	catch (IllegalAccessException ex4) {
    		msg.setText("Cannot use LookAndFeel: " + lnfName);
    	}
    	
if(TESTMODE)
	JOptionPane.showMessageDialog(null, "進入測試模式...", "系統狀態", JOptionPane.WARNING_MESSAGE);

    	msgWindow.setVisible(true);
        msgWindow.setSize(splashImage.getIconWidth() - 12, splashImage.getIconHeight());
//    	msgWindow.setSize((int) Math.round(user_screen_size.getWidth() * 0.15), (int) Math.round(user_screen_size.getHeight() * 0.05));
    	msgWindow.setLocationRelativeTo(null);
    	msgWindow.setAlwaysOnTop(true);
    	msgWindow.add(msg);
    	
//		JOptionPane.showMessageDialog(null, "即將連線至資料庫，請按確定繼續...", "系統狀態", JOptionPane.PLAIN_MESSAGE);
    	msg.setFont(new Font(null, Font.PLAIN, 12));
		msg.setText("與資料庫連線中，請稍待...");
				
		db = new DB();
		
		overall_size = new int[400];
        for (int i = 0; i < 400; i++)
            overall_size[i] = (int) Math.round(user_screen_size.getWidth() * 0.0025 * i);
        
		msgWindow.setVisible(false);
    }
    
    // Constructor
	public GUI()	{
		stockId = 1101;		
	}
	
	private void loadSubitem() {
		trade = new Trade();
		siap = new StockInfoAddPersonalInfo();
	}
	
    public void createGUI() {
		Container c = getContentPane();
		setTitle("JUSTOCK 就是股");
		
		// get the screen size
		desktop = new JDesktopPane();		
		desktop.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);		
		desktop.setBackground(new Color(230, 230, 230));
		c.add(desktop, BorderLayout.CENTER);
		
		// 載入自選股看盤
		createFrame(Real[0]);
		frame.setLocation(Double.valueOf(user_screen_size.getWidth()*0.001).intValue(), Double.valueOf(user_screen_size.getHeight()*0.005).intValue());
		frame.setSize(Double.valueOf(user_screen_size.getWidth()*0.44).intValue(), Double.valueOf(user_screen_size.getHeight()*0.2).intValue());
		
		// 載入大盤資訊圖
		createFrame(Real[1]);
		frame.setLocation(Double.valueOf(user_screen_size.getWidth()*0.446).intValue(), Double.valueOf(user_screen_size.getHeight()*0.0065).intValue());
		frame.setSize(Double.valueOf(user_screen_size.getWidth()*0.556).intValue(), Double.valueOf(user_screen_size.getHeight()*0.7).intValue());
		
		// 載入店頭市場資訊圖
		createFrame(Real[2]);
		frame.setLocation(Double.valueOf(user_screen_size.getWidth()*0.001).intValue(), Double.valueOf(user_screen_size.getHeight()*0.205).intValue());
		frame.setSize(Double.valueOf(user_screen_size.getWidth()*0.44).intValue(), Double.valueOf(user_screen_size.getHeight()*0.5).intValue());
		
		// 產生工具列
		toolbar = new JToolBar("股票代碼");
		stockLabel = new JLabel("股票代碼");
		stockLabel.setBorder(new EtchedBorder());
		toolbar.add(stockLabel);
		
		stockBox = StockBox.create();
//		stockBox.setEditable(true);
		stockBox.setActionCommand(Real[3]);
		stockBox.getEditor().addActionListener(new StockBoxListener());
//		stockBox.addMouseListener(new StockBoxListener());
//		stockBox.addActionListener(new StockBoxListener());
//		stockBox.addKeyListener(new StockBoxListener());
		stockBox.setSelectedIndex(StockBox.searchIndexOfStockId(stockId));
		toolbar.add(stockBox);
		
		// 增加自選股按鈕
		addPersonalStockBtn = new JButton("加入自選股");
		addPersonalStockBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				siap.addToPersonalStock(((String)stockBox.getSelectedItem()).substring(0, 4));
				psi.updateData();
			}
		});
		toolbar.add(addPersonalStockBtn);

		logo = new ImageIcon("common/source/images/logo.gif");
		logoLabel = new JLabel(logo);
        logoLabel.setSize(20, 100);
        
		toolbar.add(logoLabel);

		c.add(toolbar, BorderLayout.NORTH);
		
		// 產生交易面板
		c.add(trade.getPane(), BorderLayout.SOUTH);
		
		// 設置MenuBar
		setJMenuBar(createMenuBar());
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(user_screen_size);
	}

	// 選單配置
	private JMenuBar createMenuBar() {
		menubar = new JMenuBar();
		real = new JMenu("即時行情");
		for(int i=0 ; i<Real.length ; i++)	{
			item = new JMenuItem(Real[i]);
			item.setActionCommand(Real[i]);
			item.addActionListener(this);
			real.add(item);
		}		
		trans = new JMenu("交易");
		for(int i=0 ; i<Trans.length ; i++)	{
			item = new JMenuItem(Trans[i]);
			item.setActionCommand(Trans[i]);
			item.addActionListener(this);
			trans.add(item);
		}
		analysis = new JMenu("盤後分析");
		for(int i=0 ; i<Analysis.length ; i++) {
			item = new JMenuItem(Analysis[i]);
			item.setActionCommand(Analysis[i]);
			item.addActionListener(this);
			analysis.add(item);
		}
		
		// 產生sunItem
		FinanMenu = new JMenu("財務分析");
		for(int i=0 ; i<subFinancialItem.length ; i++)  {
			item = new JMenuItem(subFinancialItem[i]);
			item.setActionCommand(subFinancialItem[i]);
			item.addActionListener(this);
			FinanMenu.add(item);
		}
		analysis.add(FinanMenu);
		
		// 產生subItem
		EachStockList = new JMenu("個股當日明細表");
		for(int i=0 ; i<eachStockList.length ; i++) {
			item = new JMenuItem(eachStockList[i]);
			item.setActionCommand(eachStockList[i]);
			item.addActionListener(this);
			EachStockList.add(item);
		}
		analysis.add(EachStockList);
		
		member = new JMenu("會員管理");
		for(int i=0 ; i<Member.length ; i++)	{
			item = new JMenuItem(Member[i]);
			item.setActionCommand(Member[i]);
			item.addActionListener(this);
			member.add(item);
		}
		document = new JMenu("文件說明");
		for(int i=0 ; i<Document.length ; i++)	{
			item = new JMenuItem(Document[i]);
			item.setActionCommand(Document[i]);
			item.addActionListener(this);
			document.add(item);
		}
		
		order = new JMenu("顯示交易紀錄");
		for(int i=0 ; i<orderList.length ; i++) {
			item = new JMenuItem(orderList[i]);
			item.setActionCommand(orderList[i]);
			item.addActionListener(this);
			order.add(item);
		}
		
//		refresh = new JMenu("系統功能");
//		refresh.add(new ShowFrameAction(desktop));
				
		menubar.add(real);
		menubar.add(trans);
		menubar.add(analysis);
		menubar.add(member);
		menubar.add(document);
//		menubar.add(refresh);
		menubar.add(order);
		return menubar;
	}
	
    public void actionPerformed(ActionEvent action) 
    {
    	String actionCommand;
    	
//    	if (action.getActionCommand().equals(Real[3])) {
//    		int sid;
//    		if((sid = StockBox.getStockId(stockBox)) == -1)
//    			return;
//    		else
//    			stockId = sid;
//    	}
    	repaint();
    	actionCommand = action.getActionCommand();    	
    	createFrame(actionCommand);
    }

    public void createFrame(String actionCommand) {
    	frame = new MyInternalFrame(actionCommand);
		frame.setSize(screen);
    	frame.setVisible(true); //necessary as of 1.3
		try {
			
			if(actionCommand.equals(Member[0])) { // 產生個人基本資料視窗
				desktop.add(frame);
				ud = new UserData();
				frame.add(ud.getFrame());
				
				ud.okBtn.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent ae) {
						frame.dispose();
					}
				});
				frame.pack();
			} else if(actionCommand.equals(Real[0])) { // 產生自選。綜合看盤
				desktop.add(frame);
				psi = new PersonalStockInfo();
				frame.add(psi.getFrame());
				frame.setResizable(true);
				frame.pack();
			} else if (actionCommand.equals(Real[1])) { // 產生上市資訊線圖
				desktop.add(frame);
				try {
					og = new OverallGraph();
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
				
				frame.add(og);
			
			} else if (actionCommand.equals(Real[2])) { // 產生上櫃資訊線圖
				desktop.add(frame);
				try {
					og = new OverallGraph(true);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
				
				frame.add(og); 
			
			} else if (actionCommand.equals(Real[3])) {  // 產生個股資訊線圖
				desktop.add(frame);
				try {
					og = new OverallGraph(stockId);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
				
				frame.add(og);
			} else if (actionCommand.equals(Real[4])) {  // 產生分類報價表格
				desktop.add(frame);
				ssi = new SeperateStockInfo();				
				frame.add(ssi.getFrame());
				frame.setResizable(true);
				frame.pack();
			} else if (actionCommand.equals(Analysis[0])) // 產生個股技術分析圖
			{
				desktop.add(frame);
				try {
					tg = new TechGraph(stockId, GeneralGraph.TYPE_KBAR_CHART, GeneralGraph.TYPE_UNLIMITED);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
				frame.add(tg);				
			} else if(actionCommand.equals(Analysis[1])) { // 產生籌碼分析圖
				desktop.add(frame);
				ms = new MajorStockInfo();				
				frame.add(ms.getFrame());
				frame.setResizable(false);
				frame.pack();
			} else if(actionCommand.equals(Analysis[2]))  { // 產生個股資訊
				desktop.add(frame);
				bsi = new BasicStockInfo();				
				frame.add(bsi.getFrame());
				frame.setResizable(false);
				frame.pack();

			}else if(actionCommand.equals(subFinancialItem[0])) { // 產生財務報表 I -- Not Done
				desktop.add(frame);
				fs = new FinancialStockInfo();				
				frame.add(fs.getFrame());
				frame.setResizable(false);
				frame.pack();
			} else if(actionCommand.equals(subFinancialItem[1])) { // 產生財務報表 II -- Done
				desktop.add(frame);
				fs2 = new FinancialStockInfo_Ver2();				
				frame.add(fs2.getFrame());
				frame.setResizable(false);
				frame.pack();
			} else if(actionCommand.equals(subFinancialItem[2])) { // 產生財務比例表
				desktop.add(frame);
				fr = new FinancialRatioStockInfo();				
				frame.add(fr.getFrame());
				frame.setResizable(false);
				frame.pack();
			} else if(actionCommand.equals(subFinancialItem[3])) { // 產生股利狀況表
				desktop.add(frame);
				fb = new FinancialBonusStockInfo();				
				frame.add(fb.getFrame());
				frame.setResizable(false);
				frame.pack();
			} else if(actionCommand.equals(subFinancialItem[4])) { // 產生營收盈餘表
				desktop.add(frame);
				rf = new ReturnFinancialStockInfo();				
				frame.add(rf.getFrame());
				frame.setResizable(false);
				frame.pack();
			} else if(actionCommand.equals(subFinancialItem[5])) { // 產生價量明細表
				desktop.add(frame);
				pav = new PriceAndVolumnInfo();				
				frame.add(pav.getFrame());
				frame.setResizable(false);
				frame.pack();
			}else if(actionCommand.equals(subFinancialItem[6])) { // 產生損益表資訊
				desktop.add(frame);
				pas = new ProfitAndStatementStockInfo();				 
				frame.add(pas.getFrame());
				frame.setResizable(false);
				frame.pack();
			} else if(actionCommand.equals(eachStockList[0])) { // 產生當日成交量排行表
				desktop.add(frame);
				tvpd = new TradeVolumnPerDayInfo();
				frame.add(tvpd.getFrame());
				frame.setResizable(false);
				frame.pack();
			} else if(actionCommand.equals(eachStockList[1])) { // 產生當日成交明細表
				desktop.add(frame);
				tsi = new TransStockInfo();
				frame.add(tsi.getFrame());
				frame.setResizable(false);
				frame.pack();
			} else if(actionCommand.equals(eachStockList[2])) { // 產生當日成交金額排行表
				desktop.add(frame);
				tpi = new TradePriceInfo();
				frame.add(tpi.getFrame());
				frame.setResizable(false);
				frame.pack();
			} else if(actionCommand.equals(eachStockList[3])) { // 當日成交價排行表
				desktop.add(frame);
				pi = new PriceInfo();
				frame.add(pi.getFrame());
				frame.setResizable(false);
				frame.pack();
			} else if(actionCommand.equals(eachStockList[4])) { // 當日漲幅排行表 -- Not Done
				desktop.add(frame);
				pi = new PriceInfo();
				frame.add(pi.getFrame());
				frame.setResizable(false);
				frame.pack();
			} else if(actionCommand.equals(eachStockList[5])) { // 當日跌幅排行表
				desktop.add(frame);
				pL = new PriceLow();
				frame.add(pL.getFrame());
				frame.setResizable(false);
				frame.pack();
			} else if(actionCommand.equals(eachStockList[6])) { // 當日漲停股票表 -- Not Done
				desktop.add(frame);
				pi = new PriceInfo();
				frame.add(pi.getFrame());
				frame.setResizable(false);
				frame.pack();
			} else if(actionCommand.equals(eachStockList[7])) { // 當日跌停股票表 -- Not Done
				desktop.add(frame);
				pi = new PriceInfo();
				frame.add(pi.getFrame());
				frame.setResizable(false);
				frame.pack();
			} else if(actionCommand.equals(orderList[0])) { // 下單一覽表
				desktop.add(frame);
				oL = new OrderList();
				frame.add(oL.getFrame());
				frame.setResizable(false);
				frame.pack();
			} else if(actionCommand.equals(orderList[1])) { // 投資組合
				desktop.add(frame);
				icL = new InvestCombinationList();
				frame.add(icL.getFrame());
				frame.setResizable(false);
				frame.pack();
			} else if(actionCommand.equals(orderList[2]))	{ // 交易紀錄一覽表
				desktop.add(frame);
				oL = new OrderList();
				frame.add(oL.getFrame());
				frame.setResizable(false);
				frame.pack();
			}
			frame.setSelected(true); // 讓呈現出來的InternalFrame為最上層視窗
			
		} catch (java.beans.PropertyVetoException e) {}  		
    }
    
    public void setStockId (int sid) {
    	stockId = sid;
    }
    
    public static void main(String[] args) {
    	gui = new GUI();
    	
if(TESTMODE)
	UserData.setUserData("xddddd");
else { 
    	Login login = new Login();
//    	JPanel mask = new JPanel();
    	
    	login.setVisible(true);
    	login.toFront();
    	synchronized (gui) {
    		try {
				gui.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
} // end of TESTMODE

    	msgWindow.setVisible(true);
    	
//    	msg.setForeground(Color.WHITE);
//    	msg.setFont(new Font("微軟正黑體", Font.BOLD, 30));
    	
//    	mask.add(msg, BorderLayout.CENTER);
//    	mask.setSize(user_screen_size);
//    	mask.setBackground(Color.DARK_GRAY);
    	
//    	gui.desktop.add(mask, 0);
    	msg.setText("元件載入中...");
    	gui.loadSubitem();
    	msg.setText("使用者介面載入中...");
    	gui.createGUI();
    	msg.setText("載入完成！");
    	
    	msgWindow.dispose();
//		gui.desktop.remove(mask);
//		gui.setEnabled(true);

    	gui.setIconImage(frameIcon.getImage());
		gui.setVisible(true);
		gui.toFront();
		gui.stockBox.requestFocus();
//		gui.requestFocusInWindow();
//		gui.repaint();
    }

//    protected void paintComponent(Graphics g) {
//        super.paintComponents(g);
//        
//        
//    }
}


// 顯示所有視窗
@SuppressWarnings("serial")
class ShowFrameAction extends AbstractAction 
{
	  private JDesktopPane desk;
	  public ShowFrameAction(JDesktopPane desk) 
	  {
		  super("顯示所有視窗");
		  this.desk = desk;
	  }

	  public void actionPerformed(ActionEvent ev) 
	  {
		  JInternalFrame[] frames = desk.getAllFrames();
		  int count = frames.length;
		  if (count == 0)
			  return;

	    // Determine the necessary grid size
		  int sqrt = (int)Math.sqrt(count);
		  int rows = sqrt;
		  int cols = sqrt;
		  if (rows * cols < count) 
		  {
		    cols++;
		    if (rows * cols < count) 
		    	rows++;		    
		  }

	    // Define some initial values for size & location.
		  Dimension size = desk.getSize();

		  int w = size.width / cols;
		  int h = size.height / rows;
		  int x = 0;
		  int y = 0;

	    // Iterate over the frames, deiconifying any iconified frames and then
	    // relocating & resizing each.
		  for(int i=0 ; i<rows ; i++) 
		  {
			  for(int j=0 ; j<cols && ((i*cols)+j<count) ; j++) 
	    	  {
				  JInternalFrame f = frames[(i * cols) + j];
	    		  if (!f.isClosed() && f.isIcon()) 
	    		  {
	    			  try{
	    				  f.setIcon(false);
	    			  }catch(PropertyVetoException ignored) {}
	    		  }
	    			desk.getDesktopManager().resizeFrame(f, x, y, w, h);
	    			x += w;
	    		}
	    		y += h; // start the next row
	    		x = 0;
	      }
	  } // end actionPerformed()
}