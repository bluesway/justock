package software.DataRetrieve;

import java.sql.ResultSet;
import java.sql.SQLException;

import common.util.DateUtil;

import software.DB;
import software.GUI;

public class StockInfoOrderList
{
	String id;
	String [][] data;
	String [][] queryData;
	int row = 0;
	int column = 0; 
	
	public StockInfoOrderList() {
		try {
			getOrderData();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void getOrderData() throws SQLException {
    	ResultSet result = null;
        try {
        		result = DB.stmt.executeQuery("SELECT * from transaction where tUid='" + GUI.userData[0] + "' AND tDate='" + DateUtil.getDBDate(System.currentTimeMillis()) + "'");

        		if(result.last())
        		row = result.getRow();
        	
        	result.beforeFirst();
        	column = result.getMetaData().getColumnCount(); 

        	queryData = new String [row][column];
        	int j = 0;
        	while(result.next()) {
        		j++;
        		for(int i=1 ; i<=column ; i++) {
        			if(i == column) {
        				switch(Integer.valueOf(result.getString(i))) {
        					case -1: 
        						queryData[j-1][i-1] = "下單成功";
        						break;
        					case 0:
        						queryData[j-1][i-1] = "交易(撮合)成功";
        						break;
        					case 1:
        						queryData[j-1][i-1] = "失敗：帳戶餘額不足 (違約交割)";
        						break;
        					case 2:
        						queryData[j-1][i-1] = "失敗：有效期已過 (當天未能撮合成功)";
        						break;
        					case 3:
        						queryData[j-1][i-1] = "失敗：限價超出範圍 (超出漲停、跌停價)";
        						break;
        					case 4:
        						queryData[j-1][i-1] = "失敗：持有餘股不足 (賣出時)";
        						break;
        					case 5:
        						queryData[j-1][i-1] = "失敗：權限不足 (網路錯誤、資料庫異常)";
        						break;
        					case 6:
        						queryData[j-1][i-1] = "失敗：限制交易 (全額交割股)";
        						break;
        					case 127:
        						queryData[j-1][i-1] = "交割完成";
        						break;
        				}
        			}
        			else
        				queryData[j-1][i-1] = result.getString(i);
        		}
        	}

        } catch (SQLException e) {
            System.err.println(e);
        }
        
        result.close();
    }
	
	public int getRow() {
		return row;
	}
	
	public int getColumn() {
		return column;
	}
	
	public String[][] getData() {
		return queryData;
	}
}