/*
*      author by Kevingo
*      Data @ 2007/09/03
*      抓取當日成交金額排行資料  
*      
*/

package software.DataRetrieve;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.swing.JOptionPane;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;

public class StockInfoSeperateStock extends HTMLEditorKit.ParserCallback
{
    private boolean inHeader = false;
    private HTML.Tag tag;
    private int count,i,j, endCount = 0;
    private String stockId;
    public static int ROW = 50;
    public static int COLUMN = 10;
    private Object [][] data = new Object[ROW][COLUMN];
    
    // 建構式
    public StockInfoSeperateStock() {      
    	setTag(HTML.Tag.TD);
    }
    
    public void retrieveData (StockInfoSeperateStock sfy,boolean READY, int page) {
    	try{	
    			if(READY)
	    		{
	    			ParserGetter kit = new ParserGetter();
	    			HTMLEditorKit.Parser parser = kit.getParser();
	    			HTMLEditorKit.ParserCallback callback = sfy;
	        		if(READY && page == 1) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-01&sn=0"); // 水泥
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 2) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-02&sn=1"); // 食品
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 3) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-03&sn=2"); // 塑膠
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 4) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-04&sn=3"); // 紡織
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 5) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-05&sn=4"); // 電機
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 6) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-06&sn=5"); // 電纜
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 7) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-08&sn=6"); // 玻璃
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 8) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-09&sn=7"); // 造紙
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 9) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-10&sn=8"); // 鋼鐵
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 10) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-11&sn=9"); // 橡膠
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 11) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-12&sn=10"); // 汽車
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 12) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-14&sn=11"); // 營建
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 13) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-15&sn=12"); // 運輸
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 14) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-16&sn=13"); // 觀光
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 16) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-17&sn=14"); // 金融
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 18) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-18&sn=15"); // 百貨
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 19) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-19&sn=16"); // 綜合
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 20) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-20&sn=17"); // 其他
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 21) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-21&sn=18"); // 化學
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 22) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-22&sn=19"); // 生技醫療
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 23) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-23&sn=20"); // 油氣燃料
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 24) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-24&sn=21"); // 半導體
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 25) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-25&sn=22"); // 電腦週邊
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 26) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-26&sn=23"); // 光電
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 27) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-27&sn=24"); // 通信網路
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 28) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-28&sn=25"); // 電子零件組
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} else if(READY && page == 29) {
		    			URL u = new URL("http://pchome.syspower.com.tw/group/tick1.htm?typecat=0-29&sn=26"); // 電子通路
		    			InputStream in = u.openStream();
		    			InputStreamReader reader = new InputStreamReader(in);
		    			parser.parse(reader, callback, true);  
		    		} 
	    		}    		
    	} catch (IOException e) {
    			JOptionPane.showMessageDialog(null, "查無資料！請重新選擇");
    	}
    }
    
    public void setTag(HTML.Tag tag) {
    	this.tag = tag;
    }
    
    public void handleText(char[] text, int position) {
        if(inHeader) {
            String tmp = String.valueOf(text); 
            count++;
			if(count > 29) {
				i++;
				setData(tmp, i);
			}
        }
        endCount = count;
    }
    
    public void handleStartTag(HTML.Tag tag, MutableAttributeSet attributes, int position) {
        if(this.tag == tag) {
            this.inHeader = true;
        }
    }

    public void handleEndTag(HTML.Tag tag, int position) {
        if(this.tag == tag) {
            inHeader = false;
        }
    }
    
    public void setData(String tmp,int count) {
    	if(count%10 == 1 && count!=1)
    		j++;
    	data[j][(count-(10*j))-1] = tmp;
    }
    
    public Object[][] getData() {
    	return data;
    }
    
    public int getEndCount() {
    	return endCount;
    }
    
    public String getStockID() {
    	return stockId;
    }
}
