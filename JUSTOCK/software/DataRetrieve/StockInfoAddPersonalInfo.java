package software.DataRetrieve;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JOptionPane;

import software.DB;

public class StockInfoAddPersonalInfo {
	public int index;
	private int personStockLength = 50;
	public String [][] personalStockInfo;
	private PrintWriter file;
	private BufferedReader br;
	private File file2;
	private String str;
	private Scanner scn;
	
	public StockInfoAddPersonalInfo() {
		file2 = new File("personalStockInfo");
		str = "";

		readData();
	}
	
	public void readData() {
		checkFile();
		setToNull();
		readStockIdFromFile();
	}
	
	public void readStockIdFromFile() {
		index = 0;
		
		try {
			scn = new Scanner(file2, "UTF-8");

			while (scn.hasNext())
				addData(scn.nextLine());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
    // 將toolBar的個股加入自選股
    public void addToPersonalStock(String stockId) {
    	FileWriter fw;
    	
		try {
			fw = new FileWriter(file2, true);
			file = new PrintWriter(fw);
			file.println(stockId);
			
			addData(stockId);
			
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public void addData(String stockId) {
    	ResultSet result;
    	
    	if(index <= -1)
    		;
    	else if(index > personStockLength - 1) 
    		JOptionPane.showMessageDialog(null, "自選股已滿，請刪除現有自選股！");
    	else {
    		index++;
    		for(int i=0 ; i<personStockLength ; i++) {
    			try {
    				result = DB.stmt.executeQuery("SELECT * from info_current where cSid='" + stockId + "'");
    				
    				if(result.next()) {
    					personalStockInfo[index-1][0] = result.getString(1);
    					personalStockInfo[index-1][2] = result.getString(2); 
    					personalStockInfo[index-1][3] = result.getString(3); 
    					personalStockInfo[index-1][4] = result.getString(4); 
    					personalStockInfo[index-1][5] = result.getString(5); 
    					personalStockInfo[index-1][6] = result.getString(6); 
    				}
    				
    				result = DB.stmt.executeQuery("SELECT * from stock where Sid='" + stockId + "'");
    				if(result.next())
    					personalStockInfo[index-1][1] = result.getString("sName");
    					
    			}catch(SQLException e) {
    				System.err.println(e);
    			}
    		}
    	}
    }
    
//    public void openFile() {
//    	index = 0;
//    	try {
//    		FileReader fr = new FileReader(file2);
//    		data = new Stack<String>();
//    		
//    		br = new BufferedReader(fr);
//    		while((str = br.readLine())!=null) {
//    			data.push(str);
//    			index++;
//    		}
//    		br.close();
//    		fr.close();
//    		
//    		FileWriter fw = new FileWriter(file2);
//    		BufferedWriter bw = new BufferedWriter(fw);
//			file = new PrintWriter(bw);
//			
//			while(!data.isEmpty())
//				file.println(data.pop());
//			
//			bw.close();
//			
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//    }
    
    public void delPersonalStockInfo(int index2) {
    	boolean del = false;
    	ArrayList<String> data;
    	index = 0;
    	
    	try {
    		FileReader fr = new FileReader(file2);
    		data = new ArrayList<String>();
    		
    		br = new BufferedReader(fr);
    		while((str = br.readLine())!=null) {
    			if (index == index2 && !del) {
    				del = true;
    				continue;
    			}
    			
    			data.add(str);
    			index++;
    		}
    		br.close();
    		fr.close();
    		
    		FileWriter fw = new FileWriter(file2);
    		BufferedWriter bw = new BufferedWriter(fw);
			file = new PrintWriter(bw);
			
			while(!data.isEmpty())
				file.println(data.remove(0));
			
			file.close();
			bw.close();
			fw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
//    	file.println(index2);
//    	if(index <= 0 || index2 >= index) 
//    		;
//    	else {
//	    	index--;
//	    	int start = personStockLength-index-1;
//	    	if(index == 49) {
//	    		for(int i=0 ; i <7 ; i++)
//	    			personalStockInfo[index][i] = null; 
//	    	}else {
//	    		for(int i=0 ; i<start ; i++) { 
//	    			personalStockInfo[index+i][0] = personalStockInfo[index+i+1][0];
//	    			personalStockInfo[index+i][1] = personalStockInfo[index+i+1][1];
//	    			personalStockInfo[index+i][2] = personalStockInfo[index+i+1][2];
//	    			personalStockInfo[index+i][3] = personalStockInfo[index+i+1][3];
//	    			personalStockInfo[index+i][4] = personalStockInfo[index+i+1][4];
//	    			personalStockInfo[index+i][5] = personalStockInfo[index+i+1][5];
//	    			personalStockInfo[index+i][6] = personalStockInfo[index+i+1][6];
//	    		}
//	    	}
//    	}
//    	file.close();
    }
    
    public void delFile() {
		file2.delete();
    }
    
    public void checkFile() {
    	if (!file2.exists())
    		createNewFile();
    }
    
    public void createNewFile() {
    	try {
			if(!file2.createNewFile()) {
				JOptionPane.showMessageDialog(null, "自選股檔案建立失敗，系統即將關閉...", "系統狀態", JOptionPane.ERROR_MESSAGE);
				System.exit(0);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public void setToNull() {
    	personalStockInfo = new String[50][7];
    }

    public Object[][] getData() {
    	readData();
    	
    	return (Object[][]) personalStockInfo;
    }
}