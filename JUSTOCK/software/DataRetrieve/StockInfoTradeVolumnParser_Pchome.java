/*
 * 當日成交量排行資訊
 * from pchome
 * 
 * 20070829
 * 
 * */

package software.DataRetrieve;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.swing.JOptionPane;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;

public class StockInfoTradeVolumnParser_Pchome extends HTMLEditorKit.ParserCallback
{
    private boolean inHeader = false;
    private HTML.Tag tag;
    private int count,i,j, endCount = 0;
    private Object [][] data = new Object[30][10];
    
    // 建構式
    public StockInfoTradeVolumnParser_Pchome() {      
    	setTag(HTML.Tag.TD);
    }
    
    public void retrieveData(StockInfoTradeVolumnParser_Pchome sty, boolean READY, int page) {
    	try{	
			ParserGetter kit = new ParserGetter();
			HTMLEditorKit.Parser parser = kit.getParser();
			HTMLEditorKit.ParserCallback callback = sty;	
    		if(READY && page == 1)
	    		{
	    			URL u = new URL("http://pchome.syspower.com.tw/top/?tops=0-0-0");
	    			InputStream in = u.openStream();
	    			InputStreamReader reader = new InputStreamReader(in);
	    			parser.parse(reader, callback, true);  
	    		} else if(READY && page == 2) {
	    			URL u = new URL("http://pchome.syspower.com.tw/top/?tops=0-0-1");
	    			InputStream in = u.openStream();
	    			InputStreamReader reader = new InputStreamReader(in);
	    			parser.parse(reader, callback, true);  
	    		} else if(READY && page == 3) {
	    			URL u = new URL("http://pchome.syspower.com.tw/top/?tops=0-0-2");
	    			InputStream in = u.openStream();
	    			InputStreamReader reader = new InputStreamReader(in);
	    			parser.parse(reader, callback, true);  
	    		}
    	} catch (IOException e) {
    			JOptionPane.showMessageDialog(null, "查無資料！請重新選擇");
    	}
    }
    
    public void setTag(HTML.Tag tag) {
    	this.tag = tag;
    }
    
    public void handleText(char[] text, int position) {
        if(inHeader) {
            String tmp = String.valueOf(text);    
            count++;
    			if(count>24 && count<325) {
    				i++;
    				setData(tmp, i);
    			}
        }
    }
    
    public void handleStartTag(HTML.Tag tag, MutableAttributeSet attributes, int position) {
        if(this.tag == tag) {
            this.inHeader = true;
        }
    }

    public void handleEndTag(HTML.Tag tag, int position) {
        if(this.tag == tag) {
            inHeader = false;
        }
    }
    
    public void setData(String tmp,int count) {
    	if(count%10 == 1 && count!=1)
    		j++;
    	data[j][count-(10*j)-1] = tmp;
    }
    
    public Object[][] getData() {
    	return data;
    }
    
    public int getEndCount() {
    	return endCount;
    }
}