/*
*      author by Kevingo
*      Data @ 2007/07/30
*      抓取籌碼分析資料  
*      
*/
package software.DataRetrieve;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;

public class StockInfoMajorParser_Yahoo extends HTMLEditorKit.ParserCallback
{
    private boolean inHeader = false;
    private HTML.Tag tag;
    private int count,i,j = 0;
    private Object [][] data = new Object[15][8];
    
    // 建構式
    public StockInfoMajorParser_Yahoo() {      
    	setTag(HTML.Tag.TD);
    }
    
    public void retrieveData (StockInfoMajorParser_Yahoo smy, String stockId, boolean READY) {
    	try{	
	    		if(READY)
	    		{
	    			ParserGetter kit = new ParserGetter();
	    			HTMLEditorKit.Parser parser = kit.getParser();
	    			HTMLEditorKit.ParserCallback callback = smy;
	    			URL u = new URL("http://tw.stock.yahoo.com/d/s/major_" + stockId + ".html");  
	    			InputStream in = u.openStream();
	    			InputStreamReader reader = new InputStreamReader(in);
	    			parser.parse(reader, callback, true);    			
	    		}    		
    	} catch (IOException e) {
    			
    	}
    }
    
    public void setTag(HTML.Tag tag) {
    	this.tag = tag;
    }
    
    public void handleText(char[] text, int position) {
		if(inHeader) {
			String tmp = String.valueOf(text);
			count++;
			if(count>71 && count<192 ) {
				i++;
				setData(tmp,i);
			}
		}
    }
    
    public void handleStartTag(HTML.Tag tag, MutableAttributeSet attributes, int position) {
        if(this.tag == tag) {
            this.inHeader = true;
        }
    }

    public void handleEndTag(HTML.Tag tag, int position) {
        if(this.tag == tag) {
            inHeader = false;
        }
    }
    
    public void setData(String tmp,int count) {
    	if(count%8 == 1 && count!=1)
    		j++;
    	data[j][(count-(8*j))-1] = tmp;
    }
    
    public Object[][] getData() {
    	return data;
    }
}