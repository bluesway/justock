/**
 * @author bluesway
 * TARGET 圖形繪製的基礎模型
 */

package software.Subitem;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Arrays;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
//import javax.swing.JTextArea;

import common.util.DateUtil;
import common.util.StockBox;
import software.DB;
import software.GUI;
import software.Subitem.Listener.TradeListener;

@SuppressWarnings("serial")
public abstract class GeneralGraph extends JPanel {
	public static final int TYPE_LINE_CHART = 0;
	public static final int TYPE_KBAR_CHART = 1;
	public static final int TYPE_BIN_CHART = 2;
	public static final int TYPE_LIMITED = 0;
	public static final int TYPE_UNLIMITED = 1;
	
    // 繪圖用變數
    protected int screen_x;
    protected int screen_y;
    protected Rectangle graphD;	// 線圖邊框範圍
    protected int line_cap;				// 框線寬度
    protected int start_main;			// 主線圖上緣
    protected int height_main;		// 主線圖高度
    protected int start_quan;			// 成交量上緣
    protected int height_quan;		// 成交量高度
    protected int start_tech1;			// 技術分析1上緣
    protected int height_tech1;		// 技術分析1高度
    protected int start_tech2;			// 技術分析2上緣
    protected int height_tech2;		// 技術分析2高度
    protected int left_space;			// 顯示指數、成交量資訊的寬度
    protected int right_space;		// 顯示指標資訊的寬度
    protected int char_height;		// 預設字高
    protected int limiteType;			// 限制類別 0:有7%限制 1:無7%限制 
	protected int displayType;		// 顯示型態 0:折線圖 1:K線圖 2:兩者同時顯示
    protected int scale;						// 線圖縮放級距
    protected int dateRange;			// 線圖日期/時間單位
    protected int dateType;			// 線圖日期型式 0:日線 1:週線 2:週線 3:分線
	protected double ratio;				// 數值在圖表中的相對比例
	protected NumberFormat nf;
	protected int mouse_x;
	protected int mouse_y;
	
	// container 元件變數
	protected JPanel controlPanel;		// 控制面板
	protected int cpl_height;					// 控制面板高度
	protected JButton[] defaultBtn;				// 預設按鈕
	protected JComboBox stockBox;	// 股票選擇
	protected MouseListener[] defaultML;
	protected JPopupMenu metaMenu;
	protected JMenuItem[] menuItem;

	// 資料處理用變數
	protected int stockId;					// 繪圖資料來源 - 0:大盤 id:個股
	protected LinkedList<double[]> sourceData;		// 原始資料
	protected boolean isOverall;		// 是否為當日資料
    protected String[] date_mark = new String[3];	// 日期字串
    protected String[] time_mark = new String[2];	// 時間字串
    protected int priceNum;				// 指數參考資料顯示筆數 (須為單數筆且大於3)
    protected int quanNum;				// 成交量參考資料顯示筆數 (須為單數筆)
    protected double[] data;				// 圖表繪製range資料
	protected double[] priceInitial;	// 指數參考資料值
	protected double[] quanInitial;	// 成交量參考資料值
	protected double priceSep;			// 指數參考資料區間
	protected double quanSep;			// 成交量參考資料區間
	protected double priceUnit;		// 股價升降單位
	protected double[] prev;				// 前項指標
	protected double[] now;				// 當項指標
	protected int avgNum;					// 均線數量 (包含指數及成交量，須為雙數)
	protected double[] avgPrev;		// 前項均值
	protected double[] avgCurr;		// 當項均值
	protected int time;							// 當前處理時間
	protected String sName;				// 股票名稱
	protected int range_start;				// 資料讀取起點
	protected int range;						// 資料讀取區間
	protected int shift;							// 資料移動區間
	
	// 資料庫用變數
    public ResultSet result;

    public GeneralGraph() {
		// 數字格式
    	nf = NumberFormat.getInstance();
    	nf.setMaximumFractionDigits(2);
    	
    	// 變數處理
    	graphD = new Rectangle();
    	
    	// 控制台配置
    	defaultBtn = new JButton[6];
    	defaultML = new MouseListener[4];
    	defaultML[0] = new scaleListener();
    	defaultML[1] = new shiftListener();
    	defaultML[2] = new metaMenuListener();
    	defaultML[3] = new outputListener();
    	defaultBtn[0] = new JButton("+");		// 放大線圖
    	defaultBtn[1] = new JButton("-");		// 縮小線圖
    	defaultBtn[0].addMouseListener(defaultML[0]);
    	defaultBtn[1].addMouseListener(defaultML[0]);
    	defaultBtn[2] = new JButton("<");		// 向前平移
    	defaultBtn[3] = new JButton(">");		// 向後平移
    	defaultBtn[2].addMouseListener(defaultML[1]);
    	defaultBtn[3].addMouseListener(defaultML[1]);
    	defaultBtn[4] = new JButton("下單");		// 股票下單
    	defaultBtn[4].setActionCommand(String.valueOf(stockId));
    	defaultBtn[4].addActionListener(new TradeListener());
    	defaultBtn[5] = new JButton("輸出");		// 圖檔輸出
    	defaultBtn[5].addMouseListener(defaultML[3]);

    	metaMenu = new JPopupMenu("右鍵選單");
    	menuItem = new JMenuItem[3];
    	metaMenu.add(menuItem[0] = new JMenuItem("日線"));
    	metaMenu.add(menuItem[1] = new JMenuItem("週線"));
    	metaMenu.add(menuItem[2] = new JMenuItem("月線"));
    	for (int i = 0; i < 3; i++) {
    		menuItem[i].setActionCommand(String.valueOf(i));
    		menuItem[i].addActionListener(new menuItemListener());
    	}
    	
    	// 其它設定
		addMouseListener(defaultML[2]);
        addMouseMotionListener(new timeLineListener());
        
    	setBackground(Color.BLACK);
		setPreferredSize(GUI.screen);
		setVisible(true);
    }
    
    protected void createBox() {
    	stockBox = StockBox.create();
//		stockBox.getEditor().addActionListener(new StockBoxListener());
//		stockBox.addActionListener(new StockBoxListener());
		stockBox.getEditor().addActionListener(new stockAction());
//    	stockBox.addMouseListener(new StockBoxListener());
    }
    
    protected void readData() {
		long a = System.currentTimeMillis();
		
		sourceData.clear();
		
		int dayCount = 0;
		String date1 = "";
		String date2 = "";
		double result_info[] = new double[6];

        try {
            result = DB.stmt.executeQuery("SELECT * FROM info_history WHERE hSid='" + 
            		String.valueOf(stockId) + "' ORDER BY hDate ASC");
            
            while (result.next()) {
            	if (dateType != 0) {
            		date1 = date2;
            		date2 = numToDate(result.getInt(2));
            	}
            	
            	// 週線 or 月線
            	if ((dateType == 1 && !DateUtil.isSameWeek(date1, date2)) ||
            			(dateType == 2 && !DateUtil.isSameMonth(date1, date2))) {
        			sourceData.add(result_info.clone());
        			dayCount = 0;
        			for (int i = 1; i < 6; i++)
        				result_info[i] = 0;
            	}

            	if (dayCount == 0) {
            		result_info[0] = result.getInt(2);
            		result_info[1] = result.getDouble(3);
            	}
            	
            	if (dateType == 0) {
	            	result_info[5] = result.getInt(7) / 100.0; 
	            	for (int i = 1; i < 5; i++)
		            	result_info[i] = result.getDouble(i+2);
        			sourceData.add(result_info.clone());
            	} else {
            		if (result_info[2] < result.getDouble(4))
            			result_info[2] = result.getDouble(4);
            		if (result_info[3] > result.getDouble(5) || result_info[3] <= 0.0)
            			result_info[3] = result.getDouble(5);
            		result_info[4] = result.getDouble(6);
	            	result_info[5] += result.getInt(7) / 100.0; 
	            	dayCount++;
            	}
            }
            result = DB.stmt.executeQuery("SELECT * FROM stock WHERE Sid='" + 
            		String.valueOf(stockId) + "'");
        	sName = result.next() ? result.getString(2) : "";
        } catch (SQLException e) {
        	System.err.println(e);
        }
        
		a = System.currentTimeMillis() - a;
		System.out.println("資料已讀取，共花費" + a / 1000.0 + "秒");
    }
    
    protected void setData() {
		long a = System.currentTimeMillis();
    	int tmp = sourceData.size();
    	
    	if (!isOverall) {
    		range_start = tmp - range + shift;
    		if (range_start <= 0)
    			range_start = 0;
    		time = range_start;

    		if (range_start < 0)
    			range_start = 0;
    		if (tmp - range_start + 1 < range)
    			range = tmp - range_start;
    		data = new double[range];
    	}
    	else {
    		time = 0;
    		range = tmp;
    		data = new double[tmp];
    	}
    	
    	prev = sourceData.get(time++);
    	
    	// 處理指數部分
    	priceInitial = new double[priceNum + 1];
    	
		for (int i = 0; i < range; i++)
			if (isOverall)
				data[i] = sourceData.get(i)[1];
			else
				data[i] = sourceData.get(range_start + i)[4];
		
		if (limiteType == TYPE_LIMITED) {
			priceInitial[0] = data[0];
			Arrays.sort(data);
			if (priceInitial[0] - data[0] > data[data.length - 1] - priceInitial[0]) {
				priceInitial[1] = priceInitial[0] * 2 - data[0];
				priceInitial[priceNum] = data[0];
			} else {
				priceInitial[1] = data[data.length - 1];
				priceInitial[priceNum] = priceInitial[0] * 2 - data[data.length - 1];
			}
			priceSep = (priceInitial[1] - priceInitial[priceNum]) / (priceNum - 1) * 1.0;
			
			for (int i = priceNum - 1; i > 1; i--)
				priceInitial[i] = priceInitial[1] - priceSep * (i-1);
		} else {
			Arrays.sort(data);
    	
//			priceInitial[0] = priceInitial[(priceNum + 1) / 2] = (data[0] + data[data.length - 1]) / 2.0;
			priceInitial[1] = data[data.length - 1];
			priceInitial[priceNum] = data[0];
			priceSep = (priceInitial[1] - priceInitial[priceNum]) / (priceNum - 1) * 1.0;
			
			for (int i = priceNum - 1; i > 1; i--)
				priceInitial[i] = priceInitial[1] - priceSep * (i-1);
			
//			priceInitial[1] = priceInitial[2] + priceSep;
//			priceInitial[priceNum] = priceInitial[priceNum - 1] - priceSep;
		}
    	
    	// 處理成交量部分
    	quanInitial = new double[quanNum + 1];
    	
		for (int i = 0; i < range; i++)
			if (isOverall)
				if (stockId == 0)
					data[i] = sourceData.get(i)[3] / 100000000.0;
				else
					data[i] = sourceData.get(i)[3];
			else
				data[i] = sourceData.get(range_start + i)[5];
		
    	Arrays.sort(data);
    	
//    	quanInitial[0] = quanInitial[(quanNum + 1) / 2] = (data[0] + data[data.length - 1]) / 2.0;
    	quanInitial[1] = data[data.length - 1];
    	quanInitial[quanNum] = 0;
    	
    	quanSep = quanInitial[1] / (quanNum - 1) * 1.0;
    	
    	for (int i = quanNum - 1; i > 1; i--) {
//    		if (i == (quanNum + 1) / 2)
//    			continue;
    		
    		quanInitial[i] = quanInitial[1] - quanSep * (i-1);
    	}
    	
//    	for (int i = 107; i > 92; i--)
//    		initial[108-i] = initial[0] * i / 100;
    	
//    	shift = 0;
		a = System.currentTimeMillis() - a;
//		System.out.println("資料已處理，共花費" + a / 1000.0 + "秒");
    }
    
    public void paintComponent(Graphics page) {
    	setData();

    	// 清除背景
		page.setColor(Color.BLACK);
		page.fillRect(0, 0, screen_x, screen_y);

//        page.dispose();
//        System.out.println(range);
//        System.out.println(range_start);
//        System.out.println(shift);
//        page.drawImage(image, getInsets().left, getInsets().top, this);
    }
    
//    public void update(Graphics g)
//    {
//    		paintComponent(g);
//    }
    
    // 畫虛線
    protected void drawBrokenLine (Graphics page, int x0, int y0, int x1, int y1) {
        int b_width;				// 虛線寬
    	int b_height;				// 虛線高
    	int tmpX;					// 曲線圖 x 軸分段繪製坐標
    	int tmpY;					// 曲線圖 y 軸分段繪製坐標
    	
    	b_width = (x1 - x0) / 5;
    	b_height = (y1 - y0) / 5;
    	tmpX = x0;
    	tmpY = y0;
    	
    	while (true) {
    		if (tmpX + (b_width == 0 ? 0 : 5) > x1 || tmpY + (b_height == 0 ? 0 : 5) > y1 || (b_width == 0 && b_height == 0))
    			break;
    		page.drawLine(tmpX, tmpY, tmpX + (b_width == 0 ? 0 : 5), tmpY + (b_height == 0 ? 0 : 5));
    		tmpX += (b_width == 0 ? 0 : 5) * 2;
    		tmpY += (b_height == 0 ? 0 : 5) * 2;
    	}
    }
    
    // 畫折線圖
    protected void drawLineChart (Graphics page, double dataPrev, double dataCurr, int x, int scale) {
    	if (dataCurr < dataPrev)
    		page.setColor(Color.CYAN);
//    	else if (dataCurr == dataPrev)
//    		if (dataCurr == priceInitial[1])
//    			page.setColor(Color.RED);
//    		else if (dataCurr == priceInitial[priceNum])
//    			page.setColor(Color.GREEN);
//    		else;
    	else
    		page.setColor(Color.WHITE);
    	
        page.drawLine(x, calculateAxis(dataPrev, 0), x + scale, calculateAxis(dataCurr, 0));
    }
    
    // 畫 K 線圖
    protected void drawKBarChart (Graphics page, double[] data, int x, int scale) {
		if (data[4] > data[1]) {
			page.setColor(Color.RED);
	        page.fillRect(x, calculateAxis(data[4], 0), scale, calculateAxis(data[1], 0) - calculateAxis(data[4], 0));
	        page.drawLine(x + (scale - 1) / 2, calculateAxis(data[2], 0), x + (scale - 1) / 2, calculateAxis(data[3], 0));
		}
		else if (data[4] < data[1]) {
			page.setColor(Color.GREEN);
	        page.fillRect(x, calculateAxis(data[1], 0), scale, calculateAxis(data[4], 0) - calculateAxis(data[1], 0));
	        page.drawLine(x + (scale - 1) / 2, calculateAxis(data[2], 0), x + (scale - 1) / 2, calculateAxis(data[3], 0));
		}
		else {
			page.setColor(Color.WHITE);
	        page.drawLine(x, calculateAxis(data[1], 0), x + scale, calculateAxis(data[1], 0));
	        page.drawLine(x + (scale - 1) / 2, calculateAxis(data[2], 0), x + (scale - 1) / 2, calculateAxis(data[3], 0));
		}
	}
    
    // 畫成交量圖
    protected void drawQuanChart (Graphics page, double data, int x, int scale) {
    	int y = calculateAxis(data, 1);
        page.setColor(new Color(255, 156, 0));
        page.fillRect(x, y, scale, graphD.y + start_quan + height_quan - y);	// 畫柱圖
        page.setColor(Color.WHITE);
        page.drawLine(x += scale > 1 ? 0 : scale, y, x, graphD.y + start_quan + height_quan - line_cap);	// 畫柱圖分隔線
	}
    
    // 畫均線圖
    protected void drawAvgLine (Graphics page, int count, int len, int drawType, int index, int x, int scale, Color color) {
    	int use = drawType * avgNum / 2 + count - 1;
    	if (index - range_start < len)
    		return;
    	else if (avgPrev[use] == 0.0) {
	    	for (int i = 0; i < len; i++)
	    		if (drawType == 0)
	    			avgPrev[use] += sourceData.get(index - i - 1)[4];
	    		else
	    			avgPrev[use] += sourceData.get(index - i - 1)[5];
	    	
    	}
    	
		if (drawType == 0)
			avgCurr[use] = avgPrev[use] + sourceData.get(index)[4] - sourceData.get(index - len)[4];
		else
			avgCurr[use] = avgPrev[use] + sourceData.get(index)[5] - sourceData.get(index - len)[5];
		
    	page.setColor(color);
        page.drawLine(x, calculateAxis(avgPrev[use] / len, drawType), x + scale, calculateAxis(avgCurr[use] / len, drawType));
        
        // 文字說明
        if (index  == range_start + range - 1)
	        if (drawType == 0)
	        	page.drawString("Avg" + len + "=" + nf.format(avgCurr[use] / len), graphD.x + graphD.width + line_cap, 
	        			graphD.y + start_main + line_cap + char_height * count);
	        else
	        	page.drawString("Avg" + len + "=" + nf.format(avgCurr[use] / len), graphD.x + graphD.width + line_cap, 
	        			graphD.y + start_quan + line_cap + char_height * count);
        
        avgPrev[use] = avgCurr[use];
    }
    
    // 標示線圖資訊 (指數及成交量)
    protected void drawChartLabel (Graphics page, int drawType) {	// 0:指數 1:成交量
    	int y;
    	int showNum = drawType == 0 ? priceNum : quanNum;
    	double[] showString = drawType == 0 ? priceInitial : quanInitial;
    	
		for (int i = 0; i < showNum; i++) {
			if (limiteType == TYPE_UNLIMITED)
				if (drawType == 0)
					page.setColor(Color.WHITE);
				else
					page.setColor(new Color(255, 156, 0));
			else if (drawType == 0)
	    		if (i == 0 && showString[1] > showString[0] * 1.07 - calculateUnit(showString[0]))
	                page.setColor(Color.RED);
	    		else if (i == priceNum - 1 && showString[priceNum] < showString[0] * 0.93 + calculateUnit(showString[0]))
	                page.setColor(new Color(0, 128, 0));
	    		else
	                page.setColor(Color.WHITE);
			else
				page.setColor(new Color(255, 156, 0));
			
			if (i < showNum - 1 || drawType != 1)
				page.drawString(nf.format(showString[i + 1]), 0, calculateAxis(-1.0 * i - 1, drawType));
			
			if (limiteType == TYPE_UNLIMITED)
	        	if (drawType == 0 && i == (showNum + 1) / 2)
	        		page.setColor(Color.LIGHT_GRAY);
	        	else
	        		page.setColor(Color.DARK_GRAY);
			else if (drawType == 1)
				page.setColor(Color.DARK_GRAY);
			else if (i == 0)
				page.setColor(new Color(128, 0, 0));
			else if (i == (showNum - 1) / 2)
				page.setColor(Color.LIGHT_GRAY);
			else if (i == showNum - 1)
				page.setColor(new Color(0, 128, 0));
			else
				page.setColor(Color.DARK_GRAY);
			
			y = calculateAxis(showString[i + 1], drawType);
			drawBrokenLine(page, graphD.x, y, graphD.x + graphD.width, y); // 畫橫虛線
		}
		
		if (drawType == 1) {
			page.setColor(new Color(255, 156, 0));
			page.drawString(stockId == 1000 || stockId == 0 || stockId == 4000 ? "億(新台幣)" : "張", 
					0, graphD.y + start_quan + height_quan + char_height / 2);
		}
    }
    
    // 標示線圖資訊 (時間)
    protected void drawChartLabel (Graphics page, int x, double date) {
    	page.setColor(Color.LIGHT_GRAY);
    	page.drawString(numToDate(date), x - GUI.overall_size[13], screen_y - 1);	// 秀出時間資訊
    	page.setColor(Color.DARK_GRAY);
    	drawBrokenLine(page, x, graphD.y, x, graphD.y + graphD.height);	// 畫縱虛線
    }
    
    // 標示滑鼠指標資訊
    protected void drawMouseInfo (Graphics2D page, boolean drawQuan) {
        try {
			int data;
			double[] currData;

			if (mouse_x > graphD.x && mouse_x < graphD.x + graphD.width &&	// 判斷滑鼠有沒有在作動範圍內
					mouse_y >= graphD.y + line_cap / 2 && mouse_y <= graphD.y + graphD.height - line_cap / 2) {
				// 顯示個股基本資料
				currData = mouse_x == -1 ?
						sourceData.getLast() : sourceData.get((int) Math.round((mouse_x - graphD.x - line_cap / 2) / scale + range_start + 1));

				page.setColor(Color.YELLOW);
				page.drawLine(graphD.x - line_cap / 2, mouse_y, 
						graphD.x + graphD.width - line_cap / 2, mouse_y);	// 畫滑鼠指數線
				page.drawLine(mouse_x, graphD.y + line_cap / 2, 
						mouse_x, graphD.y + graphD.height - line_cap / 2); 	// 畫滑鼠時間線
				
				page.setColor(Color.WHITE);
				if (mouse_y < graphD.y + start_quan + height_quan)
					page.fillRect(0, mouse_y - char_height / 2 - 1, graphD.x - line_cap / 2, char_height + 2);		// 畫指數軸欄位
				if (mouse_x + GUI.overall_size[14] > graphD.x + graphD.width) {			// 避免圖形繪出範圍外
					page.fillRect(mouse_x - GUI.overall_size[26], graphD.y + graphD.height + line_cap / 2 + 1, 
							GUI.overall_size[26], char_height + 2);	// 畫時間軸欄位
					page.setColor(Color.BLACK);
					page.drawString(numToDate(currData[0]), mouse_x - GUI.overall_size[25], screen_y - 1);		// 秀出時間軸時間
				} else if (mouse_x - GUI.overall_size[14] < GUI.overall_size[20]) {
					page.fillRect(mouse_x, graphD.y + graphD.height + line_cap / 2 + 1, GUI.overall_size[26], 
							char_height + 2);	// 畫時間軸欄位
					page.setColor(Color.BLACK);
					page.drawString(numToDate(currData[0]), mouse_x + GUI.overall_size[1], screen_y - 1);		// 秀出時間軸時間
				} else {
					page.fillRect(mouse_x - GUI.overall_size[13], graphD.y + graphD.height + line_cap / 2 + 1, 
							GUI.overall_size[26], char_height + 2);	// 畫時間軸欄位
					page.setColor(Color.BLACK);
					page.drawString(numToDate(currData[0]), mouse_x - GUI.overall_size[12], screen_y - 1);		// 秀出時間軸時間
				}

				if (drawQuan && mouse_y > graphD.y + (start_main + height_main + start_quan) / 2) {	// 判斷是價還是量
					data = 5;
					
					if (mouse_y < graphD.y + start_quan + height_quan) {
//						return;
						page.setColor(new Color(0, 99, 255));
						page.drawString(nf.format(calculateQuan(mouse_y)), 0, mouse_y + char_height / 2);	// 秀出成交量軸資訊
						page.setColor(Color.WHITE);
					}
				}
				else {
					data = 4;
					page.setColor(Color.BLACK);
					page.drawString(nf.format(calculatePrice(mouse_y)), 0, mouse_y + char_height / 2);	// 秀出指數軸資訊
					page.setColor(Color.YELLOW);
				}
				
				// 顯示十字游標上的資訊
				if (mouse_y < graphD.y + start_quan + height_quan) {
					String currdata = nf.format(currData[data]);
					if (mouse_x + GUI.overall_size[18]< graphD.x + graphD.width)			// 避免字串繪出範圍外
						if (mouse_y < graphD.y + line_cap / 2 + char_height)
							page.drawString(currdata, mouse_x - GUI.overall_size[18] - 1, mouse_y + char_height + 1);	// 秀出股價資訊
						else
							page.drawString(currdata, mouse_x + 1, mouse_y - 1);		// 秀出股價資訊
					else
						if (mouse_y < graphD.y + line_cap / 2 + char_height)
							page.drawString(currdata, mouse_x - GUI.overall_size[18] - 1, mouse_y + char_height + 1);	// 秀出股價資訊
						else
							page.drawString(currdata, mouse_x - GUI.overall_size[18] - 1, mouse_y - 1);	// 秀出股價資訊
				}
			}
			else
				currData = sourceData.getLast();

			page.setColor(Color.CYAN);
			page.drawString(String.valueOf(stockId), screen_x * 0.01f, GUI.overall_size[19]);
			page.drawString(sName, screen_x * 0.01f, GUI.overall_size[24]);
			page.setColor(Color.WHITE);
			page.drawString("日期 " + numToDate(currData[0]), screen_x * 0.17f, GUI.overall_size[19]);
			page.drawString("開盤 " + nf.format(currData[1]), screen_x * 0.47f, GUI.overall_size[19]);
			if (currData[4] > currData[1])
				if (currData[4] > currData[1] * 1.07 - calculateUnit(currData[1])) {
					page.setColor(Color.RED);
					page.fillRect((int) Math.round(screen_x * 0.82f), GUI.overall_size[15], GUI.overall_size[19], GUI.overall_size[5]);
					page.setColor(Color.WHITE);
				}
				else
					page.setColor(Color.RED);
			else if (currData[4] < currData[1])
				if (currData[4] < currData[1] * 0.93 + calculateUnit(currData[1])) {
					page.setColor(new Color(0, 128, 0));
					page.fillRect((int) Math.round(screen_x * 0.82f), GUI.overall_size[15], GUI.overall_size[19], GUI.overall_size[5]);
					page.setColor(Color.WHITE);
				}
				else
					page.setColor(Color.GREEN);
			page.drawString("收盤 " + nf.format(currData[4]), screen_x * 0.77f, GUI.overall_size[19]);
			page.setColor(Color.WHITE);
			page.drawString("成交量 " + nf.format(currData[5]) + "(億元)", screen_x * 0.17f, GUI.overall_size[19] + char_height);
			page.setColor(Color.RED);
			page.drawString("最高 " + nf.format(currData[2]), screen_x * 0.47f, GUI.overall_size[19] + char_height);
			page.setColor(Color.GREEN);
			page.drawString("最低 " + nf.format(currData[3]), screen_x * 0.77f, GUI.overall_size[19] + char_height);
		} catch (IndexOutOfBoundsException e) {
			mouse_x = graphD.x + graphD.width - line_cap / 2;
			repaint();
		}
    }
    
    protected void drawCanvas (Graphics2D page) {
        page.setPaint(Color.BLUE);
        page.setStroke(new BasicStroke(line_cap, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND));
        page.draw(new Line2D.Double(graphD.x, graphD.y, graphD.x, graphD.y + graphD.height));	// 畫左框
        page.draw(new Line2D.Double(graphD.x + graphD.width, graphD.y, graphD.x + graphD.width, graphD.y + graphD.height));	// 畫右框
        page.draw(new Line2D.Double(graphD.x, graphD.y, graphD.x + graphD.width, graphD.y)); 	// 畫上框
        page.draw(new Line2D.Double(graphD.x, graphD.y + graphD.height, graphD.x + graphD.width, graphD.y + graphD.height)); 	// 畫下框
        page.setStroke(new BasicStroke(1, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND));
    }
    
    // 由數值計算座標
    protected int calculateAxis (double data, int calType) {	// 0:指數 1:成交量
    	if (data < 0)
    		if (calType == 0)
    			return (int) Math.round((height_main) / (priceNum - 1) * -1.0 * (data + 1) + 
    					graphD.y + start_main + char_height / 2);
    		else
        		return (int) Math.round(height_quan / (quanNum - 1) * -1.0 * (data + 1) + 
        				graphD.y + start_quan + char_height / 2);
//    	else if (calType == 0)
//    		if (data > priceInitial[1])
//    			data = priceInitial[1];
//    		else if (data < priceInitial[priceNum])
//    			data = priceInitial[priceNum];
//    	else
//    	    if (data > quanInitial[1])
//    			data = quanInitial[1];
//    		else if (data < quanInitial[quanNum])
//    			data = quanInitial[quanNum];
    	
    	if (calType == 0) {
//    		if (limiteType == TYPE_LIMITED)
//    			ratio = (1 + (0.01 * (priceNum - 1) / 2) - data / priceInitial[0]) * 100.0;
//    		else
    			ratio = (priceInitial[1] - data) / priceSep;
        	return (int) Math.round((height_main) / (priceNum - 1) * 1.0 * ratio + graphD.y + start_main);
    	}
    	else {
//        	if (limiteType == TYPE_LIMITED)
//        		ratio = (1 + (0.01 * (quanNum - 1) / 2) - data / quanInitial[0]) * 100.0;
//        	else
        		ratio = (quanInitial[1] - data) / quanSep;
        	return (int) Math.round(height_quan / (quanNum - 1) * ratio + graphD.y + start_quan);
    	}
    }
    
    // 由座標計算成交價
    protected double calculatePrice (int point_y) {
//    	int initialAxis = calculatePriceAxis(priceInitial[1]);
    	return priceInitial[1] - (priceNum - 1) * (point_y - graphD.y - start_main) * priceSep / height_main;
//    	return priceInitial[1] - (point_y - initialAxis) * 1.0 / (calculatePriceAxis(priceInitial[2]) - initialAxis) * priceSep;
    }
    
    // 由座標計算成交量
    protected double calculateQuan (int point_y) {
    	int initialAxis = calculateAxis(quanInitial[1], 1);
//    	return quanInitial[1] - (quanNum - 1) * (point_y - graphD.y - start_quan) * quanSep / height_quan;
    	return quanInitial[1] - (point_y - initialAxis) * 1.0 / (calculateAxis(quanInitial[2], 1) - initialAxis) * quanSep;
    }
    
    // 計算升降單位
    public static double calculateUnit (double data) {
    	double unit;
    	
    	if (data < 10.0)
    		unit = 0.01;
    	else if (data < 50.0)
    		unit = 0.05;
    	else if (data < 100.0)
    		unit = 0.1;
    	else if (data < 500.0)
    		unit = 0.5;
    	else if (data < 1000.0)
    		unit = 1.0;
    	else
    		unit = 5.0;
    	
    	return unit;
    }
    
    // 產生日期格式
    protected String numToDate (double date) {
    	date_mark[0] = String.valueOf((int) date);
    	date_mark[2] = date_mark[0].substring(6);
    	date_mark[1] = date_mark[0].substring(4, 6);
    	date_mark[0] = date_mark[0].substring(0, 4); 
    	
    	return date_mark[0].concat("/").concat(date_mark[1]).concat("/").concat(date_mark[2]);
    }
    
    // 產生時間格式
    protected String numToTime (double time) {
    	time_mark[0] = String.valueOf((int) time);
    	if (time_mark[0].length() == 5) {
    		time_mark[1] = time_mark[0].substring(1, 3);
    		time_mark[0] = time_mark[0].substring(0, 1);
    	} else {
    		time_mark[1] = time_mark[0].substring(2, 4);
    		time_mark[0] = time_mark[0].substring(0, 2);
    	}
    	
    	return time_mark[0].concat(":").concat(time_mark[1]);
    }
    
    // 將線圖輸出圖檔
    public void outputImage () {
    	BufferedImage image = new BufferedImage(screen_x, screen_y - cpl_height, BufferedImage.TYPE_INT_RGB);
		Graphics bufferGraph = image.createGraphics();
		String imageFormat = "png";
		JPEGImageWriteParam jpgIWP = null;
		
		bufferGraph.translate(0, -1 * cpl_height);
		getRootPane().paintAll(bufferGraph);

		Date date = new Date(System.currentTimeMillis());
		DateFormat df= new SimpleDateFormat("yyyyMMdd-HHmmss");
        ImageOutputStream ios;
        Iterator<ImageWriter> ite = ImageIO.getImageWritersByFormatName(imageFormat);
        ImageWriter imageWriter = ite.next();

        if (imageFormat.equals("jpeg")) {
        	jpgIWP = (JPEGImageWriteParam) imageWriter.getDefaultWriteParam();
        	jpgIWP.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        	jpgIWP.setCompressionQuality(1.0f);
        }
        
        
		try {
			ios = ImageIO.createImageOutputStream(new File("common/source/" + stockId + "_" + df.format(date) + "." + imageFormat));
            imageWriter.setOutput(ios);
            if (imageFormat.equals("jpeg"))
            	imageWriter.write(null, new IIOImage(image, null, null), jpgIWP);
            else
            	imageWriter.write(image);
            ios.flush();
			ios.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
    }
    
    public class outputListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			outputImage();
		}
    }
    
    public class stockAction implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			int sid;
//			System.out.println("item changed!" + System.currentTimeMillis());
			sid = StockBox.getStockId(stockBox);
			if (sid != -1) {
				stockId = sid;
				defaultBtn[4].setActionCommand(String.valueOf(stockId));
				readData();
				repaint();
			}
		}
    }
    
    public class metaMenuListener extends MouseAdapter {
		public void mouseClicked (MouseEvent event) {
			if (event.isMetaDown()) {
				metaMenu.show(event.getComponent(), event.getX(), event.getY());
			}
		}
    }
    
    public class menuItemListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			dateType = Integer.parseInt(event.getActionCommand());
			readData();
			repaint();
		}
    }
    
    public class scaleListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			if (e.getComponent().equals(defaultBtn[0])) {
				if (!defaultBtn[1].isVisible())
					defaultBtn[1].setVisible(true);
				scale += 1;
				if (scale >= 30)
					defaultBtn[0].setVisible(false);
				range = (int) Math.round(graphD.width / scale);
				repaint();
			} else if (e.getComponent().equals(defaultBtn[1])) {
				if (!defaultBtn[0].isVisible())
					defaultBtn[0].setVisible(true);
				scale -= 1;
				if (scale <= 1)
					defaultBtn[1].setVisible(false);
				range = (int) Math.round(graphD.width / scale);
				repaint();
			}
		}
    }
    
    public class shiftListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			if (e.getComponent().equals(defaultBtn[2]))
				shift += range_start - range > 0 ? -1 * range : -1 * (sourceData.size() + shift - range);
			else
				shift += range_start + range < sourceData.size() ? range : -1 * shift;
			
			repaint();
		}
    }
    
//    public class stockIdListener implements KeyListener {
//		public void keyPressed(KeyEvent e) {
//			if (stockField.isVisible()) {
//				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
//					stockId = Integer.parseInt(stockField.getText());
//					readData();
//					repaint();
//					stockField.setVisible(false);
//				}
//			} else {
//				stockField.setVisible(true);
//				stockField.setText(String.valueOf(e.getKeyChar()));
//			}
//		}
//		
//		public void keyReleased(KeyEvent e) {}
//		public void keyTyped(KeyEvent e) {}
//    }
    
    public class timeLineListener implements MouseMotionListener {
		public void mouseDragged(MouseEvent e) {
			mouse_x = e.getX();
			mouse_y = e.getY();
			
//			if (mouse_x > left_space && mouse_x < graphD.x + graphD.width - line_cap / 2 && 
//					mouse_y > graphD.y + line_cap / 2 && mouse_y < graphD.y + graphD.height - line_cap / 2) {
//				System.out.println(screen_x - 11 + " " + mouse_x);
				repaint();
//			}
		}

		public void mouseMoved(MouseEvent e) {

			mouse_x = e.getX();
			mouse_y = e.getY();
			
//			if (mouse_x > left_space && mouse_x < graphD.x + graphD.width - line_cap / 2 && 
//					mouse_y > graphD.y + line_cap / 2 && mouse_y < graphD.y + graphD.height - line_cap / 2) {
//				System.out.println(screen_x - 11 + " " + mouse_x);
				repaint();
//			}
		}
    }
}