/**
 * @author bluesway
 * TARGET ��Ʈw�C����@
 */

package software.Subitem.Transaction;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;


import software.DB;

public class Maintain {
	public void maintain() {
		Statement stmt;
		try {
			stmt = DB.conn.createStatement();
			
			stmt.addBatch("TRUNCATE TABLE `info_tse`");
			stmt.addBatch("TRUNCATE TABLE `info_otc`");
			stmt.addBatch("TRUNCATE TABLE `info_today`");
			stmt.addBatch("TRUNCATE TABLE `info_current`");
			stmt.addBatch("TRUNCATE TABLE `best_five`");
			stmt.addBatch("OPTIMIZE TABLE `transaction`");
			stmt.addBatch("OPTIMIZE TABLE `transaction_five`");
			stmt.addBatch("OPTIMIZE TABLE `holding`");
			stmt.addBatch("OPTIMIZE TABLE `login_record`");
			
			stmt.executeBatch();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}
	
    public void execute(JobExecutionContext context) throws JobExecutionException {
    	Logger log = Logger.getLogger("Execution");
    	FileHandler fileHandler;
    	String jobName = context.getJobDetail().getFullName();
    	
		try {
			fileHandler = new FileHandler("execution.log");
			fileHandler.setFormatter(new SimpleFormatter());
			log.addHandler(fileHandler);
		} catch (SecurityException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		Maintain man = new Maintain();
		
		log.info("Maintain: " + jobName + " executing at " + new Date());
		
		man.maintain();
    }
}
