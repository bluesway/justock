/**
 * @author bluesway
 * TARGET 下單交易
 * TODO 錯誤代碼寫入
 */

package software.Subitem.Transaction;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.LinkedList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import software.DB;
import software.GUI;
import software.Subitem.GeneralGraph;
import software.Subitem.UserData;
import software.Subitem.Graph.OverallGraph;

import common.util.DateUtil;
import common.util.StockBox;
import common.util.TimeUtil;

public class Trade {
	private int stockId;						// 股票代號
	
	private JPanel tradePane;					// 主面板
	private Container typeCon;					// 交易類別區
	private Container tradeCon;					// 主要交易區
	private Container execCon;					// 控制處理區
	private LinkedList<double[]> sourceData;	// 股票資料
	private JButton tradeBtn;					// 下單
	private JButton cancelBtn;					// 取消交易
	private JTextField quanField;				// 數量欄
	public JPasswordField passField;			// 密碼欄
	private JRadioButton typeBuyRadio;			// 交易類別: 買
	private JRadioButton typeSellRadio;			// 交易類別: 賣
	private ButtonGroup typeGroup;				// 交易類別群組
	private JComboBox accountBox;				// 帳號選擇
	private JComboBox stockBox;					// 商品選擇
	private JComboBox hourBox;					// 時間--小時選擇
	private JComboBox minBox;					// 時間--分鐘選擇
	private JComboBox quanTypeBox;				// 數量類別: 整股/零股
	private JComboBox stockTypeBox;				// 股票類別: 現股/融資/融券
	private JComboBox priceBox;					// 價格: 買進/賣出/成交/漲停/平盤/跌停
	private JLabel typeLabel;					// 交易類別
	private JLabel accountLabel;				// 帳號
	private JLabel stockLabel;					// 商品
	private JLabel timeLabel;					// 時間
	private JLabel hourLabel;					// 時間--小時
	private JLabel minLabel;					// 時間--分鐘
	private JLabel quanLabel;					// 數量
	private JLabel priceLabel;					// 價格
	private JLabel passLabel;					// 密碼

	private double highVal;						// 漲停價
	private double lowVal;						// 跌停價
	private int statusCode;						// 狀態代碼
	
	private Color BUYCOLOR;
	private Color SELLCOLOR;
	
	private ResultSet result;
	
	public Trade() {
		stockId = 1101;
		sourceData = OverallGraph.getStockData(stockId);
		
		tradePane = new JPanel(new FlowLayout(FlowLayout.LEFT));
		typeCon = new Container();
		tradeCon = new Container();
		execCon = new Container();
		tradeBtn = new JButton("下單");
		cancelBtn = new JButton("重設");
		tradeBtn.addMouseListener(new executeListener());
		cancelBtn.addMouseListener(new executeListener());
		quanField = new JTextField(4);
		quanField.setText("1");
		passField = new JPasswordField(5);
		typeBuyRadio = new JRadioButton("買");
		typeSellRadio = new JRadioButton("賣");
		typeBuyRadio.addItemListener(new typeSelectedItemListener());
		typeSellRadio.addItemListener(new typeSelectedItemListener());
		typeGroup = new ButtonGroup();
		typeGroup.add(typeBuyRadio);
		typeGroup.add(typeSellRadio);
		accountBox = new JComboBox();
		stockBox = StockBox.create();
		stockBox.getEditor().addActionListener(new stockAction());
		quanTypeBox = new JComboBox(new Object[] {"整股", "零股"});
		stockTypeBox = new JComboBox(new Object[] {"現股", "融資", "融券"});
		hourBox = new JComboBox(new Object[] {"9", "10", "11", "12", "13"});
		hourBox.setSelectedItem("13");
		minBox = new JComboBox(new Object[] {"00", "01", "02", "03", "04", "05", "06",
				"07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", 
				"19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
				"31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", 
				"43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", 
				"55", "56", "57", "58", "59"});
		minBox.setSelectedItem("29");
		priceBox = new JComboBox();
		priceBox.setEditable(true);
		createPriceBox();
		accountLabel = new JLabel("帳號");
		typeLabel = new JLabel("買");
		stockLabel = new JLabel("商品");
		timeLabel = new JLabel("有效期");
		hourLabel = new JLabel("時");
		minLabel = new JLabel("分");
		quanLabel = new JLabel("數量");
		priceLabel = new JLabel("價格");
		passLabel = new JLabel("確認密碼");
		statusCode = -1;
		
		BUYCOLOR = Color.PINK;
		SELLCOLOR = new Color(128, 255, 128);
		
		setPanel();
	}
	
	// 產生交易面板
	private void setPanel() {
		typeCon.setLayout(new BoxLayout(typeCon, BoxLayout.X_AXIS));
		tradeCon.setLayout(new BoxLayout(tradeCon, BoxLayout.Y_AXIS));
		execCon.setLayout(new BoxLayout(execCon, BoxLayout.Y_AXIS));
		tradePane.setBackground(BUYCOLOR);
		tradePane.add(typeCon);
		tradePane.add(tradeCon);
		tradePane.add(execCon);
		
		typeLabel.setFont(new Font("", Font.PLAIN, GUI.overall_size[20]));
		typeCon.add(typeLabel);
		
		Box tradeBox1 = Box.createHorizontalBox();
		tradeCon.add(tradeBox1);
		tradeCon.add(Box.createVerticalStrut(5));
		accountBox.addItem(GUI.userData[0] + " - " + GUI.userData[2]);
		tradeBox1.add(accountLabel);
		tradeBox1.add(accountBox);
		tradeBox1.add(Box.createHorizontalStrut(10));
		tradeBox1.add(stockLabel);
		tradeBox1.add(stockBox);
		tradeBox1.add(Box.createHorizontalStrut(25));
		
		Box tradeBox2 = Box.createHorizontalBox();
		tradeCon.add(tradeBox2);
		tradeBox2.add(typeBuyRadio);
		tradeBox2.add(typeSellRadio);
		typeBuyRadio.setBackground(BUYCOLOR);
		typeSellRadio.setBackground(BUYCOLOR);
		typeGroup.setSelected(typeBuyRadio.getModel(), true);
		tradeBox2.add(Box.createHorizontalStrut(10));
		tradeBox2.add(quanTypeBox);
		tradeBox2.add(Box.createHorizontalStrut(10));
		tradeBox2.add(stockTypeBox);
		tradeBox2.add(Box.createHorizontalStrut(10));
		tradeBox2.add(priceLabel);
		tradeBox2.add(priceBox);
		tradeBox2.add(Box.createHorizontalStrut(10));
		tradeBox2.add(quanLabel);
		tradeBox2.add(quanField);
		tradeBox2.add(Box.createHorizontalStrut(25));
		
		Box execBox1 = Box.createHorizontalBox();
		execCon.add(execBox1);
		execCon.add(Box.createVerticalStrut(5));
		execBox1.add(timeLabel);
		execBox1.add(Box.createHorizontalStrut(10));
		execBox1.add(hourBox);
		execBox1.add(hourLabel);
		execBox1.add(minBox);
		execBox1.add(minLabel);
		
		Box execBox2 = Box.createHorizontalBox();
		execCon.add(execBox2);
		execBox2.add(passLabel);
		execBox2.add(passField);
		tradeBox2.add(Box.createHorizontalStrut(10));
		execBox2.add(tradeBtn);
		execBox2.add(cancelBtn);
	}
	
	// 讀取股票目前資料
	public void readData (int sid) {
		stockId = sid;
		sourceData = OverallGraph.getStockData(stockId);
		updatePanel();
	}
	
	// 更新目前交易面板 (priceBox and stockBox)
	private void updatePanel () {
		stockBox.setSelectedIndex(StockBox.searchIndexOfStockId(stockId));
		createPriceBox();
	}
	
	private void createPriceBox () {
    	NumberFormat nf = new DecimalFormat("0.00");
//    	nf.setMaximumFractionDigits(2);
    	double[] data = sourceData.getLast();
    	double open = data[1] - data[2];
    	double diff = open * 0.07 - GeneralGraph.calculateUnit(open);
    	highVal = Double.parseDouble(nf.format(open + diff));
    	lowVal = Double.parseDouble(nf.format(open - diff));
    	
    	priceBox.removeAllItems();
		priceBox.addItem("買進 " + data[4]);
		priceBox.addItem("賣出 " + data[6]);
		priceBox.addItem("成交 " + data[1]);
		priceBox.addItem("漲停 " + highVal);
		priceBox.addItem("平盤 " + nf.format(open));
		priceBox.addItem("跌停 " + lowVal);
	}
	
	public void sendOrder () {
		long time;							// 交易單時間
		String hour = "0";					// 有效期--小時
		String min = "0";					// 有效期--分鐘
		int limit = 0;							// 有效期
		double price;						// 交易價格
		int quan;							// 交易量
		boolean buy;						// 交易種類
		int id;								// 交易流水號
		double bestfive[] = new double[20];	// 最佳五檔資訊
		
		if (typeGroup.getSelection().equals(typeBuyRadio.getModel()))
		{
			price = Double.parseDouble(splitDigit((String) priceBox.getSelectedItem()));
			buy = true;
			
			if (price > highVal || price < lowVal)
				statusCode = 3;
		} else {
			price = Double.parseDouble(splitDigit((String) priceBox.getSelectedItem()));
			buy = false;
			
			if (price > highVal || price < lowVal)
				statusCode = 3;
			
			price *= -1;
		}
		
		
		if (((String) quanTypeBox.getSelectedItem()).equals("整股"))
			quan = Integer.parseInt(splitDigit(quanField.getText()) + "000");
		else {
			quan = Integer.parseInt(splitDigit(quanField.getText()));
			if (quan > 999 || quan < 1) {
				JOptionPane.showMessageDialog(null, "零股數量需介於 1 - 999 之間", "系統狀態", JOptionPane.ERROR_MESSAGE);
				return;
			}
		}
		
		if ((hour = (String) hourBox.getSelectedItem()).equals("13") && 
				Integer.parseInt(min = (String) minBox.getSelectedItem()) > 29) {
			JOptionPane.showMessageDialog(null, "有效期無效", "系統狀態", JOptionPane.ERROR_MESSAGE);
			return;
		} else {
			limit = (Integer.parseInt(hour) - Integer.parseInt(TimeUtil.getCurrentHour())) * 60 + 
				(Integer.parseInt(min) - Integer.parseInt(TimeUtil.getCurrentMinute()));

if(!GUI.TESTMODE) {
			if (limit <= 0)
			{
				JOptionPane.showMessageDialog(null, "有效期已過", "系統狀態", JOptionPane.ERROR_MESSAGE);
				return;
			}
}	// end of TESTMODE

			if (hour.equals("13") && min.equals("29"))
				limit = 0;
		}
		
		if (checkOrder() && checkHolding(price, quan, buy)) {
			time = System.currentTimeMillis();
			try {
				DB.stmt.executeUpdate("INSERT INTO transaction VALUES(NULL, \"" + GUI.userData[0] + "\", \"" + stockId + "\"" +
						", " + price + ", " + quan + ", " + DateUtil.getDBDate(time) + ", " + 
						TimeUtil.getDBTime(time) + ", " + limit + ", " + statusCode + ")");

				result = DB.stmt.executeQuery("SELECT * FROM transaction WHERE tUid=\"" + GUI.userData[0] + "\" && " +
						"tSid=\"" + stockId + "\" && tTime=" + TimeUtil.getDBTime(time));
				if (result.next())
					id = result.getInt(1);
				else
					id = 0;
				
				result = DB.stmt.executeQuery("SELECT * FROM best_five WHERE bSid=\"" + stockId + "\"");
				if (result.next())
					for (int i = 0; i < 20; i++)
						bestfive[i] = result.getDouble(i + 2);
				else
					for (int i = 0; i < 20; i++)
						bestfive[i] = 0;
						
				DB.stmt.executeUpdate("INSERT INTO transaction_five VALUES(" + id + ", " + bestfive[0] + ", " + bestfive[1] + ", " + 
	            		bestfive[2] + ", " + bestfive[3] + ", " + bestfive[4] + ", " + bestfive[5] + ", " + bestfive[6] + ", " + bestfive[7] + ", " + 
	            		bestfive[8] + ", " + bestfive[9] + ", " + bestfive[10] + ", " + bestfive[11] + ", " + bestfive[12] + ", " + bestfive[13] + ", " + 
	            		bestfive[14] + ", " + bestfive[15] + ", " + bestfive[16] + ", " + bestfive[17] + ", " + bestfive[18] + ", " + bestfive[19] + ")");
				
				JOptionPane.showMessageDialog(null, "委託單成功送出！", "系統狀態", JOptionPane.PLAIN_MESSAGE);
			} catch (SQLException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "委託單送出發生錯誤！", "系統狀態", JOptionPane.ERROR_MESSAGE);
			}
		}
		
		passField.setText("");
	}
	
	private boolean checkOrder() {
		if (!UserData.chkPwd(GUI.userData[0], String.valueOf(passField.getPassword()))) {
			JOptionPane.showMessageDialog(null, "密碼錯誤！委託單將不送出..", "系統狀態", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if (splitDigit((String) priceBox.getSelectedItem()).equals("")) {
			JOptionPane.showMessageDialog(null, "出價錯誤！委託單將不送出..", "系統狀態", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if (splitDigit(quanField.getText()).equals("")) {
			JOptionPane.showMessageDialog(null, "數量錯誤！委託單將不送出..", "系統狀態", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		return true;
	}
	
	private boolean checkHolding(double price, int quan, boolean type) {
		double balance; // 帳戶餘額
		int hold;		// 持有股數
		
		try {
			if (type) { // 買進
				result = DB.stmt.executeQuery("SELECT bBalance FROM bank WHERE bUid=\"" + GUI.userData[0] + "\"");
				if (result.next())
					balance = result.getDouble(1);
				else
					balance = 0;
				
				if (balance < Math.round(price * quan))
					statusCode = 1;	// 帳戶餘額不足
			} else {
				result = DB.stmt.executeQuery("SELECT hQuan FROM holding WHERE hUid=\"" + GUI.userData[0] + "\" AND hSid=\"" + stockId + "\"");
				if (result.next())
					hold = result.getInt(1);
				else
					hold = 0;
				
				if (hold < quan)
					statusCode = 4;	// 持有股數不足
			}
		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "連線錯誤！無法進行下單。", "系統狀態", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		return true;
	}
	
	private String splitDigit(String str) {
		return str.replaceAll("[^0-9.]*", "");
	}
	
	public class typeSelectedItemListener implements ItemListener {
		public void itemStateChanged(ItemEvent e) {
			if (typeGroup.getSelection().equals(typeBuyRadio.getModel())) {
				typeLabel.setText("買");
				tradePane.setBackground(BUYCOLOR);
				typeBuyRadio.setBackground(BUYCOLOR);
				typeSellRadio.setBackground(BUYCOLOR);
			} else {
				typeLabel.setText("賣");
				tradePane.setBackground(SELLCOLOR);
				typeBuyRadio.setBackground(SELLCOLOR);
				typeSellRadio.setBackground(SELLCOLOR);
			}
		}
	}
	
    public class stockAction implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			int sid;
			sid = StockBox.getStockId(stockBox);
			if (sid != -1)
				readData(sid);
		}
    }
    
    public class itemChanged implements ItemListener {
		public void itemStateChanged(ItemEvent e) {
			if (e.getStateChange() == ItemEvent.SELECTED)
				readData(Integer.parseInt(((String) e.getItem()).substring(0, 4)));
		}
    }
    
	public class executeListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			if (e.getComponent().equals(cancelBtn)) {
				quanField.setText("1");
				passField.setText("");
				typeGroup.setSelected(typeBuyRadio.getModel(), true);
				priceBox.setSelectedIndex(0);
				quanTypeBox.setSelectedItem("整股");
				stockTypeBox.setSelectedItem("現股");
				hourBox.setSelectedItem("13");
				minBox.setSelectedItem("29");
			} else {
				sendOrder();
			}
		}
	}
	
	public Container getPane () {
		return tradePane;
	}
}
