/*
*      author by Kevingo
*      Data @ 2007/07/31
*      顯示財務分析資料
*      
*      TODO 美化 快速鍵設置問題 
*               倒閉公司財務顯示處理  
*               下拉式選單和輸入欄位同時有值的問題 會顯示不出來下拉式選單的股票代碼   
*/

package software.Subitem.table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellRenderer;

import common.util.MyTableModel_15;
import software.GUI;
import software.DataRetrieve.StockInfoTransParser_Yahoo;

public class TransStockInfo implements ActionListener {
	private static final long serialVersionUID = 1L;
	public boolean DONE = false;
	private int TABLE_HEIGHT, TABLE_WIDTH, DATA_ROW, DATA_COLUMN;
	private Object [][] data;
	private Container container;
	private JPanel panel_Table, font_Panel, color_Panel; // 放置表格的panel_Table
    private JButton okBtn,cancelBtn, applyBtn, chgBacColorBtn, chgForColorBtn; // 確定 取消按鈕
    private JTable table; // 表格
    private JTextField stockField, statusField; // 使用者輸入股票代碼
    private JLabel stockLabel, fontStyleLabel, fontSizeLabel, statusLabel;     // 股票代碼
    private JComboBox stockBox, fontStyleBox, fontSizeBox;
    private MyTableModel_15 model; // tableModel
    private Border compound;
    private TitledBorder border;
    private Scanner scan; // 讀取股票代碼
	private StockInfoTransParser_Yahoo sty;
	private String stockID;
	private String [] stockList;
	private String [] fontStyle, fontSize;
	private String [] columnNames = new String[] {
			"時間",
			"買進",
			"賣出",
			"成交價",
			"漲跌",
			"單量(張)",
	};
	
	private JButton chgToEZMode;
	private JButton chgToInitialMode;
	private JButton callTecDiagramBtn;
	private JButton callIniDiagramBtn;


    public TransStockInfo() {
    	container = new Container();
    	container.setLayout(new BorderLayout());
    	
    	try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) {
			e.printStackTrace();
		}
		DATA_ROW = 49;
		DATA_COLUMN = 6;
		
    	panel_Table = new JPanel();
    	stockList = new String[2054];
    	data = new Object[DATA_ROW][DATA_COLUMN];
    	
    	// 讀取stockList到String陣列中 ，JComboBox要用
    	readData(); 
    	int i = 0;
    	while(scan.hasNext()) {
    		stockList[i] = scan.nextLine();
    		i++;
    	}
    	
    	TABLE_HEIGHT = 500;
    	TABLE_WIDTH = 350;
    	
    	font_Panel = new JPanel(new GridLayout(10,1));
    	fontSize = new String[] {"10","12","14"};
    	fontStyle = new String[]{"PLAIN","BOLD","ITALIC"};
    	
    	fontSizeLabel = new JLabel("文字大小：");
    	fontSizeBox = new JComboBox(fontSize);
    	font_Panel.add(fontSizeLabel);
    	font_Panel.add(fontSizeBox);
    	
    	fontStyleLabel = new JLabel("文字狀態：");
    	fontStyleBox = new JComboBox(fontStyle);
    	font_Panel.add(fontStyleLabel);
    	font_Panel.add(fontStyleBox);
    	fontSizeBox.setEditable(true);
    	
    	applyBtn = new JButton("套用字型設定");
    	applyBtn.addActionListener(
    	new ActionListener() {
    		public void actionPerformed(ActionEvent e) {
    	        Font font = new Font("微軟正黑體",
    	        		fontStyleBox.getSelectedIndex(),
    	        		Integer.parseInt((String)fontSizeBox.getSelectedItem()));
    	        
    	        table.setFont(font);
    	        table.setColumnSelectionAllowed(true);
    	        table.clearSelection();
    	        table.revalidate();
    	        table.repaint();
    		}
    	});
    	font_Panel.add(applyBtn);
    	
    	chgToEZMode = new JButton("更改為色彩標記模式");
    	chgToEZMode.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			TableCellRenderer renderer = new CustomTableCellRenderer();
    	    			table.setDefaultRenderer( Object.class, renderer );
    	    	        table.setColumnSelectionAllowed(true);
    	    	        table.clearSelection();
    	    	        table.revalidate();
    	    	        table.repaint();
    	    		}
    	    	});
    	font_Panel.add(chgToEZMode);
    	
    	chgToInitialMode = new JButton("更改為原始模式");
    	chgToInitialMode.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			TableCellRenderer renderer = new CustomTableInitialStatus();
    	    			table.setDefaultRenderer( Object.class, renderer );
    	    	        table.setColumnSelectionAllowed(true);
    	    	        table.clearSelection();
    	    	        table.revalidate();
    	    	        table.repaint();
    	    		}
    	    	});
    	font_Panel.add(chgToInitialMode);
    	
    	callTecDiagramBtn = new JButton("查詢個股技術分析圖表");
    	callTecDiagramBtn.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			GUI.gui.setStockId(Integer.valueOf(stockID));
    	    			GUI.gui.createFrame("技術分析");
    	    		}
    	    	});
    	font_Panel.add(callTecDiagramBtn);
    	
    	callIniDiagramBtn = new JButton("查詢個股即時行情圖表");
    	callIniDiagramBtn.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			GUI.gui.setStockId(Integer.valueOf(stockID));
    	    			GUI.gui.createFrame("個股即時行情");
    	    		}
    	    	});
    	font_Panel.add(callIniDiagramBtn);
    	
    	color_Panel = new JPanel(new GridLayout(2, 1));
    	chgBacColorBtn = new JButton("更改背景色(表格底色)");
    	chgForColorBtn = new JButton("更改前景色(字型顏色)");
    	chgBacColorBtn.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			   Color selectedColor = JColorChooser.showDialog(
    	    					   table.getRootPane(), "選擇字體顏色", table.getForeground());
    	    			   table.setBackground(selectedColor);
    	    		}
    	    	});
    	
    	chgForColorBtn.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			   Color selectedColor = JColorChooser.showDialog(
    	    					   table.getRootPane(), "選擇字體顏色", table.getForeground());
    	    			   table.setForeground(selectedColor);
    	    		}
    	    	});
    	
    	color_Panel.add(chgBacColorBtn);
    	color_Panel.add(chgForColorBtn);

    	// 股票代碼
    	stockLabel = new JLabel("股票代碼：");
    	panel_Table.add(stockLabel);
    	stockField = new JTextField(4);
    	panel_Table.add(stockField);
    	
    	// 股票代碼下拉式選單
    	stockBox = new JComboBox(stockList);
    	Border redline = BorderFactory.createLineBorder(Color.ORANGE, 2);
        compound = BorderFactory.createCompoundBorder(
                redline, compound);
        border = BorderFactory.createTitledBorder(
                compound, "請選擇股票代碼",
                TitledBorder.CENTER,
                TitledBorder.DEFAULT_POSITION);
    	stockBox.setBorder(border);
    	stockBox.setEditable(false);
    	stockBox.addActionListener(this);
    	enterPressesWhenFocused(stockBox);
    	panel_Table.add(stockBox);
    	
    	// 確定按鈕
    	okBtn = new JButton("確定");
    	enterPressesWhenFocused(okBtn);
    	okBtn.addActionListener(
        		new ActionListener() {
        			private String tmpProcess;

					public void actionPerformed(ActionEvent ae) {
        				if(!stockField.getText().equals("")) {
        					DONE = true;
        					sty = new StockInfoTransParser_Yahoo();
        					sty.retrieveData(sty, stockField.getText(), DONE);
        					data = sty.getData();
        					setStatus(searchName(stockField.getText()));
        				}   	
        				
        		    	for(int i=0 ; i<DATA_ROW ; i++) {
        		    		for(int j=0 ; j<DATA_COLUMN ; j++) {
        		    			tmpProcess = String.valueOf(data[i][j]);
        		    			if(tmpProcess.startsWith("+")) {
        		    				tmpProcess = "▲ " + tmpProcess.substring(1, tmpProcess.length());
        		    				table.setSelectionForeground(Color.YELLOW);
        		    			}
        		    			else if(tmpProcess.startsWith("-") && tmpProcess.length() != 1)
        		    				tmpProcess = "▼ " + tmpProcess.substring(1, tmpProcess.length());
        		    			else if(tmpProcess == "null")
        		    				tmpProcess = "";

        		    			model.setValueAt(tmpProcess, i, j);
        		    		}		
        		    	}
        		        model.fireTableDataChanged(); 
        			}
        		}
        	);
    	panel_Table.add(okBtn);
    	
    	// 取消按鈕
    	cancelBtn = new JButton("取消");
    	cancelBtn.addActionListener(new ActionListener() {    		 
    		public void actionPerformed(ActionEvent arg0) {
    			stockField.setText("");
    		}
    	});
    	panel_Table.add(cancelBtn);

    	statusLabel = new JLabel("目前查詢的股票為：");
    	panel_Table.add(statusLabel);
    	
    	statusField = new JTextField(10);
    	panel_Table.add(statusField);
    	
    	// 表格
		model = new MyTableModel_15(DATA_ROW, DATA_COLUMN);
		model.setColumnNames(columnNames);
		
    	table = new JTable(model);
    	table.setPreferredScrollableViewportSize(new Dimension(TABLE_HEIGHT, TABLE_WIDTH));
    	table.setFillsViewportHeight(true);   
    	table.setRowSelectionAllowed(true);
    	table.setColumnSelectionAllowed(true);
        model.addTableModelListener(new TableModelListener(){
            public void tableChanged(TableModelEvent e) {
                table.updateUI();
            }});
        
        // 拉動式捲軸 和 主要Frame
    	JScrollPane scrollPane = new JScrollPane(table);
        RowHeaderer rowHead= new RowHeaderer(table);
        scrollPane.setRowHeaderView(rowHead);
        scrollPane.getRowHeader().setPreferredSize(new Dimension(35,100));
        
        table.setBackground(Color.BLACK);
        table.setForeground(Color.WHITE);
        table.setFont(new Font("標楷體", Font.PLAIN, 14));
        container.add(panel_Table, BorderLayout.NORTH);
        container.add(font_Panel, BorderLayout.EAST);
        container.add(color_Panel, BorderLayout.SOUTH);
        container.add(scrollPane, BorderLayout.WEST);
        container.setVisible(true);   
    }
    
    // 從檔案讀取股票資料
    private void readData() {
        File tmpfile;
        tmpfile = new File("common/source/stockId.list");
        try {
            scan = new Scanner(tmpfile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void actionPerformed(ActionEvent e) {
    	stockID = String.valueOf(stockBox.getSelectedItem()).substring(0, 4);
		DONE = true;
		sty = new StockInfoTransParser_Yahoo();
		sty.retrieveData(sty, stockID, DONE);
		data = sty.getData(); 
		setStatus(stockBox.getSelectedItem().toString());
    }
    
    // 顯示當前查詢股票代碼和名稱
    public void setStatus(String stockId) {
    	statusField.setText(stockId);
    	statusField.setEditable(false);
    }
    
    public String searchName(String id) {
    	id = common.util.StockBox.searchStockId(id);
    	return id;
    }
    
    
    public Container getFrame() {
		return container;
    }
    
    // 處理快速鍵的問題   enter 確定 esc 取消
    public void enterPressesWhenFocused(JComponent object) {
    		object.registerKeyboardAction(
    				object.getActionForKeyStroke(
    						KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)), 
    						KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false), 
    						JComponent.WHEN_IN_FOCUSED_WINDOW);
    		
    		object.registerKeyboardAction(
    				object.getActionForKeyStroke(
    						KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)), 
    						KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), 
    						JComponent.WHEN_IN_FOCUSED_WINDOW);
    }
    
//    public static void main(String[] args) {
//    	new TransStockInfo();
//    }
}