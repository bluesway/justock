/*
*      author by Kevingo
*      Data @ 2007/09/03
*      上市成交價排行資料
*      pchome version
*      
*      TODO 增加所有的排行榜  http://pchome.syspower.com.tw/top/
*/

package software.Subitem.table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import software.DataRetrieve.StockInfoPriceParser_Pchome;
import common.util.MyTableModel_8;

public class PriceInfo {
	private static final long serialVersionUID = 1L;
	public boolean DONE = false;
	private int TABLE_HEIGHT, TABLE_WIDTH, DATA_ROW, DATA_COLUMN, vColIndex, width, currentPage;
	private Object [][] data;
	private Container container;
	private JPanel panel_Table, font_Panel, color_Panel; // 放置表格的panel_Table
    private JButton applyBtn, chgBacColorBtn, chgForColorBtn, chgToEZMode, chgToInitialMode; // 確定 取消按鈕
    private JTable table; // 表格
    private JLabel fontStyleLabel, fontSizeLabel;
    private JComboBox fontStyleBox, fontSizeBox;
    private JMenuItem menuItem_1, menuItem_2;
    private JPopupMenu popupMenu;
    private MyTableModel_8 model; // tableModel
	private StockInfoPriceParser_Pchome smp;
	private String [] fontStyle, fontSize;
	private String [] columnNames = new String[] {
			"排行",
			"股票",
			"成交",
			"漲跌",
			"漲跌幅",
			"最高價",
			"最低價",
			"成交量(張)",
			"成交金額(百萬)",
	};
	private String tmpProcess;

    public PriceInfo() {
    	container = new Container();
    	container.setLayout(new BorderLayout());
    	
    	try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		DATA_ROW = StockInfoPriceParser_Pchome.ROW;
		DATA_COLUMN = StockInfoPriceParser_Pchome.COLUMN;
		
		vColIndex = 0; // 決定column 的 index
    	width = 30; // column index 的寬度
		
    	panel_Table = new JPanel();
    	data = new Object[DATA_ROW][DATA_COLUMN];
    	
    	TABLE_HEIGHT = 800;
    	TABLE_WIDTH = 350;
    	
    	font_Panel = new JPanel(new GridLayout(10,1));
    	fontSize = new String[] {"10","12","14"};
    	fontStyle = new String[]{"PLAIN","BOLD","ITALIC"};
    	
    	fontSizeLabel = new JLabel("文字大小：");
    	fontSizeBox = new JComboBox(fontSize);
    	font_Panel.add(fontSizeLabel);
    	font_Panel.add(fontSizeBox);
    	
    	fontStyleLabel = new JLabel("文字狀態：");
    	fontStyleBox = new JComboBox(fontStyle);
    	font_Panel.add(fontStyleLabel);
    	font_Panel.add(fontStyleBox);
    	fontSizeBox.setEditable(true);
    	applyBtn = new JButton("套用字型設定");
    	applyBtn.addActionListener(
    	new ActionListener() {
    		public void actionPerformed(ActionEvent e) {
    	        Font font = new Font("微軟正黑體",
    	        		fontStyleBox.getSelectedIndex(),
    	        		Integer.parseInt((String)fontSizeBox.getSelectedItem()));

    	        table.setFont(font);
    	        table.setColumnSelectionAllowed(true);
    	        table.clearSelection();
    	        table.revalidate();
    	        table.repaint();
    		}
    	});
    	font_Panel.add(applyBtn);
    	
    	// 配置顏色
    	chgToEZMode = new JButton("更改為色彩標記模式");
    	chgToEZMode.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			TableCellRenderer renderer = new CustomTableCellRenderer();
    	    			table.setDefaultRenderer( Object.class, renderer );
    	    	        table.setColumnSelectionAllowed(true);
    	    	        table.clearSelection();
    	    	        table.revalidate();
    	    	        table.repaint();
    	    		}
    	    	});
    	font_Panel.add(chgToEZMode);
    	
    	chgToInitialMode = new JButton("更改為原始模式");
    	chgToInitialMode.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			TableCellRenderer renderer = new CustomTableInitialStatus();
    	    			table.setDefaultRenderer( Object.class, renderer );
    	    	        table.setColumnSelectionAllowed(true);
    	    	        table.clearSelection();
    	    	        table.revalidate();
    	    	        table.repaint();
    	    		}
    	    	});
    	font_Panel.add(chgToInitialMode);
    	
    	// 變更顏色按鈕設置
    	color_Panel = new JPanel(new GridLayout(2, 1));
    	chgBacColorBtn = new JButton("更改背景色(表格底色)");
    	chgForColorBtn = new JButton("更改前景色(字型顏色)");
    	chgBacColorBtn.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			   Color selectedColor = JColorChooser.showDialog(
    	    					   table.getRootPane(), "選擇字體顏色", table.getForeground());
    	    			   table.setBackground(selectedColor);
    	    		}
    	    	});
    	
    	chgForColorBtn.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			   Color selectedColor = JColorChooser.showDialog(
    	    					   table.getRootPane(), "選擇字體顏色", table.getForeground());
    	    			   table.setForeground(selectedColor);
    	    		}
    	    	});
    	
    	color_Panel.add(chgBacColorBtn);
    	color_Panel.add(chgForColorBtn);
    	
		DONE = true;
		smp = new StockInfoPriceParser_Pchome();
		smp.retrieveData(smp, DONE, 1);
		data = smp.getData();
		currentPage = 1;
    	
    	// 表格
		model = new MyTableModel_8(DATA_ROW, DATA_COLUMN);
		model.setColumnNames(columnNames);
		
    	table = new JTable(model);
		TableCellRenderer renderer = new CustomTableCellRenderer();
		table.setDefaultRenderer( Object.class, renderer );
        table.setColumnSelectionAllowed(true);
        table.clearSelection();
        table.revalidate();
        table.repaint();
    	table.setPreferredScrollableViewportSize(new Dimension(TABLE_HEIGHT, TABLE_WIDTH));
    	table.setFillsViewportHeight(true);   
    	table.setRowSelectionAllowed(true);
    	table.setColumnSelectionAllowed(true);
        model.addTableModelListener(new TableModelListener(){
            public void tableChanged(TableModelEvent e) {
                table.updateUI();
            }});
        
        // 設定column的寬度
        TableColumn col = table.getColumnModel().getColumn(vColIndex);
        col.setPreferredWidth(width);
        
    	for(int k=0 ; k<DATA_ROW ; k++) {
    		for(int j=0 ; j<DATA_COLUMN ; j++) {
    			tmpProcess = String.valueOf(data[k][j]);
    			if(tmpProcess.startsWith("+")) {
    				tmpProcess = "▲ " + tmpProcess.substring(1, tmpProcess.length());
    				table.setSelectionForeground(Color.YELLOW);
    			}
    			else if(tmpProcess.startsWith("-") && tmpProcess.length() != 1)
    				tmpProcess = "▼ " + tmpProcess.substring(1, tmpProcess.length());
    			else if(tmpProcess == "null")
    				tmpProcess = "";

    			model.setValueAt(tmpProcess, k, j);
    		}		
    	}
        model.fireTableDataChanged(); 
        
        // 產生彈出式選單
        createPopupMenu();
        
        // 拉動式捲軸 和 主要Frame
    	JScrollPane scrollPane = new JScrollPane(table);
        
        RowHeaderer rowHeader= new RowHeaderer(table);
        scrollPane.setRowHeaderView(rowHeader);
        scrollPane.getRowHeader().setPreferredSize(new Dimension(35,100));
        
        table.setBackground(Color.BLACK);
        table.setForeground(Color.WHITE);
        
        container.add(panel_Table, BorderLayout.NORTH);
        container.add(font_Panel, BorderLayout.EAST);
        container.add(color_Panel, BorderLayout.SOUTH);
        container.add(scrollPane, BorderLayout.WEST);
        container.setVisible(true);
    }
    
    // 處理快速鍵的問題   enter 確定 esc 取消
    public void enterPressesWhenFocused(JComponent object) {
    		object.registerKeyboardAction(
    				object.getActionForKeyStroke(
    						KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)), 
    						KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false), 
    						JComponent.WHEN_IN_FOCUSED_WINDOW);
    		
    		object.registerKeyboardAction(
    				object.getActionForKeyStroke(
    						KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)), 
    						KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), 
    						JComponent.WHEN_IN_FOCUSED_WINDOW);
    }
    
    public Container getFrame() {
    	return container;
    }
    
    // popupMenu 呈現下30筆和上30筆資料
    public void createPopupMenu() {
    	popupMenu = new JPopupMenu();
    	menuItem_1 = new JMenuItem("下30筆資料");
    	popupMenu.add(menuItem_1);
    	menuItem_1.addActionListener(new ActionListener() {    		 
    		public void actionPerformed(ActionEvent ae) {
    			DONE = true;
				smp = new StockInfoPriceParser_Pchome();
				
				if(currentPage == 1) {
					smp.retrieveData(smp, DONE, 2);
					currentPage = 2;
				}
				else if(currentPage == 2) {
					smp.retrieveData(smp, DONE, 3);
					currentPage = 3;
				} else {
					smp.retrieveData(smp, DONE, currentPage);
				}
				
				data = smp.getData();
		    	for(int i=0 ; i<DATA_ROW ; i++) 
		    		for(int j=0 ; j<DATA_COLUMN ; j++) 
		        			model.setValueAt(data[i][j], i, j);
		        model.fireTableDataChanged(); 
    		}
    	});
    	
    	menuItem_2 = new JMenuItem("上30筆資料");
    	popupMenu.add(menuItem_2);
    	menuItem_2.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent ae) {
    			DONE = true;
				smp = new StockInfoPriceParser_Pchome();
				
				if(currentPage == 2) {
					smp.retrieveData(smp, DONE, 1);
					currentPage = 1;
				} else if(currentPage == 3) {
					smp.retrieveData(smp, DONE, 2);
					currentPage = 2;
				} else {
					smp.retrieveData(smp, DONE, currentPage);
				}
				
				data = smp.getData();
		    	for(int i=0 ; i<DATA_ROW ; i++) 
		    		for(int j=0 ; j<DATA_COLUMN ; j++) 
		        			model.setValueAt(data[i][j], i, j);
		        model.fireTableDataChanged(); 
    		}
    	});
    	MouseListener popupListener = new PopupListener(popupMenu);
    	table.addMouseListener(popupListener);
    }
}