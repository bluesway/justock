package software.Subitem.table;

/*
*      author by Kevingo
*      Data @ 2007/07/31
*      顯示基本分析資料
*      pchome version
*      
*      TODO 美化 快速鍵設置問題 
*               倒閉公司財務顯示處理  
*               下拉式選單和輸入欄位同時有值的問題 會顯示不出來下拉式選單的股票代碼   
*               增加右鍵選單
*/

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import software.DataRetrieve.StockInfoBasicParser_Pchome;

import common.util.MyTableModel;
import common.util.StockBox;

public class BasicStockInfo implements ActionListener {
	private static final long serialVersionUID = 1L;
	public boolean DONE = false;
	private int TABLE_HEIGHT, TABLE_WIDTH;
	private int DATA_COLUMN = StockInfoBasicParser_Pchome.COLUMN;
	private int DATA_ROW = StockInfoBasicParser_Pchome.ROW;
	private Object [][] data; // 存放抓取下來的籌碼分析資料
	private Container container;
	private JPanel panel_Table, font_Panel, color_Panel; // 放置表格的panel_Table
    private JButton okBtn,cancelBtn, applyBtn, chgForColorBtn, chgBacColorBtn; // 確定 取消按鈕
    private JTable table;      // 表格
    private JTextField stockField, statusField; // 使用者輸入股票代碼
    private JLabel fontStyleLabel, fontSizeLabel, statusLabel;     // 股票代碼
    private JComboBox stockBox, fontStyleBox, fontSizeBox;
    private MyTableModel model; // tableModel
    private Border compound;
    private TitledBorder border;
	private StockInfoBasicParser_Pchome sbp;
	private Scanner scan;
	private String stockID;
	private String tmpProcess;
	private String [] stockList;
	private String [] fontStyle, fontSize;
	private String [] columnNames = new String[] {
	    		""," ","","","",""}; 

    public BasicStockInfo() {
    	container = new Container();
    	container.setLayout(new BorderLayout());
    	
    	try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) {
			e.printStackTrace();
		}
		
    	panel_Table = new JPanel();
    	data = new Object[25][6];
    	stockList = new String[2054];
    	
    	// 讀取stockList到String陣列中 ，JComboBox要用
    	readData(); 
    	int i = 0;
    	while(scan.hasNext()) {
    		stockList[i] = scan.nextLine();
    		i++;
    	}
    	
    	TABLE_HEIGHT =600;
    	TABLE_WIDTH = 150;
    	
    	font_Panel = new JPanel(new GridLayout(10,1));
    	fontSize = new String[] {"10","12","14"};
    	fontStyle = new String[] {"PLAIN","BOLD","ITALIC"};
    	
    	fontSizeLabel = new JLabel("文字大小：");
    	fontSizeBox = new JComboBox(fontSize);
    	font_Panel.add(fontSizeLabel);
    	font_Panel.add(fontSizeBox);
    	
    	fontStyleLabel = new JLabel("文字狀態：");
    	fontStyleBox = new JComboBox(fontStyle);
    	font_Panel.add(fontStyleLabel);
    	font_Panel.add(fontStyleBox);
    	
    	fontSizeBox.setEditable(true);
    	
    	applyBtn = new JButton("Apply");
    	applyBtn.addActionListener(
    	new ActionListener() {
    		public void actionPerformed(ActionEvent e) {
    	        Font font = new Font("微軟正黑體",
    	        		fontStyleBox.getSelectedIndex(),
    	        		Integer.parseInt((String)fontSizeBox.getSelectedItem()));
    	        
    	        table.setFont(font);
    	        table.setColumnSelectionAllowed(true);
    	        table.clearSelection();
    	        table.revalidate();
    	        table.repaint();
    		}
    	});
    	font_Panel.add(applyBtn);
    	
    	color_Panel = new JPanel(new GridLayout(2, 1));
    	chgBacColorBtn = new JButton("更改背景色(表格底色)");
    	chgForColorBtn = new JButton("更改前景色(字型顏色)");
    	chgBacColorBtn.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			   Color selectedColor = JColorChooser.showDialog(
    	    					   table.getRootPane(), "選擇字體顏色", table.getForeground());
    	    			   table.setBackground(selectedColor);
    	    		}
    	    	});
    	
    	chgForColorBtn.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			   Color selectedColor = JColorChooser.showDialog(
    	    					   table.getRootPane(), "選擇字體顏色", table.getForeground());
    	    			   table.setForeground(selectedColor);
    	    		}
    	    	});
    	
    	color_Panel.add(chgBacColorBtn);
    	color_Panel.add(chgForColorBtn);

    	// 股票代碼下拉式選單
    	stockBox = StockBox.create();    	
    	Border redline = BorderFactory.createLineBorder(Color.darkGray, 1);
        compound = BorderFactory.createCompoundBorder(
                redline, compound);
        border = BorderFactory.createTitledBorder(
                compound, "請選擇股票代碼",
                TitledBorder.CENTER,
                TitledBorder.DEFAULT_POSITION);
    	stockBox.setBorder(border);
    	stockBox.getEditor().addActionListener(this);
    	enterPressesWhenFocused(stockBox);
    	panel_Table.add(stockBox);
    	
    	// 確定按鈕
    	okBtn = new JButton("確定");
    	enterPressesWhenFocused(okBtn);
    	okBtn.addActionListener(    		
    			new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				updataData();
		        model.fireTableDataChanged(); 
			}
		}
	);
    	panel_Table.add(okBtn);
    	
    	// 取消按鈕
    	cancelBtn = new JButton("取消");
    	cancelBtn.addActionListener(new ActionListener() {    		 
    		public void actionPerformed(ActionEvent arg0) {
    			stockField.setText("");
    		}
    	});
    	panel_Table.add(cancelBtn);

    	statusLabel = new JLabel("目前查詢的股票為：");
    	panel_Table.add(statusLabel);
    	
    	statusField = new JTextField(10);
    	panel_Table.add(statusField);
    	
    	// 表格
		model = new MyTableModel(DATA_ROW, DATA_COLUMN);
		model.setColumnNames(columnNames);
    	table = new JTable(model);
    	table.setPreferredScrollableViewportSize(new Dimension(TABLE_HEIGHT, TABLE_WIDTH));
    	table.setFillsViewportHeight(true);   
    	table.setRowSelectionAllowed(true);
    	table.setColumnSelectionAllowed(true); 
        model.addTableModelListener(new TableModelListener(){
            public void tableChanged(TableModelEvent e) {
                table.updateUI();
            }});
        
        // 拉動式捲軸 和 主要Frame
    	JScrollPane scrollPane = new JScrollPane(table);
        
        RowHeaderer rowHeader= new RowHeaderer(table);
        scrollPane.setRowHeaderView(rowHeader);
        scrollPane.getRowHeader().setPreferredSize(new Dimension(35,100));
        
        table.setBackground(Color.BLACK);
        table.setForeground(Color.WHITE);
        table.setFont(new Font("標楷體", Font.PLAIN, 14));
        container.add(panel_Table, BorderLayout.NORTH);
        container.add(font_Panel, BorderLayout.EAST);
        container.add(color_Panel, BorderLayout.SOUTH);
        container.add(scrollPane, BorderLayout.WEST);

    }
    
    public Container getFrame() {
    	return container;
    }
    
    // 顯示當前查詢股票代碼和名稱
    public void setStatus(String stockId) {
    	statusField.setText(stockId);
    	statusField.setEditable(false);
    }
    
    public String searchName(String id) {
    	id = common.util.StockBox.searchStockId(id);
    	return id;
    }

    // 股票代碼ComboBox的確認動作
    public void actionPerformed(ActionEvent e) {
    	stockID = String.valueOf(StockBox.getStockId(stockBox));
    	updataData();
    }
    
    // 從檔案讀取股票資料
    private void readData() {
        File tmpfile;
        tmpfile = new File("common/source/stockId.list");
        try {
            scan = new Scanner(tmpfile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    
    public void updataData() {
    	if(StockBox.getStockId(stockBox) != -1) {
			DONE = true;
			sbp = new StockInfoBasicParser_Pchome();
			sbp.retrieveData(sbp, String.valueOf(StockBox.getStockId(stockBox)), DONE);
			data = sbp.getData();
			setStatus(stockID);
		
	    	for(int i=0 ; i<DATA_ROW ; i++) {
	    		for(int j=0 ; j<DATA_COLUMN ; j++) {
	    			tmpProcess = String.valueOf(data[i][j]);
	    			model.setValueAt(tmpProcess, i, j);
	    		}		
	    	}
    	}
    }

	// 處理快速鍵的問題   enter 確定 esc 取消
    public void enterPressesWhenFocused(JComponent object) {
    		object.registerKeyboardAction(
    				object.getActionForKeyStroke(
    						KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)), 
    						KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false), 
    						JComponent.WHEN_IN_FOCUSED_WINDOW);
    		
    		object.registerKeyboardAction(
    				object.getActionForKeyStroke(
    						KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)), 
    						KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), 
    						JComponent.WHEN_IN_FOCUSED_WINDOW);
    }
}