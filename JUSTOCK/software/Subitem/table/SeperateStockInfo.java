/*
*      author by Kevingo
*      Data @ 2007/09/03
*      上市成交價排行資料
*      pchome version
*      
*      TODO 增加所有的排行榜  http://pchome.syspower.com.tw/top/
*/

package software.Subitem.table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import software.DataRetrieve.StockInfoSeperateStock;

import common.util.MyTableModel_17;
import common.util.MyTableModel_8;

public class SeperateStockInfo implements ActionListener {
	private static final long serialVersionUID = 1L;
	public boolean DONE = false;
	private int TABLE_HEIGHT, TABLE_WIDTH, DATA_ROW, DATA_COLUMN, vColIndex, width, currentPage;
	private Object [][] data;
	private Container container;
	private JPanel panel_Table, font_Panel, color_Panel; // 放置表格的panel_Table
    private JButton applyBtn, chgBacColorBtn, chgForColorBtn, chgToEZMode, chgToInitialMode; // 確定 取消按鈕
    private JTable table; // 表格
    private JLabel fontStyleLabel, fontSizeLabel;
    private JComboBox fontStyleBox, fontSizeBox;
    private JMenuItem [] menuItem;
    private JPopupMenu popupMenu;
    private MyTableModel_17 model; // tableModel
	private StockInfoSeperateStock sss;
	private String [] fontStyle, fontSize;
	private String [] item = new String [] {
			"水泥",
			"食品",
			"塑膠",
			"紡織",
			"電機",
			"電器電纜",
			"玻璃",
			"造紙",
			"鋼鐵",
			"塑膠",
			"汽車",
			"營建",
			"運輸",
			"觀光",
			"金融",
			"百貨",
			"綜合",
			"其他",
			"化學",
			"生技醫療",
			"油氣燃料",
			"半導體",
			"電腦週邊",
			"光電",
			"通信網路",
			"電子零件組",
			"電子通路",
	};
	private String [] columnNames = new String[] {
			"股市",
			"時間",
			"成交",
			"漲跌",
			"成交張",
			"開盤",
			"最高",
			"最低",
			"昨日",
	};
	private String tmpProcess;

    public SeperateStockInfo() {
    	container = new Container();
    	container.setLayout(new BorderLayout());
    	
    	try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		DATA_ROW = StockInfoSeperateStock.ROW;
		DATA_COLUMN = StockInfoSeperateStock.COLUMN;
		
		vColIndex = 0; // 決定column 的 index
    	width = 90; // column index 的寬度
		
    	panel_Table = new JPanel();
    	data = new Object[DATA_ROW][DATA_COLUMN];
    	
    	TABLE_HEIGHT = 800;
    	TABLE_WIDTH = 500;
    	
    	font_Panel = new JPanel(new GridLayout(10,1));
    	fontSize = new String[] {"10","12","14"};
    	fontStyle = new String[]{"PLAIN","BOLD","ITALIC"};
    	
    	fontSizeLabel = new JLabel("文字大小：");
    	fontSizeBox = new JComboBox(fontSize);
    	font_Panel.add(fontSizeLabel);
    	font_Panel.add(fontSizeBox);
    	
    	fontStyleLabel = new JLabel("文字狀態：");
    	fontStyleBox = new JComboBox(fontStyle);
    	font_Panel.add(fontStyleLabel);
    	font_Panel.add(fontStyleBox);
    	fontSizeBox.setEditable(true);
    	applyBtn = new JButton("套用字型設定");
    	applyBtn.addActionListener(
    	new ActionListener() {
    		public void actionPerformed(ActionEvent e) {
    	        Font font = new Font("微軟正黑體",
    	        		fontStyleBox.getSelectedIndex(),
    	        		Integer.parseInt((String)fontSizeBox.getSelectedItem()));

    	        table.setFont(font);
    	        table.setColumnSelectionAllowed(true);
    	        table.clearSelection();
    	        table.revalidate();
    	        table.repaint();
    		}
    	});
    	font_Panel.add(applyBtn);
    	
    	// 配置顏色
    	chgToEZMode = new JButton("更改為色彩標記模式");
    	chgToEZMode.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			TableCellRenderer renderer = new CustomTableCellRenderer();
    	    			table.setDefaultRenderer( Object.class, renderer );
    	    	        table.setColumnSelectionAllowed(true);
    	    	        table.clearSelection();
    	    	        table.revalidate();
    	    	        table.repaint();
    	    		}
    	    	});
    	font_Panel.add(chgToEZMode);
    	
    	chgToInitialMode = new JButton("更改為原始模式");
    	chgToInitialMode.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			TableCellRenderer renderer = new CustomTableInitialStatus();
    	    			table.setDefaultRenderer( Object.class, renderer );
    	    	        table.setColumnSelectionAllowed(true);
    	    	        table.clearSelection();
    	    	        table.revalidate();
    	    	        table.repaint();
    	    		}
    	    	});
    	font_Panel.add(chgToInitialMode);
    	
    	// 變更顏色按鈕設置
    	color_Panel = new JPanel(new GridLayout(2, 1));
    	chgBacColorBtn = new JButton("更改背景色(表格底色)");
    	chgForColorBtn = new JButton("更改前景色(字型顏色)");
    	chgBacColorBtn.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			   Color selectedColor = JColorChooser.showDialog(
    	    					   table.getRootPane(), "選擇字體顏色", table.getForeground());
    	    			   table.setBackground(selectedColor);
    	    		}
    	    	});
    	
    	chgForColorBtn.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			   Color selectedColor = JColorChooser.showDialog(
    	    					   table.getRootPane(), "選擇字體顏色", table.getForeground());
    	    			   table.setForeground(selectedColor);
    	    		}
    	    	});
    	
    	color_Panel.add(chgBacColorBtn);
    	color_Panel.add(chgForColorBtn);
    	
		DONE = true;
		sss = new StockInfoSeperateStock();
		sss.retrieveData(sss, DONE, 1);
		data = sss.getData();
		currentPage = 1;
    	
    	// 表格
		model = new MyTableModel_17(DATA_ROW, DATA_COLUMN);
		model.setColumnNames(columnNames);
		
    	table = new JTable(model);
		TableCellRenderer renderer = new CustomTableCellRenderer();
		table.setDefaultRenderer( Object.class, renderer );
        table.setColumnSelectionAllowed(true);
        table.clearSelection();
        table.revalidate();
        table.repaint();
    	table.setPreferredScrollableViewportSize(new Dimension(TABLE_HEIGHT, TABLE_WIDTH));
    	table.setFillsViewportHeight(true);   
    	table.setRowSelectionAllowed(true);
    	table.setColumnSelectionAllowed(true);
        model.addTableModelListener(new TableModelListener(){
            public void tableChanged(TableModelEvent e) {
                table.updateUI();
            }});
        
        // 設定column的寬度
        TableColumn col = table.getColumnModel().getColumn(vColIndex);
        col.setPreferredWidth(width);
        
    	for(int k=0 ; k<DATA_ROW ; k++) {
    		for(int j=0 ; j<DATA_COLUMN ; j++) {
    			tmpProcess = String.valueOf(data[k][j]);
    			if(tmpProcess.startsWith("+")) {
    				tmpProcess = "▲ " + tmpProcess.substring(1, tmpProcess.length());
    				table.setSelectionForeground(Color.YELLOW);
    			}
    			else if(tmpProcess.startsWith("-") && tmpProcess.length() != 1)
    				tmpProcess = "▼ " + tmpProcess.substring(1, tmpProcess.length());
    			else if(tmpProcess == "null")
    				tmpProcess = "";

    			model.setValueAt(tmpProcess, k, j);
    		}		
    	}
        model.fireTableDataChanged(); 
        
        // 產生彈出式選單
        createPopupMenu();
        
        // 拉動式捲軸 和 主要Frame
    	JScrollPane scrollPane = new JScrollPane(table);
        
        RowHeaderer rowHeader= new RowHeaderer(table);
        scrollPane.setRowHeaderView(rowHeader);
        scrollPane.getRowHeader().setPreferredSize(new Dimension(35,100));
        
        table.setBackground(Color.BLACK);
        table.setForeground(Color.WHITE);
        table.setFont(new Font("標楷體", Font.PLAIN, 14));
        container.add(panel_Table, BorderLayout.NORTH);
        container.add(font_Panel, BorderLayout.EAST);
        container.add(color_Panel, BorderLayout.SOUTH);
        container.add(scrollPane, BorderLayout.WEST);
        container.setVisible(true);
    }
    
    // 處理快速鍵的問題   enter 確定 esc 取消
    public void enterPressesWhenFocused(JComponent object) {
    		object.registerKeyboardAction(
    				object.getActionForKeyStroke(
    						KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)), 
    						KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false), 
    						JComponent.WHEN_IN_FOCUSED_WINDOW);
    		
    		object.registerKeyboardAction(
    				object.getActionForKeyStroke(
    						KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)), 
    						KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), 
    						JComponent.WHEN_IN_FOCUSED_WINDOW);
    }
    
    public Container getFrame() {
    	return container;
    }
    
    // popupMenu 呈現資料
    public void createPopupMenu() {
        popupMenu = new JPopupMenu();
    	menuItem = new JMenuItem[29];
    	for(int i=0 ; i<item.length ; i++) {
    		menuItem[i] = new JMenuItem(item[i]);
    		menuItem[i].addActionListener(this);
    		popupMenu.add(menuItem[i]);
    	}
    	MouseListener popupListener = new PopupListener(popupMenu);
    	table.addMouseListener(popupListener);
    }

	public void actionPerformed(ActionEvent ae) {
		sss = new StockInfoSeperateStock();
		
		if(ae.getActionCommand() == "水泥") {
			sss.retrieveData(sss, DONE, 1);
			currentPage = 1;
		} else if(ae.getActionCommand() == "食品") {
			sss.retrieveData(sss, DONE, 2);
			currentPage = 2;
		} else if(ae.getActionCommand() == "塑膠") {
			sss.retrieveData(sss, DONE, 3);
			currentPage = 3;
		} else if(ae.getActionCommand() == "紡織") {
			sss.retrieveData(sss, DONE, 4);
			currentPage = 4;
		} else if(ae.getActionCommand() == "電機") {
			sss.retrieveData(sss, DONE, 5);
			currentPage = 5;
		} else if(ae.getActionCommand() == "電纜") {
			sss.retrieveData(sss, DONE, 6);
			currentPage = 6;
		} else if(ae.getActionCommand() == "玻璃") {
			sss.retrieveData(sss, DONE, 7);
			currentPage = 7;
		} else if(ae.getActionCommand() == "造紙") {
			sss.retrieveData(sss, DONE, 8);
			currentPage = 8;
		} else if(ae.getActionCommand() == "鋼鐵") {
			sss.retrieveData(sss, DONE, 9);
			currentPage = 9;
		} else if(ae.getActionCommand() == "橡膠") {
			sss.retrieveData(sss, DONE, 10);
			currentPage = 10;
		} else if(ae.getActionCommand() == "汽車") {
			sss.retrieveData(sss, DONE, 11);
			currentPage = 11;
		} else if(ae.getActionCommand() == "營建") {
			sss.retrieveData(sss, DONE, 12);
			currentPage = 12;
		} else if(ae.getActionCommand() == "運輸") {
			sss.retrieveData(sss, DONE, 13);
			currentPage = 13;
		} else if(ae.getActionCommand() == "觀光") {
			sss.retrieveData(sss, DONE, 14);
			currentPage = 14;
		} else if(ae.getActionCommand() == "金融") {
			sss.retrieveData(sss, DONE, 16);
			currentPage = 16;
		} else if(ae.getActionCommand() == "百貨") {
			sss.retrieveData(sss, DONE, 18);
			currentPage = 18;
		} else if(ae.getActionCommand() == "綜合") {
			sss.retrieveData(sss, DONE, 19);
			currentPage = 19;
		} else if(ae.getActionCommand() == "其他") {
			sss.retrieveData(sss, DONE, 20);
			currentPage = 20;
		} else if(ae.getActionCommand() == "化學") {
			sss.retrieveData(sss, DONE, 21);
			currentPage = 21;
		} else if(ae.getActionCommand() == "生技醫療") {
			sss.retrieveData(sss, DONE, 22);
			currentPage = 22;
		} else if(ae.getActionCommand() == "油氣燃料") {
			sss.retrieveData(sss, DONE, 23);
			currentPage = 23;
		} else if(ae.getActionCommand() == "半導體") {
			sss.retrieveData(sss, DONE, 24);
			currentPage = 24;
		} else if(ae.getActionCommand() == "電腦週邊") {
			sss.retrieveData(sss, DONE, 25);
			currentPage = 25;
		} else if(ae.getActionCommand() == "光電") {
			sss.retrieveData(sss, DONE, 26);
			currentPage = 26;
		} else if(ae.getActionCommand() == "通信網路") {
			sss.retrieveData(sss, DONE, 27);
			currentPage = 27;
		} else if(ae.getActionCommand() == "電子零件組") {
			sss.retrieveData(sss, DONE, 28);
			currentPage = 28;
		} else if(ae.getActionCommand() == "電子通路") {
			sss.retrieveData(sss, DONE, 29);
			currentPage = 29;
		} 
			  
			data = sss.getData();
	    	for(int i=0 ; i<DATA_ROW ; i++) 
	    		for(int j=0 ; j<DATA_COLUMN ; j++) 
	        			model.setValueAt(data[i][j], i, j);
	        model.fireTableDataChanged(); 
		} 
}