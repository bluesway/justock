/*
*      author by Kevingo
*      Data @ 2007/07/30
*      顯示籌碼分析資料
*      yahoo version
*      
*      TODO 製作股票下拉式選單 美化      
*                下市公司顯示處理
*/

package software.Subitem.table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import common.util.MyTableModel_6;
import common.util.StockBox;
import software.GUI;
import software.DataRetrieve.StockInfoMajorParser_Yahoo;

public class MajorStockInfo implements ActionListener {
	private static final long serialVersionUID = 1L;
	public boolean DONE = false;
	private int TABLE_HEIGHT, TABLE_WIDTH, vColIndex, width, DATA_ROW, DATA_COLUMN ;
	public Container container;
	private Object [][] data;
	private JButton okBtn,cancelBtn, applyBtn, chgForColorBtn, chgBacColorBtn; // 確定 取消按鈕
	private JComboBox stockBox, fontStyleBox, fontSizeBox;
	private JLabel fontStyleLabel, fontSizeLabel, statusLabel;     // 股票代碼
	private JPanel panel_Table, font_Panel, color_Panel; // 放置表格的panel_Table
    private JTable table; // 表格
    private JTextField stockField, statusField; // 使用者輸入股票代碼, 目前查詢代碼
    private MyTableModel_6 model; // tableModel
    private Border compound;
    private TitledBorder border;
	private StockInfoMajorParser_Yahoo smy;
	private String tmpProcess; // 處理表格資料轉換過程中需要暫存的字串
	private String stockID;
	private String [] fontStyle, fontSize;
	private String [] columnNames = new String[] {
			"買超券商",
			"買進",
			"賣出",
			"買超",
			"賣超券商",
			"買進",
			"賣出",
			"賣超"
	};
	private JButton chgToEZMode;
	private JButton chgToInitialMode;
	private JButton callTecDiagramBtn;
	private JButton callIniDiagramBtn;

    public MajorStockInfo() {
    	try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) {
			e.printStackTrace();
		}
		container = new Container();
		container.setLayout(new BorderLayout());
		DATA_ROW = 15; 
		DATA_COLUMN = 8;
		TABLE_HEIGHT = 700 ; // 表格長度
		TABLE_WIDTH = 350; // 表格寬度
		
    	panel_Table = new JPanel();
    	data = new Object[DATA_ROW][DATA_COLUMN];
    	
    	// 表格
		model = new MyTableModel_6(DATA_ROW, DATA_COLUMN);
		model.setColumnNames(columnNames);
    	
    	vColIndex = 0; // 決定column 的 index
    	width = 80; // column index 的寬度
    	
    	font_Panel = new JPanel(new GridLayout(10, 1));
    	fontSize = new String[] {"10","12","14"};
    	fontStyle = new String[]{"PLAIN","BOLD","ITALIC"};
    	
    	fontSizeLabel = new JLabel("文字大小：");
    	fontSizeBox = new JComboBox(fontSize);
    	font_Panel.add(fontSizeLabel);
    	font_Panel.add(fontSizeBox);
    	
    	fontStyleLabel = new JLabel("文字狀態：");
    	fontStyleBox = new JComboBox(fontStyle);
    	font_Panel.add(fontStyleLabel);
    	font_Panel.add(fontStyleBox);
    	fontSizeBox.setEditable(true);
    	applyBtn = new JButton("套用字型設定");
    	applyBtn.addActionListener(
    	new ActionListener() {
    		public void actionPerformed(ActionEvent e) {
    	        Font font = new Font("標楷體",
    	        		fontStyleBox.getSelectedIndex(),
    	        		Integer.parseInt((String)fontSizeBox.getSelectedItem()));
    	        
    	        table.setFont(font);
    	        table.setColumnSelectionAllowed(true);
    	        table.clearSelection();
    	        table.revalidate();
    	        table.repaint();
    		}
    	});
    	font_Panel.add(applyBtn);
    	
    	chgToEZMode = new JButton("更改為色彩標記模式");
    	chgToEZMode.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			TableCellRenderer renderer = new CustomTableCellRenderer();
    	    			table.setDefaultRenderer( Object.class, renderer );
    	    	        table.setColumnSelectionAllowed(true);
    	    	        table.clearSelection();
    	    	        table.revalidate();
    	    	        table.repaint();
    	    		}
    	    	});
    	font_Panel.add(chgToEZMode);
    	
    	chgToInitialMode = new JButton("更改為原始模式");
    	chgToInitialMode.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			TableCellRenderer renderer = new CustomTableInitialStatus();
    	    			table.setDefaultRenderer( Object.class, renderer );
    	    	        table.setColumnSelectionAllowed(true);
    	    	        table.clearSelection();
    	    	        table.revalidate();
    	    	        table.repaint();
    	    		}
    	    	});
    	font_Panel.add(chgToInitialMode);
    	
    	callTecDiagramBtn = new JButton("查詢個股技術分析圖表");
    	callTecDiagramBtn.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			GUI.gui.setStockId(Integer.valueOf(stockID));
    	    			GUI.gui.createFrame("技術分析");
    	    		}
    	    	});
    	font_Panel.add(callTecDiagramBtn);
    	
    	callIniDiagramBtn = new JButton("查詢個股即時行情圖表");
    	callIniDiagramBtn.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			GUI.gui.setStockId(Integer.valueOf(stockID));
    	    			GUI.gui.createFrame("個股即時行情");
    	    		}
    	    	});
    	font_Panel.add(callIniDiagramBtn);
    	
    	color_Panel = new JPanel(new GridLayout(2, 1));
    	chgBacColorBtn = new JButton("更改背景色(表格底色)");
    	chgForColorBtn = new JButton("更改前景色(字型顏色)");
    	chgBacColorBtn.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			   Color selectedColor = JColorChooser.showDialog(
    	    					   table.getRootPane(), "選擇字體顏色", table.getForeground());
    	    			   table.setBackground(selectedColor);
    	    		}
    	    	});
    	
    	chgForColorBtn.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			   Color selectedColor = JColorChooser.showDialog(
    	    					   table.getRootPane(), "選擇字體顏色", table.getForeground());
    	    			   table.setForeground(selectedColor);
    	    		}
    	    	});
    	color_Panel.add(chgBacColorBtn);
    	color_Panel.add(chgForColorBtn);
    	
    	// 股票代碼下拉式選單
    	stockBox = StockBox.create();    	
    	Border redline = BorderFactory.createLineBorder(Color.darkGray, 1);
        compound = BorderFactory.createCompoundBorder(
                redline, compound);
        border = BorderFactory.createTitledBorder(
                compound, "請選擇股票代碼",
                TitledBorder.CENTER,
                TitledBorder.DEFAULT_POSITION);
    	stockBox.setBorder(border);
    	stockBox.getEditor().addActionListener(this);
    	enterPressesWhenFocused(stockBox);
    	panel_Table.add(stockBox);
    	
    	// 確定按鈕
    	okBtn = new JButton("確定");
    	enterPressesWhenFocused(okBtn);
    	okBtn.addActionListener(
    		new ActionListener() {
    			public void actionPerformed(ActionEvent ae) {
    					updataData();
        		        model.fireTableDataChanged(); 
        			}
    			}
    	);
    	panel_Table.add(okBtn);
    	
    	// 取消按鈕
    	cancelBtn = new JButton("取消");
    	cancelBtn.addActionListener(new ActionListener() {    		 
    		public void actionPerformed(ActionEvent arg0) {
    			stockField.setText("");
    		}
    	});
    	panel_Table.add(cancelBtn);
    	
    	statusLabel = new JLabel("目前查詢的股票為：");
    	panel_Table.add(statusLabel);
    	
    	statusField = new JTextField(10);
    	panel_Table.add(statusField);
    	
    	table = new JTable(model);
		TableCellRenderer renderer = new CustomTableCellRenderer();
		table.setDefaultRenderer( Object.class, renderer );
        table.setColumnSelectionAllowed(true);
        table.clearSelection();
        table.revalidate();
        table.repaint();
    	table.setPreferredScrollableViewportSize(new Dimension(TABLE_HEIGHT, TABLE_WIDTH));
    	table.setFillsViewportHeight(true); // 確保這個表格會填滿整個元件
    	table.setRowSelectionAllowed(true);
    	table.setColumnSelectionAllowed(true); 
        model.addTableModelListener(new TableModelListener(){
            public void tableChanged(TableModelEvent e) {
                table.updateUI();
            }});
        
        // 設定column的寬度
        TableColumn col = table.getColumnModel().getColumn(vColIndex);
        col.setPreferredWidth(width);
        
        // 拉動式捲軸 和 主要Frame
    	JScrollPane scrollPane = new JScrollPane(table);
        RowHeaderer rowHeader= new RowHeaderer(table);
        scrollPane.setRowHeaderView(rowHeader);
        scrollPane.getRowHeader().setPreferredSize(new Dimension(35,100));	

        table.setBackground(Color.BLACK);
        table.setForeground(Color.WHITE);
        table.setFont(new Font("標楷體", Font.PLAIN, 14));
        container.add(panel_Table, BorderLayout.NORTH);
        container.add(font_Panel, BorderLayout.EAST);
        container.add(color_Panel, BorderLayout.SOUTH);
        container.add(scrollPane, BorderLayout.WEST);
        container.setVisible(true);
    }
    
    public Container getFrame() {
		return container;
    }
    
    // 股票代碼ComboBox的確認動作
    public void actionPerformed(ActionEvent e) {
    	stockID = String.valueOf(StockBox.getStockId(stockBox));
    	updataData();
    }
    
    // 顯示當前查詢股票代碼和名稱
    public void setStatus(String stockId) {
    	statusField.setText(stockId);
    	statusField.setEditable(false);
    }
    
    public String searchName(String id) {
    	id = common.util.StockBox.searchStockId(id);
    	return id;
    }
    
    public void updataData() {
    	if(StockBox.getStockId(stockBox) != -1) {
			DONE = true;
			smy = new StockInfoMajorParser_Yahoo();
			smy.retrieveData(smy, String.valueOf(StockBox.getStockId(stockBox)), DONE);
			data = smy.getData();
			setStatus(String.valueOf(StockBox.getStockId(stockBox)));
		
	    	for(int i=0 ; i<DATA_ROW ; i++) {
	    		for(int j=0 ; j<DATA_COLUMN ; j++) {
	    			tmpProcess = String.valueOf(data[i][j]);
	    			if(tmpProcess.startsWith("+") && tmpProcess.length() != 1) {
	    				tmpProcess = "▲ " + tmpProcess.substring(1, tmpProcess.length());
	    				table.setSelectionForeground(Color.YELLOW);
	    			}
	    			else if(tmpProcess.startsWith("-") && tmpProcess.length() != 1)
	    				tmpProcess = "▼ " + tmpProcess.substring(1, tmpProcess.length());
	    			else if(tmpProcess == "null")
	    				tmpProcess = "";
	
	    			model.setValueAt(tmpProcess, i, j);
	    		}		
	    	}
    	}
    }
    
    // 處理快速鍵的問題   enter 確定 esc 取消
    public void enterPressesWhenFocused(JComponent object) {
    		object.registerKeyboardAction(
    				object.getActionForKeyStroke(
    						KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)), 
    						KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false), 
    						JComponent.WHEN_IN_FOCUSED_WINDOW);
    		
    		object.registerKeyboardAction(
    				object.getActionForKeyStroke(
    						KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)), 
    						KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), 
    						JComponent.WHEN_IN_FOCUSED_WINDOW);
    }
}