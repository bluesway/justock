/*
*      author by Kevingo
*      Data @ 2007/07/31
*      顯示大股東申報轉讓分析資料
*      yahoo version

*/

package software.Subitem.table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableColumn;

import common.util.MyTableModel_14;

public class TransferStockInfo implements ActionListener {
	private static final long serialVersionUID = 1L;
	public boolean DONE = false;
	private int TABLE_HEIGHT, TABLE_WIDTH, vColIndex, width, DATA_ROW, DATA_COLUMN ;
	private Object [][] data;
	private JButton okBtn,cancelBtn, applyBtn, chgForColorBtn, chgBacColorBtn; // 確定 取消按鈕
	private JComboBox stockBox, fontStyleBox, fontSizeBox;
	private JLabel stockLabel, fontStyleLabel, fontSizeLabel, statusLabel;     // 股票代碼
	private JPanel panel_Table, font_Panel, color_Panel; // 放置表格的panel_Table
    private JTable table; // 表格
    private JTextField stockField, statusField; // 使用者輸入股票代碼, 目前查詢代碼
    private MyTableModel_14 model; // tableModel
    private Border compound;
    private TitledBorder border;
//	private StockInfoTransferParser_Yahoo sty;
//	private String stockID;
	private String [] fontStyle, fontSize;
	private String [] columnNames = new String[] {
			"日期",
			"申報張數",
			"申報人",
			"身份"
	};

    public TransferStockInfo() {
    	try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) {
			e.printStackTrace();
		}
		
//		DATA_ROW = StockInfoTransferParser_Yahoo.ROW; // 
//		DATA_COLUMN = StockInfoTransferParser_Yahoo.COLUMN;
		TABLE_HEIGHT = 500; // 表格長度
		TABLE_WIDTH = 350; // 表格寬度
		
    	panel_Table = new JPanel();
    	data = new Object[DATA_ROW][DATA_COLUMN];
    	
    	vColIndex = 0; // 決定column 的 index
    	width = 220; // column index 的寬度
    	
    	font_Panel = new JPanel(new GridLayout(10, 1));
    	fontSize = new String[] {"10","12","14"};
    	fontStyle = new String[]{"PLAIN","BOLD","ITALIC"};
    	
    	fontSizeLabel = new JLabel("文字大小：");
    	fontSizeBox = new JComboBox(fontSize);
    	font_Panel.add(fontSizeLabel);
    	font_Panel.add(fontSizeBox);
    	
    	fontStyleLabel = new JLabel("文字狀態：");
    	fontStyleBox = new JComboBox(fontStyle);
    	font_Panel.add(fontStyleLabel);
    	font_Panel.add(fontStyleBox);
    	fontSizeBox.setEditable(true);
    	applyBtn = new JButton("Apply");
    	applyBtn.addActionListener(
    	new ActionListener() {
    		public void actionPerformed(ActionEvent e) {
    	        Font font = new Font("微軟正黑體",
    	        		fontStyleBox.getSelectedIndex(),
    	        		Integer.parseInt((String)fontSizeBox.getSelectedItem()));
    	        
    	        table.setFont(font);
    	        table.setColumnSelectionAllowed(true);
    	        table.clearSelection();
    	        table.revalidate();
    	        table.repaint();
    		}
    	});
    	font_Panel.add(applyBtn);
    	
    	color_Panel = new JPanel(new GridLayout(2, 1));
    	chgBacColorBtn = new JButton("更改背景色(表格底色)");
    	chgForColorBtn = new JButton("更改前景色(字型顏色)");
    	chgBacColorBtn.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			   Color selectedColor = JColorChooser.showDialog(
    	    					   table.getRootPane(), "選擇字體顏色", table.getForeground());
    	    			   table.setBackground(selectedColor);
    	    		}
    	    	});
    	
    	chgForColorBtn.addActionListener(
    	    	new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			   Color selectedColor = JColorChooser.showDialog(
    	    					   table.getRootPane(), "選擇字體顏色", table.getForeground());
    	    			   table.setForeground(selectedColor);
    	    		}
    	    	});
    	
    	color_Panel.add(chgBacColorBtn);
    	color_Panel.add(chgForColorBtn);

    	// 股票代碼
    	stockLabel = new JLabel("股票代碼：");
    	panel_Table.add(stockLabel);
    	stockField = new JTextField(4);
    	panel_Table.add(stockField);
    	
    	// 股票代碼下拉式選單
    	stockBox = common.util.StockBox.create();
    	Border redline = BorderFactory.createLineBorder(Color.darkGray, 1);
        compound = BorderFactory.createCompoundBorder(
                redline, compound);
        border = BorderFactory.createTitledBorder(
                compound, "請選擇股票代碼",
                TitledBorder.CENTER,
                TitledBorder.DEFAULT_POSITION);
    	stockBox.setBorder(border);
    	stockBox.setEditable(false);
    	stockBox.addActionListener(this);
    	enterPressesWhenFocused(stockBox);
    	panel_Table.add(stockBox);
    	
    	// 確定按鈕
    	okBtn = new JButton("確定");
    	enterPressesWhenFocused(okBtn);
    	okBtn.addActionListener(
    		new ActionListener() {
    			public void actionPerformed(ActionEvent ae) {
    				if(!stockField.getText().equals("")) {
    					DONE = true;
//    					sty = new StockInfoTransferParser_Yahoo();
//    					sty.retrieveData(sty, stockField.getText(), DONE);
//    					data = sty.getData();
    					setStatus(searchName(stockField.getText()));
    				}   	
    				
    		    	for(int i=0 ; i<DATA_ROW ; i++) {
    		    		for(int j=0 ; j<DATA_COLUMN ; j++) {
    		        			model.setValueAt(data[i][j], i, j);
    		        			System.out.println(data[i][j]);
    		    		}		
    		    	}
    		        model.fireTableDataChanged(); 
    			}
    		}
    	);
    	panel_Table.add(okBtn);
    	
    	// 取消按鈕
    	cancelBtn = new JButton("取消");
    	cancelBtn.addActionListener(new ActionListener() {    		 
    		public void actionPerformed(ActionEvent arg0) {
    			stockField.setText("");
    		}
    	});
    	panel_Table.add(cancelBtn);
    	
    	statusLabel = new JLabel("目前查詢的股票為：");
    	panel_Table.add(statusLabel);
    	
    	statusField = new JTextField(10);
    	panel_Table.add(statusField);
    	
    	// 表格
		model = new MyTableModel_14(DATA_ROW, DATA_COLUMN);
		model.setColumnNames(columnNames);
		
    	table = new JTable(model);
    	table.setPreferredScrollableViewportSize(new Dimension(TABLE_HEIGHT, TABLE_WIDTH));
    	table.setFillsViewportHeight(true); // 確保這個表格會填滿整個元件
    	table.setRowSelectionAllowed(true);
    	table.setColumnSelectionAllowed(true); 
        model.addTableModelListener(new TableModelListener(){
            public void tableChanged(TableModelEvent e) {
                table.updateUI();
            }});
        
        // 設定column的寬度
        TableColumn col = table.getColumnModel().getColumn(vColIndex);
        col.setPreferredWidth(width);
        
        // 拉動式捲軸 和 主要Frame
    	JScrollPane scrollPane = new JScrollPane(table);
        JFrame frame = new JFrame("大股東申報轉讓表");
        
        RowHeaderer rowHeader= new RowHeaderer(table);
        scrollPane.setRowHeaderView(rowHeader);
        scrollPane.getRowHeader().setPreferredSize(new Dimension(35,100));
        
        Container contentPane = frame.getContentPane();
        contentPane.add(panel_Table, BorderLayout.NORTH);
        contentPane.add(font_Panel, BorderLayout.EAST);
        contentPane.add(color_Panel, BorderLayout.SOUTH);
        contentPane.add(scrollPane, BorderLayout.WEST);
        
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
    
    // 股票代碼ComboBox的確認動作
    public void actionPerformed(ActionEvent e) {
    	stockField.setText("");
//    	stockID = String.valueOf(stockBox.getSelectedItem()).substring(0, 4);
		DONE = true;
//		sty = new StockInfoTransferParser_Yahoo();
//		sty.retrieveData(sty, stockID, DONE);
//		data = sty.getData(); 
		setStatus(stockBox.getSelectedItem().toString());
    }
    
    // 顯示當前查詢股票代碼和名稱
    public void setStatus(String stockId) {
    	statusField.setText(stockId);
    	statusField.setEditable(false);
    }
    
    public String searchName(String id) {
    	id = common.util.StockBox.searchStockId(id);
    	return id;
    }
    
    // 處理快速鍵的問題   enter 確定 esc 取消
    public void enterPressesWhenFocused(JComponent object) {
    		object.registerKeyboardAction(
    				object.getActionForKeyStroke(
    						KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)), 
    						KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false), 
    						JComponent.WHEN_IN_FOCUSED_WINDOW);
    		
    		object.registerKeyboardAction(
    				object.getActionForKeyStroke(
    						KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)), 
    						KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), 
    						JComponent.WHEN_IN_FOCUSED_WINDOW);
    }
    
    public static void main(String[] args) {
    	new TransferStockInfo();
    }
} 