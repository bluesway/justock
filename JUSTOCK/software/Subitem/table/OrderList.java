/*
*      author by Kevingo
*      Data @ 2007/07/30
*      下單一覽表	
*/

package software.Subitem.table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.sql.SQLException;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import software.DataRetrieve.StockInfoOrderList;

import common.util.MyTableModel_18;

public class OrderList {
	private static final long serialVersionUID = 1L;
	public boolean DONE = false;
	private int TABLE_HEIGHT, TABLE_WIDTH, vColIndex, width, DATA_ROW, DATA_COLUMN ;
	public Container container;
	private String [][] data;
	private JPanel panel_Table; // 放置表格的panel_Table
    private JTable table; // 表格
    private MyTableModel_18 model; // tableModel
	private StockInfoOrderList soL;
	private String tmpProcess; // 處理表格資料轉換過程中需要暫存的字串
	private String [] columnNames = new String[] {
			"交易流水號",
			"會員帳號",
			"股票代號",
			"交易價",
			"交易量",
			"交易日期",
			"交易時間",
			"交易有效期(分)",
			"狀態"
	};
	
    public OrderList() {
    	soL = new StockInfoOrderList();
		container = new Container();
		container.setLayout(new BorderLayout());
		DATA_ROW = soL.getRow(); 
		DATA_COLUMN = soL.getColumn();
		TABLE_HEIGHT = 800 ; // 表格長度
		TABLE_WIDTH = 350; // 表格寬度
		
    	panel_Table = new JPanel();
    	data = new String[DATA_ROW][DATA_COLUMN];
    	
    	// 表格
		model = new MyTableModel_18(DATA_ROW, DATA_COLUMN);
		model.setColumnNames(columnNames);

    	vColIndex = 0; // 決定column 的 index
    	width = 80; // column index 的寬度
    	
    	table = new JTable(model); // 使用下單一覽的model
		TableCellRenderer renderer = new CustomTableCellRenderer();
		table.setDefaultRenderer( Object.class, renderer );
        table.setColumnSelectionAllowed(true);
        table.clearSelection();
        table.revalidate();
        table.repaint();
    	table.setPreferredScrollableViewportSize(new Dimension(TABLE_HEIGHT, TABLE_WIDTH));
    	table.setFillsViewportHeight(true); // 確保這個表格會填滿整個元件
    	table.setRowSelectionAllowed(true);
    	table.setColumnSelectionAllowed(true); 
        model.addTableModelListener(new TableModelListener(){
            public void tableChanged(TableModelEvent e) {
                table.updateUI();
            }});

        // 設定column的寬度
        TableColumn col = table.getColumnModel().getColumn(vColIndex);
        col.setPreferredWidth(width);
        
        // 拉動式捲軸 和 主要Frame
    	JScrollPane scrollPane = new JScrollPane(table);
        RowHeaderer rowHeader= new RowHeaderer(table);
        scrollPane.setRowHeaderView(rowHeader);
        scrollPane.getRowHeader().setPreferredSize(new Dimension(35,100));	

        table.setBackground(Color.BLACK);
        table.setForeground(Color.WHITE);
        table.setFont(new Font("標楷體", Font.PLAIN, 14));
        container.add(panel_Table, BorderLayout.NORTH);
        container.add(scrollPane, BorderLayout.WEST);
        container.setVisible(true);

        try {
        	updataData();
        } catch (SQLException e1) {
        	e1.printStackTrace();
        }
    }
    
    public Container getFrame() {
		return container;
    }
    
    public void updataData() throws SQLException {
			DONE = true;
			data = soL.getData();
	    	for(int i=0 ; i<DATA_ROW ; i++) {
	    		for(int j=0 ; j<DATA_COLUMN ; j++) {
	    			tmpProcess = String.valueOf(data[i][j]);
	    				model.setValueAt(tmpProcess, i, j);
	    		}	
	    	}
	    	model.fireTableDataChanged();
    }
    
    // 處理快速鍵的問題   enter 確定 esc 取消
    public void enterPressesWhenFocused(JComponent object) {
    		object.registerKeyboardAction(
    				object.getActionForKeyStroke(
    						KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)), 
    						KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false), 
    						JComponent.WHEN_IN_FOCUSED_WINDOW);
    		
    		object.registerKeyboardAction(
    				object.getActionForKeyStroke(
    						KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)), 
    						KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), 
    						JComponent.WHEN_IN_FOCUSED_WINDOW);
    }
}