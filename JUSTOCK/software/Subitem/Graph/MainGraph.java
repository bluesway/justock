/**
 * @author bluesway
 * TARGET 一般性線圖，顯示個股或大盤的歷史資料，似乎沒有實用性......
 */

package software.Subitem.Graph;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.LinkedList;
import javax.swing.JFrame;

import common.util.StockBox;
import software.GUI;
import software.Subitem.GeneralGraph;

@SuppressWarnings("serial")
public class MainGraph extends GeneralGraph {

    public MainGraph () {
    	this(1000, 0, 1);
    }
    
	public MainGraph (int stockId, int dType, int lType) {
		this.stockId = stockId;
    	isOverall = false;
		
    	// 顯示型態
    	if (stockId == 0)
    		limiteType = GeneralGraph.TYPE_LIMITED;
    	else
    		limiteType = lType;
    	displayType = dType;
    	
    	// 圖形繪製
    	line_cap = 2;
    	char_height = GUI.overall_size[5];
    	cpl_height = GUI.overall_size[25];
    	left_space = GUI.overall_size[22];
    	right_space = GUI.overall_size[35];
    	graphD = new Rectangle();
    	start_main = 15;

    	// 資料處理
    	sourceData = new LinkedList<double[]>();
    	priceNum = 9;
    	quanNum = 5;
    	shift = 0;
    	scale = 3;
    	range_start = 0;
    	range = (int) Math.round(graphD.width / scale);
    	mouse_x = -1;
    	mouse_y = -1;
    	avgNum = 6;
    	avgPrev = new double[avgNum];
    	avgCurr = new double[avgNum];
    	
    	// 控制台配置
    	if (stockId != 1000 && stockId != 4000) {
    		createBox();
    		add(stockBox);
    	}

    	add(defaultBtn[2]);
    	add(defaultBtn[1]);
    	add(defaultBtn[0]);
    	add(defaultBtn[3]);
    	
		readData();
		
    	if (stockId != 1000 && stockId != 4000) {
    		add(defaultBtn[4]);
    		stockBox.setSelectedIndex(StockBox.searchIndexOfStockId(stockId));
    	}
	}
	
	public void paintComponent(Graphics g) {
		long a = System.currentTimeMillis();
    	Graphics2D page = (Graphics2D) g;
    	
    	// 重新讀取資料
     	screen_x = getWidth();
     	screen_y = getHeight();
     	graphD.setLocation(getX() + left_space, getY() + cpl_height + 10);
    	graphD.setSize(screen_x - left_space - right_space, screen_y - cpl_height - 25); 
    	range = ((int) Math.round((graphD.width - line_cap / 2) / scale)) + 1;
    	height_main = (int) Math.round(graphD.height * 0.55);
    	start_quan = (int) Math.round(graphD.height * 0.7);
    	height_quan = (int) Math.round(graphD.height * 0.3);
    	avgPrev = new double[avgNum];
    	avgCurr = new double[avgNum];
    	
    	super.paintComponent(page);
    	
		int x = graphD.x + line_cap / 2;
		int scale_diff = (x + scale < graphD.x + graphD.width && graphD.x + graphD.width - x - scale > scale) ? scale : graphD.x + graphD.width - x;
		boolean drawQuan;
		if (stockId == 0 || sourceData.get(range_start + range - 1)[0] > 19870106)
			drawQuan = true;
		else
			drawQuan = false;
		
		// 顯示指數資訊
		drawChartLabel(page, 0);

        // 顯示成交量資訊
        if (drawQuan)
        	drawChartLabel(page, 1);

        // 畫線圖、柱圖及顯示時間資訊
        while (time - range_start < range && time < sourceData.size()) {
        	now = sourceData.get(time);
        	if (stockId == 0)
        		drawLineChart(page, prev[1], now[1], x, scale_diff);
        	else if (displayType == TYPE_LINE_CHART )
        		drawLineChart(page, prev[4], now[4], x, scale_diff);
        	else if (displayType == TYPE_KBAR_CHART)
        		drawKBarChart(page, now, x, scale_diff);
        	else {
        		drawKBarChart(page, now, x, scale_diff);
        		drawLineChart(page, prev[4], now[4], x, scale_diff);
        	}

        	if (stockId != 0) {
	        	drawAvgLine(page, 1, 5, 0, time, x, scale_diff, Color.YELLOW);
	        	drawAvgLine(page, 2, 10, 0, time, x, scale_diff, Color.CYAN);
	        	drawAvgLine(page, 3, 20, 0, time, x, scale_diff, Color.MAGENTA);
        	}
        	
            if (drawQuan)
            	drawQuanChart(page, stockId == 0 ? now[3] : now[5], x, scale_diff);

            if (stockId != 0) {
	            drawAvgLine(page, 1, 5, 1, time, x, scale_diff, Color.YELLOW);
	            drawAvgLine(page, 2, 10, 1, time, x, scale_diff, Color.CYAN);
	            drawAvgLine(page, 3, 20, 1, time, x, scale_diff, Color.MAGENTA);
            }
            
            x += scale_diff;

            if ((stockId == 0 && (int) now[0] % 10000 == 0) || ((time - range_start) % (int) Math.round(graphD.width / scale / GUI.overall_size[2]) == 0 && (time - range_start)> 0 && x + GUI.overall_size[6] < graphD.x + graphD.width - line_cap / 2))
            	drawChartLabel(page, x, now[0]);

            prev = now;
            time++;
        }
        
        // 處理滑鼠指標
        drawMouseInfo(page, drawQuan);

        // 畫邊框
		drawCanvas(page);
		
		a = System.currentTimeMillis() - a;
//		System.out.println("繪圖完成，共花費" + a / 1000.0 + "秒");
	}
	
    public static LinkedList<double[]> getStockData(int stockId) {
    	MainGraph gg = new MainGraph(stockId, TYPE_LINE_CHART, TYPE_LIMITED);
    	gg.readData();
    	
    	return gg.sourceData;
    }
    
    public static void main (String[] args)
    {
    	GeneralGraph mg = new MainGraph (1000, TYPE_KBAR_CHART, TYPE_UNLIMITED);
    	JFrame jf = new JFrame ("- Graph Demo -");
    	jf.getContentPane().add(mg);
       
    	jf.setVisible(true);
    	jf.toFront();
    	jf.setResizable(true);
    	jf.pack();
       
    	jf.setLocationRelativeTo(null);
    	jf.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
//    	mg.repaint();
//    	mg.outputStream(mg.getOutputStream(500, 500));
    }
}
