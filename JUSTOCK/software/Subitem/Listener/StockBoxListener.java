package software.Subitem.Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import software.GUI;
import common.util.StockBox;

public class StockBoxListener implements ActionListener, ItemListener, MouseListener, KeyListener {
	int sid;
	
    public void itemStateChanged(ItemEvent ie) {
    	if (ie.getStateChange() == ItemEvent.SELECTED) {
    		sid = StockBox.getStockId(GUI.gui.stockBox);
    		if (sid != -1) {
    			GUI.gui.setStockId(sid);
    			GUI.gui.repaint();
    			GUI.gui.createFrame(GUI.gui.stockBox.getActionCommand());
    		}
    	}
    }
    
    public void actionPerformed(ActionEvent ae) {
    	sid = StockBox.getStockId(GUI.gui.stockBox);
    	if (sid != -1) {
    		GUI.gui.setStockId(sid);
    		GUI.gui.repaint();
			GUI.gui.createFrame(GUI.gui.stockBox.getActionCommand());
    	}
    }

	public void mouseClicked(MouseEvent me) {
    	System.out.println("�ƹ�");
		sid = StockBox.getStockId(GUI.gui.stockBox);
		if (sid != -1) {
			GUI.gui.stockId = sid;
			GUI.gui.repaint();
			GUI.gui.createFrame(GUI.gui.stockBox.getActionCommand());
		}
	}
	
	public void mouseEntered(MouseEvent me) {}
	public void mouseExited(MouseEvent me) {}
	public void mousePressed(MouseEvent me) {}
	public void mouseReleased(MouseEvent me) {}

	public void keyPressed(KeyEvent ke) {
		sid = StockBox.getStockId(GUI.gui.stockBox);
		if (sid != -1) {
			GUI.gui.stockId = sid;
			GUI.gui.repaint();
			GUI.gui.createFrame(GUI.gui.stockBox.getActionCommand());
		}
		
//		GUI.gui.stockBox.requestFocusInWindow();
	}
	
	public void keyReleased(KeyEvent ke) {}
	public void keyTyped(KeyEvent ke) {}
}