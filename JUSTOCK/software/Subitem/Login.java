/**
 * @author kevingo
 * @modify bluesway
 * TARGET 系統登入
 */

package software.Subitem;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
//import java.io.File;
//import java.io.FileWriter;
import java.io.IOException;
//import java.sql.ResultSet;
import java.sql.SQLException;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import software.GUI;

public class Login 
{
	// 顯示使用者畫面變數
	private JFrame loginFrame;	// login Frame
	private JButton okBtn, cancelBtn; // 登入確認、取消按鈕
	private JLabel accountLabel, pwdLabel; // 帳號、密碼label
	private JTextField accountField; // 輸入帳號
	private JPasswordField pwdField; // 輸入密碼的欄位
	
	// 資料處理變數
	private boolean checkAccount; // 確認此帳號是否存在 true-存在、 false-不存在
//	private int CONFIRM_TO_UPDATE_DB = 0, DONOT_UPDATE_DB = 1;
//	private int dateTime, count; // 取得資料庫最後更新時間(day)
//	private FileWriter fileWrite_current, fileWrite_history;
//	private File file; // 建立離線資料庫
//	private Date date;
//	private String strDBDay; // 存入資料庫中最後更新的日期
//	private LinkedList<String> linkedList; // 處理離線資料庫讀取的問題
//	private Scanner scn, scn2;
//private Map<String, String> map;
	private byte errCount;	// 登入錯誤次數
	
    // 資料庫變數	
//	private static ResultSet result;
	
	public Login() {
		errCount = 0;
		createFrame();
//		map = new HashMap<String, String>();
	}
	
//    private void push(String name) { 
//        linkedList.addFirst(name);
//    }
	
	private void createFrame() {
		okBtn = new JButton("登入");
		cancelBtn = new JButton("取消");
		loginFrame = new JFrame("登入系統");
		accountLabel = new JLabel("請輸入帳號：");
		pwdLabel = new JLabel("請輸入密碼：");
		accountField = new JTextField(10);
		pwdField = new JPasswordField(10);
//		pwdField.setEchoChar((char)9);
		
		loginFrame.setLayout(new GridBagLayout());
		GridBagConstraints g = new GridBagConstraints();
		g.gridx = 0;
		g.gridy = 0;
		loginFrame.add(accountLabel, g);
		g.gridx = 1;
		g.gridy = 0;			
		loginFrame.add(accountField, g);
		g.gridx = 0;
		g.gridy = 1;
		loginFrame.add(pwdLabel, g);
		g.gridx = 1;
		g.gridy = 1;			
		loginFrame.add(pwdField, g);
		g.gridx = 0;
		g.gridy = 2;
		loginFrame.add(okBtn, g);
		g.gridx = 1;
		g.gridy = 2;			
		loginFrame.add(cancelBtn, g);
		
		accountField.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					pwdField.requestFocus();
				else if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
					cancelBtn.doClick();
			}
			
			public void keyReleased(KeyEvent e) {}
			public void keyTyped(KeyEvent e) {}
		});
		
		pwdField.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					okBtn.doClick();
				else if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
					cancelBtn.doClick();
			}
			
			public void keyReleased(KeyEvent e) {}
			public void keyTyped(KeyEvent e) {}
		});

		okBtn.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				String userAccount = accountField.getText();
				String userPasswd = String.valueOf(pwdField.getPassword());
				checkAccount = UserData.chkPwd(userAccount, userPasswd);
				if(checkAccount) 	{
					UserData.setUserData(userAccount);
//					update(userAccount);
					exitLogin();
				} else {
					if (errCount++ > 3) {
						JOptionPane.showMessageDialog(null, "登入失敗超過三次，本程式即將結束", "系統狀態", JOptionPane.ERROR_MESSAGE);
						System.exit(0);
					} else {
						JOptionPane.showMessageDialog(null, "登入失敗，請檢查帳號密碼後再試一次", "系統狀態", JOptionPane.ERROR_MESSAGE);
						pwdField.setText("");
					}
				}
			}
		});
		
		cancelBtn.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae) 
			{
				System.exit(0);
			}
		});

//		loginWindow.setIconImage(new ImageIcon("common.source.logo.jpg").getImage());
		loginFrame.setResizable(false);
//		loginFrame.setUndecorated(true);
		loginFrame.pack();
		loginFrame.setVisible(true);
		loginFrame.setLocationRelativeTo(null);
		loginFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		accountField.requestFocus();
		accountField.requestFocusInWindow();
	}	
	
	public void setVisible (boolean visible) {
		loginFrame.setVisible(visible);
	}
	
	public void closeFrame() {
		loginFrame.dispose();
	}
	
	public void toFront() {
		loginFrame.toFront();
	}

/*
	private void update(String id) // 更新上站次數
	{
		Calendar cal = Calendar.getInstance();
		
		int i =0;
//		i = JOptionPane.showConfirmDialog(null, "請問是否要更新離線資料庫?", "確認對話框", DONOT_UPDATE_DB, CONFIRM_TO_UPDATE_DB);
		i = JOptionPane.showConfirmDialog(null, "請問是否要更新離線資料庫?", "確認對話框", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		if(i == JOptionPane.YES_OPTION) // 使用者選擇更新
		{
			try {
				GUI.stmt.executeUpdate("UPDATE account SET Ucount=Ucount+1 where Uid='"+ id + "'");
				file = new File("info_current");
				file = new File("info_history");
				date = new Date(file.lastModified());
				cal.setTime(date);
				dateTime = cal.get(Calendar.DATE);
				
				if(file.exists())
				{
					result = GUI.stmt.executeQuery("SELECT max(hDate) FROM info_history");
					result.next();
					strDBDay = result.getString(1).substring(6, 8); // 把日期截取出來
					if((!strDBDay.equals(dateTime))) // 判斷日期是否相同
					{
						JOptionPane.showMessageDialog(null, "離線資料庫更新中...\n請稍候...");
						long initial = System.currentTimeMillis();
						result = GUI.stmt.executeQuery("SELECT * FROM info_current");
						fileWrite_current = new FileWriter("info_current");
						fileWrite_history = new FileWriter("info_history");
						
						while(result.next()) // 把info_current的資料存入離線資料庫
						{
							fileWrite_current.write(result.getString(1), 0, result.getString(1).length());
							fileWrite_current.write("|", 0, 1);
							fileWrite_current.write(result.getString(2), 0, result.getString(2).length());
							fileWrite_current.write("|", 0, 1);
							fileWrite_current.write(result.getString(3), 0, result.getString(3).length());
							fileWrite_current.write("|", 0, 1);
							fileWrite_current.write(result.getString(4), 0, result.getString(4).length());
							fileWrite_current.write("|", 0, 1);
							fileWrite_current.write(result.getString(5), 0, result.getString(5).length());
							fileWrite_current.write("|", 0, 1);
							fileWrite_current.write(result.getString(6), 0, result.getString(6).length());
							fileWrite_current.write("\n");
						}
						System.out.println("--寫入info_current成功--");
						result = GUI.stmt.executeQuery("SELECT * FROM info_history LIMIT 0 , 1000000");
						while(result.next()) // 寫入第一批歷史離線資料
						{
							count++;
							fileWrite_history.write(result.getString(1), 0, result.getString(1).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(2), 0, result.getString(2).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(3), 0, result.getString(3).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(4), 0, result.getString(4).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(5), 0, result.getString(5).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(6), 0, result.getString(6).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(7), 0, result.getString(7).length());
							fileWrite_history.write("\n");
						}
						System.out.println("寫入第一批info_history成功" + count);
						result = GUI.stmt.executeQuery("SELECT * FROM info_history LIMIT 1000000 , 1000000");
						count = 0;
						while(result.next()) // 寫入第二批歷史離線資料
						{
							count++;
							fileWrite_history.write(result.getString(1), 0, result.getString(1).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(2), 0, result.getString(2).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(3), 0, result.getString(3).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(4), 0, result.getString(4).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(5), 0, result.getString(5).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(6), 0, result.getString(6).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(7), 0, result.getString(7).length());
							fileWrite_history.write("\n");
						}
						System.out.println("寫入第二批info_history成功" + count);
						result = GUI.stmt.executeQuery("SELECT * FROM info_history LIMIT 2000000 , 1000000");
						count =0;
						while(result.next()) // 寫入第三批歷史離線資料
						{
							count++;
							fileWrite_history.write(result.getString(1), 0, result.getString(1).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(2), 0, result.getString(2).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(3), 0, result.getString(3).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(4), 0, result.getString(4).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(5), 0, result.getString(5).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(6), 0, result.getString(6).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(7), 0, result.getString(7).length());
							fileWrite_history.write("\n");
						}
						System.out.println("寫入第三批 info_history成功" + count);
						result = GUI.stmt.executeQuery("SELECT * FROM info_history LIMIT 3000000 , 1000000");
						count = 0;
						while(result.next()) // 寫入第四批歷史離線資料
						{
							count++;
							fileWrite_history.write(result.getString(1), 0, result.getString(1).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(2), 0, result.getString(2).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(3), 0, result.getString(3).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(4), 0, result.getString(4).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(5), 0, result.getString(5).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(6), 0, result.getString(6).length());
							fileWrite_history.write("|", 0, 1);
							fileWrite_history.write(result.getString(7), 0, result.getString(7).length());
							fileWrite_history.write("\n");
						}
						fileWrite_current.close();	
						fileWrite_history.close();
						
						System.out.println("寫入第四批info_history成功" + count);
						long endTime = (System.currentTimeMillis()-initial) / 1000;
						JOptionPane.showMessageDialog(null, "更新離線資料庫成功！\n" + endTime);
						
//						new OffLineDBToMap();
//						file = new File("info_history");
//						scn = new Scanner(file);
//						scn2 = new Scanner(file);					
//
//						try{
//							scn = new Scanner(file);
//							while(scn.hasNextLine())
//								map.put(scn2.nextLine().substring(0, 13), scn.nextLine());
//						}catch(FileNotFoundException z){
//							z.printStackTrace();
//						}
//						System.out.println(map.get("2311|20030424"));
					}
					else // 離線資料庫內的資料不需要更新
					{
						JOptionPane.showMessageDialog(null, "您的資料不需要更新！", "系統狀態", JOptionPane.PLAIN_MESSAGE);
					}
				} // end of file.exists()
				else if(!file.exists())
				{
					file.createNewFile();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}			
		}
//		else if(i == 1) // 使用者選擇不更新
//		{
//			new software.GUI().show(true);
//			loginWindow.show(false);
//		}
	}
*/

	// 結束登入程序
	private void exitLogin() {
		closeFrame();
		synchronized (GUI.gui) {
			GUI.gui.notify();
		}
	}
	
	public static void main(String [] args) throws IOException, ClassNotFoundException, SQLException
	{
		new Login();
	}
}