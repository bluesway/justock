package common.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class checkDay {
	public checkDay() {

	}

	public static boolean isSameMonth(String date1, String date2) {
		if (date1.equals("") || date2.equals(""))
			return true;
		
		if (date1.substring(5, 7).equals(date2.substring(5, 7)))
			return true;
		else
			return false;
	}
	public static boolean isSameWeek(String date1, String date2) {
		if (date1.equals("") || date2.equals(""))
			return true;
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		Date d1 = null;
		Date d2 = null;
		try {
			d1 = format.parse(date1);
			d2 = format.parse(date2);
		} catch (Exception e) {
			e.printStackTrace();
//			return true;
		}
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(d1);
		cal2.setTime(d2);
		int subYear = cal1.get(Calendar.YEAR) - cal2.get(Calendar.YEAR);
		// subYear==0,說明是同一年
		if (subYear == 0) {
			if (cal1.get(Calendar.WEEK_OF_YEAR) == cal2
					.get(Calendar.WEEK_OF_YEAR))
				return true;
		}
		// 例子:cal1是"2005-1-1"，cal2是"2004-12-25"
		// java對"2004-12-25"處理成第52周
		// "2004-12-26"它處理成了第1周，和"2005-1-1"相同了
		// 大家可以查一下自己的日曆
		// 處理的比較好
		// 說明:java的一月用"0"標識，那麼12月用"11"
		else if (subYear == 1 && cal2.get(Calendar.MONTH) == 11) {
			if (cal1.get(Calendar.WEEK_OF_YEAR) == cal2
					.get(Calendar.WEEK_OF_YEAR))
				return true;
		}
		// 例子:cal1是"2004-12-31"，cal2是"2005-1-1"
		else if (subYear == -1 && cal1.get(Calendar.MONTH) == 11) {
			if (cal1.get(Calendar.WEEK_OF_YEAR) == cal2
					.get(Calendar.WEEK_OF_YEAR))
				return true;

		}
		return false;
	}
}
