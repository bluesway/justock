package common.util;

import javax.swing.table.AbstractTableModel;

public class MyTableModel_13 extends AbstractTableModel {
	private static final long serialVersionUID = 1L;
	private static Object [][] data;
	private static String[] columnNames;
	public static String str;
    
    public MyTableModel_13(int row, int col) {
    	data = new Object[row][col];
    }

    public void setColumnNames(String [] str) {
    	columnNames = str;
    }
    
    public String getColumnName(int col){
    	return columnNames[col];
    }
    
	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return data.length;
	}

	public Object getValueAt(int row, int col) {
		return data[row][col];
	}
	
    public void setValueAt(Object value, int row, int col) {
//    	str = check(value, row, col);
        data[row][col] = value;
        fireTableCellUpdated(row, col);
    }
    
//    public static String check(Object cell, int row, int col) {
//    	if(String.valueOf(cell).substring(0, 1).equals("-"))
//    		return String.valueOf(cell);
//    	else
//    		return "no";
//    }
}